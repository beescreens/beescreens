#!/bin/sh

# MkDocs dependencies
pip3 install \
	cairosvg \
	mkdocs-git-revision-date-localized-plugin \
	mkdocs-glightbox \
	mkdocs-material \
	mkdocs-minify-plugin \
	mkdocs_puml \
	pillow

# Golang dependencies
go install github.com/spf13/cobra-cli@latest
go get github.com/spf13/viper
go install honnef.co/go/tools/cmd/staticcheck@latest

# Install nest CLI
npm install --global @nestjs/cli

# Install K6 Dashboard
go install go.k6.io/xk6/cmd/xk6@latest
xk6 build --with github.com/szkiba/xk6-dashboard@latest --output k6
sudo mv k6 /usr/local/bin/k6-dashboard

# Install Clinic.js CLI
npm install --global clinic
