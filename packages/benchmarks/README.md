# `@beescreens/benchmarks` NPM package

## Package usage

These instructions are related to usage of the packages in other Node's projects.

You can simply install the package with the following command:

```sh
pnpm install @beescreens/benchmarks
```

## Package development

These instructions are related to the package's development.

### Prerequisites

The following prerequisites must be filled to run this project:

- [Node.js](https://nodejs.org/) must be installed.
- [pnpm](https://pnpm.js.org/) must be installed.

### Set up the project

```sh
# Install the dependencies
pnpm install
```

### Build the package

```sh
# Build the app
pnpm build
```

### Additional resources

- [Node.js](https://nodejs.org/) - A JavaScript runtime built on Chrome's V8 JavaScript engine.
- [pnpm](https://pnpm.js.org/) - Fast, disk space efficient package manager.
- [TypeScript](https://www.typescriptlang.org/) - An open-source language which builds on JavaScript by adding static type definitions.
- [k6](https://k6.io/) - A modern load testing tool, using Go and JavaScript.
