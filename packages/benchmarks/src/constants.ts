// Source: https://gist.github.com/andrew-delph/05a45f7f4293a85755df5fbd15ad26c1

export enum ResponseType {
	open,
	close,
	ping,
	pong,
	message,
	upgrade,
	noop,
}

export enum ResponseCode {
	connect,
	disconnect,
	event,
	ack,
	error,
}
