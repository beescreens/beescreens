// Source: https://gist.github.com/andrew-delph/05a45f7f4293a85755df5fbd15ad26c1

import { K6SocketIoBase } from './K6SocketIoBase';
import { EventName, WebSocket } from 'k6/experimental/websockets';

export class K6SocketIo extends K6SocketIoBase {
	connect(): void {
		this.setSocket(new WebSocket(this.url));
		this.socket.addEventListener(`open` as EventName, () => {});
	}

	on(event: string, callback: (data: any) => void): void {
		this.socket.addEventListener(event as EventName, callback);
	}

	parseMessage(message: any): string {
		return message.data;
	}
}
