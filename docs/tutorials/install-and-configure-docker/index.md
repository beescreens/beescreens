# Install and configure Docker

[Docker](../../explanations/about-docker/index.md) allows to create containers that are independent from your operating system. This allows to embed all your software needs and ship it to someone else.

In this tutorial, you will install Docker in order to develop, build and ship your applications so that will work on other Dockers environments.

## Steps

Docker can be installed with the following methods.

=== ":simple-linux: Linux"

	You can follow [the official documentation](https://docs.docker.com/engine/install/) depending on your Linux distribution.

	You might want to allow non-root users to run Docker following [the post-install documentation](https://docs.docker.com/engine/install/linux-postinstall/).

	The Compose plugin has to be installed externally to work. You can install it with your package manager with [the related documentation](https://docs.docker.com/compose/install/linux/).

	For Debian, you can execute the following commands to install Docker with the Compose plugin.

	```sh
	# Uninstall old versions
	sudo apt remove docker docker-engine docker.io containerd runc

	# Update the available package lists
	sudo apt update

	# Install required packages to add Docker's repository
	sudo apt install \
		ca-certificates \
		curl \
		gnupg \
		lsb-release

	# Add Docker's official GPG key
	sudo mkdir -p /etc/apt/keyrings && curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

	# Set up the repository
	echo \
	"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
	$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

	# Update the available package lists
	sudo apt update

	# Install Docker and the Compose plugin
	sudo apt install docker-ce docker-ce-cli containerd.io docker-compose-plugin

	# Create the `docker` group (it might already exist)
	sudo groupadd docker

	# Add your user to the `docker` group
	sudo usermod -aG docker $USER

	# Activate the changes to group
	newgrp docker
	```

=== ":simple-apple: macOS"

	Download and install Docker Desktop from [the official website](https://www.docker.com/). You can then start Docker Desktop and you should be good to go.

=== ":simple-windows: Windows"

	### Install Windows Subsystem for Linux (WSL)

	On Windows, it is highly recommended to install a WSL distribution. You can follow [the official documentation](https://learn.microsoft.com/en-us/windows/wsl/install) to install your Linux distribution.

	For the default Ubuntu WSL distribution, you can execute the following steps.

	First, download and install the [Linux kernel update package](https://learn.microsoft.com/en-gb/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package)

	Press the ++windows++ key. Search for **Powershell**. Right-click on it, and select **Run as administrator**. Type the following commands. You'll be asked to create a new user in the WSL Linux distribution with its password.

	```sh
	# Set default version of WSL to 2
	wsl --set-default-version 2

	# Install Ubuntu WSL
	wsl --install --distribution Ubuntu

	# Set Ubuntu as the default WSL
	wsl --setdefault Ubuntu

	# Update WSL just to be sure
	wsl --update
	```

	Verify in the **Powershell** that the version on WSL is the right with `wsl -l -v`, the output should looks like that.

	```sh
	> NAME		STATE			Version
	> Ubuntu	Running			2
	```

	Once the process of installing your Linux distribution with WSL is complete, open the distribution **Ubuntu** using the **Start menu**. This will open a command line in your distribution. You will be asked to create a User Name and Password for your Linux distribution.

	### Install Docker

	Once the WSL has been installed, download and install Docker Desktop from [the official website](https://docs.docker.com/desktop/install/windows-install/). Restart your computer. You can then start Docker Desktop and you should be good to go.

	**Troubleshooting**

	If your Ubuntu log into a root user, it means your installation process of Ubuntu was aborted. Like **NotTheDr01ds** said on [ask.ubuntu.com](https://askubuntu.com/questions/1439123/how-to-return-to-default-user-after-switching-to-root-in-ubuntu-on-wsl/1439133#1439133):

	!!! quote

		Option 1: **Reinstall Ubuntu on WSL**

		If you've just recently installed and don't have any files that you don't mind losing in Ubuntu, then the best solution is going to be to simply reinstall the distribution so that it completes properly.

		To do so, open a regular (non-Admin) PowerShell terminal, and ...
		```sh
		wsl --unregister Ubuntu
		ubuntu.exe
		```

		[...]

		That will complete the installation and get you back to the point where it asks for a default username and password.

		Option: 2 **Recreate user manually**

		If you do have files and need to recover without a reinstall, you need to create a new "default" WSL user without reinstalling

		As far as I (Vincent) can tell, the following will create the default user with the same settings that the stock installer/configuration does. From within Ubuntu:

		```sh
		# Create an encrypted version of your password
		echo "Password" | openssl passwd -1 -stdin
		> encryptedPassword

		# Create the user
		useradd --create-home --user-group --groups  adm,dialout,cdrom,floppy,sudo,audio,dip,video,plugdev,netdev --password "encryptedPassword" username
		```

		Of course, modifying `username` to be whatever you want. Since you are already running as root, no need for `sudo` or any trickery there, of course, but other readers without the OP's use-case may need `sudo` or `wsl -u root`.

		[...]

		And then need to set the default WSL user

		"Normally", the WSL distro installer (e.g. `ubuntu.exe`) modifies the Windows Registry with the default user name for the distro. I recommend a newer method, though. As root, create a file `/etc/wsl.conf` with the following contents:

 		```sh
		[user]
		default=username
		```

		Again, of course, substituting your username.


## Summary

Congrats! You have successfully installed and configured Docker!

## Versions

At the time of writing this tutorial, the versions for Docker and Docker Compose are as follow (on macOS). Yours might be more recent but it should work nonetheless.

```sh
> docker --version
Docker version 20.10.21, build baeda1f

> docker compose version
Docker Compose version v2.13.0
```

## Resources and inspirations

These resources and inspirations were used to create this tutorial (in alphabetical order).

- [Docker Desktop WSL 2 backend on Windows](https://docs.docker.com/desktop/windows/wsl/)
- [Download the Linux kernel update package](https://learn.microsoft.com/en-gb/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package)
- [Resolve root user in Ubuntu on WSL](https://askubuntu.com/a/1439133)
- [Set up a WSL development environment](https://learn.microsoft.com/en-gb/windows/wsl/setup/environment)
- [Using Dev Containers in WSL 2](https://code.visualstudio.com/blogs/2020/07/01/containers-wsl)
