# Introduction

Welcome to the interactive drawing application tutorial!

In this tutorial, you will create an interactive drawing application that will be able to display drawings in realtime in different devices.

The application consists of a two applications made with [Next.js](../../../explanations/about-nextjs/index.md) for the frontend and [NestJS](../../../explanations/about-nestjs/index.md) for the backend.

The application uses the [Konva.js](../../../explanations/about-konvajs/index.md) library for drawing.

You can find the final result of this tutorial on this Git repository: [A simple interactive drawing application](https://gitlab.com/beescreens/tutorials/create-an-interactive-drawing-application).

## Resources and inspirations

These resources and inspirations were used to create this tutorial (in alphabetical order).

- [BeeScreens](https://gitlab.com/beescreens/beescreens)
- [GitHub Gist - Test a WebSocket using curl.](https://gist.github.com/htp/fbce19069187ec1cc486b594104f01d0)
- [Konva - How to implement free drawing on canvas with react?](https://konvajs.org/docs/react/Free_Drawing.html)
- [NestJS - Configuration](https://docs.nestjs.com/techniques/configuration)
- [NestJS - Gateways](https://docs.nestjs.com/websockets/gateways)
- [Next.js - Create Next app](https://nextjs.org/docs/api-reference/create-next-app)
- [Next.js - About Next.js](https://nextjs.org/learn/foundations/about-nextjs)
