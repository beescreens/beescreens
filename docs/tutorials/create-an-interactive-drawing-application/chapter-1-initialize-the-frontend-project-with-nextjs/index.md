# Chapter 1 - Initialize the frontend project with Next.js

In this chapter, you will create a frontend project with the [Next.js](../../../explanations/about-nextjs/index.md) CLI.

You will then be able to start the application to see if it runs correctly.

## Steps

!!! warning
	Make sure to have your environment set up! You can check the [Install and configure Visual Studio Code](../../install-and-configure-visual-studio-code/index.md), [Install and configure Docker](../../install-and-configure-docker/index.md) and [Install and configure Dev Containers](../../install-and-configure-dev-containers/index.md) tutorials if needed.

### Create a Next.js project

```sh title="In a terminal, execute the following command(s)."
npx create-next-app@latest --typescript --eslint --use-npm --src-dir --import-alias "@/*" frontend
```

You will be asked to install `create-next-app@latest`, say **Yes**.

Then, you will be asked whether you want to use TailwindCSS and App Router. Reply **No** to the two questions.

Your working directory should look like this.

```yaml
.
├── .devcontainer
│   ├── devcontainer.json
│   ├── Dockerfile
│   └── npm-global-without-sudo.sh
└── frontend
    ├── node_modules
    │   └── ...
    ├── public
    │   ├── favicon.ico
    │   ├── next.svg
    │   ├── thirteen.svg
    │   └── vercel.svg
    ├── src
    │   ├── pages
    │   │   ├── api
    │   │   │   └── hello.ts
    │   │   ├── _app.tsx
    │   │   ├── _document.tsx
    │   │   └── index.tsx
    │   └── styles
    │       ├── globals.css
    │       └── Home.module.css
    ├── .eslintrc.json
    ├── .gitignore
    ├── next.config.js
    ├── next-env.d.ts
    ├── package.json
    ├── package-lock.json
    ├── README.md
    └── tsconfig.json
```

### Start the Next.js application

```sh title="In a terminal, execute the following command(s)."
cd frontend

npm run dev
```

The output of the command should look similar to this.

```sh
> frontend@0.1.0 dev
> next dev

ready - started server on 0.0.0.0:3000, url: http://localhost:3000
event - compiled client and server successfully in 1640 ms (165 modules)
```

### Check the results

You can now access your Next.js application on <http://localhost:3000>{:target="_blank"}. You should see a welcoming page to **Next.js 13** with links to the docs, tutorials, templates and deployment pf Next.js app.

To stop your Next.js application, press ++ctrl+c++ in your terminal.

## Summary

Congrats! You have a default Next.js application running! You are now able to create Next.js projects and start/stop Next.js applications.

## Go further

Are you able to change the links from `Templates` to `Pimp My Wall` and redirect it to the BeeScreens Pimp My Wall application (<https://pmw.beescreens.ch>{:target="_blank"}). Expand the next component to see the answer!

??? example "Show me the answer!"

	The entrypoint of the Next.js application is the `index.tsx` file.

	Its the page it loads then we access the root of the website `/`. in this example, it's <http://localhost:3000/>{:target="_blank"}

	You can change the following code

	```html title="frontend/src/index.tsx"
	...
	<a
		href="https://vercel.com/templates?framework=next.js&utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
		className={styles.card}
		target="_blank"
		rel="noopener noreferrer"
	>
		<h2 className={inter.className}>
			Templates <span>-&gt;</span>
		</h2>
		<p className={inter.className}>
			Discover and deploy boilerplate example Next.js&nbsp;projects.
		</p>
	</a>
	...
	```

	to

	```html title="frontend/src/index.tsx"
	...
	<a
		href="https://pmw.beescreens.ch/"
		className={styles.card}
		target="_blank"
		rel="noopener noreferrer"
	>
		<h2 className={inter.className}>
			Pimp My Wall <span>-&gt;</span>
		</h2>
		<p className={inter.className}>
			Come and create a masterpiece with the other players!
		</p>
	</a>
	...
	```

	Save your changes. Next.js should automatically restart the application. Access <http://localhost:3000>{:target="_blank"}, refresh the page and you should see the expected result.
