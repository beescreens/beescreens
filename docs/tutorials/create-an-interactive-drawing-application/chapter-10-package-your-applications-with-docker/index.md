# Chapter 9 - Manage the environment variables with dotenv

[https://nextjs.org/docs/deployment#docker-image](https://nextjs.org/docs/deployment#docker-image)

## Frontend steps

!!! info

    All the following steps will be executed in the `frontend` directory.

    In a new terminal, you can switch to the frontend directory with the following command.

    ```sh title="In a terminal, execute the following command(s)."
    cd frontend
    ```

### Create the Dockerfile

```docker title="frontend/Dockerfile"
FROM node:20-alpine

EXPOSE 3000
```

## Frontend steps

!!! info

    All the following steps will be executed in the `backend` directory.

    In a new terminal, you can switch to the backend directory with the following command.

    ```sh title="In a terminal, execute the following command(s)."
    cd backend
    ```

### Create the Dockerfile

```docker title="backend/Dockerfile"
FROM node:20-alpine


EXPOSE 4000
```
