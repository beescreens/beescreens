# Chapter 4 - Initialize the backend project with NestJS

With the later chapters, you have created a simple drawing application that allows you to draw in your browser.

Now, what if it was possible to draw with multiple people at the same time using your drawing application? That would be kind of cool right?

In this chapter, you will create a new [NestJS](../../../explanations/about-nestjs/index.md) backend project with the NestJS CLI.

You will then be able to start the application to see if it runs correctly.

The backend will allow all players to connect and exchange the points they are drawing.

!!! info

	All the following steps will be executed at the root level of your working directory.

## Steps

### Install NestJS CLI

The NestJS CLI allows to create and manage NestJS projects.

```sh title="In a terminal, execute the following command(s)."
npm install --global @nestjs/cli
```

### Create a NestJS project

Create a NestJS project in the local directory.

```sh title="In a terminal, execute the following command(s)."
nest new --package-manager npm --strict --skip-git backend
```

NestJS should create a project that looks like this in your working directory.

```yaml hl_lines="4-23"
.
├── .devcontainer
│   └── ...
├── backend
│   ├── node_modules
│   │   └── ...
│   ├── src
│   │   ├── app.controller.spec.ts
│   │   ├── app.controller.ts
│   │   ├── app.module.ts
│   │   ├── app.service.ts
│   │   └── main.ts
│   ├── test
│   │   ├── app.e2e-spec.ts
│   │   └── jest-e2e.json
│   ├── .eslintrc.js
│   ├── .prettierrc
│   ├── README.md
│   ├── nest-cli.json
│   ├── package-lock.json
│   ├── package.json
│   ├── tsconfig.build.json
│   └── tsconfig.json
└── frontend
    └── ...
```

### Change the port configuration

Change the port from `3000`to `4000`.

```ts title="backend/src/main.ts" hl_lines="6"
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(4000);
}
bootstrap();
```

### Start the NestJS application

```sh title="In a terminal, execute the following command(s)."
cd backend

npm run start:dev
```

The output of the command should look similar to this.

```sh
> backend@0.0.1 start:dev
> nest start --watch

[7:14:11 PM] Starting compilation in watch mode...

[7:14:16 PM] Found 0 errors. Watching for file changes.

[Nest] 4800  - 02/02/2023, 7:14:17 PM     LOG [NestFactory] Starting Nest application...
[Nest] 4800  - 02/02/2023, 7:14:17 PM     LOG [InstanceLoader] AppModule dependencies initialized +39ms
[Nest] 4800  - 02/02/2023, 7:14:17 PM     LOG [RoutesResolver] AppController {/}: +17ms
[Nest] 4800  - 02/02/2023, 7:14:17 PM     LOG [RouterExplorer] Mapped {/, GET} route +7ms
[Nest] 4800  - 02/02/2023, 7:14:17 PM     LOG [NestApplication] Nest application successfully started +5ms
```

### Check the results

You can now access your NestJS application on <http://localhost:4000>{:target="_blank"}. You should see the message `Hello World!`.

To stop your NestJS application, press ++ctrl+c++ in your terminal.

## Summary

Congrats! You have a default NestJS application running! You are now able to create NestJS projects and start/stop NestJS applications.

## Go further

Are you able to change the message to `Hello BeeScreens!`? Expand the next component to see the answer!

??? example "Show me the answer!"

	The entrypoint of the NestJS application is the `main.ts` file.

	It loads the `AppModule` module.

	The `AppModule` is a collection of other dependencies that can be loaded as a single unit.

	The `AppModule` loads the `AppController` controller and the `AppService` service.

	The `AppController` is the the entry point of your request when you access <http://localhost:4000>{:target="_blank"}.

	The method `#!ts getHello()` call the method `#!ts this.appService.getHello()` from the `AppService`.

	You can change the following code to display `Hello BeeScreens!`.

	```ts title="src/app.service.ts"
	// ...
	getHello(): string {
	  return 'Hello World!';
	}
	// ...
	```

	```ts title="src/app.service.ts"
	// ...
	getHello(): string {
	  return 'Hello BeeScreens!';
	}
	// ...
	```

	Save your changes. NestJS should automatically restart the application. Access <http://localhost:3000>{:target="_blank"}. You should see the expected result.
