# Become a BeeScreens contributor

You would like to contribute to BeeScreens and add new features to our set of apps, packages, tools and more? What good news! Follow this tutorial to set up the BeeScreens repository and make your first contribution!

You'll use [Git](../../explanations/about-git/index.md) and [OpenSSH](../../explanations/about-openssh/index.md) to configure your [GitLab](../../explanations/about-gitlab/index.md) account in order to clone and open the repository in [Visual Studio Code](../../explanations/about-visual-studio-code/index.md) in the [Dev Container](../../explanations/about-dev-containers/index.md) environment. When pushing your changes, [Husky](../../explanations/about-husky/index.md) will run the local pipeline to ensure everything's right.

You'll then make your first contribution by adding yourself to the list of the contributors and be part of the BeeScreens community!

This tutorial assumes that you have already followed the following tutorials. If you haven't, please follow them first.

- [Install and configure Visual Studio Code](../../tutorials/install-and-configure-visual-studio-code/index.md)
- [Install and configure Docker](../../tutorials/install-and-configure-docker/index.md)
- [Install and configure Dev Containers](../../tutorials/install-and-configure-dev-containers/index.md)

## Steps

### Create a GitLab account

If you don't have a GitLab account yet, create a new one following the guide [How to use GitLab - Create a GitLab account](../../how-to-guides/how-to-use-gitlab/index.md#create-a-gitlab-account).

### Create a SSH keypair

If you don't have a SSH keypair yet, create a new one following the guide [How to use OpenSSH - Create a SSH keypair](../../how-to-guides/how-to-use-openssh/index.md#create-a-ssh-keypair).

!!! danger

    The SSH private key must be treated as a sensitive information that must not be shared with others.

The public SSH key will allow to clone the repository without the need to login with your username/password and sign your commits as well. This will ensure you are the real author of the commits you do for a more secure repository.

### Add the public SSH key to your GitLab account

Add your public SSH key following the guide [How to use GitLab - Add a public SSH key to your account](../../how-to-guides/how-to-use-gitlab/index.md#add-a-public-ssh-key-to-your-account).

### Ask a maintainer to add you to the BeeScreens GitLab project

(Check if it's necessary)

### Clone and open the BeeScreens repository in Visual Studio Code

You'll now clone the repository locally on your machine.

Open a new window in Visual Studio Code. Select **Clone Git Repository...** and provide the BeeScreens repository URL: `git@gitlab.com:beescreens/beescreens.git`. Select **Clone from URL**.

Select a directory on your computer to save the repository.

Open the cloned repository.

### Set up Git

Before opening the project in a Dev Container, Git must be configured locally. This must be done only once.

Add your SSH key to the local SSH agent following the guide [How to use OpenSSH - Add the SSH key to the local SSH agent](../../how-to-guides/how-to-use-openssh/index.md#add-the-ssh-key-to-the-local-ssh-agent).

Configure Git with the following commands.

Replace `<filename>` with the name of the file. Leave empty for default name.

```sh title="In a terminal, execute the following command(s)."
# Set your username
git config --global user.name "Your Name"

# Set your email
git config --global user.email "your.email@address"

# Configure Git to use SSH for commit signing
git config --global gpg.format ssh

# Specify which SSH key should be used as the signing key
git config --global user.signingkey ~/.ssh/<filename>

# Enable signing of all commits
git config --global commit.gpgsign true

# TODO: Add rebase configuration for Git?
```

### Open the project in Dev Container

Open the project in the Dev Container with **View > Command palette...** and type the **Dev Containers: Reopen in Container** command and select it.

### Set up the project

Install the dependencies.

```sh title="In a terminal, execute the following command(s)."
pnpm install
```

Initialize all `.env` files with defaults values.

```sh title="In a terminal, execute the following command(s)."
pnpm setup:env
```

### Create an issue on GitLab

Create a new issue by going to the **Issues** section from the left sidebar of the [BeeScreens GitLab project](https://gitlab.com/beescreens/beescreens).

Select **New issue** to create a new issue. The default template will inform you what type of issue to create depending on the type of contribution you want to make.

As this is your first contribution, you can use the issue type **Issue** (selected by default) and the template **First contribution**.

Follow the instructions in the template carefully. To check the boxes, simply change `[ ]` to `[x]`.

Create the issue by selecting **Submit new issue**.

### Open a merge request

In the newly created issue, select **Create merge request**. A new window opens and you can create the merge request by selecting **Create merge request**. This will automatically create a new branch linked to the issue.

### Checkout the branch locally

On your machine, check out the new branch.

```sh title="In a terminal, execute the following command(s)."
# Get the latest updates from the remote origin
git fetch origin

# Check to the new branch
git checkout <the name of the new branch>
```

### Add yourself to the list of contributors

Add yourself to the list of contributors in the file `docs/reference/index.md` under the heading **Contributors**.

Add your name and GitLab username in the list. Feel free to add your other social media profiles if you'd like it.

### Commit your changes

Check the changes with Git to ensure all wanted files are here.

```sh title="In a terminal, execute the following command(s)."
# Add all the files
git add .

# Check the status
git status
```

The output of the `git status` command should be similar to this.

```
On branch main
Your branch is up to date with 'origin/<the name of the new branch>'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   docs/reference/index.md
```

You can now commit the changes.

```sh title="In a terminal, execute the following command(s)."
# Commit the changes
git commit -m "Add myself to the list of the contributors"
```

### Push your changes to GitLab

Push your changes to GitLab. Husky will run the local pipeline to ensure everything's alright.

??? question "Did Husky complain about something? Expand me to show how to solve this!"

    As this is your first contribution, it is more likely to be linked to the spellchecking of your changes. As you've added your own name and username, our spellcheck [cspell](../../explanations/about-cspell/index.md) might complain if a word is not recognized.

    ```sh title="Output of the `git push` command"
    . review:spellcheck$ cspell lint --dot --config .cspell.yaml
    . review:spellcheck: /workspaces/beescreens/docs/reference/index.md:7:11 - Unknown word (Delafontaine)
    . review:spellcheck: /workspaces/beescreens/docs/reference/index.md:7:26 - Unknown word (ludelafo)
    . review:spellcheck: /workspaces/beescreens/docs/reference/index.md:8:11 - Unknown word (Guidoux)
    . review:spellcheck: /workspaces/beescreens/docs/reference/index.md:8:21 - Unknown word (Nortalle)
    . review:spellcheck: CSpell: Files checked: 411, Issues found: 4 in 1 files
    . review:spellcheck: Failed
     ERR_PNPM_RECURSIVE_RUN_FIRST_FAIL  @beescreens/root@0.0.0 review:spellcheck: `cspell lint --dot --config .cspell.yaml`
    Exit status 1
    husky - pre-push hook exited with code 1 (error)
    error: failed to push some refs to 'gitlab.com:beescreens/beescreens.git'
    ```

    In such case, you must add all missing words to cspell. You can check the guide [How to use cspell - Add a word to dictionary](../../how-to-guides/how-to-use-cspell/index.md#add-a-word-to-dictionary). Add your name and all related words to the right dictionaries and [run cspell](../../how-to-guides/how-to-use-cspell/index.md#run-cspell) to validate the changes.

    Once everything is fixed, add the changes to Git and commit them.

```sh title="In a terminal, execute the following command(s)."
# Push the changes
git push
```

The output of the `git push` command should be similar to this.

```sh title="Output"
git push
. review:spellcheck$ cspell lint --dot --config .cspell.yaml
. review:spellcheck: CSpell: Files checked: 411, Issues found: 0 in 0 files
. review:spellcheck: Done
Enumerating objects: 71, done.
Counting objects: 100% (71/71), done.
Delta compression using up to 6 threads
Compressing objects: 100% (29/29), done.
Writing objects: 100% (42/42), 11.96 KiB | 816.00 KiB/s, done.
Total 42 (delta 15), reused 1 (delta 0), pack-reused 0
remote:
remote: View merge request for <the name of the new branch>:
remote: https://gitlab.com/beescreens/beescreens/-/merge_requests/<number of the merge request>
remote:
To gitlab.com:beescreens/beescreens.git

- df5a69d...9299b2c <the name of the new branch> -> <the name of the new branch>
```

### Review the merge request

You can now review the merge request on GitLab. You can find the link to the merge request in the output of the `git push` command.

GitLab will automatically run the pipeline to ensure everything's alright. At this point, it shouldn't complain about anything.

### Mark the merge request as ready

Once you're happy with your changes, you can mark the merge request as ready.

This will allow the merge of the merge request after the review.

### Add a reviewer to your merge request

Using the right sidebar, add a reviewer to your merge request. You can add any maintainer of the [BeeScreens team](../../reference/index.md#maintainers).

### Wait for the review from the reviewer

The reviewer will review your changes and will either approve them or ask for changes.

If the reviewer asks for changes, they will comment on the merge request and mark it as draft again.

If the reviewer approves your changes, you can merge the changes.

### Merge your changes

Merge your changes by clicking on the "Merge" button.

The commits will be squashed on the main branch. The branch will be deleted automatically as well.

### Switch back to the main branch and pull latest changes

Now that the merge is done, you can get the changes on the main branch on your machine.

```sh title="Execute the following command(s) in a terminal"
# Get the latest updates from the remote origin
git fetch origin

# Check to the main branch
git checkout main

# Pull the changes made by the pull request/merge request
git pull
```

## Summary

Congrats! You have successfully made your first contribution to the BeeScreens project! The workflow will be the same for all your next contributions.

Please have a look to the [How to contribute to BeeScreens](../../how-to-guides/how-to-contribute-to-beescreens/index.md) guide for an in-depth explanation regarding our workflow and elements to pay attention to when contributing to our project.

Want to add your own application? Check out the [How to add an application to BeeScreens](../../how-to-guides/how-to-add-an-application-to-beescreens/index.md) guide!
