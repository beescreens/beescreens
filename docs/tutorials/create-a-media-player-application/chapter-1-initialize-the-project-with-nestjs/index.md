# Chapter 1 - Initialize the project with NestJS

In this chapter, you will create a new [NestJS](../../../explanations/about-nestjs/index.md) project with the NestJS CLI.

You will then be able to start the application to see if it runs correctly.

## Steps

!!! warning

	Make sure to have your environment set up! You can check the [Install and configure Visual Studio Code](../../install-and-configure-visual-studio-code/index.md), [Install and configure Docker](../../install-and-configure-docker/index.md) and [Install and configure Dev Containers](../../install-and-configure-dev-containers/index.md) tutorials if needed.

### Install NestJS CLI

The NestJS CLI allows to create and manage NestJS projects.

```sh title="In a terminal, execute the following command(s)."
npm install --global @nestjs/cli
```

### Create a NestJS project

Create a NestJS project in the local directory.

```sh title="In a terminal, execute the following command(s)."
nest new --package-manager npm --strict --skip-git .
```

NestJS should create a project that looks like this in your working directory.

```yaml
.
├── node_modules # (1)!
│   └─── ...
├── src # (2)!
│   ├── app.controller.spec.ts
│   ├── app.controller.ts
│   ├── app.module.ts
│   ├── app.service.ts
│   └── main.ts
├── test
│   ├── app.e2e-spec.ts
│   └── jest-e2e.json
├── .eslintrc.js
├── .prettierrc
├── README.md
├── nest-cli.json
├── package-lock.json
├── package.json # (3)!
├── tsconfig.build.json
└── tsconfig.json
```

1. The `node_modules` directory contains all the dependencies needed to run your project.
2. The `src` directory contains all the source code of your project.
3. The `package-lock.json` file is the main file of your project. It defines the available scripts, the dependencies and some information regarding your project.

### Start the NestJS application

```sh title="In a terminal, execute the following command(s)."
npm run start:dev
```

The output of the command should look similar to this.

```sh
> create-a-media-player-application@0.0.1 start:dev
> nest start --watch

[12:02:22 PM] Starting compilation in watch mode...

[12:02:32 PM] Found 0 errors. Watching for file changes.

[Nest] 14334  - 01/27/2023, 12:02:38 PM     LOG [NestFactory] Starting Nest application...
[Nest] 14334  - 01/27/2023, 12:02:38 PM     LOG [InstanceLoader] AppModule dependencies initialized +43ms
[Nest] 14334  - 01/27/2023, 12:02:38 PM     LOG [RoutesResolver] AppController {/}: +23ms
[Nest] 14334  - 01/27/2023, 12:02:38 PM     LOG [RouterExplorer] Mapped {/, GET} route +5ms
[Nest] 14334  - 01/27/2023, 12:02:38 PM     LOG [NestApplication] Nest application successfully started +4ms
```

### Check the results

You can now access your NestJS application on <http://localhost:3000>{:target="_blank"}. You should see the message `Hello World!`.

To stop your NestJS application, press ++ctrl+c++ in your terminal.

## Summary

Congrats! You have a default NestJS application running! You are now able to create NestJS projects and start/stop NestJS applications.

## Go further

Are you able to change the message to `Hello BeeScreens!`? Expand the next component to see the answer!

??? example "Show me the answer!"

	The entrypoint of the NestJS application is the `main.ts` file.

	It loads the `AppModule` module.

	The `AppModule` is a collection of other dependencies that can be loaded as a single unit.

	The `AppModule` loads the `AppController` controller and the `AppService` service.

	The `AppController` is the the entry point of your request when you access <http://localhost:3000>{:target="_blank"}.

	The method `#!ts getHello()` call the method `#!ts this.appService.getHello()` from the `AppService`.

	You can change the following code to display `Hello BeeScreens!`.

	```ts title="src/app.service.ts"
	// ...
	getHello(): string {
		return 'Hello World!';
	}
	// ...
	```

	```ts title="src/app.service.ts"
	// ...
	getHello(): string {
		return 'Hello BeeScreens!';
	}
	// ...
	```

	Save your changes. NestJS should automatically restart the application. Access <http://localhost:3000>{:target="_blank"}. You should see the expected result.
