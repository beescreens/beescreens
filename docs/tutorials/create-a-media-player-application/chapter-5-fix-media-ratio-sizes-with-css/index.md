# Chapter 5 - Fix media ratio sizes with CSS

In this chapter, you will add custom CSS to fix the issues you have with media of various ratio sizes. It will allow to use the maximum horizontal space and center smaller media.

## Steps

### Add a custom CSS file

Create a new `public/css/stylesheet.css` file with the following content.

```css title="public/css/stylesheet.css"
body {
	/* This allows to hide the margin in the border of the page */
	margin: 0;
}

.splide {
	background-color: black;
	/* This guarantee to use the maximum horizontal space as possible */
	max-width: 100vw;
}

/* This guarantee that images are always as large as possible and centered */
.splide__slide img {
	display: flex;
	height: 100vh;
	max-height: 100vh;
	margin: auto;
}
```

### Include the custom CSS file in the slideshow template

Update the slideshow template.

```html title="views/slideshow.hbs" hl_lines="9"
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Media Player</title>
		<link rel="stylesheet" href="/css/themes/splide-default.min.css">
		<link rel="stylesheet" href="/css/splide-extension-video.min.css">
		<link rel="stylesheet" href="/css/splide.min.css">
		<link rel="stylesheet" href="/css/stylesheet.css"> <!-- (1)! -->
		<script src="/js/splide-extension-video.min.js"></script>
		<script src="/js/splide.min.js"></script>
	</head>
	<body>
		<section id="slideshow" class="splide" aria-label="Random media slideshow">
			<div class="splide__track">
				<ul class="splide__list">
					<li class="splide__slide">
						<img src="https://source.unsplash.com/random?1" alt="Random photo on Unsplash">
					</li>
					<li class="splide__slide">
						<img src="https://source.unsplash.com/random?2" alt="Random photo on Unsplash">
					</li>
					<li
						class="splide__slide"
						data-splide-html-video="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4"
					></li>
					<li class="splide__slide">
						<img src="https://source.unsplash.com/random?3" alt="Random photo on Unsplash">
					</li>
				</ul>
			</div>
		</section>
	</body>
	<script>
		new Splide('#slideshow').mount(window.splide.Extensions);
	</script>
</html>
```

1. This file was added.

### Check the results

Start the application and access <http://localhost:3000>{:target="_blank"}. You should see a slideshow of a video and three **centered** random images at their **maximum** size. If you refresh, the images should change as well.

To stop your NestJS application, press ++ctrl+c++ in your terminal.

## Summary

Congrats! You have a NestJS application that displays a slideshow with random images as well as a video that are always centered!

## Go further

Are you able to change the background color of the slideshow? Expand the next component to see the answer!

??? example "Show me the answer!"

	Change the `#!css background-color` property in the `public/css/style.css` file to any color you'd like.

	Save your changes. If you didn't stop your NestJS application, it should automatically restart the application. Access <http://localhost:3000>{:target="_blank"}, refresh the page and you should see the expected result.
