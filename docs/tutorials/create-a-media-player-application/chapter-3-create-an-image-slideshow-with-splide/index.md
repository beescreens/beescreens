# Chapter 3 - Create an image slideshow with Splide

In this chapter, you will set up [Splide](../../../explanations/about-splide/index.md) to display a slideshow that supports images.

## Steps

### Serve static files

Splide can be downloaded as separated JavaScript and CSS files that your application will host.

Create the `public` directory at the root level of your working directory that NestJS will use to serve those files.

```sh title="In a terminal, execute the following command(s)."
mkdir public

mkdir public/css

mkdir public/css/themes

mkdir public/js
```

!!! note

	"_Serving [static] files_" is a common term to illustrate the hosting and distribution of files to others. In this case, your application will serve (= distribute) the files required by Splide so when people use your application, it will download your own copy of Splide. It is called "_static_" because it doesn't change often.

Update the main NestJS file to serve the files from the `public` directory.

```ts title="src/main.ts" hl_lines="10"
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import { AppModule } from './app.module';

async function bootstrap() {
	const app = await NestFactory.create<NestExpressApplication>(AppModule);

	app.setBaseViewsDir(join(__dirname, '..', 'views'));
	app.useStaticAssets(join(__dirname, '..', 'public')); // (1)!
	app.setViewEngine('hbs');

	await app.listen(3000);
}

bootstrap();
```

1. NestJS will serve files from this directory that is one level upper than the current `src` directory.

### Download Splide

Splide is available on GitHub. Download the Git repository as a `.zip` file with the following link: <https://github.com/Splidejs/splide/archive/refs/heads/master.zip>{:target="_blank"}.

At the time of writing this tutorial, Splide is at version `4.1.3`. Yours might be more recent but it should work nonetheless.

Open the archive. From the archive, copy the following files to their destinations in your working directory:

- `dist/css/themes/splide-default.min.css` to `public/css/themes/splide-default.min.css`
- `dist/css/splide.min.css` to `public/css/splide.min.css`
- `dist/js/splide.min.js` to `public/js/splide.min.js`

Your working directory should look like this.

```yaml
.
├── .devcontainer
│   └── ...
├── node_modules
│   └── ...
├── public # (1)!
│   ├── css
│   │   ├── themes
│   │   │   └── splide-default.min.css
│   │   └── splide.min.css
│   └── js
│       └── splide.min.js
├── src
│   ├── app.controller.spec.ts
│   ├── app.controller.ts
│   ├── app.module.ts
│   ├── app.service.ts
│   └── main.ts
├── test
│   ├── app.e2e-spec.ts
│   └── jest-e2e.json
├── views
│   └── index.hbs
├── .eslintrc.js
├── .gitignore
├── .prettierrc
├── README.md
├── nest-cli.json
├── package-lock.json
├── package.json
├── tsconfig.build.json
└── tsconfig.json
```

1. Sometimes the `public` directory is called `static`.

### Validate the files are correctly served

Start the application. NestJS will serve the `public` directory. This directory is automatically mapped to the root of you application (`/`). Is means all files in the `public` directory can be accessed on `http://localhost:3000/path/to/my/file/in/the/public/directory`.

Try to access to the three files you added to the `public` directory.

- <http://localhost:3000/css/themes/splide-default.min.css>{:target="_blank"}
- <http://localhost:3000/css/splide.min.css>{:target="_blank"}
- <http://localhost:3000/js/splide.min.js>{:target="_blank"}

All these links should display a lot of text. You can safely close the tabs once all links are validated. If you have a `Error 404 - Not Found` message, the files aren't at the right place.

### Create a new Handlebar template for Splide

Now that Splide files are accessible, you can use them in a new template.

Create a new Handlebars template named `views/slideshow.hbs`. This template will have a four slides slideshow.

```html title="views/slideshow.hbs"
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Media Player</title>
		<link rel="stylesheet" href="/css/themes/splide-default.min.css"> <!-- (1)! -->
		<link rel="stylesheet" href="/css/splide.min.css"> <!-- (2)! -->
		<script src="/js/splide.min.js"></script> <!-- (3)! -->
	</head>
	<body>
		<section id="slideshow" class="splide" aria-label="A beautiful slideshow"> <!-- (4)! -->
			<div class="splide__track">
				<ul class="splide__list">
					<li class="splide__slide">
						<img src="https://source.unsplash.com/random?1" alt="Random photo on Unsplash">
					</li>
					<li class="splide__slide">
						<img src="https://source.unsplash.com/random?2" alt="Random photo on Unsplash">
					</li>
					<li class="splide__slide">
						<img src="https://source.unsplash.com/random?3" alt="Random photo on Unsplash">
					</li>
				</ul>
			</div>
		</section>
	</body>
	<script>
		new Splide('#slideshow').mount(); // (5)!
	</script>
</html>
```

1. This file is served by our application.
2. This file is served by our application.
3. This file is served by our application.
4. This section defines the content slideshow.
5. This script initialize the slideshow that has the id `slideshow`.

### Update the controller

Update the controller to use your new Handlebar template.

```ts title="src/app.controller.ts" hl_lines="9-10"
import { Get, Controller, Render } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
	constructor(private readonly appService: AppService) {}

	@Get()
	@Render('slideshow')
	displayOneSlideshow() {} // (1)!
}
```

1. As the template doesn't use any variables, the function can be empty.

### Check the results

Access <http://localhost:3000>{:target="_blank"}. You should see a slideshow of three random images. If you refresh, the images should change as well.

To stop your NestJS application, press ++ctrl+c++ in your terminal.

## Summary

Congrats! You have a NestJS application that displays a slideshow with random images! At the moment, the page isn't pretty as images can have several ratios but you are now able to create slideshows with images support.

## Go further

Are you able to add your own image to the slideshow? Expand the next component to see the answer!

??? example "Show me the answer!"

	Add your image to the `public/images` directory. It is recommended to be of format `.png`, `.jpg` or `.jpeg`.

	Add a list element to the Splide slideshow with your file.

	```html title="views/index.hbs"
	<li class="splide__slide">
		<img src="/images/path-to-your-image.jpg" alt="My own image on the slideshow">
	</li>
	```

	Save your changes. If you didn't stop your NestJS application, it should automatically restart the application. Access <http://localhost:3000>{:target="_blank"}, refresh the page and you should see the expected result.
