# Chapter 6 - Customize the slideshow to your will

In this chapter, you will customize Splide to your will. Splide offers many options to hide/show some buttons and you will configure your slideshow to autoplay all media with defined interval.

## Steps

### Customize Splide

Update the `views/slideshow.hbs`file's `script` section with the following content.

```html title="views/slideshow.hbs" hl_lines="1-39"
<script>
	const splide = new Splide(
		'#slideshow',
		{
			type: 'fade',
			rewind: true,
			speed: 1000,
			interval: 5000, // Default interval
			start: 0,
			lazyLoad: 'nearby',
			arrows: false,
			pagination: false,
			autoplay: true,
			pauseOnHover: false,
			drag: true,
			rewindByDrag: true,
			keyboard: 'global',
			mouse: false,
			video: {
				loop: true,
				mute: true,
				autoplay: true,
				disableOverlayUI: true,
				hideControls: true,
				htmlVideo: {
					controls: false,
				},
			},
		}
	)

	splide.mount(window.splide.Extensions);

	// Get the autoplay component
	const autoplay = splide.Components.Autoplay;

	// Start the autoplay
	autoplay.play();
</script>
```

You can customize each slide interval with the `#!html data-splide-interval` HTML attribute. To use it, update one of your slide with the following content.

```html title="views/slideshow.hbs" hl_lines="4"
<li
	class="splide__slide"
	data-splide-html-video="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4"
	data-splide-interval="10000"
></li>
```

!!! note

	The time is often expressed as milliseconds (`1000 ms` = `1 s`).

### Check the results

Start the application and access <http://localhost:3000>{:target="_blank"}. You should see a slideshow of a video and three images that autoplays. All controls are hidden and slides change every 5 seconds. The video starts on its own and continues each time it comes back. It lasts 10 seconds as it has a custom interval. If you refresh, the images should change as well.

Check out all the options on the [official documentation](https://splidejs.com/guides/options/). Play with them and customize your media player to your will.

!!! note

	Often, everything is explained in the documentation (this is how the tutorials are made actually). Using open source products is usually a good way to have access to a good documentation and community. If you spot an error or have troubles, you can always reach for help.

To stop your NestJS application, press ++ctrl+c++ in your terminal.

## Summary

Congrats! You have a NestJS application that displays a slideshow that will autoplay! You can customize Splide to your will and change some slides interval to a custom interval if needed.

## Go further

Can you explain the differences between `speed` and `interval`? Can you make the slideshow starts at the second slide? Expand the next component to see the answer!

??? example "Show me the answer!"

	The `speed` is the transition time between two slides.

	The `interval` is the time that a slide will be displayed.

	To start the slideshow at the second slide, just set the `start` property to `1`. As array starts to index `0`, the second slide is at index `1`.
