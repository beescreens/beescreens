# Introduction

Welcome to the media player application tutorial!

In this tutorial, you will create a media player application that will be able to play images and videos as a slideshow.

The application consists of a single application made with [NestJS](../../../explanations/about-nestjs/index.md). NestJS will render the pages requested by the user using the [Handlebars](../../../explanations/about-handlebars/index.md) templating engine.

An administration page will allow to manage the slideshows through an API. Each slide of the slideshow can have a time to display and can be an image or a video. The slideshows will be saved in a [SQLite](../../../explanations/about-sqlite/index.md) database.

A slideshow page will allow to run the desired slideshow with [Splide](../../../explanations/about-splide/index.md).

You can find the final result of this tutorial on this Git repository: [Create a media player application](https://gitlab.com/beescreens/tutorials/create-a-media-player-application).

## Resources and inspirations

These resources and inspirations were used to create this tutorial (in alphabetical order).

- [GitHub Gist public test videos](https://gist.github.com/jsturgis/3b19447b304616f18657)
- [Gute frage Splide.js Autoplay stoppen?](https://www.gutefrage.net/frage/splidejs-autoplay-stoppen)
- [MDN Web Docs HTTP request methods](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods)
- [MDN Web Docs HTTP response status codes](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)
- [NestJS Exception filters #Built-in HTTP exceptions](https://docs.nestjs.com/exception-filters#built-in-http-exceptions)
- [NestJS First steps](https://docs.nestjs.com/first-steps)
- [NestJS Model-View-Controller](https://docs.nestjs.com/techniques/mvc)
- [NestJS Prisma recipe](https://docs.nestjs.com/recipes/prisma)
- [Prisma Quickstart](https://www.prisma.io/docs/getting-started/quickstart)
- [Splide Image Carousel](http://splidejs.com/tutorials/image-carousel/)
- [Splide Options](http://splidejs.com/guides/options/)
- [Splide Video extension](http://splidejs.com/extensions/video/)
- [TypeScript Utility Types](https://www.typescriptlang.org/docs/handbook/utility-types.html)
- [Unsplash API Documentation](https://unsplash.com/documentation#get-a-random-photo)
