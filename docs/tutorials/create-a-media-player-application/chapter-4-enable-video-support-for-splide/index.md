# Chapter 4 - Enable video support with Splide

In this chapter, you will enable video support with [Splide](../../../explanations/about-splide/index.md) to display a slideshow that supports images as well as videos.

## Steps

### Download Splide Video extension

Splide offers different extensions to extend their library. The Video extension
Splide is available on GitHub. Download the Git repository as a `.zip` file with the following link: <https://github.com/Splidejs/splide-extension-video/archive/refs/heads/master.zip>{:target="_blank"}.

At the time of writing this tutorial, Splide Video is at version `0.8.0`. Yours might be more recent but it should work nonetheless.

Open the archive. From the archive, copy the following files to their destinations in your working directory:

- `dist/css/splide-extension-video.min.css` to `public/css/splide-extension-video.min.css`
- `dist/js/splide-extension-video.min.js` to `public/js/splide-extension-video.min.js`

### Include the extension in the slideshow template

Update the slideshow template.

```html title="views/slideshow.hbs" hl_lines="7 9 22-25 34"
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Media Player</title>
		<link rel="stylesheet" href="/css/themes/splide-default.min.css">
		<link rel="stylesheet" href="/css/splide-extension-video.min.css"> <!-- (1)! -->
		<link rel="stylesheet" href="/css/splide.min.css">
		<script src="/js/splide-extension-video.min.js"></script> <!-- (2)! -->
		<script src="/js/splide.min.js"></script>
	</head>
	<body>
		<section id="slideshow" class="splide" aria-label="Random media slideshow">
			<div class="splide__track">
				<ul class="splide__list">
					<li class="splide__slide">
						<img src="https://source.unsplash.com/random?1" alt="Random photo on Unsplash">
					</li>
					<li class="splide__slide">
						<img src="https://source.unsplash.com/random?2" alt="Random photo on Unsplash">
					</li>
					<li
						class="splide__slide"
						data-splide-html-video="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4"
					></li> <!-- (3)! -->
					<li class="splide__slide">
						<img src="https://source.unsplash.com/random?3" alt="Random photo on Unsplash">
					</li>
				</ul>
			</div>
		</section>
	</body>
	<script>
		new Splide('#slideshow').mount(window.splide.Extensions); // (4)!
	</script>
</html>

```

1. This file was added.
2. This file was added.
3. This video was added.
4. The Video extension is loaded by Splide.

### Check the results

Start the application and access <http://localhost:3000>{:target="_blank"}. You should see a slideshow of three random images and a video. If you refresh, the images should change as well.

To stop your NestJS application, press ++ctrl+c++ in your terminal.

## Summary

Congrats! You have a NestJS application that displays a slideshow with random images as well as a video!
