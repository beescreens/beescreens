---
description: "Install and configure a Raspberry Pi tutorial"
---

# Chapter 10 - Assign Sway workspaces to outputs

In this chapter, you will assign Sway workspaces to specific outputs. The Raspberry Pi has two micro HDMI ports that we will use to display certains workspaces on specific screens.

The idea behind this is to have two workspaces to display the BeeScreens applications on each screens and two others workspaces as a fallback in case of debugging purposes.

It will be easier to understand if you have a second screen connected to your Raspberry Pi but it is not required.

## Steps

### Identify the outputs

To identify the outputs, you will use the `swaymsg` command. This command is used to send commands to Sway. You can use it to get information about the current state of Sway.

=== ":material-monitor: Single screen"

    The following command will identify the outputs. It must be executed on the Raspberry Pi within Sway with the help of the Foot terminal.

    ```sh title="On the Raspberry Pi, execute the following command(s)."
    swaymsg -t get_outputs
    ```

    The output of the command should look similar to this. Notice the name of the output, `HDMI-A-1`. This is the name that will be used to identify the outputs. It could be `HDMI-A-2` if you plugged the micro HDMI to the other port of the Raspberry Pi.

    ```yaml
    Output HDMI-A-1 'Philips Consumer Electronics Company PHL 275E1 0x00000B7B' (focused) # (1)!
      Current mode: 2560x1440 @ 59.951000 Hz
      Position: 0,0
      Scale factor: 1.000000
      Scale filter: nearest
      Subpixel hinting: unknown
      Transform: normal
      Workspace: 1
      Max render time: off
      Adaptive sync: disabled
      Available modes:
        720x400 @ 70.082001 Hz
        640x480 @ 59.939999 Hz
        640x480 @ 59.939999 Hz
        640x480 @ 60.000000 Hz
        640x480 @ 66.667000 Hz
        640x480 @ 72.808998 Hz
        640x480 @ 75.000000 Hz
        720x480 @ 59.939999 Hz
        720x480 @ 59.939999 Hz
        720x480 @ 59.939999 Hz
        720x480 @ 60.000000 Hz
        720x480 @ 60.000000 Hz
        720x576 @ 50.000000 Hz
        720x576 @ 50.000000 Hz
        720x576 @ 50.000000 Hz
        800x600 @ 56.250000 Hz
        800x600 @ 60.317001 Hz
        800x600 @ 72.188004 Hz
        800x600 @ 75.000000 Hz
        832x624 @ 74.551003 Hz
        1024x768 @ 60.004002 Hz
        1024x768 @ 70.069000 Hz
        1024x768 @ 75.028999 Hz
        1280x720 @ 50.000000 Hz
        1280x720 @ 59.939999 Hz
        1280x720 @ 60.000000 Hz
        1280x720 @ 60.000000 Hz
        1280x960 @ 60.000000 Hz
        1440x900 @ 59.901001 Hz
        1280x1024 @ 60.020000 Hz
        1280x1024 @ 75.025002 Hz
        1680x1050 @ 59.882999 Hz
        1280x1440 @ 59.912998 Hz
        1920x1080 @ 50.000000 Hz
        1920x1080 @ 59.939999 Hz
        1920x1080 @ 60.000000 Hz
        1920x1080 @ 60.000000 Hz
        2560x1440 @ 74.968002 Hz
        2560x1440 @ 59.951000 Hz
    ```

    1. The output is connected to a Philips 275E1 monitor model.

    Switch the output to the other screen and execute the command again. The output should look similar to this. Notice the name of the output, `HDMI-A-2`. This is the name that will be used to identify the outputs. It could be `HDMI-A-1` if you plugged the micro HDMI to the other port of the Raspberry Pi the first time.

    ```yaml
    Output HDMI-A-2 'Philips Consumer Electronics Company PHL 275E1 0x00000B7B' (focused) # (1)!
      Current mode: 2560x1440 @ 59.951000 Hz
      Position: 0,0
      Scale factor: 1.000000
      Scale filter: nearest
      Subpixel hinting: unknown
      Transform: normal
      Workspace: 1
      Max render time: off
      Adaptive sync: disabled
      Available modes:
        720x400 @ 70.082001 Hz
        640x480 @ 59.939999 Hz
        640x480 @ 59.939999 Hz
        640x480 @ 60.000000 Hz
        640x480 @ 66.667000 Hz
        640x480 @ 72.808998 Hz
        640x480 @ 75.000000 Hz
        720x480 @ 59.939999 Hz
        720x480 @ 59.939999 Hz
        720x480 @ 59.939999 Hz
        720x480 @ 60.000000 Hz
        720x480 @ 60.000000 Hz
        720x576 @ 50.000000 Hz
        720x576 @ 50.000000 Hz
        720x576 @ 50.000000 Hz
        800x600 @ 56.250000 Hz
        800x600 @ 60.317001 Hz
        800x600 @ 72.188004 Hz
        800x600 @ 75.000000 Hz
        832x624 @ 74.551003 Hz
        1024x768 @ 60.004002 Hz
        1024x768 @ 70.069000 Hz
        1024x768 @ 75.028999 Hz
        1280x720 @ 50.000000 Hz
        1280x720 @ 59.939999 Hz
        1280x720 @ 60.000000 Hz
        1280x720 @ 60.000000 Hz
        1280x960 @ 60.000000 Hz
        1440x900 @ 59.901001 Hz
        1280x1024 @ 60.020000 Hz
        1280x1024 @ 75.025002 Hz
        1680x1050 @ 59.882999 Hz
        1280x1440 @ 59.912998 Hz
        1920x1080 @ 50.000000 Hz
        1920x1080 @ 59.939999 Hz
        1920x1080 @ 60.000000 Hz
        1920x1080 @ 60.000000 Hz
        2560x1440 @ 74.968002 Hz
        2560x1440 @ 59.951000 Hz
    ```

    1. The output is connected to a Philips 275E1 monitor model.

=== ":material-monitor-multiple: Multiple screens"

    The following command will identify the outputs. It must be executed on the Raspberry Pi within Sway with the help of the Foot terminal.

    ```sh title="On the Raspberry Pi, execute the following command(s)."
    swaymsg -t get_outputs
    ```

    The output of the command should look similar to this. Notice the name of the outputs, `HDMI-A-1` and `HDMI-A-2`. These are the names that will be used to identify the outputs.

    ```yaml
    Output HDMI-A-1 'Philips Consumer Electronics Company PHL 275E1 0x00000B7B' (focused) # (1)!
      Current mode: 2560x1440 @ 59.951000 Hz
      Position: 0,0
      Scale factor: 1.000000
      Scale filter: nearest
      Subpixel hinting: unknown
      Transform: normal
      Workspace: 1
      Max render time: off
      Adaptive sync: disabled
      Available modes:
        720x400 @ 70.082001 Hz
        640x480 @ 59.939999 Hz
        640x480 @ 59.939999 Hz
        640x480 @ 60.000000 Hz
        640x480 @ 66.667000 Hz
        640x480 @ 72.808998 Hz
        640x480 @ 75.000000 Hz
        720x480 @ 59.939999 Hz
        720x480 @ 59.939999 Hz
        720x480 @ 59.939999 Hz
        720x480 @ 60.000000 Hz
        720x480 @ 60.000000 Hz
        720x576 @ 50.000000 Hz
        720x576 @ 50.000000 Hz
        720x576 @ 50.000000 Hz
        800x600 @ 56.250000 Hz
        800x600 @ 60.317001 Hz
        800x600 @ 72.188004 Hz
        800x600 @ 75.000000 Hz
        832x624 @ 74.551003 Hz
        1024x768 @ 60.004002 Hz
        1024x768 @ 70.069000 Hz
        1024x768 @ 75.028999 Hz
        1280x720 @ 50.000000 Hz
        1280x720 @ 59.939999 Hz
        1280x720 @ 60.000000 Hz
        1280x720 @ 60.000000 Hz
        1280x960 @ 60.000000 Hz
        1440x900 @ 59.901001 Hz
        1280x1024 @ 60.020000 Hz
        1280x1024 @ 75.025002 Hz
        1680x1050 @ 59.882999 Hz
        1280x1440 @ 59.912998 Hz
        1920x1080 @ 50.000000 Hz
        1920x1080 @ 59.939999 Hz
        1920x1080 @ 60.000000 Hz
        1920x1080 @ 60.000000 Hz
        2560x1440 @ 74.968002 Hz
        2560x1440 @ 59.951000 Hz

    Output HDMI-A-2 'Philips Consumer Electronics Company PHL 275E1 0x00000B7B' (focused) # (2)!
      Current mode: 2560x1440 @ 59.951000 Hz
      Position: 0,0
      Scale factor: 1.000000
      Scale filter: nearest
      Subpixel hinting: unknown
      Transform: normal
      Workspace: 1
      Max render time: off
      Adaptive sync: disabled
      Available modes:
        720x400 @ 70.082001 Hz
        640x480 @ 59.939999 Hz
        640x480 @ 59.939999 Hz
        640x480 @ 60.000000 Hz
        640x480 @ 66.667000 Hz
        640x480 @ 72.808998 Hz
        640x480 @ 75.000000 Hz
        720x480 @ 59.939999 Hz
        720x480 @ 59.939999 Hz
        720x480 @ 59.939999 Hz
        720x480 @ 60.000000 Hz
        720x480 @ 60.000000 Hz
        720x576 @ 50.000000 Hz
        720x576 @ 50.000000 Hz
        720x576 @ 50.000000 Hz
        800x600 @ 56.250000 Hz
        800x600 @ 60.317001 Hz
        800x600 @ 72.188004 Hz
        800x600 @ 75.000000 Hz
        832x624 @ 74.551003 Hz
        1024x768 @ 60.004002 Hz
        1024x768 @ 70.069000 Hz
        1024x768 @ 75.028999 Hz
        1280x720 @ 50.000000 Hz
        1280x720 @ 59.939999 Hz
        1280x720 @ 60.000000 Hz
        1280x720 @ 60.000000 Hz
        1280x960 @ 60.000000 Hz
        1440x900 @ 59.901001 Hz
        1280x1024 @ 60.020000 Hz
        1280x1024 @ 75.025002 Hz
        1680x1050 @ 59.882999 Hz
        1280x1440 @ 59.912998 Hz
        1920x1080 @ 50.000000 Hz
        1920x1080 @ 59.939999 Hz
        1920x1080 @ 60.000000 Hz
        1920x1080 @ 60.000000 Hz
        2560x1440 @ 74.968002 Hz
        2560x1440 @ 59.951000 Hz
    ```

    1. Both output is connected to a Philips 275E1 monitor model.
    2. Both output is connected to a Philips 275E1 monitor model.

### Set the outputs layout in Sway

Set the outputs layout in Sway. Change the position of the screens and their resolution if needed. In this configuration, the two screens are side by side.

A bonus step is to hide the cursor after 1 second of inactivity and move it to the bottom right corner of the screen.

```sh title="On the Raspberry Pi, execute the following command(s)."
tee ~/.config/sway/config.d/outputs > /dev/null <<"EOT"
### Outputs configuration
#
# You can get the names of your outputs by running: swaymsg -t get_outputs
#
output HDMI-A-1 pos 0 0 res 2560x1440
output HDMI-A-2 pos 2560 0 res 2560x1440

# Hide cursor after 1 sec
seat * hide_cursor 1000
seat * cursor move 2560 1440
EOT
```

### Set the workspaces outputs in Sway

Set the workspaces outputs in Sway. In this configuration, the left screen will have the workspaces 1 and 3 and the right screen will have the workspaces 2 and 4.

This will allow to switch from one screen to the other by pressing ++windows+1++ or ++windows+2++ and to switch to a "debug" workspace on the left screen by pressing ++windows+3++ and to a "debug" workspace on the right screen by pressing ++windows+4++.

For workspaces greater than 4, the assignment will be done on the screen that is focused.

```sh title="On the Raspberry Pi, execute the following command(s)."
tee ~/.config/sway/config.d/workspaces-outputs > /dev/null <<"EOT"
#
# Workspaces outputs configuration
#
workspace 1 output HDMI-A-1
workspace 2 output HDMI-A-2
workspace 3 output HDMI-A-1
workspace 4 output HDMI-A-2
EOT
```

### Check the results

You must have two screens plugged to the Raspberry Pi to check the results.

Log out from Sway with ++windows+shift+e++. Log back to Sway.

The two screens should be side by side and the workspaces should be assigned to the correct screen. The applications defined in the previous chapter should be running on the correct screen.

## Summary

Congrats! You now have a multi-screen setup with Sway on the Raspberry Pi. Each screen has its own workspace with its own applications.
