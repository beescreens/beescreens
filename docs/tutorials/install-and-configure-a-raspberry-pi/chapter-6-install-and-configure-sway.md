---
description: "Install and configure a Raspberry Pi tutorial"
---

# Chapter 6 - Install and configure Sway

In this chapter, you will install and configure [Sway](../../explanations/about-sway/index.md), a tiling Wayland compositor. Sway will allow to create workspaces and to display applications on the screen. These applications will start on the workspaces that you will configure.

## Steps

### Install Sway and its dependencies

Sway is a tiling Wayland compositor.

Wofi is a applications launcher for Wayland.

Foot is a terminal emulator for Wayland.

```sh title="On the Raspberry Pi, execute the following command(s)."
sudo apt install --yes foot mesa-utils mesa-va-drivers mesa-vdpau-drivers mesa-vulkan-drivers pango1.0-tools sway wofi
```

### Create the Sway configuration files

Start by creating the configuration directory for Sway.

```sh title="On the Raspberry Pi, execute the following command(s)."
# Create the configuration directories
mkdir --parent ~/.config/sway/config.d
```

The following Sway configuration file is based on the default configuration available in `/etc/sway/config`. It has been modified and simplified to make it easier to understand.

Take some time to read the file and to understand what each line does.

```sh title="On the Raspberry Pi, execute the following command(s)."
# Create the configuration file for sway
tee ~/.config/sway/config > /dev/null <<"EOT"
# Config for sway
#
# Read `man 5 sway` for a complete reference.

### Variables
#
# Logo (Windows) key. Use Mod1 for Alt.
set $mod Mod4
# Your preferred terminal emulator
set $term foot
# Your preferred application launcher
set $menu dmenu_path | wofi | xargs swaymsg exec --
# Font family and size
font monospace 12

include /etc/sway/config-vars.d/*

### Key bindings
#
# Basics:
#
    # Start a terminal
    bindsym $mod+Return exec $term

    # Kill focused window
    bindsym $mod+Shift+q kill

    # Start your launcher
    bindsym $mod+d exec $menu

    # Drag floating windows by holding down $mod and left mouse button.
    # Resize them with right mouse button + $mod.
    # Despite the name, also works for non-floating windows.
    # Change normal to inverse to use left mouse button for resizing and right
    # mouse button for dragging.
    floating_modifier $mod normal

    # Reload the configuration file
    bindsym $mod+Shift+c reload

    # Exit sway (logs you out of your Wayland session)
    bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'
#
# Moving around:
#
    # Move your focus around
    bindsym $mod+Left focus left
    bindsym $mod+Down focus down
    bindsym $mod+Up focus up
    bindsym $mod+Right focus right

    # Move the focused window with the same, but add Shift
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right
#
# Workspaces:
#
		# Switch to workspace
    bindsym $mod+1 workspace number 1
    bindsym $mod+2 workspace number 2
    bindsym $mod+3 workspace number 3
    bindsym $mod+4 workspace number 4
    bindsym $mod+5 workspace number 5
    bindsym $mod+6 workspace number 6
    bindsym $mod+7 workspace number 7
    bindsym $mod+8 workspace number 8
    bindsym $mod+9 workspace number 9
    bindsym $mod+0 workspace number 10
    # Move focused container to workspace
    bindsym $mod+Shift+1 move container to workspace number 1
    bindsym $mod+Shift+2 move container to workspace number 2
    bindsym $mod+Shift+3 move container to workspace number 3
    bindsym $mod+Shift+4 move container to workspace number 4
    bindsym $mod+Shift+5 move container to workspace number 5
    bindsym $mod+Shift+6 move container to workspace number 6
    bindsym $mod+Shift+7 move container to workspace number 7
    bindsym $mod+Shift+8 move container to workspace number 8
    bindsym $mod+Shift+9 move container to workspace number 9
    bindsym $mod+Shift+0 move container to workspace number 10
    # Note: workspaces can have any name you want, not just numbers.
    # We just use 1-10 as the default.

#
# Layout stuff:
#
    # You can "split" the current object of your focus with
    # $mod+b or $mod+v, for horizontal and vertical splits
    # respectively.
    bindsym $mod+h splith
    bindsym $mod+v splitv

    # Make the current focus fullscreen
    bindsym $mod+f fullscreen

    # Toggle the current focus between tiling and floating mode
    bindsym $mod+Shift+space floating toggle

    # Swap focus between the tiling area and the floating area
    bindsym $mod+space focus mode_toggle

    # Move focus to the parent container
    bindsym $mod+a focus parent
#
# Resizing containers:
#
mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

#
# Status Bar:
#
# Read `man 5 sway-bar` for more information about this section.
bar {
    position top

    # When the status_command prints a new line to stdout, swaybar updates.
    # The default just shows the current date and time.
    status_command while date +'%Y-%m-%d %l:%M:%S %p'; do sleep 1; done

    colors {
        statusline #ffffff
        background #323232
        inactive_workspace #32323200 #32323200 #5c5c5c
    }
}

include ~/.config/sway/config.d/*
EOT
```

Set the keyboard layout. Replace the `<layout>` and `<variant>` placeholders with the layout and variant that you want to use from previous chapter [Chapter 5 - Configure the operating system](./chapter-5-configure-the-operating-system.md).

```sh title="On the Raspberry Pi, execute the following command(s)."
tee ~/.config/sway/config.d/inputs > /dev/null <<"EOT"
### Inputs configuration
input * {
    xkb_layout "<layout>"
    xkb_variant "<variant>"
}
EOT
```

### Create the Wofi configuration files

Start by creating the configuration directory for Wofi.

```sh title="On the Raspberry Pi, execute the following command(s)."
# Create the configuration directory
mkdir --parent ~/.config/wofi
```

Take some time to read the file and to understand what each line does of the following Wofi configuration file.

```sh title="On the Raspberry Pi, execute the following command(s)."
tee ~/.config/wofi/config > /dev/null <<"EOT"
insensitive=true
location=center
mode=drun
EOT
```

```sh title="On the Raspberry Pi, execute the following command(s)."
tee ~/.config/wofi/style.css > /dev/null <<"EOT"
/* Author: TODO */
window {
	font-family: monospace;
	font-size: 12px;
	margin: 5px;
	background-color: #282a36;
}

#outer-box {
	margin: 5px;
	border: 2px solid #44475a;
	background-color: #282a36;
}

#input {
	margin: 5px;
	border: 2px solid #6272a4;
	background-color: #44475a;
	color: #f8f8f2;
}

#inner-box {
	margin: 5px;
	background-color: #282a36;
}

#scroll {
	margin: 5px;
	border: 2px solid #6272a4;
	background-color: #282a36;
	color: #f8f8f2;
}

#scroll label {
	margin: 2px 0px;
}

#entry {
	margin: 5px;
	background-color: #282a36;
}

#entry:selected {
	background-color: #44475a;
	border: 2px solid #bd93f9;
}

#img {
	margin: 5px;
}

#img:selected {
	background-color: #44475a;
}

#text {
	margin: 5px;
	border: none;
	color: #f8f8f2;
}

#text:selected {
	background-color: #44475a;
}
EOT
```

### Create the Foot configuration file

Start by creating the configuration directory for Foot.

```sh title="On the Raspberry Pi, execute the following command(s)."
# Create the configuration directory
mkdir --parent ~/.config/foot
```

The following Foot configuration file is based on the default configuration available in `/usr/share/foot/foot.ini`. It has been modified and simplified to make it easier to understand.

Take some time to read the comments and to understand what each line does of the following Wofi configuration file.

```sh title="On the Raspberry Pi, execute the following command(s)."
tee ~/.config/foot/foot.ini > /dev/null <<"EOT"
# -*- conf -*-

#
# Font
#
font=monospace:size=12
dpi-aware=never
EOT
```

### Check the results

For the next step, the command must be executed directly on a Raspberry Pi connected to a screen and a keyboard. The command will start Sway and you will be able to test it.

```sh title="On the Raspberry Pi, execute the following command(s)."
sway
```

Sway should start.

#### Open a terminal

To open a terminal, press ++windows+enter++. Foot should open.

To close Foot, press ++windows+shift+q++.

#### Open the application launcher

To open the application launcher, press ++windows+d++. Wofi should open.

Select Foot from the list and press ++enter++ to open it.

To close Foot, press ++windows+shift+q++.

#### Navigate between two terminals

Open two terminals by pressing ++windows+enter++ twice. Two Foot terminals should open side by side.

Use the ++windows+left++ and ++windows+right++ shortcuts to navigate between the two terminals.

#### Move terminals

Open two terminals by pressing ++windows+enter++ twice. Two Foot terminals should open side by side.

Use the ++windows+shift+left++ and ++windows+shift+right++ shortcuts to navigate between the two terminals.

#### Open terminals vertically and horizontally

Press ++windows+v++ to set the split orientation to vertical.

Open a new terminal with ++windows+enter++. It should open under the current terminal.

Press ++windows+h++ to set the split orientation back to horizontal.

Open a new terminal with ++windows+enter++. It should open to the right of the last terminal.

#### Switch between workspaces

Press ++windows+2++ to switch to the second workspace. You can have up to 10 workspaces as from the Sway configuration file.

See the other workspaces with ++windows+"workspace number"++.

Switch back to the first workspace with ++windows+1++.

#### Send an application to a specific workspace

Select one of the Foot terminal.

Send it to the second workspace with ++windows+shift+2++.

Switch to the second workspace with ++windows+2++.

You should see the terminal.

Switch back to the first workspace with ++windows+shift+1++.

#### Resize an application

When two applications are open side by side, you can resize them.

Enter the resize mod with ++windows+r++.

Resize the application with ++left++, ++right++, ++up++ or ++down++.

Exit the resize mod with ++esc++.

#### Reload the Sway configuration file

If you ever edit the Sway configuration file, you can reload it with ++windows+shift+c++.

#### Close Sway

To close Sway, press ++windows+shift+e++ and confirm the message dialog with the mouse.

### Start Sway on boot if network is available and attached to a screen

First, enable autologin on boot. Replace `<username>` with the username of the user you want to autologin.

```sh title="On the Raspberry Pi, execute the following command(s)."
sudo tee /etc/systemd/system/getty@tty1.service.d/autologin.conf > /dev/null <<"EOT"
[Service]
ExecStart=
ExecStart=-/sbin/agetty --autologin <username> --noclear %I $TERM
EOT
```

Then, start Sway when the user logs in on default terminal with the help of the `~/.profile` file.

```sh title="On the Raspberry Pi, execute the following command(s)."
# Call the script if display is present and on tty1
tee ~/.profile > /dev/null <<"EOT"
if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
  exec sway
fi
EOT
```

## Summary

Congrats! You have successfully configured Sway, Wofi and Foot. You have a lightweight and fast graphical environment that you can use on your Raspberry Pi to squeeze the last drop of performance out of it.

Sway will automatically start on boot if the Raspberry Pi is connected to a screen.

!!! question

    Why do you have to use Sway? Could't we use a more conventional graphical environment like XFCE, LXDE, Gnome or KDE?

    Sway is a tiling window manager that manages your applications in a grid. It means it can automatically open applications side by side, vertically or horizontally, using the maximum of the available space. Getting used to it can take a bit of time but it will be very useful in the next chapters to open the BeeScreens applications in a browser assigned to a specific workspace, in full screen and automatically resized to the maximum of the available space.

    It is also very efficient and fast, based on Wayland which is a more modern and lightweight graphical protocol than X11.
