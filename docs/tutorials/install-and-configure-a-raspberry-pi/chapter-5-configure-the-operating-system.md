---
description: "Install and configure a Raspberry Pi tutorial"
---

# Chapter 5 - Configure the operating system

In this chapter, you will configure and secure the operating system on your Raspberry Pi. The configuration will be quite advanced but will ensure that the Raspberry Pi will be ready to run the BeeScreens applications in a production environment.

## Steps

!!! warning

    The following steps are executed on the Raspberry Pi. It is recommended to execute them trough SSH as you'll be able to copy/paste the commands from this tutorial.

### Update the software

Run the following commands to update the installed softwares to their latest versions.

```sh title="On the Raspberry Pi, execute the following command(s)."
# Update the system's package list
sudo apt update

# Upgrade all the installed packages to their latest versions
sudo apt upgrade --yes

# Update to the latest firmware and kernel
echo "y" | sudo rpi-update

# Remove no longer required packages
sudo apt autoremove --yes
```

### Reboot the Raspberry Pi

At this point, reboot the Raspberry Pi.

```sh title="On the Raspberry Pi, execute the following command(s)."
sudo reboot
```

Access it again after the reboot.

### Set up locales

The US English locale is quite the standard in the IT world. You'll set it up on your Raspberry Pi to avoid any issues. You might see some warnings in the following commands but you can ignore them.

Run the following commands to set up locales.

```sh title="On the Raspberry Pi, execute the following command(s)."
# Disable the en_GB locale
sudo sed -i "s/^en_GB.UTF-8 UTF-8/# en_GB.UTF-8 UTF-8/" /etc/locale.gen

# Enable the en_US locale
sudo sed -i "s/^# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/" /etc/locale.gen

# Set the locales
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
```

Reload the locales configuration.

```sh title="On the Raspberry Pi, execute the following command(s)."
# Reload the locales configuration
sudo dpkg-reconfigure --frontend=noninteractive locales

# Update the locales
sudo update-locale "LANG=en_US.UTF-8"
```

### Set up keyboard layout

By default, the Raspberry Pi OS is configured to use the UK English keyboard layout. If you want to use a different keyboard layout, you will need to change the configuration.

Run the following commands to set up the keyboard layout.

=== ":material-keyboard-outline: Swiss French"

    ```sh title="On the Raspberry Pi, execute the following command(s)."
    # Set the keyboard layout
    sudo tee /etc/default/keyboard > /dev/null <<EOT
    # KEYBOARD CONFIGURATION FILE

    # Consult the keyboard(5) manual page.

    XKBMODEL="pc105"
    XKBLAYOUT="ch"
    XKBVARIANT="fr"
    XKBOPTIONS=""

    BACKSPACE="guess"
    EOT
    ```

=== ":material-keyboard-outline: US English"

    ```sh title="On the Raspberry Pi, execute the following command(s)."
    # Set the keyboard layout
    sudo tee /etc/default/keyboard > /dev/null <<EOT
    # KEYBOARD CONFIGURATION FILE

    # Consult the keyboard(5) manual page.

    XKBMODEL="pc105"
    XKBLAYOUT="en"
    XKBVARIANT=""
    XKBOPTIONS=""

    BACKSPACE="guess"
    EOT
    ```

=== ":material-keyboard-outline: Generic"

    Replace the `<layout>` and `<variant>` placeholders with the layout and variant that you want to use.

    ```sh title="On the Raspberry Pi, execute the following command(s)."
    # Set the keyboard layout
    sudo tee /etc/default/keyboard > /dev/null <<EOT
    # KEYBOARD CONFIGURATION FILE

    # Consult the keyboard(5) manual page.

    XKBMODEL="pc105"
    XKBLAYOUT="<layout>"
    XKBVARIANT="<variant>"
    XKBOPTIONS=""

    BACKSPACE="guess"
    EOT
    ```

Reload the keyboard configuration.

```sh title="On the Raspberry Pi, execute the following command(s)."
# Reload the keyboard configuration
sudo dpkg-reconfigure --frontend=noninteractive keyboard-configuration
```

### Set up the hostname

The hostname is the name of your Raspberry Pi. It is used to identify the device on the network.

Run the following commands to set up the hostname. Replace the `<hostname>` placeholder with the hostname that you want to use.

```sh title="On the Raspberry Pi, execute the following command(s)."
# Set up the hosts
sudo tee /etc/hosts > /dev/null <<EOT
127.0.0.1       localhost
::1             localhost ip6-localhost ip6-loopback
ff02::1         ip6-allnodes
ff02::2         ip6-allrouters

127.0.1.1       <hostname>
::1             <hostname>
EOT

# Set the hostname
sudo hostnamectl set-hostname <hostname>
```

### Set up timezone

The timezone is used to display the correct time on the Raspberry Pi. You will set it up to the timezone of your location.

Run the following commands to set up the timezone.

=== ":material-earth: Europe/Zurich"

    ```sh
    # Remove localtime
    sudo rm --force /etc/localtime

    # Set timezone
    sudo timedatectl set-timezone Europe/Zurich
    ```

=== ":material-earth: Generic"

    Replace the `<tz-identifier>` placeholder with the timezone identifier (check the full list [here](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)).

    ```sh
    # Remove localtime
    sudo rm --force /etc/localtime

    # Set timezone
    sudo timedatectl set-timezone <tz-identifier>
    ```

Reload the timezone configuration.

```sh title="On the Raspberry Pi, execute the following command(s)."
# Reload the timezone configuration
sudo dpkg-reconfigure --frontend=noninteractive tzdata
```

### Configure the firewall

Configure the firewall to only allow SSH connections from the outside.

```sh title="On the Raspberry Pi, execute the following command(s)."
# Install the firewall
sudo apt install --yes ufw

# Allow SSH connections
sudo ufw allow ssh

# Start the firewall
echo "y" | sudo ufw enable

# Enable the firewall service on boot
sudo systemctl enable ufw

# Start the firewall service
sudo systemctl start ufw
```

### Extend the life of the microSD card

Run the following commands to tweak and improve the microSD performance:

```sh title="On the Raspberry Pi, execute the following command(s)."
# Disable swap
sudo systemctl stop dphys-swapfile
sudo systemctl disable dphys-swapfile

# Mount certain partitions to RAM
sudo tee --append /etc/fstab > /dev/null <<"EOT"

# Custom configuration
tmpfs                 /tmp            tmpfs   defaults,noatime,nosuid,nodev,noexec,mode=0755,size=100M  0 0
tmpfs                 /var/log        tmpfs   defaults,noatime,nosuid,nodev,noexec,mode=0755,size=50M   0 0
EOT
```

### Set `config.txt` boot configuration file

Run the following commands to set specific configuration through the `config.txt` boot configuration file.

```sh title="On the Raspberry Pi, execute the following command(s)."
# Set the configuration
sudo tee /boot/config.txt > /dev/null <<"EOT"
# For more options and information see
# http://rpf.io/configtxt
# Some settings may impact device functionality. See link above for details

[all]
# Enable audio (loads snd_bcm2835)
dtparam=audio=on

# Automatically load overlays for detected cameras
camera_auto_detect=1

# Automatically load overlays for detected DSI displays
display_auto_detect=1

# Enable DRM VC4 V3D driver
dtoverlay=vc4-kms-v3d
max_framebuffers=2
gpu_mem=128

# Run in 64-bit mode
arm_64bit=1

# Disable compensation for displays with overscan
disable_overscan=1

[pi4]
# Run as fast as firmware / board allows
arm_boost=1
EOT
```

### Disable passwordless `sudo`

By default, any commands using `sudo` do not ask for the user's password to be executed. Run the following commands to change this behavior:

```sh
# Delete the directive for passwordless sudo for beescreens user
sudo rm /etc/sudoers.d/010_pi-nopasswd
```

### Optional: Secure SSH connections

A good practice is to secure the SSH connections with SSH key pairs. You won't be able to connect to the Raspberry Pi with a password anymore but you will be able to connect with a SSH keypair that is more secure against brute force attacks.

Create a SSH key pair on your computer with the [How to use OpenSSH - Create a SSH keypair](../../how-to-guides/how-to-use-openssh/index.md#create-a-ssh-keypair) guide.

Transfer the SSH public key from your computer to the Raspberry Pi with the [How to use OpenSSH - Transfer the public SSH key to a remote host](../../how-to-guides/how-to-use-openssh/index.md#transfer-the-public-ssh-key-to-a-remote-host) guide.

Update the SSH configuration to only allow connection with public/private key pairs.

```sh title="On the Raspberry Pi, execute the following command(s)."
sudo sed -i "s/^.*ChallengeResponseAuthentication.*$/ChallengeResponseAuthentication no/" /etc/ssh/sshd_config
sudo sed -i "s/^.*PasswordAuthentication.*$/PasswordAuthentication no/" /etc/ssh/sshd_config
sudo sed -i "s/^.*UsePAM.*$/UsePAM no/" /etc/ssh/sshd_config
```

Reload the SSH service.

```sh title="On the Raspberry Pi, execute the following command(s)."
sudo service ssh reload
```

### Reboot the Raspberry Pi

At this point, reboot the Raspberry Pi.

```sh title="On the Raspberry Pi, execute the following command(s)."
sudo reboot
```

### Check the results

Verify everything is working as expected by accessing it again after the reboot.

## Summary

Congrats! You now have an updated and secure Raspberry Pi.
