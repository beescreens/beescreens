---
description: "Install and configure a Raspberry Pi tutorial"
---

# Chapter 1 - Download the Raspberry Pi OS image(s)

In this chapter, you will download the Raspberry Pi OS image(s) that you will use to install the operating system on your Raspberry Pi.

The [Raspberry Pi Foundation](../../explanations/about-raspberry-pi-foundation/index.md) provides different variants of their Raspberry Pi OSes. We will the following variants in this tutorial:

- Raspberry Pi OS with desktop (the 64-bit version if using the Raspberry Pi 3B or newer)
- Raspberry Pi OS Lite (the 64-bit version if using the Raspberry Pi 3B or newer)

The Raspberry Pi OS with desktop variant is the one that you will use if you want to use the Raspberry Pi as a desktop computer embedded with many software and tools for development and testing purposes.

The Raspberry Pi OS Lite variant is the one that you will use if you want to install and configure the Raspberry Pi from scratch and only install the software needed for optimal performances.

## Steps

### Download the Raspberry Pi OS image(s)

Go to the [Raspberry Pi OS downloads page](https://www.raspberrypi.com/software/operating-systems/){:target="\_blank"} and download the latest Raspberry Pi OS image(s) that you want to use.

### Check the results

After downloading the Raspberry Pi OS image(s), you should have the at least one of the following images on your system (the version numbers may be different):

- `2023-02-21-raspios-bullseye-arm64.img.xz`: The Raspberry Pi OS with desktop image
- `2023-02-21-raspios-bullseye-arm64-lite.img.xz`: The Raspberry Pi OS Lite image

## Summary

Congrats! You have downloaded the latest version of the Raspberry Pi OS.
