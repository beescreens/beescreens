---
description: "Install and configure a Raspberry Pi tutorial"
---

# Chapter 11 - Backup and restore the microSD card

In this chapter, you will learn how to clone and replicate the microSD card to be able to deploy multiple Raspberry Pi.

The first step is to backup the microSD card. This will create an exact copy of the microSD card on your disk.

The second step is to shrink the the microSD card image to the minimum size possible. This will allow to write the image to a smaller microSD card in a faster manner.

The third step is to write the newly shirked image to a new microSD card. This will create a new microSD card that you can use to deploy a new Raspberry Pi.

## Steps

### Backup the microSD card

Raspberry Pi Imager cannot backup a USB drive to an image. You will have to use another software to backup the microSD card. The backup might take a while depending on the size of the microSD card.

=== ":simple-linux: Linux + :simple-windows: Windows: with USBImager"

    If not already done, shutdown the Raspberry Pi and unplug the microSD card.

    Plug the microSD card into your computer.

    Download and run [**USBImager**](https://gitlab.com/bztsrc/usbimager) for your platform.

    In USBImager, select a path to save the image. For the next steps, we will assume that the image is named `rpi-clone.img`.

    Select the microSD card from the list.

    Press "_Read_" to start the backup.

    For the next steps, we will assume that the image is named `rpi-clone.img`.

=== ":simple-apple: macOS + :simple-linux: Linux: with `dd`"

    If not already done, shutdown the Raspberry Pi and unplug the microSD card.

    Plug the microSD card into your computer.

    Open a terminal.

    On Linux: identify the microSD card device with the following command: `fdisk -l`. For the next steps, we will assume that the device is named `/dev/mmcblk0`.

    On macOS: identify the microSD card device with the following command: `diskutil list`. For the next steps, we will assume that the device is named `/dev/disk2`.

    Backup the microSD card to an image with the following command. Replace `<device>` with the device name from the previous command and `<image>` with the path to the image file. For the next steps, we will assume that the image is named `rpi-clone.img`.

    ```sh title="In a terminal, execute the following command(s)."
    sudo dd if=<device> of=<image> bs=4096
    ```

    The output of the command should look similar to this.

    ```sh
    7633408+0 records in
    7633408+0 records out
    31266439168 bytes (31 GB, 29 GiB) copied, 1492.59 s, 20.9 MB/s
    ```

### Optional: Shrink the microSD card image

Shrinking the microSD card image will allow to write the image to a smaller microSD card.

If you do not shrink the image, you will have to use a microSD card of the same size or bigger than the original one.

Shrink the image with [PiShrink](https://github.com/Drewsif/PiShrink) with Docker. This will take a while depending on the size of the microSD card.

```sh title="In a terminal, execute the following command(s)."
# Run PiShrink in a Docker container
docker run --privileged=true --rm \
    --volume $(pwd):/workdir \
    mgomesborges/pishrink \
    pishrink -Zadv rpi-clone.img
```

The output of the command should look similar to this.

```sh
Unable to find image 'mgomesborges/pishrink:latest' locally
latest: Pulling from mgomesborges/pishrink
b549f31133a9: Pull complete
dcfcb01a0806: Pull complete
704dab92b589: Pull complete
5c9cc949e9c9: Pull complete
Digest: sha256:7595f4c90111d97788de72ff6dbfc45e6278dea069e56b203f88a4cf6a982b13
Status: Downloaded newer image for mgomesborges/pishrink:latest
pishrink: Creating log file /workdir/pishrink.log ...
pishrink v0.1.3
pishrink: Gathering data ...
Creating new /etc/rc.local
pishrink: Checking filesystem ...
rootfs: 63202/1840608 files (0.2% non-contiguous), 732996/7566848 blocks
resize2fs 1.45.5 (07-Jan-2020)
pishrink: Shrinking filesystem ...
resize2fs 1.45.5 (07-Jan-2020)
Resizing the filesystem on /dev/loop27 to 909551 (4k) blocks.
Begin pass 2 (max = 162476)
Relocating blocks             XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
Begin pass 3 (max = 231)
Scanning inode table          XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
Begin pass 4 (max = 6439)
Updating inode references     XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
The filesystem on /dev/loop27 is now 909551 (4k) blocks long.

pishrink: Shrinking image ...
pishrink: Using xz on the shrunk image ...
rpi-clone.img: 732.2 MiB / 3812.9 MiB = 0.192, 8.9 MiB/s, 7:07
pishrink: Shrunk rpi-clone.img.xz from 30G to 733M ...
stat: cannot stat 'rpi-clone.img': No such file or directory
```

You now have a shirked image that you can use to deploy a new Raspberry Pi.

### Restore the microSD card image to a new microSD card

Follow the instructions from [Chapter 2 - Write the image(s) on a microSD card](./chapter-2-write-the-image-s-on-a-microsd-card.md) to write the backup image to a new microSD card. Instead of the official Raspberry Pi OS image, you will have to select the backup image.

### Check the results

Follow the instructions from [Chapter 4 - Start and log in on the Raspberry Pi](./chapter-4-start-and-log-in-on-the-raspberry-pi.md) to start and log in on the Raspberry Pi.

If you have restored the shrunk version of the image, the Raspberry Pi should reboot once to resize the partition to its maximum size.

Everything should work as expected from the previous chapters.

## Summary

Congrats! You have cloned and replicated the microSD card to be able to deploy multiple Raspberry Pi. The backup image can be used to deploy a new Raspberry Pi in case of a failure.
