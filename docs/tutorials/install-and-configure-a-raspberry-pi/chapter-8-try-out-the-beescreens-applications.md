---
description: "Install and configure a Raspberry Pi tutorial"
---

# Chapter 8 - Try out the BeeScreens applications

In this chapter, you will try out the BeeScreens applications.

If you come from chapter [Chapter 4 - Start and log in on the Raspberry Pi](./chapter-4-start-and-log-in-on-the-raspberry-pi.md), you might want to follow the [Chapter 7 - Install and configure the browser](./chapter-7-install-and-configure-the-browser.md) to ensure all hardware acceleration features are enabled.

## Steps

### Access the BeeScreens applications

Open Chromium on your Raspberry Pi and go to the following URLs to try out the BeeScreens applications:

- Media Player - <https://mp.beescreens.ch>
- Pimp My Wall - <https://pmw.beescreens.ch>

Try out the applications and see how they work and if performances are good.

## Summary

You have now validated that the Raspberry Pi is capable of running the BeeScreens applications.

If you come from chapter [Chapter 4 - Start and log in on the Raspberry Pi](./chapter-4-start-and-log-in-on-the-raspberry-pi.md), this tutorial is now finished. You can use the Raspberry Pi for debugging and development purposes but you will need to follow the entire tutorial again if you want to use the Raspberry Pi in a production environment.
