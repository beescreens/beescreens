---
description: "Install and configure a Raspberry Pi tutorial"
---

# Chapter 3 - Preconfigure the operating system

In this chapter, you will preconfigure the operating system to create a user account, enable SSH, and optionally set up the WiFi network on its first boot.

## Steps

### Access the boot partition of microSD card

If you haven't already, unplug the microSD card from your computer and plug it back in. This will allow the operating system to acknowledge the changes made to the microSD card.

Your operating system should automatically mount the microSD card. On macOS and Windows, you should see a single FAT32 partition named `boot` or `bootfs`. On Linux, you should see two partitions, one FAT32 partition named `boot` or `bootfs` and one ext4 partition named `root` or `rootfs`.

You can open the boot partition to access its files and folders.

### Configure a user

To avoid any security issues, you will create a user account with a password for your Raspberry Pi.

Start by generating a password hash for the user account you want to create. You will need [OpenSSH](../../explanations/about-openssh/index.md). OpenSSH is available in the [Dev Container](../../explanations/about-dev-containers/index.md) as well if you use Windows. Generate a password hash with the following command. You will be prompted to enter a password.

```sh title="In a terminal, execute the following command(s)."
# openssl will prompt you to enter a password
openssl passwd -6
```

In the boot partition, create a file named `userconf` (without any extension). Open the `userconf` file and add the following lines. Replace `<username>` with the username you want to create and `<password hash>` with the password hash you generated.

```sh title="userconf"
<username>:<password hash>
```

Save and close the file.

### Set up the WiFi network

If you want to set up the WiFi network, you can create a file named `wpa_supplicant.conf` in the boot partition.

Open the `wpa_supplicant.conf` file and add the following lines. Replace the `<iso-3166-1-country-code>` placeholder with the ISO 3166-1 country code (check the full list [here](https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes), for Switzerland it's `CH`), `<network>` with the name of the WiFi network and `<password>` with the password of the WiFi network.

=== ":material-wifi: Consumer network"

    ```sh title="wpa_supplicant.conf"
    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
    country=<iso-3166-1-country-code>
    update_config=1

    network={
    	ssid="<network>"
    	psk="<password>"
    }
    ```

=== ":material-wifi: Enterprise network"

    ```sh title="wpa_supplicant.conf"
    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
    country=<iso-3166-1-country-code>
    update_config=1

    network={
    	ssid="<network>"
    	key_mgmt=WPA-EAP
    	identity="<username>"
    	password="<password>"
    }
    ```

Save and close the file.

### Enable SSH

To enable SSH, create a file named `ssh` (without any extension) in the boot partition.

### Check the results

Your boot partition should look like this:

```yaml
.
├── overlays
│   └── ...
├── COPYING.linux
├── LICENCE.broadcom
├── bcm2710-rpi-2-b.dtb
├── bcm2710-rpi-3-b-plus.dtb
├── bcm2710-rpi-3-b.dtb
├── bcm2710-rpi-cm3.dtb
├── bcm2710-rpi-zero-2-w.dtb
├── bcm2710-rpi-zero-2.dtb
├── bcm2711-rpi-4-b.dtb
├── bcm2711-rpi-400.dtb
├── bcm2711-rpi-cm4.dtb
├── bcm2711-rpi-cm4s.dtb
├── bootcode.bin
├── cmdline.txt
├── config.txt
├── fixup.dat
├── fixup4.dat
├── fixup4cd.dat
├── fixup4db.dat
├── fixup4x.dat
├── fixup_cd.dat
├── fixup_db.dat
├── fixup_x.dat
├── issue.txt
├── kernel8.img
├── ssh # (1)!
├── start.elf
├── start4.elf
├── start4cd.elf
├── start4db.elf
├── start4x.elf
├── start_cd.elf
├── start_db.elf
├── start_x.elf
├── userconf # (2)!
└── wpa_supplicant.conf # (3)!
```

1. This enables SSH on the Raspberry Pi.
2. This creates a user account.
3. This sets up the WiFi network.

## Summary

Congrats! You have preconfigured the Raspberry for its first boot! You can unmount and unplug the microSD card from your computer.
