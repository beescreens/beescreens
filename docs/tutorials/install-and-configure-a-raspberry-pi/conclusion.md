---
description: "Install and configure a Raspberry Pi tutorial"
---

# Conclusion

Congratulations! You have finished the tutorial. You now have a Raspberry Pi that is ready to be used in a production environment. The Raspberry Pi can display the BeeScreens applications in a browser assigned to a specific Sway workspace and output. When satisfied with the results, you can backup and replicate the microSD card to be able to deploy multiple Raspberry Pi.

You have learned how to:

- Download and write the Raspberry Pi OS image to a microSD card.
- Boot the Raspberry Pi from the microSD card.
- Configure the Raspberry Pi with all the required packages.
- Configure Sway and the browser to display the BeeScreens applications.
- Clone and replicate the microSD card to be able to deploy multiple Raspberry Pi.
