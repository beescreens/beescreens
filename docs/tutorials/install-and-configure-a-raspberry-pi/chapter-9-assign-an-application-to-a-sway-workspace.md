---
description: "Install and configure a Raspberry Pi tutorial"
---

# Chapter 9 - Assign an application to a Sway workspace

In this chapter, you will assign a browser to a Sway workspace. This will allow to start two different browsers each on their own workspace to display two different BeeScreens applications.

## Steps

### Create the workspaces applications configuration files

Sway allows to configure applications to start on a specific workspace. This is done by creating a configuration file for each workspace.

Chromium will be started from the command line with the same options that were used in the chapter [Chapter 7 - Install and configure the browser](./chapter-7-install-and-configure-the-browser.md).

The first workspace will be assigned to the browser displaying the Pimp My Wall application. The second workspace will be assigned to the browser displaying the Media Player application.

```sh title="On the Raspberry Pi, execute the following command(s)."
# Configure the first workspace
tee ~/.config/sway/config.d/workspace-1-applications > /dev/null <<"EOT"
#
# Workspaces applications configuration
#

# Switch to workspace
workspace 1

# Start the browser with a clean profile
exec chromium-browser \
	--user-data-dir="$(mktemp -d)" \
	--enable-gpu-rasterization \
	--enable-zero-copy \
	--ozone-platform-hint=wayland \
	--enable-features="CanvasOopRasterization,EnableDrDc,RawDraw" \
	--ozone-platform=wayland \
	--start-fullscreen \
	https://pmw.beescreens.ch
EOT

# Configure the second workspace
tee ~/.config/sway/config.d/workspace-2-applications > /dev/null <<"EOT"
#
# Workspaces applications configuration
#

# Switch to workspace
workspace 2

# Start the browser with a clean profile
exec chromium-browser \
	--user-data-dir="$(mktemp -d)" \
	--enable-gpu-rasterization \
	--enable-zero-copy \
	--ozone-platform-hint=wayland \
	--enable-features="CanvasOopRasterization,EnableDrDc,RawDraw" \
	--ozone-platform=wayland \
	--start-fullscreen \
	https://mp.beescreens.ch
EOT
```

### Check the results

Restart Sway.

On login, the first workspace should be assigned to the browser displaying the Pimp My Wall application. The second workspace should be assigned to the browser displaying the Media Player application.

## Summary

Congrats! You have successfully assigned a browser to a Sway workspace. This will allow in the next chapter to assign a Sway workspace to a specific output to display both BeeScreens applications on each screen.
