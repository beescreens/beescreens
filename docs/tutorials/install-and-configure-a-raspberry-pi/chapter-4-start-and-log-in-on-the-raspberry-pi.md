---
description: "Install and configure a Raspberry Pi tutorial"
---

# Chapter 4 - Start and log in on the Raspberry Pi

In this chapter, you will start the Raspberry Pi for the first time and log in on it. This will ensure you have a working Raspberry Pi.

## Steps

### Start the Raspberry Pi

If you haven't already, unmount and unplug the microSD card from your computer. Plug it in the Raspberry Pi. The Raspberry Pi should be connected to a screen, a keyboard, a mouse and a power supply.

### Log in on the Raspberry Pi

When the Raspberry Pi is plugged to a power source, it should start, resize the microSD card and reboot. After a few seconds, you should see the login screen.

If you have written the Raspberry Pi OS with desktop image, you should see a colorful login page.

If you have written the Raspberry Pi OS Lite, you should see a black and white login page.

Log in using the username and password you have chosen during the [preconfiguration chapter](./chapter-3-preconfigure-the-operating-system.md).

### Verify the Raspberry Pi is connected to the Internet

Use `ping` to confirm that the Raspberry Pi is connected to the Internet.

```sh title="In a terminal, execute the following command(s)."
ping www.google.com
```

The output of the command should look similar to this.

```sh
PING www.google.com (123.456.789.01): 56 data bytes
64 bytes from 123.456.789.01: icmp_seq=0 ttl=115 time=10.169 ms
64 bytes from 123.456.789.01: icmp_seq=1 ttl=115 time=8.606 ms
64 bytes from 123.456.789.01: icmp_seq=2 ttl=115 time=8.386 ms
```

To stop the ping process, press ++ctrl+c++ in your terminal.

### Verify you can connect to the Raspberry Pi using SSH

Get the IP of the Raspberry Pi with the following command.

```sh title="In a terminal, execute the following command(s)."
ip addr
```

The output of the command should look similar to this. Note the `192.168.1.203` IP address. This identifies the Raspberry Pi on your network using the `wlan0` Wi-Fi interface. If plugged to an Ethernet cable, the IP address would be different on `eth0`.

```sh
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether 12:34:56:78:90:ab brd ff:ff:ff:ff:ff:ff
3: wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether ab:cd:ef:12:34:56 brd ff:ff:ff:ff:ff:ff
    inet 192.168.1.203/24 brd 192.168.1.255 scope global dynamic noprefixroute wlan0
       valid_lft 258882sec preferred_lft 226482sec
    inet6 fe80::3df1:8ad0:4cf1:b39f/64 scope link
       valid_lft forever preferred_lft forever
```

From your computer, open a terminal and access your Raspberry Pi with SSH. Replace `<username>` and `<ip>` with the Raspberry Pi username and IP. Enter the password when prompted.

```sh title="In a terminal, execute the following command(s)."
ssh <username>@<ip>
```

The output of the command should look similar to this.

```sh
<username>@<ip>'s password:
Linux raspberrypi 5.15.84-v8+ #1613 SMP PREEMPT Thu Jan 5 12:03:08 GMT 2023 aarch64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Fri Apr  7 17:34:19 2023 from 192.168.1.200
<username>@raspberrypi:~ $
```

You can exit the SSH session by typing `exit` and ++enter++ in the terminal.

## Summary

Congrats! You have started the Raspberry Pi and logged in on it. You have also verified that the Raspberry Pi is connected to the Internet and that you can connect to it using SSH.

If you have written the Raspberry Pi OS with desktop image, you can go to the chapter [_Chapter 8 - Try out the BeeScreens applications_](./chapter-8-try-out-the-beescreens-applications.md).

Otherwise, you can continue the tutorial. For the next chapters, you can use the keyboard of the Raspberry Pi or the SSH connection to set up the Raspberry Pi.
