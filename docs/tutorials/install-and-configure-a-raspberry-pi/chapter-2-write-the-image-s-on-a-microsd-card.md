---
description: "Install and configure a Raspberry Pi tutorial"
---

# Chapter 2 - Write the image(s) on a microSD card

In this chapter, you will write the image(s) on a microSD card using [Raspberry Pi Imager](../../explanations/about-raspberry-pi-imager/index.md) with the microSD card reader.

## Steps

### Download and start Raspberry Pi Imager

Go to the [Raspberry Pi Imager downloads page](https://www.raspberrypi.com/software/) and download the binary for your platform.

Start Raspberry Pi Imager.

### Plug the microSD to your computer

Take the microSD out from the Raspberry Pi and plug it in the card reader.

Plug the microSD card reader in a USB port of your computer.

### Select and write the image to the microSD card

In Raspberry Pi Imager, select **Operating System > Use custom image**.

Select the previously downloaded image that you want to write to the microSD card.

Select the microSD card that you want to write the image to from the **Storage** list.

Click on the **Write** button.

The writing process will start and might take a few minutes.

### Check the results

At the end of the writing process, Raspberry Pi Imager should let you know it is finished. You can take out the microSD card from your microSD card reader.

## Summary

Congrats! You have written the latest version of the Raspberry Pi OS image on a microSD card.
