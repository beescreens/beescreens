---
description: "Install and configure a Raspberry Pi tutorial"
---

# Chapter 7 - Install and configure the browser

In this chapter, you will install and configure the browser on your Raspberry Pi to display the BeeScreens applications.

Chromium, the open source version of Google Chrome, will be installed and configured. In the past, Firefox was used but it was replaced by Chromium because it was more stable and had better performances regarding hardware acceleration.

## Steps

### Install Chromium

Raspberry Pi OS offers two Chromium packages: `chromium` and `chromium-browser`. The `chromium-browser` is the one maintained by the Raspberry Pi Foundation and has various patches to enable hardware acceleration. It is the one that you will use.

Install the `chromium-browser` package:

```sh title="On the Raspberry Pi, execute the following command(s)."
sudo apt install --yes chromium-browser rpi-chromium-mods
```

### Configure Chromium

Chromium must be configured in order to start correctly on Wayland and enable all the hardware acceleration features.

Start by creating the configuration directory for Chromium.

```sh title="On the Raspberry Pi, execute the following command(s)."
# Create the configuration directories
mkdir --parent ~/.config/chromium
```

Create the `.config/chromium/Local State` file.

```sh title="On the Raspberry Pi, execute the following command(s)."
# Create the configuration file for chromium
tee .config/chromium/Local\ State > /dev/null <<"EOT"
{
	"browser": {
		"enabled_labs_experiments": [
			"canvas-oop-rasterization@1",
			"enable-drdc@1",
			"enable-gpu-rasterization@1",
			"enable-raw-draw@1",
			"enable-zero-copy@1",
			"ozone-platform-hint@3"
		]
	}
}
EOT
```

This file will enable all the required features for Chromium to run on Wayland and enable hardware acceleration.

All these features can be enabled when starting Chromium from the command line but it is easier to enable them by default. This will ensure Chromium can be started from Wofi as well.

### Start Chromium

If you haven't already, start Sway.

Start Wofi with ++windows+d++ and select Chromium.

Chromium should start.

### Validate Chromium has hardware acceleration enabled

You can check that all the required features are enabled by opening the [chrome://flags](chrome://flags) page.

You can validate that Chromium has hardware acceleration enabled by opening the [chrome://gpu](chrome://gpu) page.

The output of the page should look like this:

```markdown title="Output of the <chrome://gpu> page"
Graphics Feature Status

- Canvas: Hardware accelerated
- Canvas out-of-process rasterization: Enabled
- Direct Rendering Display Compositor: Disabled
- Compositing: Hardware accelerated
- Multiple Raster Threads: Enabled
- OpenGL: Enabled
- Rasterization: Hardware accelerated on all pages
- Raw Draw: Enabled
- Video Decode: Hardware accelerated
- Video Encode: Software only. Hardware acceleration disabled
- Vulkan: Disabled
- WebGL: Hardware accelerated
- WebGL2: Hardware accelerated
- WebGPU: Disabled
```

## Summary

Congrats! You have successfully installed and configured Chromium on your Raspberry Pi with hardware acceleration enabled. This will allow you to run the BeeScreens applications on your Raspberry Pi.
