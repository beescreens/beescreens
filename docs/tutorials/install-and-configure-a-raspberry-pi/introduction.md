---
description: "Install and configure a Raspberry Pi tutorial"
---

# Introduction

This tutorial will show you how to use a [Raspberry Pi](../../explanations/about-raspberry-pi-foundation/index.md) to display BeeScreens applications (or any content you'd like from the Internet).

The Raspberry Pi will be install and configured to display applications in a browser such as Firefox or Chromium with the help of [Sway](../../explanations/about-sway/index.md) and [Raspberry Pi Imager](../../explanations/about-raspberry-pi-imager/index.md).

Almost all the configuration you'll see can be achieved using a graphical user interface (GUI) with the help of the Raspberry Imager tool (Advanced menu) or the `raspi-config` configuration tool. We will not cover this in this tutorial to allow you to do all the configuration "by hand" so you get a full understanding of the files location and their meaning.

!!! warning

    This tutorial is not meant to be followed in the Dev Container environment like the other tutorials. You will need to install the tools and dependencies on your computer.

## Resources and inspirations

These resources and inspirations were used to create this tutorial (in alphabetical order).

- [Arch Linux Wiki Command-line shell #Login shell](https://wiki.archlinux.org/title/Command-line_shell#Login_shell)
- [Arch Linux Wiki Firefox #Tweaks](https://wiki.archlinux.org/title/Firefox/Tweaks)
- [Arch Linux Wiki Wayland](https://wiki.archlinux.org/index.php/wayland)
- [Arch Linux Wiki Sway](https://wiki.archlinux.org/index.php/Sway)
- [Arch Linux Wiki Sway #Starting](https://wiki.archlinux.org/index.php/Sway#Starting)
- [Debian Wiki Wayland](https://wiki.debian.org/Wayland)
- [Gentoo Wiki Raspberry Pi VC4](https://wiki.gentoo.org/wiki/Raspberry_Pi_VC4)
- [PiShrink](https://github.com/Drewsif/PiShrink)
- [Raspberry Pi Documentation Configuration #Configuring Networking using the Command Line](https://www.raspberrypi.com/documentation/computers/configuration.html#using-the-command-line)
- [Raspberry Pi Documentation Configuration #Setting up a Headless Raspberry Pi](https://www.raspberrypi.com/documentation/computers/configuration.html#setting-up-a-headless-raspberry-pi)
- [Raspberry Pi Documentation Configuration #Securing your Raspberry Pi](https://www.raspberrypi.com/documentation/computers/configuration.html#securing-your-raspberry-pi)
- [Raspberry Pi Documentation Remote access #Passwordless SSH Access](https://www.raspberrypi.com/documentation/computers/remote-access.html#passwordless-ssh-access)
- [Raspberry Pi Documentation Raspberry Pi OS #Updating and upgrading Raspberry Pi OS](https://www.raspberrypi.com/documentation/computers/os.html#updating-and-upgrading-raspberry-pi-os)
- [Raspberry Pi Documentation The config.txt file](https://www.raspberrypi.com/documentation/computers/config_txt.html)
- [Raspberry Pi Documentation The config.txt file #Memory Options](https://www.raspberrypi.com/documentation/computers/config_txt.html#memory-options)
- [Raspberry Pi Documentation The config.txt file #Overclocking Options](https://www.raspberrypi.com/documentation/computers/config_txt.html#overclocking-options)
- [Raspberry Pi Forums _"Permanently disable swap on Raspbian Buster"_](https://www.raspberrypi.org/forums/viewtopic.php?t=244130)
- [Raspberry Pi StackExchange _"Setting timezone non-interactively" Raspberry Stack Exchange question"_](https://raspberrypi.stackexchange.com/questions/87164/setting-timezone-non-interactively)
- [Raspberry Pi StackExchange _"How can I extend the life of my SD card?"_](https://raspberrypi.stackexchange.com/questions/169/how-can-i-extend-the-life-of-my-sd-card)
- [Server Fault _"How do you set a locale non-interactively on Debian/Ubuntu?"_](https://serverfault.com/questions/362903/how-do-you-set-a-locale-non-interactively-on-debian-ubuntu)
- [StackOverflow _"installing `lightdm` in Dockerfile raises interactive keyboard layout menu"_](https://stackoverflow.com/questions/38165407/installing-lightdm-in-dockerfile-raises-interactive-keyboard-layout-menu)
- [Sway official documentation](https://swaywm.org/)
- [Wayland official documentation](https://wayland.freedesktop.org/)
