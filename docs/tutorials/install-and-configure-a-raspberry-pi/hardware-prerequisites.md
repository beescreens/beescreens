---
description: "Install and configure a Raspberry Pi tutorial"
---

# Hardware prerequisites

This tutorial was written for the Raspberry Pi 4 model. It should work on other Raspberry Pi models (except the < Raspberry Pi 2B models, they are not powerful enough to run the BeeScreens applications), but it has not been extensively tested.

In order to follow this tutorial, you will need the following hardware:

- A Raspberry Pi 4
- A power supply for the Raspberry Pi
- A microSD card (a 8 GiB microSD is recommended)
- A microSD card reader
- A screen with an HDMI port
- If using a Raspberry Pi 4, a micro HDMI to HDMI cable
- A keyboard
- A mouse
- An Internet connection
- Optional: an ethernet cable
