# Home

## Introduction

Welcome to the BeeScreens Documentation website!

This documentation is based on the [Diátaxis Documentation Framework](./explanations/about-this-documentation/index.md).

The documentation is separated in the following axes:

- [Tutorials](./tutorials/index.md)
- [How-to guides](./how-to-guides/index.md)
- [Reference](./reference/index.md)
- [Explanations](./explanations/index.md)

## Getting started

Don't know where to start? Check these first!

- About BeeScreens - TODO
- [Install and configure Visual Studio Code](./tutorials/install-and-configure-visual-studio-code/index.md)
- [Install and configure Docker](./tutorials/install-and-configure-docker/index.md)
- [Install and configure Dev Containers](./tutorials/install-and-configure-dev-containers/index.md)
- [Create a Media Player application](./tutorials/create-a-media-player-application/introduction/index.md)
- [Create an interactive drawing application](./tutorials/create-a-media-player-application/introduction/index.md)
- [Become a BeeScreens contributor](./tutorials/become-a-beescreens-contributor/index.md)
- [About this documentation](./explanations/about-this-documentation/index.md)
