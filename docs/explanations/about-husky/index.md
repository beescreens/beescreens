# About Husky

As the website of [Husky](https://typicode.github.io/husky/#/) mentions:

!!! quote

	Modern native git hooks made easy

	You can use it to lint your commit messages, run tests, lint code, etc... when you commit or push.

## How and why do we use Husky

We use Husky to run a local pipeline to spot errors as fast as possible before pushing our changes to the remote Git repository.

We chose Husky because it is easy to use and easily set up native Git Hooks. Running the pipeline locally is also a good way to avoid unnecessary runs on the remote server.

## Documentation

Husky defines two Git Hooks:

- `pre-push` (`.husky/pre-push`) - Executed before a `git push` command.
- `post-merge` (`.husky/post-merge`) - Executed after a `git pull` command.

### `pre-push`

Run the same pipeline as on GitLab but locally in order to avoid unnecessary runs on the remote server.

### `post-merge`

As our `.env` files are gitignored to give freedom to our developers to set their own values, it can happen that environment variables were added to the `.env.defaults` and `.env.schema` files by other developers.

The `post-merge` hook runs the `pnpm review:env:postmerge` script in order to check the current values in your `.env` files against the values in the `.env.defaults` and `.env.schema` files.

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [Git githooks documentation](https://git-scm.com/docs/githooks)
