# About SQLite

As the website of [SQLite](https://sqlite.org/index.html) mentions:

!!! quote

	SQLite is a C-language library that implements a small, fast, self-contained, high-reliability, full-featured, SQL database engine.

This database engine is mostly used during development or when the application is small and its data not crucial.

## How and why do we use SQLite

_TODO_

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [PostgreSQL](https://www.postgresql.org/)
- [MariaDB](https://mariadb.org/)
