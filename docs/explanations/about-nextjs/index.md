# About Next.js

As the website of [Next.js](https://nextjs.org/) mentions:

!!! quote
	Next.js enables you to create full-stack web applications by extending the latest React features, and integrating powerful Rust-based JavaScript tooling for the fastest builds.

## How and why do we use Next.js

_TODO_

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [About Next.js](https://nextjs.org/learn/foundations/about-nextjs)
- [From JavaScript to React](https://nextjs.org/learn/foundations/from-javascript-to-react/building-ui-with-components)
