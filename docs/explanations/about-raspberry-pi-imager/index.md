# About Raspberry Pi Imager

As the website of [Raspberry Pi Imager](https://www.raspberrypi.com/software/) mentions:

!!! quote

    Raspberry Pi Imager is the quick and easy way to install Raspberry Pi OS and other operating systems to a microSD card, ready to use with your Raspberry Pi.

## How and why do we use Raspberry Pi Imager

We use Raspberry Pi Imager to write the Raspberry Pi images to the microSD cards.

We use Raspberry Pi Imager because it is a lightweight and easy to use tool to write the Raspberry Pi images to the microSD cards that work on Windows, Linux and macOS.

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [USBImager](https://gitlab.com/bztsrc/usbimager) - A very minimal GUI app that can write compressed disk images to USB drives
- [balenaEtcher](https://www.balena.io/etcher/) - Flash OS images to SD cards & USB drives, safely and easily
