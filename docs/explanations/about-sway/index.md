# About Sway

As the website of [Sway](https://swaywm.org/) mentions:

!!! quote

    Sway is a tiling Wayland compositor and a drop-in replacement for the [i3 window manager](http://i3wm.org/) for X11. It works with your existing i3 configuration and supports most of i3's features, plus a few extras.

    Sway allows you to arrange your application windows logically, rather than spatially. Windows are arranged into a grid by default which maximizes the efficiency of your screen and can be quickly manipulated using only the keyboard.

## How and why do we use Sway

We use Sway to automatically arrange the different workspaces and windows on the [Raspberry Pis](../about-raspberry-pi-foundation/index.md) to display the different BeeScreens applications during our events.

We use Sway because it is a tiling window manager that works with the Wayland protocol. We use Wayland because it is a more modern and fast protocol than X11.

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [i3](https://i3wm.org/) - A tiling window manager for X11
- [Wayland](https://wayland.freedesktop.org/) - A replacement for the X11 window system protocol and architecture
