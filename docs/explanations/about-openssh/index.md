# About OpenSSH

As the website of [OpenSSH](https://www.openssh.com/) mentions:

!!! quote

	OpenSSH is the premier connectivity tool for remote login with the SSH protocol. It encrypts all traffic to eliminate eavesdropping, connection hijacking, and other attacks. In addition, OpenSSH provides a large suite of secure tunneling capabilities, several authentication methods, and sophisticated configuration options.

## How and why do we use OpenSSH

We use OpenSSH to clone the BeeScreens repository, sign our commits, connect to the virtual machine and the Raspberry Pis.

We chose OpenSSH because it is a standard to secure connections and access remote instances.

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

_None at the moment._
