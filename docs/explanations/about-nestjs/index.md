# About NestJS

As the website of [NestJS](https://nestjs.com/) mentions:

!!! quote

	[NestJS is] a progressive Node.js framework for building efficient, reliable and scalable server-side (backend) applications.

## How and why do we use NestJS

_TODO_

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [AdonisJS](https://adonisjs.com/) is a fully featured web framework for Node.js
