# About Ansible

As the website of [Ansible](https://www.ansible.com/) mentions:

!!! quote

	Ansible is an open source community project sponsored by Red Hat, it's the simplest way to automate IT. Ansible is the only automation language that can be used across entire IT teams from systems and network administrators to developers and managers.

Ansible is used to create configurations 

## Ansible Pull

As the website of [Ansible Pull](https://docs.ansible.com/ansible/latest/cli/ansible-pull.html) mentions:

!!! quote

	Ansible Pull is a small script that will checkout a repo of configuration instructions from git, and run ansible-playbook against that content.

## How and why do we use Ansible

Ansible Pull is used to apply configurations to Raspberry Pi devices.

