# About Nunjucks

As the website of [Nunjucks](https://mozilla.github.io/nunjucks/) mentions:

!!! quote

    A rich and powerful templating language for JavaScript.

## How and why do we use Nunjucks

We use Nunjucks to render the pages of our most simple applications. It allows us to have a single page application that is rendered on the server side.

Nunjucks is more modern than [Handlebars](../about-handlebars/index.md) and offers more features.

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [Handlebars](../about-handlebars/index.md)
