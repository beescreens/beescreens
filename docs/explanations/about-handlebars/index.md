# About Handlebars

As the website of [Handlebars](https://handlebarsjs.com/) mentions:

!!! quote

    Handlebars provides the power necessary to let you build semantic templates effectively with no frustration.

Handlebars offers [built-in helpers](https://handlebarsjs.com/guide/builtin-helpers.html) that can be used in templates.

[Custom helpers](https://handlebarsjs.com/guide/expressions.html#helpers) can be added to extend the functionalities of Handlebars.

## How and why do we use Handlebars

We use Handlebars to render the pages of our most simple applications. It allows us to have a single page application that is rendered on the server side.

[Nunjucks](../about-nunjunks/index.md) is more modern than Handlebars and offers more features.

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [Nunjucks](../about-nunjunks/index.md)
