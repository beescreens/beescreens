# About cspell

As the website of [CSpell](https://cspell.org/) mentions:

!!! quote

	A Spell Checker for Code!

	cspell is a command line tool and library for spell checking code.

## How and why do we use cspell

We use cspell to spellcheck our codebase and spot obvious mistakes. cspell is not capable to spot grammar issues but can help regarding spellcheck errors.

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [gramma](https://caderek.github.io/gramma/) - command-line grammar checker.
