# About pnpm

As the website of [pnpm](https://pnpm.io/) mentions:

!!! quote

    Fast, disk space efficient package manager.

    **Fast** pnpm is up to 2x faster than the alternatives.

    **Efficient** Files inside `node_modules` are cloned or hard linked from a single content-addressable storage.

    **Supports monorepos** pnpm has built-in support for multiple packages in a repository.

    **Strict** pnpm creates a non-flat `node_modules` by default, so code has no access to arbitrary packages.

## How and why do we use pnpm

We use pnpm to manage our monorepo applications and packages as well as our dependencies.

We chose pnpm because it has built-in support for monorepos and it is fast and disk space efficient.

## Documentation

### `package.json` structure

We tend to uniformize the `package.json` files so all commands are mostly the same across the workspace. Here is an example of a `package.json` file.

```json title="package.json"
{
	"name": "@beescreens/<application name>[-<application service name>]", // (1)!
	"version": "0.0.0", // (2)!
	"description": "", // (3)!
	"license": "", // (4)!
	"repository": {
		"type": "git",
		"url": "git+https://gitlab.com/beescreens/beescreens.git"
	},
	"author": "", // (5)!
	"bugs": {
		"url": "https://gitlab.com/beescreens/beescreens/issues"
	},
	"homepage": "", // (6)!
	"scripts": {
		"preinstall": "npx only-allow pnpm", // (7)!
		"setup:env": "shx cp .env.defaults .env", // (8)!
		"review:env": "dotenv-extended --schema=.env.schema --defaults=.env.defaults --errorOnMissing=true --errorOnExtra=true --errorOnRegex=true true", // (9)!
		"review:env:postmerge": "dotenv-extended --schema=.env.schema --defaults=.env --errorOnMissing=true --errorOnExtra=true --errorOnRegex=true true", // (10)!
		"review:code:format": "prettier --check \"**/*.{ts,js}\" --ignore-path .gitignore", // (11)!
		"review:code:format:fix": "prettier --write \"**/*.{ts,js}\" --ignore-path .gitignore", // (12)!
		"review:code:lint": "eslint --ext .ts,.js --ignore-path .gitignore .", // (13)!
		"review:code:lint:fix": "eslint --fix --ext .ts,.js --ignore-path .gitignore .", // (14)!
		"test:unit": "", // (15)!
		"test:unit:watch": "", // (16)!
		"test:integration": "", // (17)!
		"test:integration:watch": "", // (18)!
		"test": "pnpm test:unit && pnpm test:integration", // (19)!
		"dev": "", // (20)!
		"build": "", // (21)!
		"start": "", // (22)!
		"bundle": "" // (23)!
	},
	"dependencies": {
		// (24)!
	},
	"devDependencies": {
		// (25)!
	}
}
```

1. The name of the Node project in the form of `@beescreens/<application/package name>[-<application service name>]`.
2. The version of the Node project.
3. The description of the Node project.
4. The license of the Node project (usually AGPL-3.0).
5. The main author(s) of the Node project. If made my multiple people, usually the author becomes "_BeeScreens contributors_".
6. Link to the homepage of the Node project.
7. Ensure that the project is installed with pnpm.
8. Copy the default environment variables its own `.env` file.
9. Check if the environment variables are valid.
10. Check if the environment variables are valid after a merge with the help of [Husky](../about-husky/index.md).
11. Check if the code is formatted correctly.
12. Fix the code formatting.
13. Check if the code is linted correctly.
14. Fix the code linting.
15. Run the unit tests.
16. Run the unit tests in watch mode.
17. Run the integration tests.
18. Run the integration tests in watch mode.
19. Run the unit and integration tests.
20. Run the application in development mode.
21. Build the application/package.
22. Run the application in production mode.
23. Bundle the application/package for release.
24. Dependencies of the project used in production.
25. Dependencies of the project used during development.

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [npm](https://www.npmjs.com/)
- [pnpm website](https://pnpm.io/)
