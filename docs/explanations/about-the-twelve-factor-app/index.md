# About The Twelve-Factor App

As the website of [The Twelve-Factor App](https://12factor.net/) mentions:

!!! quote

	In the modern era, software is commonly delivered as a service: called web apps, or software-as-a-service. The twelve-factor app is a methodology for building software-as-a-service apps that:

	- Use declarative formats for setup automation, to minimize time and cost for new developers joining the project;
	- Have a clean contract with the underlying operating system, offering maximum portability between execution environments;
	- Are suitable for deployment on modern cloud platforms, obviating the need for servers and systems administration;
	- Minimize divergence between development and production, enabling continuous deployment for maximum agility;
	- And can scale up without significant changes to tooling, architecture, or development practices.

	The twelve-factor methodology can be applied to apps written in any programming language, and which use any combination of backing services (database, queue, memory cache, etc).

## How and why do we use The Twelve-Factor App

_TODO_

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

_None at the moment._
