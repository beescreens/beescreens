# About systemd

As the website of [systemd](https://systemd.io/) mentions:

!!! quote

	systemd is a suite of basic building blocks for a Linux system. It provides a system and service manager that runs as PID 1 and starts the rest of the system.

## How and why do we use systemd

Systemd is used for the agent service of the Dashboard application. It is used to run the agent periodically on the Raspberry Pi. The agent will then check if new configurations are available and if so, it will fetch them and apply them with Ansible.
