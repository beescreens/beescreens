# About dotenv

As the page of the [dotenv](https://www.npmjs.com/package/dotenv) package mentions:

!!! quote

	Dotenv is a zero-dependency module that loads environment variables from a .env file into process.env.

## How and why do we use dotenv

_TODO_

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [npm - dotenv](https://www.npmjs.com/package/dotenv)
- [dotenv - merchandising](https://dotenv.gumroad.com/l/redacted)
