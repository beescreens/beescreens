# About Raspberry Pi Foundation

The Raspberry Pi Foundation is the creator of the Raspberry Pi computers. The Raspberry Pi Foundation is a UK-based charity that aims to promote the teaching of basic computer science in schools and in developing countries.

## How and why do we use the Raspberry Pis

We use Raspberry Pis to display the different BeeScreens applications during our events.

We use the Raspberry Pis because they are (or were..?) cheap, small, with a large community and powerful enough to run the BeeScreens applications in a browser.

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [Official documentation](https://www.raspberrypi.org/documentation/) - The official documentation for the Raspberry Pi, written by the Raspberry Pi Foundation with community contributions.
- [Operating system images](https://www.raspberrypi.org/software/operating-systems/) - The official operating system images for the Raspberry Pi, written by the Raspberry Pi Foundation with community contributions.
- [Raspberry Pi](https://www.raspberrypi.org/) - The Raspberry Pi is a tiny and affordable computer that you can use to learn programming through fun, practical projects.
