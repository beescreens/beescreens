# About Separation of concerns

As the [Wikipedia's page of Separation of concerns](https://en.wikipedia.org/wiki/Separation_of_concerns) mentions:

!!! quote

	In computer science, separation of concerns is a design principle for separating a computer program into distinct sections. Each section addresses a separate concern, a set of information that affects the code of a computer program. [...] Modularity, and hence separation of concerns, is achieved by encapsulating information inside a section of code that has a well-defined interface.

	[...]

	Separation of concerns results in more degrees of freedom for some aspect of the program's design, deployment, or usage. Common among these is increased freedom for simplification and maintenance of code. When concerns are well-separated, there are more opportunities for module upgrade, reuse, and independent development. Hiding the implementation details of modules behind an interface enables improving or modifying a single concern's section of code without having to know the details of other sections and without having to make corresponding changes to those other sections.

## How and why do we use Separation of concerns

_TODO_

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

_None at the moment._
