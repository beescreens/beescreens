# About cURL

As the [Wikipedia's page of cURL](https://en.wikipedia.org/wiki/CURL) mentions:

!!! quote

	cURL is a computer software project providing a library and command-line tool for transferring data using various network protocols. The name stands for "Client URL".

## How and why do we use cURL

_TODO_

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [cURL website](https://curl.se/)
