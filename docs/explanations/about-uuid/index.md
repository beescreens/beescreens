# About UUID

As the [RFC4122](https://www.ietf.org/rfc/rfc4122.txt) mentions:

!!! quote

	This specification defines a Uniform Resource Name namespace for UUIDs (Universally Unique IDentifier), also known as GUIDs (Globally Unique IDentifier). A UUID is 128 bits long, and can guarantee uniqueness across space and time.

It is a unique label that can be used for IDs (in databases for example).

## How and why do we use UUID

_TODO_

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [UUID npm package](https://www.npmjs.com/package/uuid)
