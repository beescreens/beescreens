# About Immer

As the [Immer npm package page](https://www.npmjs.com/package/immer) mentions:

!!! quote

	Create the next immutable state tree by simply modifying the current tree.

Immer helps you manages your state without having to think about immutability.

## How and why do we use Immer

_TODO_

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

_None at the moment._
