# About Cobra

As the website of [Cobra](https://cobra.dev/) mentions:

!!! quote

	Cobra is both a library for creating powerful modern CLI applications as well as a program to generate applications and command files.

## How and why do we use Cobra

Cobra is used for the agent service of the Dashboard application. It is used to create a CLI that allows the communication between the backend and the Raspberry Pi devices.
