# About GitLab

As the website of [GitLab](https://about.gitlab.com/) mentions:

!!! quote

    GitLab is an open source end-to-end software development platform with built-in version control, issue tracking, code review, CI/CD, and more.

## How and why do we use GitLab

We use GitLab to store our [Git](../about-git/index.md) repositories and ship our code to production.

We chose GitLab because it is open source and offers a lot of features to prepare and deploy our applications. The CI/CD features are one of the most complete solutions we found.

## Documentation

### GitLab protected branches configuration

The protected branches configuration is available at <https://gitlab.com/beescreens/beescreens/-/settings/repository>.

Only the `main` branch is protected.

- Every member of the BeeScreens project are allowed to merge on the branch from merge requests
- No one can push and merge directly on the branch.
- No one is allowed to force push on the branch.
- All changes must be approved by at least one code owner of the BeeScreens project.

### GitLab merge requests configuration

The merge requests configuration is available at <https://gitlab.com/beescreens/beescreens/-/settings/merge_requests>.

These options differs from the defaults:

- **Merge method**: Fast-forward merge
- **Enable merged results pipelines**: Enabled
- **Enable "Delete source branch" option by default**: Enabled
- **Squash commits when merging**: Require
- **Merge request approvals: All eligible users**: At least one 1
- **Prevent approval by author**: Enabled

### GitLab CI/CD pipelines

Our GitLab CI/CD configuration defines the following stages:

- `debug` - A stage for debugging.
- `init` - A stage for initializing the pipeline.
- `review` - A stage for reviewing the code and environment variables.
- `build` - A stage for building the application(s) and package(s).
- `test` - A stage for testing the application(s) and package(s) (all kind of testing).
- `deliver` - A stage for delivering the application(s) and package(s) to the registries.
- `deploy` - A stage for deploying the application(s) and package(s) on the remote environments.

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [GitHub](https://github.com/)
- [GitLab Quick actions](https://docs.gitlab.com/ee/user/project/quick_actions.html)
- [GitLab Description templates (issue templates, merge request templates)](https://docs.gitlab.com/ee/user/project/description_templates.html)
- [GitLab Auto-cancel redundant pipelines](https://docs.gitlab.com/ee/ci/pipelines/settings.html#auto-cancel-redundant-pipelines)
