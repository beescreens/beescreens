# About Socket.IO

As the [Wikipedia's page of Socket.IO](https://en.wikipedia.org/wiki/Socket.IO) mentions:

!!! quote

	Socket.IO is an event-driven library for real-time web applications. It enables real-time, bi-directional communication between web clients and servers. It consists of two parts: a client-side library that runs in the browser, and a server-side library for Node.js.

## How and why do we use Socket.IO

_TODO_

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [SocketIO website](https://socket.io/)
