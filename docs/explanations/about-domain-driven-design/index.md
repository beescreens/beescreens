# About Domain-driven design

As the [Wikipedia's page of Domain-driven design](https://en.wikipedia.org/wiki/Domain-driven_design) mentions:

!!! quote

	Domain-driven design (DDD) is a major software design approach, focusing on modeling software to match a domain according to input from that domain's experts.

	Under domain-driven design, the structure and language of software code (class names, class methods, class variables) should match the business domain. For example, if software processes loan applications, it might have classes like loan application, customer, and methods such as accept offer and withdraw.

	Domain-driven design is predicated on the following goals:

	- placing the project's primary focus on the core domain and domain logic;
	- basing complex designs on a model of the domain;
	- initiating a creative collaboration between technical and domain experts to iteratively refine a conceptual model that addresses particular domain problems.

## How and why do we use Domain-driven design

_TODO_

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

_None at the moment._
