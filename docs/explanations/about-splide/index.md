# About Splide

As the website of [Splide](https://splidejs.com/) mentions:

!!! quote

	Splide is a lightweight, flexible and accessible slider/carousel written in TypeScript.

## How and why do we use Splide

_TODO_

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [revealJS](https://revealjs.com/)
- [Swiper](https://swiperjs.com/)
