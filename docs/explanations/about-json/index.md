# About JSON

As the website of [JSON](https://www.json.org) mentions:

!!! quote

	JSON (JavaScript Object Notation) is a lightweight data-interchange format. It is easy for humans to read and write. It is easy for machines to parse and generate.

## How and why do we use JSON

_TODO_

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

_None at the moment._
