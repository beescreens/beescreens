# About k6

As the website of [k6](https://k6.io/) mentions:

!!! quote

    k6 is a developer-centric, free and open-source load testing tool built for making performance testing a productive and enjoyable experience.

## How and why do we use k6

We use k6 to run load tests on our services, for now only the BeePlace backend. It's a great tool to test the performance of our services and to find bottlenecks.

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

_None at the moment._
