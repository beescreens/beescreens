# About WebSocket

As the [Wikipedia's page of WebSocket](https://en.wikipedia.org/wiki/WebSocket) mentions:

!!! quote

	WebSocket is a computer communications protocol, providing full-duplex communication channels over a single TCP connection.

## How and why do we use WebSocket

_TODO_

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

_None at the moment._
