# About Prisma

As the website of [Prisma](https://www.prisma.io/) mentions:

!!! quote

	Prisma unlocks a new level of developer experience when working with databases thanks to its intuitive data model, automated migrations, type-safety & auto-completion.

## How and why do we use Prisma

_TODO_

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [Sequelize](https://sequelize.org/)
