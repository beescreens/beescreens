# About Golang

As the website of [Golang](https://golang.org/) mentions:

!!! quote

	Go is an open source programming language that makes it easy to build simple, reliable, and efficient software.

## How and why do we use Golang

Golang is used for the agent service of the Dashboard application. It is used to create a CLI that allows the communication between the backend and the Raspberry Pi devices.
