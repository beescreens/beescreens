# About TypeScript

As the website of [TypeScript](https://sqlite.org/index.html) mentions:

!!! quote

	TypeScript is a strongly typed programming language that builds on JavaScript, giving you better tooling at any scale.

	TypeScript adds additional syntax to JavaScript to support a tighter integration with your editor. Catch errors early in your editor.

## How and why do we use TypeScript

_TODO_

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

_None at the moment._
