# Media Player

[website]: https://mp.beescreens.ch
[api]: https://mp.beescreens.ch/api
[deployment-configuration]: https://gitlab.com/beescreens/beescreens/-/tree/main/deployment/media-player
[code]: https://gitlab.com/beescreens/beescreens/-/tree/main/apps/media-player
[tutorial]: ../tutorials/create-a-media-player-application/introduction/index.md

- [:material-web: Website][website]
- [:material-swap-horizontal: API documentation][api]
- [:material-docker: Deployment configuration][deployment-configuration]
- [:material-factory: Code][code]
- [:material-school: Tutorial][tutorial]

## Introduction

[Media Player][website] is an application that allows you to play images and videos as a slideshow.

If you want to know how to create that application, you can follow the [tutorial][tutorial].

## How to run

- Follow instructions to start the application on [GitLab][code].
- Access the application at <http://localhost:9292>.

## Architecture and design

The Media Player application consists of a single application made with [NestJS](../explanations/about-nestjs/index.md). [NestJS](../explanations/about-nestjs/index.md) will render the pages requested by the user using the [Nunjucks](../explanations/about-nunjunks/index.md) templating engine.

An administration page will allow to manage the slideshows through an [API][api]. Each slide of the slideshow can have a time to display and can be an image or a video. The slideshows will be saved in a PostgreSQL database.

A slideshow page will allow to run the desired slideshow with [Splide](../explanations/about-splide/index.md).

Code and how to run on [GitLab][code].

## Technical details

The following section describe how the app works and how to use it. It assume your app is running on `http://localhost:9292` and that the API key is `media-player`.

The only way to manage the slideshows is through the API. The API is protected by an API key.

### How to manage the slideshows

Go to `http://localhost:9292/api`, click on the "authorize" button and set the value of the `apiKey` to `media-player`.

Follow the instructions on the page to create, read, update and delete slideshow

### How to manage the slideshows through the API

#### Create a slideshow

```sh title="In a terminal, execute the following command(s)."
curl -X 'POST' \
  'http://localhost:9292/api/slideshows' \
  -H 'accept: application/json' \
  -H 'apiKey: media-player' \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "First slideshow",
  "slides": [
    {
      "src": "https://source.unsplash.com/random?1",
      "type": "IMAGE",
      "interval": 1000,
      "alt": "Random photo on Unsplash"
    },
		{
			"src": "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4",
			"type": "VIDEO",
			"interval": 10000,
			"alt": "Sintel: first open movie"
		}
	]
}'
```

In the response, you will get the ID of the slideshow. it will be used to get, update or delete the slideshow.

#### Get the slideshows

```sh title="In a terminal, execute the following command(s)."
curl -X 'GET' \
  'http://localhost:9292/api/slideshows' \
  -H 'accept: application/json'
```

#### Get a specific slideshow

```sh title="In a terminal, execute the following command(s)."
curl -X 'GET' \
  'http://localhost:9292/api/slideshows/<id of the slideshow>' \
  -H 'accept: application/json'
```

#### Update a specific slideshow

```sh title="In a terminal, execute the following command(s)."
curl -X 'PATCH' \
  'http://localhost:9292/api/slideshows/<id of the slideshow>' \
  -H 'accept: application/json' \
  -H 'apiKey: media-player' \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "Modified slideshow",
  "slides": [
			{
				"src": "https://source.unsplash.com/random?2",
				"type": "IMAGE",
				"interval": 1000,
				"alt": "Random photo on Unsplash"
			},
			{
				"src": "https://source.unsplash.com/random?3",
				"type": "IMAGE",
				"interval": 1500,
				"alt": "Random photo on Unsplash"
			},
			{
				"src": "https://source.unsplash.com/random?4",
				"type": "IMAGE",
				"interval": 500,
				"alt": "Random photo on Unsplash"
			}
		]
	}'
```

#### Delete a specific slideshow

```sh title="In a terminal, execute the following command(s)."
curl -X 'DELETE' \
  'http://localhost:9292/api/slideshows/<id of the slideshow>' \
  -H 'accept: */*' \
  -H 'apiKey: media-player'
```
