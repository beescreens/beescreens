# Pimp My Wall

[website]: https://pmw.beescreens.ch
[api]: https://pmw.beescreens.ch/api
[deployment-configuration]: https://gitlab.com/beescreens/beescreens/-/tree/main/deployment/pimp-my-wall
[backend-code]: https://gitlab.com/beescreens/beescreens/-/tree/main/apps/pimp-my-wall/backend
[frontend-code]: https://gitlab.com/beescreens/beescreens/-/tree/main/apps/pimp-my-wall/frontend
[package-code]: https://gitlab.com/beescreens/beescreens/-/tree/main/packages/pimp-my-wall
[tutorial]: ../tutorials/create-an-interactive-drawing-application/introduction/index.md

- [:material-web: Website][website]
- [:material-swap-horizontal: API documentation][api]
- [:material-docker: Deployment configuration][deployment-configuration]
- [:material-factory: Backend code][backend-code]
- [:material-image-frame: Frontend code][frontend-code]
- [:material-package-variant-closed: Package code][package-code]
- [:material-school: Tutorial][tutorial]

## Introduction

[Pimp My Wall][website] is a BeeScreens application allowing players to draw on a canvas together. A [tutorial][tutorial] is available to learn how to create an interactive drawing application.

![Pimp My Wall - Screenshot 1](../assets/images/pimp-my-wall/pimp-my-wall-1.png){ loading=lazy }
![Pimp My Wall - Screenshot 2](../assets/images/pimp-my-wall/pimp-my-wall-2.png){ loading=lazy }

## How to run

- Follow instructions to start the backend in the [Backend code][backend-code].
- Follow instructions to start the frontend in the [Frontend code][frontend-code].
- Access the frontend at <http://localhost:3000>.

## Architecture and design

The Pimp My Wall application is built around three main components:

- [Backend](#backend) - The backend service used by Pimp My Wall.
- [Frontend](#frontend) - The frontend service used by Pimp My Wall.
- [Package](#package) - The package shared by Pimp My Wall between the frontend and the backend side.

```plantuml
@startuml pimp-my-wall-architecture
left to right direction

title Pimp My Wall - Architecture

interface "Backend" as backend
interface "Frontend" as frontend
interface "Package" as package

backend <--> package
package <--> frontend
frontend -> backend
@enduml
```

### Backend

The backend is a [NestJS](../explanations/about-nestjs/index.md) application. It is a [WebSocket](../explanations/about-websocket/index.md) server that allows players to join a game and draw together on a canvas.

Code and how to run in the [Backend code][backend-code].

### Frontend

The frontend is a [Next.js](../explanations/about-nextjs/index.md) application. It is a [WebSocket](../explanations/about-websocket/index.md) client that allows players to join a game and draw together on a canvas.

Code and how to run in the [Frontend code][frontend-code].

### Package

The package is a [TypeScript](../explanations/about-typescript/index.md) package shared by the frontend and the backend. It contains all the types and enums used by the frontend and the backend.

Code and how to run in the [Package code][package-code].

## Technical details

The following section describe how the app works.

### The player joins a game

#### Schema

```plantuml
@startuml
!pragma teoz true
skinparam maxMessageSize 200
skinparam ParticipantPadding 20

caption Join a game

box "Player side" #LightBlue
participant "Browser" as browser
end box

box "Backend side" #LightCoral
participant "Backend" as backend
end box

autonumber

browser <--> backend: A WebSocket connection is established

backend --> browser: The backend emits the game state to the player

browser -> browser: The browser saves the game state and renders the draw

@enduml
```

#### Implementation details

1. When a WebSocket connection is established, the socket automatically joins a default room. All events can be emitted to this/from this default room. The connection is bi-directional.
2. The backend emits a `GAME` event with a `GameData` containing all the game structure in its current state.
3. The browser listen on a `GAME` event with a `GameData` object type and saves it. The browser renders the draw from the `GameData` object type.

### Send stroke data

#### Schema

```plantuml
@startuml
!pragma teoz true
skinparam maxMessageSize 200
skinparam ParticipantPadding 20

caption Send stroke data

box "Player 1 side" #LightBlue
participant "Browser 1" as browser1
end box
box "Backend side" #LightCoral
participant "Backend" as backend
end box
box "Player 2 side" #LightGreen
participant "Browser 2" as browser2
end box

autonumber

browser1 --> backend: Stroke data is emitted to the backend

backend -> backend: The backend merges the stroke data

backend --> browser2: Emit the stroke data to all other players

browser2 -> browser2: Save the stroke data and renders the draw
@enduml
```

#### Implementation details

1. We must distinguish two cases when emitting a stroke data: a new stroke is started or we continue to add points to the previous stroke:
   - A new stroke is started: The `POINT` event is emitted with a `Point` object type with all attributes.
   - A previous stroke is continued: The `POINT` event is emitted with a `Point` object type only with the { `x`, `y` } coordinates. `size` and `color` attributes are kept from previous points.
2. Considering the two cases presented above:
   - A new stroke is started: The backend listens to a `POINT` event with a `Point` object type and updates the `GameData` object type by creating a new stroke and adding the point for the player.
   - A previous stroke is continued: The backend listens to a `POINT` event with a `Point` object type and updates the `GameData` object type by adding the point to the last stroke for the player.
3. A `PLAYER_POINT` event is emitted with a `PlayerPoint` object type to all other players in the room.
4. The browser listens to a `PLAYER_POINT` event with a `PlayerPoint` object type and updates the `GameData` object type according to the two cases presented above. The browser renders the draw from the `GameData` object type.

### Send background data

#### Schema

```plantuml
@startuml
!pragma teoz true
skinparam maxMessageSize 200
skinparam ParticipantPadding 20

caption Send background data

box "Player 1 side" #LightBlue
participant "Browser 1" as browser1
end box
box "Backend side" #LightCoral
participant "Backend" as backend
end box
box "Player 2 side" #LightGreen
participant "Browser 2" as browser2
end box

autonumber

browser1 --> backend: Background data is emitted to the backend

backend -> backend: The backend updates the background data

backend --> browser2: Emit the background data to all other players

browser2 -> browser2: Update the background data and renders the draw
@enduml
```

#### Implementation details

1. The `BACKGROUND` event is emitted with a `Background` object type.
2. The backend listens to a `BACKGROUND` event and updates the `GameData` object type.
3. A `PLAYER_BACKGROUND` event is emitted with a `PlayerBackground ` object type to all other players in the room.
4. The browser listens to a `PLAYER_BACKGROUND` event with a `PlayerBackground` object type and updates the `GameData` object type. The browser renders the draw from the `GameData` object type.

### Send end stroke event

#### Schema

```plantuml
@startuml
!pragma teoz true
skinparam maxMessageSize 200
skinparam ParticipantPadding 20

caption Send end stroke event

box "Player 1 side" #LightBlue
participant "Browser 1" as browser1
end box
box "Backend side" #LightCoral
participant "Backend" as backend
end box
box "Player 2 side" #LightGreen
participant "Browser 2" as browser2
end box

autonumber

browser1 --> backend: End stroke event is emitted to the backend

backend -> backend: The backend ends the stroke and optimises it

backend --> browser2: Emit the end stroke event to all other players

browser2 -> browser2: Ends the stroke and optimises it
@enduml
```

#### Implementation details

1. The `END_STROKE` event is emitted.
2. The backend listens to a `END_STROKE` event and optimises the last stroke of the player in the `Game` object type.
3. A `END_STROKE_FROM_PLAYER` event is emitted with a `Player ` object type to all other players in the room.
4. The browser listens to a `END_STROKE_FROM_PLAYER` event with a `Player` object type and optimises the last stroke of the player in the `GameData` object type. The browser renders the draw from the `GameData` object type.

### Reset event

#### Schema

```plantuml
@startuml
!pragma teoz true
skinparam maxMessageSize 200
skinparam ParticipantPadding 20

caption Reset event

box "Backend side" #LightCoral
participant "Backend" as backend
end box
box "Player 1 side" #LightBlue
participant "Browser 1" as browser1
end box

autonumber

backend --> browser1: Reset event is emitted with the new game state

browser1 -> browser1: The browser saves the game state and renders the draw
@enduml
```

### Undo event

#### Schema

```plantuml
@startuml
!pragma teoz true
skinparam maxMessageSize 200
skinparam ParticipantPadding 20

caption Send end stroke event

box "Player 1 side" #LightBlue
participant "Browser 1" as browser1
end box
box "Backend side" #LightCoral
participant "Backend" as backend
end box
box "Player 2 side" #LightGreen
participant "Browser 2" as browser2
end box

autonumber

browser1 --> backend: Undo event is emitted to the backend

backend -> backend: The backend removes the last stroke

backend --> browser2: Emit the undo event to all other players

browser2 -> browser2: Removes the last stroke
@enduml
```

#### Implementation details

1. The `UNDO` event is emitted.
2. The browser listens on a `UNDO` event.

### Configure the frontend using query parameters

The frontend can be configured using the query parameters. At the moment, two pages exists and have their own query parameters to customize the frontend.

#### Common query parameters

- `x` - Set the x coordinate to display the sketch
- `y` - Set the y coordinate to display the sketch
- `scaleX` - Set the scale of the x coordinate to display the sketch
- `scaleY` - Set the scale of the y coordinate to display the sketch

#### Query parameters specific to the `play/:gameId` page

- `move` - Set the sketch in moving mode
- `draw` - Set the sketch in drawing mode
- `strokeColor` - Set the color of the stroke
- `strokeSize` - Set the size of the stroke

#### Query parameters specific to the `display/:gameId` page

- `screensaver` - Hide the toolbar and enable the screensaver

Here is an example of an URL with query parameters to start the frontend of Pimp My Wall with a specific configuration:

<https://pmw.beescreens.ch/display/1?x=0&y=0&scaleX=2&scaleY=2>
