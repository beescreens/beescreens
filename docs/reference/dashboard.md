# Dashboard

[website]: https://dashboard.beescreens.ch
[api]: https://dashboard.beescreens.ch/api
[deployment-configuration]: https://gitlab.com/beescreens/beescreens/-/tree/main/deployment/dashboard
[backend-code]: https://gitlab.com/beescreens/beescreens/-/tree/main/apps/dashboard/backend
[frontend-code]: https://gitlab.com/beescreens/beescreens/-/tree/main/apps/dashboard/frontend
[agent-code]: https://gitlab.com/beescreens/beescreens/-/tree/main/apps/dashboard/beegent
[agent-playbook]: https://gitlab.com/beescreens/beescreens/-/tree/main/apps/dashboard/backend/ansible

- [:material-web: Website][website]
- [:material-swap-horizontal: API documentation][api]
- [:material-docker: Deployment configuration][deployment-configuration]
- [:material-factory: Backend code][backend-code]
- [:material-image-frame: Frontend code][frontend-code]
- [:material-robot: Agent code][agent-code]

## Introduction

[Dashboard][website] is a remote content management application for Raspberry Pi that simplifies administration. This system makes it possible to change the configuration of Raspberry Pi devices and perform actions on them remotely via a web interface.

![Dashboard](../assets/images/dashboard/device_page.png){ loading=lazy }

## How to run

- Follow instructions to start the backend at [backend code][backend-code].
- Follow instructions to start the frontend at [frontend code][frontend-code].
- Follow instructions to start the agent at [agent code][agent-code].
- Access the frontend at <http://localhost:3000>.

## Architecture and design

The Dashboard application is built around three main components:

- [Backend](#backend) - The backend service used by Dashboard.
- [Frontend](#frontend) - The frontend service used by Dashboard.
- [Agent](#agent) - The agent service used by Dashboard.

```plantuml
@startuml

left to right direction

skinparam componentStyle rectangle

component "Raspberry Pi" as rpi {
  component "Agent" as agent
}

component "Dashboard" as dashboard {
  component "API (Backend)" as api
  component "Web (Frontend)" as web
}

component "Users" {
  actor "User" as user
}

agent <--> api
api <--> web
web <--> user

@enduml
```

### Backend

The backend is a [NestJS](../explanations/about-nestjs/index.md) application. It is an API that allows users to manage their Raspberry Pi devices. This API allows users to create devices (Raspberry Pi or other), create configurations (a set of actions to perform on a device) and apply configurations to devices.

Code and how to run at <https://gitlab.com/beescreens/beescreens/-/tree/main/apps/dashboard/backend>.

### Frontend

The frontend is a [Next.js](../explanations/about-nextjs/index.md) application. It is a web application that allows users to manage their Raspberry Pi devices via the backend API with a user-friendly interface.

Code and how to run at <https://gitlab.com/beescreens/beescreens/-/tree/main/apps/dashboard/frontend>.

### Agent

The agent is a [Golang](../explanations/about-golang/index.md) and [Cobra](../explanations/about-cobra/index.md) application. It is a CLI that allows the communication between the backend and the Raspberry Pi devices. The agent is installed on the Raspberry Pi devices and allows the execution of actions on the devices.

Code and how to run at <https://gitlab.com/beescreens/beescreens/-/tree/main/apps/dashboard/beegent>.

## Technical details

### Network considerations

To enable the agent installed on the Raspberry Pi to communicate with the Dashboard API, the architecture used is a Pull architecture. With this type of architecture, it's the Raspberry Pi that periodically contacts the API to ask for the latest configurations to be applied.

```plantuml
@startuml
left to right direction

skinparam BackgroundColor transparent
skinparam componentStyle rectangle
component  "Private Network" {
  rectangle "RPi 1" {
    [Agent] as Agent1
  }
  rectangle "RPi 2" {
    [Agent] as Agent2
  }
  rectangle "RPi 3" {
    [Agent] as Agent3
  }
}

skinparam BackgroundColor transparent
skinparam componentStyle rectangle
component "Internal Network (DMZ)" {
  rectangle "Web Server BeeScreens" {
    [Dashboard (API)] as Dashboard
  }
}

[Agent1] --> [Dashboard] : PULL
[Agent2] --> [Dashboard] : PULL
[Agent3] --> [Dashboard] : PULL

@enduml
```

### Configuration management

[Ansible](../explanations/about-ansible/index.md) is used to create configurations (aka playbooks) and apply them to Raspberry Pi devices. By default, Ansible uses a push architecture that is not suitable for the Dashboard application. To solve this problem, we use a pull architecture with the [Ansible Pull](../explanations/about-ansible/index.md) method.

```plantuml
@startuml
database git as "Git Server \n(Internet)" order 0
participant rpi1 as "Raspberry Pi 1 \n (with Ansible)" order 1
participant rpi2 as "Raspberry Pi 2 \n (with Ansible)" order 2
participant rpi3 as "Raspberry Pi 3 \n (with Ansible)" order 3

rpi1 -> git : ansible-pull
activate rpi1
rpi1 -> rpi1 : apply playbook
rpi1 -[hidden]-> rpi1
deactivate rpi1

rpi2 -> git : ansible-pull
activate rpi2
rpi2 -> rpi2 : apply playbook
rpi2 -[hidden]-> rpi2
deactivate rpi2

rpi3 -> git : ansible-pull
activate rpi3
rpi3 -> rpi3 : apply playbook
rpi3 -[hidden]-> rpi3
deactivate rpi3
@enduml
```

The existing Ansible playbooks (configurations) are stored in the [backend/ansible](https://gitlab.com/beescreens/beescreens/-/tree/main/apps/dashboard/backend/ansible) directory of the Dashboard application. New configurations must be added to this directory and then pushed to the Git server.

### Global architecture

The following diagram shows the global architecture of the Dashboard application and how the different components interact with each other.

1. The user creates a device via the frontend.
2. The user creates a configuration via the frontend.
3. The user associates a device with a configuration via the frontend.
4. The agent installed on the device contacts the API to retrieve the configurations to be applied.
5. The agent applies the configuration to the device via Ansible Pull.
6. The agent updates the status of the configuration via the API.

```plantuml
@startuml
hide footbox

participant frontend as "Frontend" order 1
participant agent as "Agent" order 3
participant backend as "Backend" order 2

== Configuration ==

frontend -> backend: **1** \t**POST** /devices \n\t Create a device
backend --> frontend: \t\t201 Created + body

frontend -[hidden]-> frontend

frontend -> backend: **2** \t**POST** /configurations \n\t Create a configuration
backend --> frontend: \t\t201 Created + body

frontend -[hidden]-> frontend

frontend -> backend: **3** **POST** /devices-configurations \n with DeviceId and ConfigId
activate backend
backend -> backend: Create new device-configuration \n with status New
backend --> frontend: \t\t201 Created + body
deactivate backend

== Production ==

loop each 15 minutes
  agent -> backend: **4** \t**GET** /devices-configurations
  activate agent
  backend --> agent: \t\t200 OK + body \n Only get configurations with status New
  loop for each configuration
    agent -> agent : **5** Apply configuration \n with ansible-pull
    agent -> backend: **6** **PATCH** /devices-configurations/{id} \n\t with Success or Error status
    backend --> agent: \t\t200 OK + body
    deactivate agent
  end
end

@enduml
```

### Database schema

The following diagram shows the database schema used by the Dashboard application.

```plantuml
@startuml
entity Configuration {
  * <<PK>> id : uuid
  --
  * name : string
  * description : string | null
  * repository_url : url
  * repository_path : string
  * repository_branch : string | null
  * created_at : date
  * updated_at : date
}

entity Device {
  * <<PK>> id : uuid
  --
  * api_key : string
  * name : string
  * description : string | null
  * last_seen_at : date
  * created_at : date
  * updated_at : date
}

entity DevicesConfigurations {
  * <<PK>> configuration_id : uuid
  * <<PK>> device_id : uuid
  --
  state : DeviceConfigurationState
  log : string | null
  created_at : date
  updated_at : date
}

enum DeviceConfigurationState {
  NEW
  PENDING
  SUCCESS
  ERROR
}

Device "0..*" - "0..*" Configuration : owns >
(Device, Configuration) .. DevicesConfigurations

legend right
    | Color   | Type    |
    |<#add1b2>| Entity  |
    |<#eb937f>| Enum    |
endlegend

@enduml
```

### Details about the agent

This section gives more details about the agent and how it works.

#### How it works

The following diagram shows how the agent works and how it interacts with the Dashboard application.

```plantuml
@startuml

start
:Get all device-configuration \n\t(dashboard service);
if (Number of configurations == 0 ?) then (yes)
  stop
else (no)
  if (Dry run enabled ?) then (yes)
    :Inform user of fetch;
    stop
  else (no)
    while (All device-configuration applied ?) is (no)
      :Patch device-configuration with PENDING \n\t\t(dashboard service);
      :Apply device-configuration with Ansible \n\t\t(ansible service);
      if (Apply successful) then (yes)
        :Patch device-configuration \nwith SUCCESS and logs \n(dashboard service);
      else (no)
        :Patch device-configuration \nwith ERROR and logs \n(dashboard service);
      endif
    endwhile (yes)
  endif
  stop
endif

@enduml
```

#### How to build and publish the agent

As mentioned above, the agent is a CLI that allows the communication between the backend and the Raspberry Pi devices. The agent is written in [Golang](../explanations/about-golang/index.md) and uses the [Cobra](../explanations/about-cobra/index.md) library to manage the CLI. The GitLab CI / CD pipeline is used to build the agent for the different architectures (thanks to Go cross compilation) and to publish the binaries in the GitLab package registry.

The agent is then installed on the Raspberry Pi devices manually.

#### How to run the agent

To synchronize the Raspberry Pi devices with the Dashboard application, the agent must be run periodically. To do this, we use the [systemd](../explanations/about-systemd/index.md) service manager.

The following systemd service must be created on the Raspberry Pi devices and is responsible for running the agent periodically. The service is configured to run every 30 seconds.

```ini
[Unit]
Description=Run beegent to sync with Dashboard

[Service]
User=<user>
Group=<group>
WorkingDirectory=~
ExecStart=/bin/bash -c "beegent-linux-arm64 run \
    --api-key $$(cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2)"
Restart=always
RestartSec=30

[Install]
WantedBy=multi-user.target
```

#### Install and configure the Raspberry Pi

1. Download the [beegent binary](https://gitlab.com/beescreens/beescreens/-/packages) and `config.yaml` from GitLab Package Registry
2. Move the beegent binary to `/usr/local/bin` and the config file to `~/.beegent/config.yaml`
3. Install Ansible pull with `sudo apt install --yes ansible` (to install Ansible inside `/usr/bin`)
4. Create a new systemd service (file) under `/etc/systemd/system/beegent.service`
5. The content of the service should be the following: (See [How to run the agent](#how-to-run-the-agent) for more details)
6. Replace `<user>` and `<group>` with the user and group of the device
7. Run `systemctl daemon-reload` to reload the systemd daemon
8. Enable the service with `sudo systemctl enable beegent.service`
9. Start the service with `sudo systemctl start beegent.service`
