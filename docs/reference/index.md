# Reference

## Maintainers

These people are maintainers of the BeeScreens project (in alphabetical order).

- Ludovic Delafontaine [@ludelafo](https://gitlab.com/ludelafo)
- Vincent Guidoux [@Nortalle](https://gitlab.com/Nortalle)
- Valentin Kaelin [@vkaelin](https://gitlab.com/vkaelin)
- Hadrien Louis [@hadrylouis](https://gitlab.com/hadrylouis)

## Contributors

These people are contributors to the BeeScreens project (in alphabetical order). Thank you very much!

- Melissa Gehring [@lollipoke](https://gitlab.com/Lollipoke)
