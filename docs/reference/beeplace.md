# BeePlace

[website]: https://beeplace.beescreens.ch
[api]: https://place.beescreens.ch/api
[deployment-configuration]: https://gitlab.com/beescreens/beescreens/-/tree/main/deployment/beeplace
[backend-code]: https://gitlab.com/beescreens/beescreens/-/tree/main/apps/beeplace/backend
[frontend-code]: https://gitlab.com/beescreens/beescreens/-/tree/main/apps/beeplace/frontend
[common-code]: https://gitlab.com/beescreens/beescreens/-/tree/main/apps/beeplace/common
[load-tests-code]: https://gitlab.com/beescreens/beescreens/-/tree/main/apps/beeplace/tests/load-tests

- [:material-web: Website][website]
- [:material-swap-horizontal: API][api]
- [:material-docker: Deployment configuration][deployment-configuration]
- [:material-factory: Backend code][backend-code]
- [:material-image-frame: Frontend code][frontend-code]
- [:material-package-variant-closed: Common code][common-code]
- [:material-test-tube: Load tests code][load-tests-code]

## Introduction

[BeePlace][website] is a BeeScreens application in which users can create collaborative works of art. Each user can draw one pixel at a time on the shared canvas, and must wait a certain amount of time before drawing again. This forces collaboration and limits overflow.

<figure markdown>
  ![beeplace-interface](../assets/images/beeplace/screenshot-beeplace-app.png){ loading=lazy, width=400 }
  <figcaption>BeePlace mobile interface</figcaption>
</figure>

<figure markdown>
  ![display-mode-config](../assets/images/beeplace/display-mode-config.png){ loading=lazy, width=800 }
  <figcaption>Configuration of the display mode to split the presentation</figcaption>
</figure>

## How to run

- Follow instructions to start the backend at [backend code][backend-code].
- Follow instructions to start the frontend at [frontend code][frontend-code].
- Access the frontend at <http://localhost:3000>.

## Architecture and design

The BeePlace application is built around three main components:

- [Backend](#backend) - The backend service used by BeePlace.
- [Frontend](#frontend) - The frontend service used by BeePlace.
- [Common/Package](#common) - The common code shared between the frontend and the backend side of BeePlace.

<figure markdown>
  ![technos](../assets/images/beeplace/technos.png){ loading=lazy, width=800 }
  <figcaption>Architecture schema</figcaption>
</figure>

### Backend

The backend is a [NestJS](../explanations/about-nestjs/index.md) application. It is a [WebSocket](../explanations/about-websocket/index.md) server that allows users to join the board and put pixels on it.

Code and how to run at [backend-code].

### Frontend

The frontend is a [Next.js](../explanations/about-nextjs/index.md) application. It is a [WebSocket](../explanations/about-websocket/index.md) client that allows players to users to display the shared canvas and put pixels on it.

Code and how to run at [frontend-code].

### Common

The common code is a [TypeScript](../explanations/about-typescript/index.md) project shared by the frontend and the backend. It contains all the types and enums used by the frontend and the backend.

Code and how to run at [common-code].

## Technical details

The following section describe how the app works.

### The user joins the application

#### Schema

```plantuml
@startuml
!pragma teoz true
participant Frontend
participant Backend

autonumber

Frontend --> Backend: Initialize the WebSockets connection

|||

Frontend --> Backend: Emit ""JOIN"" event

note left
  Generate a fingerprint
end note

note right
  Backend stores the fingerprint
  in the socket instance
end note

|||

Backend --> Frontend: Respond to ""JOIN"" event, send initial configuration

note right
  Initial configuration contains:
  - Current state of the board
  - Board configuration (colors, size, ...)
  - Cooldown of the player
end note

note left Frontend
  Display board
end note

```

#### Implementation details

1. The frontend create the WebSockets connection with the backend. The connection is bi-directional.
2. The frontend generates a fingerprint, a unique identifier for the user and send it to the backend with the `JOIN` event. The backend stores it in the socket instance.
3. The backend responds to the `JOIN` event with the initial configuration of the board. The frontend now displays the board in the HTML canvas.

### Draw a pixel

#### Schema

```plantuml
@startuml
!pragma teoz true

box "Users" #LightBlue
skinparam ParticipantPadding 20
participant User1
participant User2
end box

autonumber

note left User1
  Pixel is drawn for User1
end note

User1 --> Backend: Emit ""PIXEL_FROM_PLAYER"" event

note right Backend
  Backend stores the pixel in Redis
  and the database
end note

...//max 100ms later//...

Backend --> User1 & User2: Emit ""UPDATE_BOARD"" event

note over User1, User2
  Pixel is drawn for User1 and User2
end note

|||
```

#### Implementation details

1. When the user draws a pixel, the frontend emits a `PIXEL_FROM_PLAYER` event with a `Pixel` object type to the backend. The pixel object contains the { `x`, `y` } coordinates and the `color` of the pixel. The backend stores the pixel in Redis and the database as explained in the [database section](#database).
2. Every `BOARD_REFRESH_RATE` millisecond (base on environment variable), the backend emits an `UPDATE_BOARD` event to all the connected users. The payload contains all the drawn pixels during the interval.

### Database

What is stored in the database ? The **coordinates** and the **color** of all the drawn pixels.

A **3 level storage** is used to optimize the application:

1. In the memory of the backend
2. In a Bitfield Redis
3. In the PostgreSQL database

#### In the memory of the backend

Initially, the pixels where directly stored and retrieved from the Redis Bitfield. However, it was a performance bottleneck. The conversions from the Bitfield (a string) to a number array was too slow.

The solution was to store the pixels in the memory of the backend. The pixels are stored in a number array in addition to the Redis Bitfield. The Bitfield is necessary on the first application load, when the backend has restarted.

#### In a Bitfield Redis

The Bitfield is a Redis data structure that allows to store bits in a array of bytes. It is used to store the pixels in a very compact way. Each read or write operation is done in a single Redis command in O(1) time complexity.

Only the color id of the pixel is stored in the Bitfield. To know the coordinates of the pixel, an offset is used. The offset is calculated with the following formula: `offset = x + BOARD_SIZE * y`. The `BOARD_SIZE` is a constant defined in the backend code.

#### In the PostgreSQL database

The SQL database is not yet used very intensively. It is used to create a history of all pixels placed. This history will be used for future statistics, for example on a future administration dashboard.

The schema of the database's single table **pixels** is as follows:

```json
{
	"id": "uuid",
	"x": "integer",
	"y": "integer",
	"color": "integer",
	"createdAt": "timestamp",
	"userId": "text" // The fingerprint of the user
}
```

### Administration

For now, the administration is done with protected routes. The user must use the correct api key to access the administration routes (the one defined with the `API_KEY` environment variable):

```bash
curl -X POST http://localhost:4000/api/v1 --header "apiKey: API_KEY_VALUE_HERE"
```

Here are the available actions:

- `POST /board/read-only` - Set the board in read-only mode, no more pixels can be drawn.
- `POST /board/reset` - Reset the board, all the pixels are removed for all the users.
- `POST /board/reset-area` - Reset the board in a specific area, all the pixels are removed for all the users.
- `POST /board/restore-from-database-backup` - Restore the board state at a specific date from the SQL database.

The payload of the `POST` requests are described in the [API documentation][api].

### Configure the display mode

The presentation of the different displays can be configured here: <https://place.beescreens.ch/displays>

The chosen configuration will generate links with query parameters for each display. These links displayed on the right can be clicked to display the right portion of the canvas on the right display.

Theses options can be changed:

- `Display width` - The size in pixels of all the displays width
- `Display height` - The size in pixels of all the displays height
- `Number of columns` - The number of columns of displays
- `Number of rows` - The number of rows of displays
