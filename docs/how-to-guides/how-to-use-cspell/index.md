# How to use cspell

## Installation

Install [cspell](../../explanations/about-cspell/index.md) with the following commands.

```sh title="In a terminal, execute the following command(s)."
npm install --save-dev \
	cspell \
	@cspell/dict-bash \
	@cspell/dict-fr-fr \
	@cspell/dict-node \
	@cspell/dict-npm \
	@cspell/dict-typescript
```

## Configuration

The configuration for Husky is located in the `.cspell.yaml` file. The custom dictionaries are in the `.cspell` directory.

## Run cspell

You can run cspell from anywhere in the project with the following command.

```sh title="In a terminal, execute the following command(s)."
pnpm --workspace-root review:spellcheck
```

## Common tasks

### Add a word to dictionary

You can add any words to the available dictionaries in the `.cspell` directory.

Please be aware to add the word in the right dictionary (in alphabetical order) to keep things tidy. If you new word doesn't fit in one of the existing dictionary, [add a new dictionary](#add-a-new-dictionary).

### Add a new dictionary

Create a new dictionary file in the `.cspell` directory.

Add the definition of the dictionary in the `.cspell.yaml` file under the key `dictionaryDefinitions`.

```yaml title=".cspell.yaml"
dictionaryDefinitions:
  - name: "example"
    path: "./.cspell/example.txt"
    addWords: true
```

Add the dictionary in the `.cspell.yaml` file under the key `dictionaries`.

```yaml title=".cspell.yaml"
dictionaries:
  - "example"
```

## Related explanations

These explanations are related to the current item (in alphabetical order).

_None at the moment._

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

_None at the moment._
