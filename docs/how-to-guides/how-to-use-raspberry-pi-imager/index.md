# How to use Raspberry Pi Imager

## Installation

Install [Raspberry Pi Imager](../../explanations/about-raspberry-pi-imager/index.md) on the official website. Download and install the binary for your platform.

## Common tasks

### Write an image to a microSD card

Launch Raspberry Pi Imager and select the image you want to write to the microSD card.

### Backup a microSD card to an image

Raspberry Pi Imager cannot backup a USB drive to an image. You can use [USBImager](https://gitlab.com/bztsrc/usbimager) or [balenaEtcher](https://www.balena.io/etcher/) to backup the image.

## Related explanations

These explanations are related to the current item (in alphabetical order).

_None at the moment._

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

_None at the moment._
