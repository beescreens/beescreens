# How to contribute to BeeScreens

You want to contribute to this project? That is great, thanks! Here is some help to get you started to contribute to the project.

## Issues

[Issues](https://gitlab.com/beescreens/beescreens/-/issues) are the primary way we use for communication. For us, the term "Issues" is misleading but Issues can be about anything: new features, bugs as well as discussions, questions, tools, human workflow improvements and anything related to the project.

## Issue Boards

The [Issue Boards](https://gitlab.com/beescreens/beescreens/-/boards) allows to have a clear view on the issues and which ones require a specific attention. At the moment, a view of the issues is available for each milestone and one board per user.

## Milestones

[Milestones](https://gitlab.com/groups/beescreens/-/milestones) are used to define the key stages in the realization of the project. Multiple issues can referee the same milestone. This allows to have an overview of the elements to finish before the milestone can be considered closed. Milestones are also used like sprint of one week.

## Merge requests

[Merge requests](https://gitlab.com/beescreens/beescreens/-/merge_requests) are branches on which work has been started from the [Issues](#issues). Each issue can have one or more merge requests associated with it. The merge requests are used to merge the work done on the branch with the target branch.

## Workflow

This section describes the workflow we use to manage the project. The week is organized as follow.

| Monday      | Tuesday     | Wednesday   | Thursday          | Friday      |
|-------------|-------------|-------------|-------------------|-------------|
| Daily scrum | Daily scrum | Daily scrum | Milestone session | Daily scrum |

### Daily scrum

A Daily scrum occurs every day at 9:00. The meeting is limited to 10 minutes and is managed by one of the maintainers.

At this meeting, the maintainers give the floor to the participants one after the other for a quick update on the progress of the tasks.

Every participant has to talk about:

- Their general feelings out of 5
- Report on work done
- Confidence/feeling about the task
- Potentially ask for help
- Reassess the purpose of the task in hand
- What is going to be done during the day

### Milestone session

A Milestone session occurs every week at 9:00. The meeting is limited to 60 minutes and is managed by one of the maintainers.

At this meeting, the team reviews all merge requests that were merged during the current milestone. The team will discuss the merge requests. The team will ensure the new issues are well defined and ready to be assigned to a milestone. If a merge request is not reviewed before the milestone session, the issue is reported to the next milestone. The team will ensure that the next milestone is well defined for all team members.

The maintainers will ensure the following points are discussed:

- The issues opened during the current milestone are well defined (the issue has a title, a summary and a user story).
- A final weight is assigned to each issue. If the issue hasn't any weight prior of the meeting, the team will discuss the weight of the issue and set the final weight.
- Select the issues to be assigned to the next milestone. If the issue is not ready to be assigned to the milestone, it should be refined and assigned in a future milestone session.
- Each issue in a milestone is assigned to a team member. If the issue is not assigned to a team member, it will be assigned in a future milestone session.
- Each member has a reasonable amount of work to do for the next milestone. If not, the team will discuss the workload of each member and reassign the issues to the next milestone if needed.

### Milestones creation

A [milestone](#milestones) is created for each week and each project.

### Issues creation

The default template will inform you what **type of issue** to create and **template** to use depending on the type of contribution you want to make. Select the one that fits your needs.

The **title** of an issue should begin with a verb, like that:

- `Add a [new feature]`
- `Improve the workflow`
- `Clean the CI/CD configuration files`
- `Cannot [do something]` (mostly used for [Incident templates](#incident-template) issues)
- `Fix the [bug]` (mostly used with [Issue templates](#issue-template) related to an [Incident template](#incident-template))

Do not hesitate to open as many issues as you need. The more issues you open, the less remains in your head and the more you can focus on the tasks at hand. The opened issues will be discussed during the [Milestone session](#milestone-session) and will be assigned to a milestone.

#### Issue template

The **summary** can be used to describe the issue in a few words. It should be short and concise.

The **user story** of an issue should be written in the following format:

> **As a** persona, (_Who are we building this for?_)  
> **I want to** task, (_Here we're describing their intent - not the features they use. What is it they’re actually trying to achieve?_)  
> **So that** goal. (_How does their immediate desire to do something this fit into their bigger picture?_)

The **weight** is used to evaluate its feasibility in a milestone.

It is a numerical value assigned to an issue. This value represents an estimate based on a reflection on the four criteria below:

- Effort / Volume: How much work does this task requires?
- Risk: How much of the integrity of the application is at risk?
- Complexity: How complex are the designs and algorithms we are be dealing with?
- Uncertainty: How much technical knowledge are we missing to complete the task?

For each criterion, we will estimate from 0 to 5 (including bounds). Then add up each value of the criteria. And finally, this result will be rounded up to the next Fibonacci number (0, 1, 2, 3, 5, 8, 13 or 21).

Team members can take some time to review and estimate the weight of an issue before the [Milestone session](#milestone-session). This can be done by the person who created the issue or by any other team member. Once you have determined the weight of the task, add it to the table with the weights of the team. Try not to have a look at existing weights before you have defined your own. This will ensure you are not influenced by other people's opinion.

The final weight will be defined at the [Milestone session](#milestone-session) with all team members. The team will discuss a final weight for each issue. The final weight will be the maximum of all the weights defined by the team members.

#### Incident template

The **summary** can be used to describe the incident in a few words. It should be short and concise.

The **current behavior** should describe the current behavior of the application or the system with its problem.

The **expected behavior** should describe the expected behavior of the application or the system when it is working correctly.

The **steps to reproduce** should describe the steps to reproduce the issue.

The **attempts to solve the incident** should describe the attempts you tried to solve the incident if relevant.

The **more information** can be used to provide more information if needed.

The more details you provide, the easier it will be for the team to understand the issue and the faster it will be resolved.

### Merge requests creation

Once an issue is assigned to a milestone, the assigned team member can open a merge request by pressing the button "_Create merge request_". This will create a new branch associated to the issue. You can change the target branch if needed with the dropdown menu.

When the merge request is merged, this will automatically squash all commits, close the issue related to it, delete the branch that was created and add your changes to the target branch.

### Summary

??? tip "Too long; didn't read"

	1. Begin work on an issue.
	2. Mark the merge request as ready.
	3. Review another merge request.
	4. Add a weight to an issue.
	5. Refine an issue with no summary and user story.
	6. Repeat.

1. Check if any issues or merge requests regarding your contribution have not yet been filled.
2. Create a new issue describing the element you would like to add to the project.
3. Refine your issue as much as needed. The more details it contains, the easier it will be for the team to understand what you would like to achieve. Add this issue to the "⚖️ Add me a weight" milestone.
4. Set a temporary weight to your issue.
5. Work on your current issues for the remaining of the milestone.
6. During the [Milestone session](#milestone-session), the team will discuss the weights of new issues and assign them to a milestone if they are ready to be worked on.
7. When the issue is ready and assigned to a milestone, create a new merge request when you start to work on it.
8. Pull and switch to that new branch. Implement the new contribution and push anytime you want. The local and remote CI/CD pipelines will ensure your changes can be merged in the target branch.
9. When your new feature is ready to be merged with the target branch, go back to the merge request and press "_Mark as ready_".
10. Add reviewer(s) to your merge request so they can review your work (the menu is on the right side of the screen, hidden by default).
11. Check if other merge requests from the rest of the team are ready to be reviewed. You can review them and add your comments if needed. This will allow everyone to have an overview of the work done and can add their own experience to others' work. The team member can improve their work if the comments is still in the user story scope. If not, create a new issue from the merge request comment ([Create issue to resolve thread](https://docs.gitlab.com/ee/user/discussions))
12. Check if you can add your weight to issues for which you have not yet added weight (your own issues and the ones from the rest of the team).
13. At the [Milestone session](#milestone-session), the team will quickly pass through all the merge requests merged during the current milestone. The team will discuss the new issues and assign them to the next milestone if ready.

!!! tip "Tips and considerations"

	- Iterate with small issues at the time. The smaller feature you do, the better.
	- Many things can change in the repository by the time you finish your work. Regularly sync with the main branch to get the latest changes.
	- The main branch is protected. No one can push directly to it. This is a way we guarantee things on the main branch are stable and should be working.
	- Keep your ideas in new issues. The more issues you open, the less remains in your head and the more you can focus on the task at hand.
	- Try not to start a new issue that has not been assigned to the current milestone. This will help you to focus on the task at hand and not be distracted by other things. If you have a new idea, open a new issue that might be assigned to the next milestone.

## Related explanations

These explanations are related to the current item (in alphabetical order).

- [About Git](../../explanations/about-git/index.md)
- [About GitLab](../../explanations/about-gitlab/index.md)
- [About Husky](../../explanations/about-husky/index.md)

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

_None at the moment._
