# How to use Husky

## Installation

Install [Husky](../../explanations/about-husky/index.md) with the following commands.

```sh title="In a terminal, execute the following command(s)."
npm install --save-dev husky
```

## Configuration

The configuration for Husky is located in the `.husky` directory.

## Common tasks

### Push without running Husky

You can push your changes without running Husky with the following command.

```sh title="In a terminal, execute the following command(s)."
git push --no-verify
```

## Related explanations

These explanations are related to the current item (in alphabetical order).

_None at the moment._

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

_None at the moment._
