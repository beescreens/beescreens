# How to use OpenSSH

## Installation

Install and configure [OpenSSH](../../explanations/about-openssh/index.md) with the following commands.

=== ":simple-linux: Linux"

	Install OpenSSH.

	```sh title="In a terminal, execute the following command(s)."
	sudo apt install openssh-server
	```

	Start the SSH agent and add it your shell so it starts on login

	```sh title="~/.bash_profile (for Bash) or ~/.zprofile (for ZSH)"
	if [ -z "$SSH_AUTH_SOCK" ]; then
		# Check for a currently running instance of the agent
		RUNNING_AGENT="`ps -ax | grep 'ssh-agent -s' | grep -v grep | wc -l | tr -d '[:space:]'`"
		if [ "$RUNNING_AGENT" = "0" ]; then
					# Launch a new instance of the agent
					ssh-agent -s &> $HOME/.ssh/ssh-agent
		fi
		eval `cat $HOME/.ssh/ssh-agent`
	fi
	```

=== ":simple-apple: macOS"

	Install OpenSSH.

	```sh title="In a terminal, execute the following command(s)."
	brew install openssh
	```

=== ":simple-windows: Windows"

	Install OpenSSH.

	```sh title="In a terminal, execute the following command(s)."
	TODO
	```

	Start a local Administrator PowerShell and start the SSH agent.

	```powershell title="In a terminal, execute the following command(s)."
	Set-Service ssh-agent -StartupType Automatic
	Start-Service ssh-agent
	Get-Service ssh-agent
	```

## Common tasks

### Create a SSH keypair

Replace `<filename>` with the name of the file. Leave empty for default name.

```sh title="In a terminal, execute the following command(s)."
# Generate the public/private keys pair
ssh-keygen \
  -t ed25519 \
  -f ~/.ssh/<filename> \
  -q \
  -N "" \
  -C ""
```

### Add the SSH key to the local SSH agent

Replace `<filename>` with the name of the file.

```sh title="In a terminal, execute the following command(s)."
# Add the SSH key to the agent
ssh-add ~/.ssh/<filename>
```

### Transfer the public SSH key to a remote host

Replace `<filename>` with the name of the file, `<username>` with the username and `<host>` with the name/IP of the host.

```sh title="In a terminal, execute the following command(s)."
# Transfer the SSH public key to the remote host
ssh-copy-id [-i ~/.ssh/<filename>.pub] <username>@<host>
```

### Connect to a remote host

Replace `<filename>` with the name of the file, `<username>` with the username and `<host>` with the name/IP of the host.

```sh title="In a terminal, execute the following command(s)."
# Connect to the Raspberry Pi without a password
ssh [-i ~/.ssh/<filename>] <username>@<host>
```

## Related explanations

These explanations are related to the current item (in alphabetical order).

- [About GitLab](../../explanations/about-gitlab/index.md)

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

_None at the moment._
