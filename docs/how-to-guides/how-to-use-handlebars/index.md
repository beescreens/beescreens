# How to use Handlebars

## Installation

Install [Handlebars](../../explanations/about-handlebars/index.md) with the following commands.

```sh title="In a terminal, execute the following command(s)."
npm install --save hbs

npm install --save-dev @types/hbs
```

## Common tasks

### Add a custom helper

Register a new helper with Handlebars.

```ts
import { handlebars } from 'hbs';

handlebars.registerHelper('eq', (v1, v2) => v1 === v2);
```

Use the helper in the template.

```html
{{#if (eq "v1" "v2")}}
  <div>show me</div>
{{/if}}
```

## Related explanations

These explanations are related to the current item (in alphabetical order).

_None at the moment._

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

_None at the moment._
