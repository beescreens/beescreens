# How to use GitLab

## Access

[GitLab](../../explanations/about-gitlab/index.md) can be accessed on any browser with a GitLab account on their official website.

## Configuration

The configuration for GitLab is mostly located in the `.gitlab` directory and through the online GitLab UI.

## Common tasks

### Create a GitLab account

Create a GitLab account on the official website: <https://gitlab.com/users/sign_up>.

### Add a public SSH key to your account

Add a public SSH key by accessing your **Profile preferences > SSH keys** or accessing <https://gitlab.com/-/profile/keys>.

### Create and configure a new project

In the [main group](https://gitlab.com/beescreens) or subgroup, create a new project by clicking the **New project** button. Select **Create blank project**. You can then configure your new project with its name, group/namespace, slug and visibility. Click **Create project** to create the new project.

Once a project is created, we tend to configure it as follow.

**Settings > General > Visibility, project features, permissions**

- **Disable all unused features** (especially the **Wiki** feature as we don't want to spread our documentation and keep it in a single place).

**Settings > Repository > Protected branches**

- Protect the **`main`** branch.

### Create and configure a new subgroup

In the [main group](https://gitlab.com/beescreens) or subgroup, create a new subgroup by clicking the **New subgroup** button. You can then configure your new subgroup with its name, URL, slug and visibility. Click **Create subgroup** to create the new subgroup.

Once a project is created, we tend to configure it as follow.

**Settings > General > Permissions and group features**

- **Disable all unused features** (especially the **Wiki** feature as we don't want to spread our documentation and keep it in a single place).

## Related explanations

These explanations are related to the current item (in alphabetical order).

- [About Git](../../explanations/about-git/index.md)

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

- [Manage groups](https://docs.gitlab.com/ee/user/group/manage.html)
- [Manage projects](https://docs.gitlab.com/ee/user/project/working_with_projects.html)
- [Sign commits with SSH keys](https://docs.gitlab.com/ee/user/project/repository/ssh_signed_commits/)
- [Subgroups](https://docs.gitlab.com/ee/user/group/subgroups/)
