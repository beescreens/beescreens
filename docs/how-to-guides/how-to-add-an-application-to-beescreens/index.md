# How to add an application to BeeScreens

Adding a new application to BeeScreens is a relatively simple process. This guide will walk you through the steps to add a new application to BeeScreens.

You'll use [pnpm](../../explanations/about-pnpm/index.md) to add your application to the existing workspace.

Then, you'll add all related commands to [Husky](../../explanations/about-husky/index.md) to ensure that your application is always in a working state before you push your changes to the remote Git repository.

Finally, you'll set up the [GitLab](../../explanations/about-gitlab/index.md) CI/CD pipeline to ensure that your application is always in a working state before merging your changes on the main branch.

## Steps

### Create a new directory for your application

Create a new directory in the `apps` directory of the BeeScreens Git repository. The name of the directory will be the name of your application.

### Create the directories of your application's services

If your application has multiple services, create a directory for each service in the directory of your application. The name of the directory will be the name of the service.

For example, if your application has a frontend and a backend, you will have the following structure:

```text
apps
└── my-app
    ├── backend
    └── frontend
```

### Create or initialize the services' `package.json` files

For each service, create a `package.json` file. You can use the `pnpm init` command to initialize the file if you want. Consider following the [pnpm - package.json structure](../../explanations/about-pnpm/index.md#packagejson-structure) documentation to make your own `package.json` file.

Name the package with the following convention: `@beescreens/<application-name>-<service-name>`.

Your application should have the following structure:

```text
apps
└── my-app
    ├── backend
    │   └── package.json
    └── frontend
        └── package.json
```

### Optional: Create a package for your application

If you want, you can create a package for your application. This is useful if you want to share your application with other developers or if you want to release a package to allow the usage of your application on the npm registry.

Your application will be able to use the package as a dependency inside the same workspace.

Create a new directory in the `packages` directory of the BeeScreens Git repository. The name of the directory will be the name of your package.

Initialize the `package.json` file with the `pnpm init` command.

Name the package with the following convention: `@beescreens/<application-name>`.

Your package should have the following structure:

```text
packages
└── my-app
    └── package.json
```

### Add all directories to the pnpm workspace

Add the services to the workspace in the `pnpm-workspace.yaml` file.

If you created a package for your application, add it to the workspace as well.

Your `pnpm-workspace.yaml` file should look like this:

```yaml title="pnpm-workspace.yaml"
packages:
  # My App
  - "apps/my-app/backend"
  - "apps/my-app/frontend"
  - "packages/my-app"
```

### Check if the workspace is valid

If you used the `pnpm init` command to initialize the `package.json` files, a default `test` script was added to the `scripts` section of the files. You can use this script to check if the workspace is valid with the following command:

```sh title="In a terminal, execute the following command(s)."
pnpm \
	--filter @beescreens/my-app-backend \
	--filter @beescreens/my-app-frontend \
	--filter @beescreens/my-app \
	test
```

The output of the command should look like this:

```sh title="Output of the command"
Scope: 3 of 9 workspace projects
apps/my-app/backend test$ echo "This is a test!"
│ This is a test!
└─ Done in 21ms
packages/my-app test$ echo "This is a test!"
│ This is a test!
└─ Done in 22ms
apps/my-app/frontend test$ echo "This is a test!"
│ This is a test!
└─ Done in 21ms
```

If used with the `--parallel` flag, the output of the command should look like this:

```sh title="Output of the command"
Scope: 3 of 9 workspace projects
apps/my-app/backend test$ echo "This is a test!"
apps/my-app/frontend test$ echo "This is a test!"
packages/my-app test$ echo "This is a test!"
apps/my-app/backend test: This is a test!
apps/my-app/backend test: Done
apps/my-app/frontend test: This is a test!
packages/my-app test: This is a test!
packages/my-app test: Done
apps/my-app/frontend test: Done
```

### Set up the local CI/CD pipeline with Husky

Add the commands to test your application to the local CI/CD pipeline with Husky in the `.husky/pre-push` file. The `did_files_change_in_directory` function allows to run the pnpm commands only if changes were made in the corresponding directory.

```sh title=".husky/pre-push"
# ...

## My App Template
if
	did_files_change_in_directory "apps/my-app/**/*" ||
	did_files_change_in_directory "packages/my-app/**/*"
then
	## test
	pnpm --parallel \
		--filter @beescreens/my-app-backend \
		--filter @beescreens/my-app-frontend \
		--filter @beescreens/my-app \
		test
fi

# ...
```

### Set up the remote CI/CD pipeline with GitLab CI/CD

Add your own GitLab CI/CD configuration file to the `.gitlab/ci_cd` directory under the name `.gitlab/ci_cd/my-app.yml`. The `extends: .test` line extends the definition of the `.gitlab/ci_cd/templates/test.yml` template file.

```yaml title=".gitlab/ci_cd/my-app.yml"
## Package

# test
## Package

# test
my-app-template::package::test:
  needs:
    - setup env
  extends: .test
  variables:
    PROJECT_PATH: "packages/my-app-template"
    PROJECT_NAME: "@beescreens/my-app-template"
  script:
    - echo "Testing $PROJECT_NAME..."

## Backend

# test
my-app-template::app::backend::test:
  needs:
    - setup env
  extends: .test
  variables:
    PROJECT_PATH: "apps/my-app-template/backend"
    PROJECT_NAME: "@beescreens/my-app-template-backend"
  script:
    - echo "Testing $PROJECT_NAME..."

## Frontend

# test
my-app-template::app::frontend::test:
  needs:
    - setup env
  extends: .test
  variables:
    PROJECT_PATH: "apps/my-app-template/frontend"
    PROJECT_NAME: "@beescreens/my-app-template-frontend"
  script:
    - echo "Testing $PROJECT_NAME..."
```

Add your new file to the main `.gitlab/ci_cd/main.yml` configuration file.

```yaml
[...]

include:
  # Apps
  - .gitlab/ci_cd/my-app.yml

[...]
```

### Commit your changes and push them to the remote repository

Commit your changes and push them to the remote repository using the workflow described in the [Become a BeeScreens contributor](../../tutorials/become-a-beescreens-contributor/index.md) tutorial and the [How to contribute to BeeScreens](../how-to-contribute-to-beescreens/index.md) guide.

### Check the results

On push, Husky should run the local CI pipeline for your application.

On merge request, GitLab CI/CD should run the remote CI pipeline. You can check the results in the [GitLab CI/CD pipelines](https://gitlab.com/beescreens/beescreens/-/pipelines) section.

## Summary

Congrats! You have successfully added your application to the BeeScreens repository.

Locally, pnpm can manage your workspace from anywhere in the repository. pnpm tracks all the available commands and can call any scripts you'd like to run.

Before a push, Husky will run the tests for your application to ensure that your changes do not break the application before sharing your changes to the remote Git repository.

Remotely, on merge request, GitLab CI/CD will run the tests for your application to ensure that your changes do not break the application.

You can have a look at the existing applications to improve your applications and pipelines and start creating an amazing new application!

## Related explanations

These explanations are related to the current item (in alphabetical order).

- [About Git](../../explanations/about-git/index.md)
- [About GitLab](../../explanations/about-gitlab/index.md)
- [About Husky](../../explanations/about-husky/index.md)
- [About pnpm](../../explanations/about-pnpm/index.md)

## Resources and alternatives

These resources and alternatives are related to the current item (in alphabetical order).

_None at the moment._
