# **BeeScreens**

[![License][license-badge]][license-url]
[![Pipeline Status][pipeline-status-badge]][pipeline-status-url]
[![Coverage Report][coverage-report-badge]][coverage-report-url]
[![Issues][issues-badge]][issues-url]
[![Merge requests][merge-requests-badge]][merge-requests-url]

## Introduction

BeeScreens is a collection of interactive Web applications for your events.

The interactive applications can be games, quizzes, polls, etc. and are designed to be used by the audience of your events. Other applications are designed to be used by the organizers of your events to manage the applications used by the audience.

On the long term, it aims to be a framework to help build interactive applications that can be used from any media (i.e. a phone, computer, etc) and streamed to any other media both only using a modern Web Browser.

## Documentation

The documentation is available online at [https://docs.beescreens.ch][documentation-url] or offline in this repository at [`docs/index.md`][documentation-path].

## License

This project is free and will always be.

The project is licensed under the AGPL-3.0 License - see the [`COPYING`][license-url] file for details.

## Contributing

Here is some help to get you started to contribute to the project:

1. Please start by reading our code of conduct available in the [`CODE_OF_CONDUCT.md`][code-of-conduct-url] file.
2. All contribution information is available in the [`CONTRIBUTING.md`][contributing-url] file.

Feel free to contribute to the project in any way that you can think of, your contributions are more than welcome!

[license-badge]: https://img.shields.io/gitlab/license/beescreens/beescreens
[license-url]: ./COPYING
[latest-release-badge]: https://gitlab.com/beescreens/beescreens/-/badges/release.svg?key_text=latest%20release
[latest-release-url]: https://gitlab.com/beescreens/beescreens/-/releases
[pipeline-status-badge]: https://gitlab.com/beescreens/beescreens/badges/main/pipeline.svg
[pipeline-status-url]: https://gitlab.com/beescreens/beescreens/-/pipelines?scope=branches&ref=main
[coverage-report-badge]: https://gitlab.com/beescreens/beescreens/badges/main/coverage.svg
[coverage-report-url]: https://gitlab.com/beescreens/beescreens/-/graphs/main/charts
[issues-badge]: https://img.shields.io/gitlab/issues/open/beescreens/beescreens
[issues-url]: https://gitlab.com/beescreens/beescreens/-/issues
[merge-requests-badge]: https://img.shields.io/gitlab/merge-requests/open/beescreens/beescreens
[merge-requests-url]: https://gitlab.com/beescreens/beescreens/-/merge_requests
[documentation-url]: https://docs.beescreens.ch
[documentation-path]: ./docs/index.md
[code-of-conduct-url]: ./CODE_OF_CONDUCT.md
[contributing-url]: ./CODE_OF_CONDUCT.md
