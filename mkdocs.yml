repo_url: https://gitlab.com/beescreens/beescreens
repo_name: beescreens/beescreens
site_name: BeeScreens Documentation
site_author: BeeScreens contributors
site_url: https://docs.beescreens.ch
site_description: >-
  The official documentation for the BeeScreens project.
copyright: Copyright &copy; 2018 - 2024 BeeScreens
nav:
  - Home:
      - index.md
      - Tutorials: tutorials/index.md
      - How-to guides: how-to-guides/index.md
      - Reference: reference/index.md
      - Explanations: explanations/index.md
  - Tutorials:
      - tutorials/index.md
      - Install and configure Visual Studio Code: tutorials/install-and-configure-visual-studio-code/index.md
      - Install and configure Docker: tutorials/install-and-configure-docker/index.md
      - Install and configure Dev Containers: tutorials/install-and-configure-dev-containers/index.md
      - Create a media player application:
          - Introduction: tutorials/create-a-media-player-application/introduction/index.md
          - Initialize the project with NestJS: tutorials/create-a-media-player-application/chapter-1-initialize-the-project-with-nestjs/index.md
          - Render full HTML pages with Handlebars: tutorials/create-a-media-player-application/chapter-2-render-full-html-pages-with-handlebars/index.md
          - Create an image slideshow with Splide: tutorials/create-a-media-player-application/chapter-3-create-an-image-slideshow-with-splide/index.md
          - Enable video support for Splide: tutorials/create-a-media-player-application/chapter-4-enable-video-support-for-splide/index.md
          - Fix media ratio sizes with CSS: tutorials/create-a-media-player-application/chapter-5-fix-media-ratio-sizes-with-css/index.md
          - Customize the slideshow to your will: tutorials/create-a-media-player-application/chapter-6-customize-the-slideshow-to-your-will/index.md
          - Move the slideshow data to the service: "tutorials/create-a-media-player-application/chapter-7-move-the-slideshow-data-to-the-service/index.md"
          - Ensure code correctness with TypeScript: "tutorials/create-a-media-player-application/chapter-8-ensure-code-correctness-with-typescript/index.md"
          - Define and access multiple slideshows: "tutorials/create-a-media-player-application/chapter-9-define-and-access-multiple-slideshows/index.md"
          - Manage the slideshows through an API: "tutorials/create-a-media-player-application/chapter-10-manage-the-slideshows-through-an-api/index.md"
          - Persist the slideshows with Prisma: "tutorials/create-a-media-player-application/chapter-11-persist-the-slideshows-with-prisma/index.md"
          - Refactor your application using DDD: "tutorials/create-a-media-player-application/chapter-12-refactor-your-application-with-ddd/index.md"
          # - Protect the API access with BasicAuth: ""
          # - Manage the credentials with dotenv: ""
          # - Add API documentation with OpenAPI: ""
          # - Create an administration panel: ""
          # - Refactor?: ""
          # - Conclusion: tutorials/create-a-media-player-application/conclusion/index.md
      - Create an interactive drawing application:
          - Introduction: tutorials/create-an-interactive-drawing-application/introduction/index.md
          - Initialize the frontend project with Next.js: tutorials/create-an-interactive-drawing-application/chapter-1-initialize-the-frontend-project-with-nextjs/index.md
          - Create a drawing board with Konva.js: tutorials/create-an-interactive-drawing-application/chapter-2-create-a-drawing-board-with-konvajs/index.md
          - Create a React Context to store the lines: tutorials/create-an-interactive-drawing-application/chapter-3-create-a-react-context-to-store-the-lines/index.md
          - Initialize the backend project with NestJS: tutorials/create-an-interactive-drawing-application/chapter-4-initialize-the-backend-project-with-nestjs/index.md
          - Create WebSocket gateway with NestJS and Socket.IO: tutorials/create-an-interactive-drawing-application/chapter-5-create-a-websocket-gateway-with-nestjs-and-socketio/index.md
          - Create a React Context to connect to the backend with Socket.IO: tutorials/create-an-interactive-drawing-application/chapter-6-create-a-react-context-to-connect-to-the-backend/index.md
          - Draw simultaneously on multiple windows: tutorials/create-an-interactive-drawing-application/chapter-7-draw-simultaneously-on-multiple-windows/index.md
          - Manage the environnement variables with dotenv: tutorials/create-an-interactive-drawing-application/chapter-8-manage-the-environment-variables-with-dotenv/index.md
          - Draw from your smartphone: tutorials/create-an-interactive-drawing-application/chapter-9-draw-from-your-smartphone/index.md
          # - Package you applications with Docker: tutorials/create-an-interactive-drawing-application/chapter-10-package-your-applications-with-docker/index.md
      - Install and configure a Raspberry Pi:
          - Introduction: tutorials/install-and-configure-a-raspberry-pi/introduction.md
          - Hardware prerequisites: tutorials/install-and-configure-a-raspberry-pi/hardware-prerequisites.md
          - Download the Raspberry Pi OS image(s): tutorials/install-and-configure-a-raspberry-pi/chapter-1-download-the-raspberry-pi-os-image-s.md
          - Write the image(s) on a microSD card: tutorials/install-and-configure-a-raspberry-pi/chapter-2-write-the-image-s-on-a-microsd-card.md
          - Preconfigure the operating system: tutorials/install-and-configure-a-raspberry-pi/chapter-3-preconfigure-the-operating-system.md
          - Start and log in on the Raspberry Pi: tutorials/install-and-configure-a-raspberry-pi/chapter-4-start-and-log-in-on-the-raspberry-pi.md
          - Configure the operating system: tutorials/install-and-configure-a-raspberry-pi/chapter-5-configure-the-operating-system.md
          - Install and configure Sway: tutorials/install-and-configure-a-raspberry-pi/chapter-6-install-and-configure-sway.md
          - Install and configure the browser: tutorials/install-and-configure-a-raspberry-pi/chapter-7-install-and-configure-the-browser.md
          - Try out the BeeScreens applications: tutorials/install-and-configure-a-raspberry-pi/chapter-8-try-out-the-beescreens-applications.md
          - Assign an application to a Sway workspace: tutorials/install-and-configure-a-raspberry-pi/chapter-9-assign-an-application-to-a-sway-workspace.md
          - Assign Sway workspaces to outputs: tutorials/install-and-configure-a-raspberry-pi/chapter-10-assign-sway-workspaces-to-outputs.md
          - Backup and restore the microSD card: tutorials/install-and-configure-a-raspberry-pi/chapter-11-backup-and-restore-the-microsd-card.md
          - Conclusion: tutorials/install-and-configure-a-raspberry-pi/conclusion.md
      - Become a BeeScreens contributor: tutorials/become-a-beescreens-contributor/index.md
  - How-to guides:
      - how-to-guides/index.md
      - How to contribute to BeeScreens: how-to-guides/how-to-contribute-to-beescreens/index.md
      - How to add an application to BeeScreens: how-to-guides/how-to-add-an-application-to-beescreens/index.md
      - How to use cspell: how-to-guides/how-to-use-cspell/index.md
      - How to use GitLab: how-to-guides/how-to-use-gitlab/index.md
      - How to use Husky: how-to-guides/how-to-use-husky/index.md
      - How to use Handlebars: how-to-guides/how-to-use-handlebars/index.md
      - How to use Material for MkDocs: how-to-guides/how-to-use-material-for-mkdocs/index.md
      - How to use OpenSSH: how-to-guides/how-to-use-openssh/index.md
      - How to use pnpm: how-to-guides/how-to-use-pnpm/index.md
      - How to use Raspberry Pi Imager: how-to-guides/how-to-use-raspberry-pi-imager/index.md
  - Reference:
      - reference/index.md
      - BeePlace: reference/beeplace.md
      - Dashboard: reference/dashboard.md
      - Media Player: reference/media-player.md
      - Pimp My Wall: reference/pimp-my-wall.md
  - Explanations:
      - explanations/index.md
      - About this documentation: explanations/about-this-documentation/index.md
      - About Ansible: explanations/about-ansible/index.md
      - About Cobra: explanations/about-cobra/index.md
      - About cURL: explanations/about-curl/index.md
      - About cspell: explanations/about-cspell/index.md
      - About Dev Containers: explanations/about-dev-containers/index.md
      - About Docker: explanations/about-docker/index.md
      - About Domain-driven design: explanations/about-domain-driven-design/index.md
      - About dotenv: explanations/about-dotenv/index.md
      - About Git: explanations/about-git/index.md
      - About GitLab: explanations/about-gitlab/index.md
      - About Golang: explanations/about-golang/index.md
      - About Handlebars: explanations/about-handlebars/index.md
      - About Husky: explanations/about-husky/index.md
      - About Immer: explanations/about-immer/index.md
      - About Joi: explanations/about-joi/index.md
      - About JSON: explanations/about-json/index.md
      - About k6: explanations/about-k6/index.md
      - About Konva.js: explanations/about-konvajs/index.md
      - About Markdown: explanations/about-markdown/index.md
      - About Material for MkDocs: explanations/about-material-for-mkdocs/index.md
      - About NestJS: explanations/about-nestjs/index.md
      - About Next.js: explanations/about-nextjs/index.md
      - About Node.js: explanations/about-nodejs/index.md
      - About npm: explanations/about-npm/index.md
      - About Nunjunks: explanations/about-nunjunks/index.md
      - About OpenSSH: explanations/about-openssh/index.md
      - About Prisma: explanations/about-prisma/index.md
      - About pnpm: explanations/about-pnpm/index.md
      - About Raspberry Pi Imager: explanations/about-raspberry-pi-imager/index.md
      - About Separation of concerns: explanations/about-separation-of-concerns/index.md
      - About Sway: explanations/about-sway/index.md
      - About Socket.IO: explanations/about-socketio/index.md
      - About Splide: explanations/about-splide/index.md
      - About SQLite: explanations/about-sqlite/index.md
      - About TypeScript: explanations/about-typescript/index.md
      - About Raspberry Pi Foundation: explanations/about-raspberry-pi-foundation/index.md
      - About The Twelve-Factor App: explanations/about-the-twelve-factor-app/index.md
      - About UUID: explanations/about-uuid/index.md
      - About Visual Studio Code: explanations/about-visual-studio-code/index.md
      - About WebSocket: explanations/about-websocket/index.md
site_dir: public
theme:
  custom_dir: docs
  favicon: assets/images/favicon.svg
  language: en
  icon:
    logo: material/bee-flower
    repo: fontawesome/brands/git-alt
  name: material
  palette:
    # Palette toggle for light mode
    - media: "(prefers-color-scheme: light)"
      scheme: default
      primary: teal
      accent: teal
      toggle:
        icon: material/brightness-7
        name: Switch to dark mode
    # Palette toggle for dark mode
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      primary: blue grey
      accent: blue grey
      toggle:
        icon: material/brightness-4
        name: Switch to light mode
  features:
    - content.code.annotate
    - content.code.copy
    - content.tabs.link
    - navigation.indexes
    - navigation.instant
    - navigation.tabs
    - navigation.tabs.sticky
    - navigation.top
    - navigation.footer
    - search.suggest
    - search.highlight
    - toc.follow
plugins:
  # Development and production
  - group:
      enabled: true
      plugins:
        - glightbox
        - plantuml:
            puml_url: http://plantuml:8080
            puml_keyword: plantuml
        - search
        - tags
  # Production only
  - group:
      enabled: !ENV [CI, false]
      plugins:
        - git-revision-date-localized:
            type: datetime
            enable_creation_date: true
            fallback_to_build_date: true
        - social
        # Order in plugins matters - this must be last
        - minify:
            minify_html: true
extra:
  social:
    - icon: material/gitlab
      link: https://gitlab.com/beescreens/beescreens
      name: BeeScreens on GitLab
markdown_extensions:
  - abbr
  - admonition
  - attr_list
  - def_list
  - footnotes
  - md_in_html
  - pymdownx.arithmatex:
      generic: true
  - pymdownx.betterem
  - pymdownx.critic
  - pymdownx.caret
  - pymdownx.details
  - pymdownx.emoji:
      emoji_index: !!python/name:material.extensions.emoji.twemoji
      emoji_generator: !!python/name:material.extensions.emoji.to_svg
  - pymdownx.highlight:
      anchor_linenums: true
      linenums: true
  - pymdownx.inlinehilite
  - pymdownx.keys
  - pymdownx.mark
  - pymdownx.smartsymbols
  - pymdownx.snippets:
      check_paths: true
      base_path: ["docs"]
      auto_append:
        - glossary.md
  - pymdownx.superfences:
      preserve_tabs: true
  - pymdownx.tabbed:
      alternate_style: true
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.tilde
  - tables
  - toc:
      permalink: true
extra_css:
  - assets/stylesheets/extra.css
extra_javascript:
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  - https://unpkg.com/tablesort@5.3.0/dist/tablesort.min.js
  - assets/javascripts/mathjax.js
  - assets/javascripts/tablesort.js
