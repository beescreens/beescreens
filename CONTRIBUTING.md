# Contributing to BeeScreens

Check the online guide **How to contribute to BeeScreens** at <https://docs.beescreens.ch/how-to-guides/how-to-contribute-to-beescreens>.

You can also check the offline guide in this repository at [`docs/how-to-guides/how-to-contribute-to-beescreens/index.md`](./docs/how-to-guides/how-to-contribute-to-beescreens/index.md).
