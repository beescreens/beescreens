import { SessionProvider } from 'next-auth/react';
import { Admin } from '@/components/admin';

export default function Home() {
	return (
		<SessionProvider>
			<Admin />
		</SessionProvider>
	);
};
