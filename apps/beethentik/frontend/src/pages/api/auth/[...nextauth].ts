import NextAuth from "next-auth";
import AuthentikProvider from "next-auth/providers/authentik";

export const authOptions = {
		providers: [
				AuthentikProvider({
						clientId: process.env.AUTHENTIK_ID as string,
						clientSecret: process.env.AUTHENTIK_SECRET as string,
						issuer: process.env.AUTHENTIK_ISSUER,
				}),
		],
		callbacks: {
				async jwt({token, account}: {token: any, account: any}) {
						// account is given when logging in, save it to the token
						if (account != null) {
								token.access_token = account.access_token
								token.refresh_token = account.refresh_token
								token.id_token = account.id_token
								token.expires_at = account.expires_at * 1000 // Date.now() is in milliseconds
								token.error = undefined
						}

						// Refresh access token if it has expired
						if (Date.now() >= token.expires_at) {
								return refreshAccessToken(token);
						}

						return token
				},

				async session({session, token}: {session: any, token: any}) {
						// called whenever a session is checked (login, refresh, etc), update session
						if (token?.access_token != null) {
								session.access_token = token.access_token
								session.refresh_token = token.refresh_token
								session.id_token = token.id_token
								session.error = token.error
							}
							return session
				},
		},
};

// Refresh access token using refresh token
async function refreshAccessToken(token: any) {
		try {
				const response = await fetch(`${process.env.AUTHENTIK_TOKEN_URL}`, {
						headers: {
								"Content-Type": "application/x-www-form-urlencoded",
								"Accept": "application/json",
						},
						body: new URLSearchParams({
								grant_type: "refresh_token",
								refresh_token: token.refresh_token,
								client_id: process.env.AUTHENTIK_ID || "",
								client_secret: process.env.AUTHENTIK_SECRET || "",
						}),
						method: "POST",
				});
				const refreshedTokens = await response.json();

				// Throw error if the response is not ok
				if (!response.ok) {
						throw refreshedTokens;
				}

				return {
						...token,
						access_token: refreshedTokens.access_token,
						refresh_token: refreshedTokens.refresh_token,
						id_token: refreshedTokens.id_token,
						expires_at: Date.now() + refreshedTokens.expires_in * 1000,
						error: undefined,
				};
		} catch (error) {
				// If an error occurred, return the current token with an error
				return {
						...token,
						error: "RefreshAccessTokenError",
				};
		}
}

export default NextAuth(authOptions);
