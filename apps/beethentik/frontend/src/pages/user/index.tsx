import { SessionProvider } from 'next-auth/react';
import { User } from '@/components/user';

export default function Home() {
	return (
		<SessionProvider>
			<User />
		</SessionProvider>
	);
};
