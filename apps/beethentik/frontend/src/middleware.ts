// Default matcher, applies to every page
export { default } from "next-auth/middleware"

// Protects the pages we want to be authenticated
export const config = { matcher: ["/user","/admin"] }
