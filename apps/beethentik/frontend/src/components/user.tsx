import React, { useEffect, useState } from "react";
import { useSession, signOut } from "next-auth/react";

export const User = () => {
	const { data: session } = useSession();
	let [res, setRes] = useState("loading...");

	useEffect(() => {
		// Check the error in the session (occurs when the refresh token is expired)
		if (session?.error !== undefined) {
			// Force signout to clean up the session
			signOut();
		}

		// If the session is valid, fetch from the backend
		if (session?.access_token) {
			// TODO get address from env
			fetch("http://127.0.0.1:4000/demo-user/", {
				method: 'GET',
				headers: {
					'Authorization': `Bearer ${session.access_token}`,
				}
			})
			.then(response => {
				// If the response is 200, the user is authorized
				if (response.status === 200) {
					setRes("Authorized");
				} else {
					setRes("Unauthorized");
				}
			})
			.catch(error => {
				console.error(error);
			});
		}
	}, [session]);

	return (
		<div>
			<button onClick={() => signOut()}>Logout</button>
			<h1>Welcome on the user page</h1>
			<p>Fetching from backend : {res} </p>
		</div>
	);
};
