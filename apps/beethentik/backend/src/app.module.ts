import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from './config/config.module';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from './auth/roles/roles.guard';

@Module({
	imports: [ConfigModule],
	controllers: [AppController],
	providers: [AppService, { provide: APP_GUARD, useClass: RolesGuard }],
})
export class AppModule {}
