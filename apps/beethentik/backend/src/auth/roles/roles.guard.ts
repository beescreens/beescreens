import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Role } from './role.enum';
import { Request } from 'express';
import { JwtService } from '@nestjs/jwt';
import { ROLES_KEY } from './roles.decorator';

@Injectable()
export class RolesGuard implements CanActivate {
	constructor(private reflector: Reflector) {}

	async canActivate(context: ExecutionContext): Promise<boolean> {
		const requiredRoles = this.reflector.getAllAndOverride<Role[]>(ROLES_KEY, [
			context.getHandler(),
			context.getClass(),
		]);

		// If no roles are required, then the route is public
		if (!requiredRoles) {
			return true;
		}

		const request = context.switchToHttp().getRequest();
		const token = this.extractToken(request);
		const jwtService = new JwtService();

		// If there is no token, then the user is not authenticated
		if (!token) {
			return false;
		}

		// Check the token's validity
		try {
			const payload = await jwtService.verifyAsync(token, {
				// The public secret is used to verify the token's signature
				secret: process.env.JWT_CERTIFICATE,
			});

			// If the user has the required role, then the route is accessible
			return (
				payload &&
				payload.groups &&
				requiredRoles.some((role) => payload.groups?.includes(role))
			);
		} catch (e) {
			console.error(e);
			return false;
		}
	}

	// Extracts the token from the request headers
	private extractToken(request: Request): string | undefined {
		const [type, token] =
			(request.headers as { authorization?: string }).authorization?.split(
				' ',
			) ?? [];
		return type === 'Bearer' ? token : undefined;
	}
}
