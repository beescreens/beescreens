import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { Roles } from './auth/roles/roles.decorator';
import { Role } from './auth/roles/role.enum';

@Controller()
export class AppController {
	constructor(private readonly appService: AppService) {}

	@Get()
	root() {
		return this.appService.getRole(Role.Guest);
	}

	@Get('demo-admin')
	@Roles(Role.Admin)
	admin() {
		return this.appService.getRole(Role.Admin);
	}

	@Get('demo-user')
	@Roles(Role.User)
	user() {
		return this.appService.getRole(Role.User);
	}
}
