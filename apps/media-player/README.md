# Media Player

Check the online reference **Media Player** at <https://docs.beescreens.ch/reference/media-player/>.

You can also check the offline reference in this repository at [`../../docs/reference/media-player.md`](../../docs/reference/media-player.md).

## Start the application in development mode

```sh
# Create an .env file using provided defaults
cp .env.defaults .env

# Install dependencies
pnpm install

# Start the database
docker compose up -d

# Start the application
pnpm dev
```

You can access the application at [http://localhost:9292](http://localhost:9292), and the API documentation on [http://localhost:9292/api](http://localhost:9292/api).

To stop the database, run `docker compose down`.

## Build the application

```sh
# Build the application
pnpm build
```

## Start the application in production mode

You must build the application prior to starting it in production mode.

```sh
# Start the application
pnpm start
```
