import { MediaType, PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

async function main() {
	const slideshow = await prisma.slideshow.upsert({
		where: { name: 'My Slideshow' },
		update: {},
		create: {
			name: 'My Slideshow',
			slides: {
				create: [
					{
						alt: 'Random photo on Unsplash',
						src: 'https://source.unsplash.com/random?random',
						type: MediaType.IMAGE,
					},
					{
						alt: 'Sintel: first open movie',
						src: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4',
						type: MediaType.VIDEO,
						interval: 10000,
					},
				],
			},
		},
	});

	console.log('Database seeded with default data');
}
main()
	.then(async () => {
		await prisma.$disconnect();
	})
	.catch(async (e) => {
		console.error(e);
		await prisma.$disconnect();
		process.exit(1);
	});
