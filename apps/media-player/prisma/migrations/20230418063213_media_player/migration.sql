-- DropForeignKey
ALTER TABLE "slides" DROP CONSTRAINT "slides_slideshow_id_fkey";

-- AddForeignKey
ALTER TABLE "slides" ADD CONSTRAINT "slides_slideshow_id_fkey" FOREIGN KEY ("slideshow_id") REFERENCES "slideshows"("id") ON DELETE CASCADE ON UPDATE CASCADE;
