-- CreateEnum
CREATE TYPE "MediaType" AS ENUM ('IMAGE', 'VIDEO');

-- CreateTable
CREATE TABLE "slides" (
    "id" TEXT NOT NULL,
    "slideshow_id" TEXT NOT NULL,
    "src" TEXT NOT NULL,
    "type" "MediaType" NOT NULL,
    "alt" TEXT NOT NULL,
    "interval" INTEGER,

    CONSTRAINT "slides_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "slideshows" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,

    CONSTRAINT "slideshows_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "slides" ADD CONSTRAINT "slides_slideshow_id_fkey" FOREIGN KEY ("slideshow_id") REFERENCES "slideshows"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
