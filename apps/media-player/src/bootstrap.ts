import * as nunjucks from 'nunjucks';
import { HttpAdapterHost } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import { PrismaClientExceptionFilter, PrismaService } from 'nestjs-prisma';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export async function bootstrap(
	app: NestExpressApplication,
): Promise<NestExpressApplication> {
	const { httpAdapter } = app.get(HttpAdapterHost);

	app.useGlobalFilters(new PrismaClientExceptionFilter(httpAdapter));
	app.useGlobalPipes(
		new ValidationPipe({
			transform: true,
		}),
	);

	nunjucks.configure(join(__dirname, '..', 'views'), {
		express: app,
		watch: process.env.NODE_ENV === 'development',
	});

	app.setBaseViewsDir(join(__dirname, '..', 'views'));
	app.useStaticAssets(join(__dirname, '..', 'public'));
	app.setViewEngine('njk');

	const prismaService = app.get(PrismaService);
	await prismaService.enableShutdownHooks(app);

	const config = new DocumentBuilder()
		.setTitle('Media Player API')
		.setDescription('The Media Player API')
		.setVersion(process.env.npm_package_version as string)
		.addApiKey(
			{
				type: 'apiKey',
				name: 'apiKey',
				description: 'The API key to access protected endpoints',
			},
			'apiKey',
		)
		.build();

	const document = SwaggerModule.createDocument(app, config);

	SwaggerModule.setup('api', app, document, {
		swaggerOptions: {
			showExtensions: true,
			tagsSorter: 'alpha',
			operationsSorter: 'alpha',
			persistAuthorization: true,
		},
	});

	return app;
}
