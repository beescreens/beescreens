import { API_KEY } from '@/config/config.constants';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
	constructor(private configService: ConfigService) {}

	async validateApiKey(apiKey: string) {
		if (apiKey !== this.configService.get(API_KEY, { infer: true })) {
			throw new UnauthorizedException();
		}
	}
}
