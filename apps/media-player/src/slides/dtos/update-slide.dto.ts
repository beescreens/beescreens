import { OmitType, PartialType } from '@nestjs/swagger';
import { SlideDto } from './slide.dto';

export class UpdateSlideDto extends PartialType(
	OmitType(SlideDto, ['id', 'slideshowId'] as const),
) {}
