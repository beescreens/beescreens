import { OmitType } from '@nestjs/swagger';
import { SlideDto } from './slide.dto';

export class CreateSlideDto extends OmitType(SlideDto, [
	'id',
	'slideshowId',
] as const) {}
