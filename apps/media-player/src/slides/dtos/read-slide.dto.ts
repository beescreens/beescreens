import { OmitType } from '@nestjs/swagger';
import { SlideDto } from './slide.dto';

export class ReadSlideDto extends OmitType(SlideDto, [
	'id',
	'slideshowId',
] as const) {
	constructor(partial?: Partial<SlideDto>) {
		super();

		if (partial) {
			// Exclude properties from the object
			delete partial.id;
			delete partial.slideshowId;

			Object.assign(this, partial);
		}
	}
}
