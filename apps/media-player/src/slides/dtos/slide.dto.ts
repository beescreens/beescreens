import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
	IsString,
	IsUUID,
	IsNotEmpty,
	IsEnum,
	IsNumber,
	IsOptional,
	IsUrl,
	IsPositive,
} from 'class-validator';
import { Slide, MediaType } from '@prisma/client';

export class SlideDto implements Slide {
	// ID of the slide
	@ApiProperty({ format: 'uuid' })
	@IsUUID()
	id: string;

	// The slideshow to which the slide belongs to
	@ApiProperty({ format: 'uuid' })
	@IsUUID()
	slideshowId: string;

	// URL of the source to display
	@ApiProperty({ format: 'url' })
	@IsUrl()
	@IsNotEmpty()
	src: string;

	// Type of the slide
	@ApiProperty({ enum: MediaType })
	@IsEnum(MediaType)
	type: MediaType;

	// Alt text of the slide
	@IsString()
	@IsNotEmpty()
	alt: string;

	// Duration of the slide in seconds
	@ApiPropertyOptional()
	@IsNumber()
	@IsPositive()
	@IsOptional()
	interval: number | null;
}
