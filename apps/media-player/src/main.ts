import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import { bootstrap } from './bootstrap';

const main = async () => {
	const instance = await NestFactory.create<NestExpressApplication>(AppModule);

	const app = await bootstrap(instance);

	await app.listen(4000);
};

main();
