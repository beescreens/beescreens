import { Get, Controller, Redirect } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('Views')
@Controller()
export class AppController {
	@Get()
	@ApiOperation({
		summary: 'Redirect to the `slideshows` page',
		description: 'Redirect to the `slideshows` page.',
		operationId: 'root',
	})
	@ApiOkResponse({
		description: 'Render successful.',
	})
	@Redirect('/slideshows')
	root() {
		return {};
	}

	@Get('api')
	@ApiOperation({
		summary: 'Render the Swagger UI page',
		description: 'Render the Swagger UI page.',
		operationId: 'api',
	})
	@ApiOkResponse({
		description: 'Render successful.',
	})
	api() {
		return;
	}
}
