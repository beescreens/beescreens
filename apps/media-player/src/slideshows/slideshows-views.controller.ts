import { Get, Controller, Render, Param, Res } from '@nestjs/common';
import { Response } from 'express';
import { SlideshowsService } from './slideshows.service';
import {
	ApiNotFoundResponse,
	ApiOkResponse,
	ApiOperation,
	ApiParam,
	ApiTags,
} from '@nestjs/swagger';
import { ReadSlideshowDto } from './dtos/read-slideshow.dto';
import { ConfigService } from '@nestjs/config';
import {
	DEFAULT_IMAGE_INTERVAL,
	DEFAULT_VIDEO_INTERVAL,
} from '@/config/config.constants';

@ApiTags('Views')
@Controller('/slideshows')
export class SlideshowsViewsController {
	constructor(
		private readonly configService: ConfigService,
		private readonly slideshowsService: SlideshowsService,
	) {}

	@Get()
	@ApiOperation({
		summary: 'Render the slideshows page',
		description: 'Render the slideshows page.',
		operationId: 'getSlideshowsView',
	})
	@ApiOkResponse({
		description: 'Render successful.',
	})
	@Render('slideshows/index')
	async getSlideshowsView() {
		const slideshows = await this.slideshowsService.getSlideshows();

		const slideshowsDto = slideshows.map(
			(slideshow) => new ReadSlideshowDto(slideshow),
		);

		return { slideshows: slideshowsDto };
	}

	@Get('/:id')
	@ApiOperation({
		summary: 'Render the specified slideshow page',
		description: 'Render the specified slideshow page.',
		operationId: 'getSlideshowView',
	})
	@ApiParam({
		name: 'id',
		description: 'The slideshow ID.',
		format: 'uuid',
	})
	@ApiOkResponse({
		description: 'Render successful.',
	})
	@ApiNotFoundResponse({
		description:
			'Slideshow has not been found. Redirect to `/slideshows` page.',
	})
	async getSlideshowView(@Res() res: Response, @Param('id') id: string) {
		try {
			const slideshow = await this.slideshowsService.getSlideshow(id);

			const slideshowDto = new ReadSlideshowDto(slideshow);

			const defaultImageInterval = this.configService.get(
				DEFAULT_IMAGE_INTERVAL,
				{ infer: true },
			);
			const defaultVideoInterval = this.configService.get(
				DEFAULT_VIDEO_INTERVAL,
				{ infer: true },
			);

			return res.render('slideshows/[id]', {
				slideshow: slideshowDto,
				defaultImageInterval,
				defaultVideoInterval,
			});
		} catch (error) {
			return res.redirect('/slideshows');
		}
	}
}
