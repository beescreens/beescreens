import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'nestjs-prisma';

@Injectable()
export class SlideshowsService {
	constructor(private readonly prisma: PrismaService) {}

	async getSlideshows() {
		return await this.prisma.slideshow.findMany({
			include: {
				slides: true,
			},
		});
	}

	async getSlideshow(slideshowId: string) {
		const slideshow = await this.prisma.slideshow.findFirstOrThrow({
			where: {
				id: {
					equals: slideshowId,
				},
			},
			include: {
				slides: true,
			},
		});

		return slideshow;
	}

	async createSlideshow(createSlideshow: Prisma.SlideshowCreateInput) {
		const newSlideshow = await this.prisma.slideshow.create({
			data: {
				...createSlideshow,
			},
			include: {
				slides: true,
			},
		});

		return newSlideshow;
	}

	async updateSlideshow(
		slideshowId: string,
		updateSlideshow: Prisma.SlideshowUpdateInput,
	) {
		const updatedSlideshow = await this.prisma.slideshow.update({
			where: {
				id: slideshowId,
			},
			data: {
				...updateSlideshow,
			},
			include: {
				slides: true,
			},
		});

		return updatedSlideshow;
	}

	async deleteSlideshow(slideshowId: string) {
		await this.prisma.slideshow.delete({
			where: {
				id: slideshowId,
			},
		});
	}
}
