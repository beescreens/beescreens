import { ApiProperty } from '@nestjs/swagger';
import {
	IsString,
	IsUUID,
	IsNotEmpty,
	IsArray,
	ValidateNested,
} from 'class-validator';
import { Slideshow } from '@prisma/client';
import { ReadSlideDto } from '@/slides/dtos/read-slide.dto';
import { Type } from 'class-transformer';

export class SlideshowDto implements Slideshow {
	// ID of the slideshow
	@ApiProperty({ format: 'uuid' })
	@IsUUID()
	id: string;

	// Name of the slideshow
	@IsString()
	@IsNotEmpty()
	name: string;

	// Slides of the slideshow
	@IsArray()
	@ValidateNested({ each: true })
	@Type(() => ReadSlideDto)
	slides: ReadSlideDto[];
}
