import { OmitType } from '@nestjs/swagger';
import { SlideshowDto } from './slideshow.dto';

export class UpdateSlideshowDto extends OmitType(SlideshowDto, [
	'id',
] as const) {}
