import { OmitType } from '@nestjs/swagger';
import { SlideshowDto } from './slideshow.dto';

export class CreateSlideshowDto extends OmitType(SlideshowDto, [
	'id',
] as const) {}
