import { ReadSlideDto } from '@/slides/dtos/read-slide.dto';
import { SlideshowDto } from './slideshow.dto';

export class ReadSlideshowDto extends SlideshowDto {
	constructor(partial?: Partial<SlideshowDto>) {
		super();

		if (partial) {
			if (partial.slides) {
				partial.slides = partial.slides.map((slide) => new ReadSlideDto(slide));
			}

			Object.assign(this, partial);
		}
	}
}
