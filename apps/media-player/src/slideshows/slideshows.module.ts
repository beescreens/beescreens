import { Module } from '@nestjs/common';
import { PrismaModule } from 'nestjs-prisma';
import { SlideshowsApiController } from './slideshows-api.controller';
import { SlideshowsViewsController } from './slideshows-views.controller';
import { SlideshowsService } from './slideshows.service';
import { ConfigModule } from '@nestjs/config';

@Module({
	imports: [ConfigModule, PrismaModule],
	controllers: [SlideshowsApiController, SlideshowsViewsController],
	providers: [SlideshowsService],
})
export class SlideshowsModule {}
