import { Controller, Param, Body } from '@nestjs/common';
import { CreateSlideshowDto } from './dtos/create-slideshow.dto';
import { GetMany } from '@/common/decorators/get-many.decorator';
import { SlideshowsService } from './slideshows.service';
import { ReadSlideshowDto } from './dtos/read-slideshow.dto';
import { GetOne } from '@/common/decorators/get-one.decorator';
import { Post } from '@/common/decorators/post.decorator';
import { Patch } from '@/common/decorators/patch.decorator';
import { UpdateSlideshowDto } from './dtos/update-slideshow.dto';
import { Delete } from '@/common/decorators/delete.decorator';
import { ApiTags } from '@nestjs/swagger';
import { ApiKeyAuth } from '@/auth/api-key/api-key-auth.decorator';

@ApiTags('Slideshows')
@Controller('api/slideshows')
export class SlideshowsApiController {
	constructor(private readonly slideshowsService: SlideshowsService) {}

	@GetMany({
		name: 'Slideshows',
		summary: 'Get the slideshows',
		operationId: 'getSlideshowsApi',
		responseType: [ReadSlideshowDto],
	})
	async getSlideshowsApi() {
		const slideshows = await this.slideshowsService.getSlideshows();

		const slideshowsDto = slideshows.map(
			(slideshow) => new ReadSlideshowDto(slideshow),
		);

		return slideshowsDto;
	}

	@GetOne({
		name: 'Slideshow',
		summary: 'Get the specified slideshow',
		operationId: 'getSlideshowApi',
		responseType: ReadSlideshowDto,
	})
	async getSlideshowApi(@Param('id') id: string) {
		const slideshow = await this.slideshowsService.getSlideshow(id);

		return new ReadSlideshowDto(slideshow);
	}

	@Post({
		name: 'Slideshow',
		summary: 'Create a new slideshow',
		bodyType: CreateSlideshowDto,
		responseType: ReadSlideshowDto,
		operationId: 'createSlideshowApi',
	})
	@ApiKeyAuth()
	async createSlideshowApi(@Body() createSlideshow: CreateSlideshowDto) {
		const newSlideshow = await this.slideshowsService.createSlideshow({
			...createSlideshow,
			slides: {
				create: createSlideshow.slides,
			},
		});

		return new ReadSlideshowDto(newSlideshow);
	}

	@Patch({
		name: 'Slideshow',
		summary: 'Update the specified slideshow',
		bodyType: UpdateSlideshowDto,
		responseType: ReadSlideshowDto,
		operationId: 'updateSlideshowApi',
	})
	@ApiKeyAuth()
	async updateSlideshowApi(
		@Param('id') id: string,
		@Body() updateSlideshow: UpdateSlideshowDto,
	) {
		const updatedSlideshow = await this.slideshowsService.updateSlideshow(id, {
			...updateSlideshow,
			slides: {
				deleteMany: {},
				create: updateSlideshow.slides,
			},
		});

		return new ReadSlideshowDto(updatedSlideshow);
	}

	@Delete({
		name: 'Slideshow',
		summary: 'Delete the specified slideshow',
		operationId: 'deleteSlideshowApi',
	})
	@ApiKeyAuth()
	async deleteSlideshowApi(@Param('id') id: string) {
		await this.slideshowsService.deleteSlideshow(id);
	}
}
