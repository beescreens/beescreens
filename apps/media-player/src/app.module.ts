import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { SlideshowsModule } from './slideshows/slideshows.module';
import { PrismaModule } from 'nestjs-prisma';
import { ConfigModule } from './config/config.module';
import { AuthModule } from './auth/auth.module';

@Module({
	imports: [AuthModule, ConfigModule, PrismaModule.forRoot(), SlideshowsModule],
	controllers: [AppController],
})
export class AppModule {}
