export const API_KEY = 'MEDIA_PLAYER_API_KEY';
export const DEFAULT_IMAGE_INTERVAL = 'MEDIA_PLAYER_DEFAULT_IMAGE_INTERVAL';
export const DEFAULT_VIDEO_INTERVAL = 'MEDIA_PLAYER_DEFAULT_VIDEO_INTERVAL';
export const NODE_ENV = 'NODE_ENV';

export interface EnvironmentVariables {
	[API_KEY]: string;
	[DEFAULT_IMAGE_INTERVAL]: number;
	[DEFAULT_VIDEO_INTERVAL]: number;
	[NODE_ENV]: string;
}
