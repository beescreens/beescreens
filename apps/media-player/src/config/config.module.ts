import * as Joi from 'joi';
import { Module } from '@nestjs/common';
import { ConfigModule as NestConfigModule } from '@nestjs/config';
import { ConfigConfiguration } from './config.configuration';
import {
	API_KEY,
	DEFAULT_IMAGE_INTERVAL,
	DEFAULT_VIDEO_INTERVAL,
	NODE_ENV,
} from './config.constants';

@Module({
	imports: [
		NestConfigModule.forRoot({
			load: [ConfigConfiguration],
			validationSchema: Joi.object({
				[API_KEY]: Joi.string().required(),
				[DEFAULT_IMAGE_INTERVAL]: Joi.number().required().min(0),
				[DEFAULT_VIDEO_INTERVAL]: Joi.string().required().min(0),
				[NODE_ENV]: Joi.string()
					.valid('development', 'test', 'production')
					.default('production'),
			}),
			validationOptions: {
				allowUnknown: true,
				abortEarly: false,
			},
		}),
	],
	exports: [NestConfigModule],
})
export class ConfigModule {}
