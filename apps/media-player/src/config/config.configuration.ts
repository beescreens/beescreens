import {
	API_KEY,
	DEFAULT_IMAGE_INTERVAL,
	DEFAULT_VIDEO_INTERVAL,
	NODE_ENV,
} from './config.constants';

export const ConfigConfiguration = () => ({
	[API_KEY]: process.env.MEDIA_PLAYER_API_KEY as string,
	[DEFAULT_IMAGE_INTERVAL]: parseInt(
		process.env.MEDIA_PLAYER_DEFAULT_IMAGE_INTERVAL as string,
		10,
	),
	[DEFAULT_VIDEO_INTERVAL]: parseInt(
		process.env.MEDIA_PLAYER_DEFAULT_VIDEO_INTERVAL as string,
		10,
	),
	[NODE_ENV]: process.env.NODE_ENV as string,
});
