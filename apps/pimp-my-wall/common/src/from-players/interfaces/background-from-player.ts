import { HexColor } from '../../common';
import { FromPlayer } from './from-player';

export type BackgroundFromPlayer = FromPlayer & {
	data: {
		backgroundColor: HexColor;
	};
};
