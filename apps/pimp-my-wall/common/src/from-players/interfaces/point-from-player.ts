import { Point } from '../../points';
import { FromPlayer } from './from-player';

export type PointFromPlayer = FromPlayer & {
	data: {
		point: Point;
	};
};
