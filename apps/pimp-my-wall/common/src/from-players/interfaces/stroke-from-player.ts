import { Stroke } from '../../strokes';
import { FromPlayer } from './from-player';

export type StrokeFromPlayer = FromPlayer & {
	data: {
		stroke: Stroke;
	};
};
