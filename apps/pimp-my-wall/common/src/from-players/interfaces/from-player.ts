import { PlayerId } from '../../players';

export type FromPlayer = {
	playerId: PlayerId;
};
