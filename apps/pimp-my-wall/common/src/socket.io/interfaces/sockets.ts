import { Server, Socket as ServerSocket } from 'socket.io';
import { Socket as ClientSocket } from 'socket.io-client';
import { PlayerId } from '../../players';
import { ClientToServerEvents } from './client-to-server-events';
import { ServerToClientEvents } from './server-to-client-events';
import { GameId } from '../../games';

interface ServerSideEvents {
	ping: () => void;
}

interface SocketData {
	playerId: PlayerId;
	gameId?: GameId;
}

export type PimpMyWallBackendSocket = ServerSocket<
	ClientToServerEvents,
	ServerToClientEvents,
	ServerSideEvents,
	SocketData
>;

export type PimpMyWallFrontendSocket = ClientSocket<
	ServerToClientEvents,
	ClientToServerEvents
>;

export type PimpMyWallSocketServer = Server<
	ClientToServerEvents,
	ServerToClientEvents,
	ServerSideEvents,
	SocketData
>;
