import {
	BackgroundFromPlayer,
	FromPlayer,
	PointFromPlayer,
	StrokeFromPlayer,
	UndoFromPlayer,
} from '../../from-players';
import {
	ReadGameWithCurrentNumberOfPlayers,
	ReadGameWithPlayers,
} from '../../games';
import { ReadPlayer } from '../../players';

export enum ServerEvent {
	AVAILABLE_GAMES = 'available-games',
	BACKGROUND_COLOR_FROM_PLAYER = 'background-from-player',
	CONNECT = 'connect',
	CONNECT_ERROR = 'connect_error',
	DISCONNECT = 'disconnect',
	NEW_PLAYER = 'new-player',
	RESET = 'reset',
	STROKE_CONTINUE_FROM_PLAYER = 'continue-stroke-from-player',
	STROKE_END_FROM_PLAYER = 'end-stroke-from-player',
	STROKE_START_FROM_PLAYER = 'start-stroke-from-player',
	UNDO_FROM_PLAYER = 'undo-from-player',
}

export interface ServerToClientEvents {
	[ServerEvent.AVAILABLE_GAMES]: (
		availableGames: ReadGameWithCurrentNumberOfPlayers[],
	) => void;
	[ServerEvent.BACKGROUND_COLOR_FROM_PLAYER]: (
		data: BackgroundFromPlayer,
	) => void;
	[ServerEvent.NEW_PLAYER]: (data: ReadPlayer) => void;
	[ServerEvent.RESET]: (data?: ReadGameWithPlayers) => void;
	[ServerEvent.STROKE_CONTINUE_FROM_PLAYER]: (data: PointFromPlayer) => void;
	[ServerEvent.STROKE_END_FROM_PLAYER]: (data: FromPlayer) => void;
	[ServerEvent.STROKE_START_FROM_PLAYER]: (data: StrokeFromPlayer) => void;
	[ServerEvent.UNDO_FROM_PLAYER]: (data: UndoFromPlayer) => void;
}
