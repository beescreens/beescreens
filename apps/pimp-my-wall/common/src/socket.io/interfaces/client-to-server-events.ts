import { ReadGameWithCurrentNumberOfPlayers } from '../../games';
import { ReadGameWithPlayers } from '../../games';
import { HexColor, JoinPayload } from '../../common';
import { ReadPoint } from '../../points';
import { ReadStroke } from '../../strokes';

export enum ClientEvent {
	BACKGROUND_COLOR = 'background-color',
	GET_AVAILABLE_GAMES = 'get-available-games',
	JOIN = 'join',
	STROKE_CONTINUE = 'stroke-continue',
	STROKE_END = 'stroke-end',
	STROKE_START = 'stroke-start',
	UNDO = 'undo',
}

export interface ClientToServerEvents {
	[ClientEvent.BACKGROUND_COLOR]: (data: HexColor) => void;
	[ClientEvent.GET_AVAILABLE_GAMES]: (
		data: undefined,
		callback: (response: ReadGameWithCurrentNumberOfPlayers[]) => void,
	) => void;
	[ClientEvent.JOIN]: (
		data: JoinPayload,
		callback: (response: ReadGameWithPlayers) => void,
	) => void;
	[ClientEvent.STROKE_CONTINUE]: (data: ReadPoint) => void;
	[ClientEvent.STROKE_END]: () => void;
	[ClientEvent.STROKE_START]: (data: ReadStroke) => void;
	[ClientEvent.UNDO]: () => void;
}
