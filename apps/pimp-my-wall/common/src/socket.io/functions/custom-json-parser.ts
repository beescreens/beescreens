/*
This blog post was helpful to understand how to check if a Date object is a Date
object using the `this[key]` instead of the `value` of the JSON.stringify
replacer that already has converted the Date to a string:

https://alexwlchan.net/2023/preserved-dates/
*/

import {
	Encoder as SocketIoEncoder,
	Decoder as SocketIoDecoder,
} from 'socket.io-parser';

enum DataType {
	DATE = 'date',
	MAP = 'map',
	SET = 'set',
}

class Encoder extends SocketIoEncoder {
	constructor() {
		super(function (this: any, key: string) {
			const value = this[key];

			if (value instanceof Date) {
				return {
					dataType: DataType.DATE,
					value: value.toISOString(),
				};
			} else if (value instanceof Map) {
				return {
					dataType: DataType.MAP,
					value: [...value],
				};
			} else if (value instanceof Set) {
				return {
					dataType: DataType.SET,
					value: [...value],
				};
			}

			return value;
		});
	}
}

class Decoder extends SocketIoDecoder {
	constructor() {
		super(function (this, key) {
			const value = this[key];

			if (typeof value === 'object' && value !== null) {
				if (value.dataType === DataType.DATE) {
					return new Date(value.value);
				} else if (value.dataType === DataType.MAP) {
					return new Map(value.value);
				} else if (value.dataType === DataType.SET) {
					return new Set(value.value);
				}
			}

			return value;
		});
	}
}
export const customJsonParser = { Encoder, Decoder };
