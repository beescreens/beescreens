import { GameId } from '../../games';

export interface JoinPayload {
	gameId: GameId;
	isDisplay?: boolean;
}
