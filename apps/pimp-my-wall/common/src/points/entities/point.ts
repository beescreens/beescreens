import { Coordinate } from '../../common';

export type Point = {
	timestamp?: Date;
	x: Coordinate;
	y: Coordinate;
};
