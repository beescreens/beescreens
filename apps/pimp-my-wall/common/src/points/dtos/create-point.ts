import { Point } from '../entities/point';

export type CreatePoint = Omit<Point, 'id' | 'timestamp'>;
