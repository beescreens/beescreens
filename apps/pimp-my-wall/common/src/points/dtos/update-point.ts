import { Point } from '../entities/point';

export type UpdatePoint = Partial<Omit<Point, 'id' | 'timestamp'>>;
