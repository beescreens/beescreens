type Message = {
	message: string;
};

export class PointNotFoundException extends Error {
	private description = 'Point not found';

	constructor(message: string) {
		super(message);

		Object.setPrototypeOf(this, PointNotFoundException.prototype);
	}

	getErrorMessage(): Message {
		if (this.message) {
			return {
				message: `${this.description}. Message: ${this.message}`,
			};
		}

		return {
			message: this.description,
		};
	}
}
