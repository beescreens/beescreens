import { Stroke } from '../../strokes';
import { PlayerId } from './player-id';

export type Player = {
	id: PlayerId;
	strokes: Stroke[];
	timestamp: Date;
};
