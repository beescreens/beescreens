type Message = {
	message: string;
};

export class PlayerNotFoundException extends Error {
	private description = 'Player not found';

	constructor(message: string) {
		super(message);

		Object.setPrototypeOf(this, PlayerNotFoundException.prototype);
	}

	getErrorMessage(): Message {
		if (this.message) {
			return {
				message: `${this.description}. Message: ${this.message}`,
			};
		}

		return {
			message: this.description,
		};
	}
}
