import { Player } from '../entities/player';

export type UpdatePlayer = Partial<Omit<Player, 'id' | 'timestamp'>>;
