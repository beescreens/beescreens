import { Player } from '../entities/player';

export type ReadPlayer = Player;
