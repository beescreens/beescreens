import { Player } from '../entities/player';

export type CreatePlayer = Omit<Player, 'id' | 'timestamp'>;
