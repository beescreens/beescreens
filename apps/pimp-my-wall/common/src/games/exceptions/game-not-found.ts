type Message = {
	message: string;
};

export class GameNotFoundException extends Error {
	private description = 'Game not found';

	constructor(message: string) {
		super(message);

		Object.setPrototypeOf(this, GameNotFoundException.prototype);
	}

	getErrorMessage(): Message {
		if (this.message) {
			return {
				message: `${this.description}. Message: ${this.message}`,
			};
		}

		return {
			message: this.description,
		};
	}
}
