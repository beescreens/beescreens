type Message = {
	message: string;
};

export class GameFullException extends Error {
	private description = 'Game full';

	constructor(message: string) {
		super(message);

		Object.setPrototypeOf(this, GameFullException.prototype);
	}

	getErrorMessage(): Message {
		if (this.message) {
			return {
				message: `${this.description}. Message: ${this.message}`,
			};
		}

		return {
			message: this.description,
		};
	}
}
