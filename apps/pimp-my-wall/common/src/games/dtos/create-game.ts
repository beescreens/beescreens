import { Game } from '../entities/game';

export type CreateGame = Omit<Game, 'id' | 'createdAt' | 'updatedAt'>;
