import { ReadPlayer } from '../../players';
import { ReadGameWithCurrentNumberOfPlayers } from './read-game-with-current-number-of-players';

export type ReadGameWithPlayers = ReadGameWithCurrentNumberOfPlayers & {
	players: ReadPlayer[];
};
