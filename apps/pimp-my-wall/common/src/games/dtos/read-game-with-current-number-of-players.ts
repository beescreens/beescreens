import { ReadGame } from './read-game';

export type ReadGameWithCurrentNumberOfPlayers = ReadGame & {
	currentNumberOfPlayers: number;
};
