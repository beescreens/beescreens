import { Game } from '../entities/game';

export type UpdateGame = Partial<
	Omit<Game, 'id' | 'currentNumberOfPlayers' | 'createdAt' | 'updatedAt'>
>;
