import { HexColor } from '../../common';
import { GameId } from './game-id';

export type Game = {
	id: GameId;
	name: string;
	maximumNumberOfPlayers: number;
	canvasWidth: number;
	canvasHeight: number;
	strokeColor: HexColor;
	backgroundColor: HexColor;
	createdAt: Date;
	updatedAt: Date;
};
