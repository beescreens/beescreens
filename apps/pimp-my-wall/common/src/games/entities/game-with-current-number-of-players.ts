import { Game } from './game';

export type GameWithCurrentNumberOfPlayers = Game & {
	currentNumberOfPlayers: number;
};
