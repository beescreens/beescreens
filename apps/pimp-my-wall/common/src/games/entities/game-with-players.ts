import { Player, PlayerId } from '../../players';
import { GameWithCurrentNumberOfPlayers } from './game-with-current-number-of-players';

export type GameWithPlayers = GameWithCurrentNumberOfPlayers & {
	players: Map<PlayerId, Player>;
};
