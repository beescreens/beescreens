import { HexColor } from '../../common';
import { Point } from '../../points';

export type Stroke = {
	timestamp?: Date;
	points?: Point[];
	color: HexColor;
	size: number;
};
