import { Stroke } from '../entities/stroke';

export type CreateStroke = Omit<Stroke, 'timestamp' | 'points'>;
