import { Stroke } from '../entities/stroke';

export type ReadStroke = Stroke;
