import { Stroke } from '../entities/stroke';

export type UpdateStroke = Partial<Omit<Stroke, 'timestamp' | 'points'>>;
