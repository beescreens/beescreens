type Message = {
	message: string;
};

export class StrokeNotFoundException extends Error {
	private description = 'Stroke not found';

	constructor(message: string) {
		super(message);

		Object.setPrototypeOf(this, StrokeNotFoundException.prototype);
	}

	getErrorMessage(): Message {
		if (this.message) {
			return {
				message: `${this.description}. Message: ${this.message}`,
			};
		}

		return {
			message: this.description,
		};
	}
}
