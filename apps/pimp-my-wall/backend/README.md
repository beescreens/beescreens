# Pimp My Wall backend

## Prerequisites

The following prerequisites must be filled to run this service:

- [Node.js](../../../docs/explanations/about-nodejs/index.md) must be installed.
- [pnpm](../../../docs/explanations/about-pnpm/index.md) must be installed.
- [Docker](../../../docs/explanations/about-docker/index.md) must be installed (optional, only for deployment testing).
- [Docker Compose](../../../docs/explanations/about-docker/index.md) must be installed (optional, only for deployment testing).

## Set up the service

```sh
# Install the dependencies
pnpm install

# Copy the .env.defaults file to a .env file.
pnpm setup:env

# Edit the environment variables file if needed
vim .env
```

## Start the service in development mode

```sh
# Start PostgreSQL in the background
docker compose up -d

# Start the service in development mode
pnpm dev
```

The service should be now accessible on [http://localhost:4000](http://localhost:4000)

## Build the service

```sh
# Build the service
pnpm build
```

## Test the service

```sh
# Test the service
pnpm test
```

## Start the service in production mode

```sh
# Start the service in production mode
pnpm start
```

## Test with Docker Compose

In order to test with Docker Compose, execute the following steps:

```sh
# Switch to the deployment directory
cd ../../../deployment/pimp-my-wall

# Copy the `docker-compose.override.development.yaml` to `docker-compose.override.yaml`
cp docker-compose.override.development.yaml docker-compose.override.yaml

# Build the service
pnpm --filter @beescreens/pimp-my-wall-backend build

# Bundle the service
pnpm --filter @beescreens/pimp-my-wall-backend bundle

# Build the Docker image for this service
docker compose build backend

# Start the Docker container for this service
docker compose up backend

# Optionally, seed the database with default data
docker compose exec backend npm run prisma:seed
```
