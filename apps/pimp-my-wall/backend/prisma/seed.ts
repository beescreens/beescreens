import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

async function main() {
	await prisma.game.upsert({
		where: { name: '🌸 Cherry' },
		update: {},
		create: {
			name: '🌸 Cherry',
			maximumNumberOfPlayers: 5,
			canvasWidth: 1024,
			canvasHeight: 1536,
			strokeColor: '#3a3335',
			backgroundColor: '#ff9adc',
		},
	});

	await prisma.game.upsert({
		where: { name: '🌺 Hibiscus' },
		update: {},
		create: {
			name: '🌺 Hibiscus',
			maximumNumberOfPlayers: 8,
			canvasWidth: 2048,
			canvasHeight: 1536,
			strokeColor: '#38686a',
			backgroundColor: '#55a8f9',
		},
	});

	await prisma.game.upsert({
		where: { name: '🪷 Lotus' },
		update: {},
		create: {
			name: '🪷 Lotus',
			maximumNumberOfPlayers: 12,
			canvasWidth: 2048,
			canvasHeight: 2304,
			strokeColor: '#726da8',
			backgroundColor: '#ff9adc',
		},
	});

	await prisma.game.upsert({
		where: { name: '🦄 Unicorn' },
		update: {},
		create: {
			name: '🦄 Unicorn',
			maximumNumberOfPlayers: 15,
			canvasWidth: 2048,
			canvasHeight: 2304,
			strokeColor: '#f75c03',
			backgroundColor: '#55a8f9',
		},
	});

	console.log('Database seeded with default data');
}
main()
	.then(async () => {
		await prisma.$disconnect();
	})
	.catch(async (e) => {
		console.error(e);
		await prisma.$disconnect();
		process.exit(1);
	});
