/*
  Warnings:

  - Added the required column `stroke_color` to the `games` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "games" ADD COLUMN     "stroke_color" TEXT NOT NULL;
