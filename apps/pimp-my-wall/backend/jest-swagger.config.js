const transformer = require('@nestjs/swagger/plugin');

module.exports.name = 'nestjs-swagger-transformer';

// you should change the version number anytime you change the configuration below - otherwise, jest will not detect changes
module.exports.version = 1;

module.exports.factory = (cs) => {
	return transformer.before(
		{
			dtoFileNameSuffix: ['.dtos.ts', '.dto.ts', '.entity.ts'],
			controllerFileNameSuffix: ['.controller.ts'],
			classValidatorShim: true,
			introspectComments: true,
		},
		cs.program,
	);
};
