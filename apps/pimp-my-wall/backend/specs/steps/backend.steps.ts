// TODO: Remove callbacks when this issue is closed: https://github.com/bencompton/jest-cucumber/issues/127
import { ContextIdFactory } from '@nestjs/core';
import { DefineStepFunction } from 'jest-cucumber';
import { Nest, PlayerGame } from '../helpers';
import { GameId } from '@beescreens/pimp-my-wall';
import { GameGateway } from '@/games/game.gateway';
import { GamesService } from '@/games/games.service';

export const theBackendHasAGameReadyWithIdOne = (
	defineStepFunction: DefineStepFunction,
	backendCallback: () => Nest,
	gameIdCallback: () => GameId,
): void =>
	defineStepFunction(
		/^the backend has a game ready with ID '(.*)'$/,
		async () => {
			const backend = backendCallback();
			const gameId = gameIdCallback();

			const contextId = ContextIdFactory.create();

			jest
				.spyOn(ContextIdFactory, 'getByRequest')
				.mockImplementation(() => contextId);

			const service = await backend.app.resolve(GamesService, contextId);

			const game = service.getGameFromDatabase(gameId);

			expect(game).toMatchSnapshot();
		},
	);

export const theBackendHasTwoGamesReadyWithIdsOneAndTwo = (
	defineStepFunction: DefineStepFunction,
	backendCallback: () => Nest,
	gameId1Callback: () => GameId,
	gameId2Callback: () => GameId,
): void =>
	defineStepFunction(
		/^the backend has two games ready with IDs '(.*)' and '(.*)'$/,
		async () => {
			const backend = backendCallback();
			const gameId1 = gameId1Callback();
			const gameId2 = gameId2Callback();

			const contextId = ContextIdFactory.create();

			jest
				.spyOn(ContextIdFactory, 'getByRequest')
				.mockImplementation(() => contextId);

			const service = await backend.app.resolve(GamesService, contextId);

			const game1 = service.getGameFromDatabase(gameId1);
			const game2 = service.getGameFromDatabase(gameId2);

			expect(game1).toMatchSnapshot();
			expect(game2).toMatchSnapshot();
		},
	);

export const backendAsksToResetTheGame = (
	defineStepFunction: DefineStepFunction,
	backendCallback: () => Nest,
	player1Callback: () => PlayerGame,
	player2Callback: () => PlayerGame,
): void =>
	defineStepFunction('the backend asks to reset the game', async () => {
		const backend = backendCallback();
		const player1 = player1Callback();
		const player2 = player2Callback();

		const contextId = ContextIdFactory.create();

		jest
			.spyOn(ContextIdFactory, 'getByRequest')
			.mockImplementation(() => contextId);

		const gateway = await backend.app.resolve(GameGateway, contextId);

		const promises = Promise.all([
			new Promise<void>((resolve) => player1.onResetEvent(resolve)),
			new Promise<void>((resolve) => player2.onResetEvent(resolve)),
		]);

		gateway.resetGames();

		return promises;
	});
