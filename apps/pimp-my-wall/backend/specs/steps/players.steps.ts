// TODO: Remove callbacks when this issue is closed: https://github.com/bencompton/jest-cucumber/issues/127
import { GameId } from '@beescreens/pimp-my-wall';
import { ContextIdFactory } from '@nestjs/core';
import { DefineStepFunction } from 'jest-cucumber';
import { Nest, PlayerGame } from '../helpers';
import { GamesService } from '@/games/games.service';

const joinTheGame = async (gameId: GameId, player: PlayerGame): Promise<void> =>
	new Promise<void>((resolve) => {
		player.onGameEvent(resolve);
		player.emitJoinEvent(gameId);
		player.connect();
	});

const updateTheListOfGames = async (player: PlayerGame): Promise<void> =>
	new Promise<void>((resolve) => {
		player.onGamesEvent(resolve);
		player.emitGamesEvent();
		player.connect();
	});

export const playerJoinsTheGameWithId = (
	defineStepFunction: DefineStepFunction,
	playerCallback: () => PlayerGame,
	gameIdCallback: () => GameId,
): void =>
	defineStepFunction(/^(.*) joins the game with ID '(.*)'$/, () => {
		const player = playerCallback();
		const gameId = gameIdCallback();

		return joinTheGame(gameId, player);
	});

export const playerHasJoinedTheGameWithId = (
	defineStepFunction: DefineStepFunction,
	playerCallback: () => PlayerGame,
	gameIdCallback: () => GameId,
): void =>
	defineStepFunction(/^(.*) has joined the game with ID '(.*)'$/, () => {
		const player = playerCallback();
		const gameId = gameIdCallback();

		return joinTheGame(gameId, player);
	});

export const playerJoinsPlayerInTheGameWithId = (
	defineStepFunction: DefineStepFunction,
	player1Callback: () => PlayerGame,
	player2Callback: () => PlayerGame,
	gameIdCallback: () => GameId,
): void =>
	defineStepFunction(/^(.*) joins (.*) in the game with ID '(.*)'$/, () => {
		const player1 = player1Callback();
		const player2 = player2Callback();
		const gameId = gameIdCallback();

		return Promise.all([
			new Promise<void>((resolve) => {
				player1.onPlayerEvent(resolve);
				player1.connect();
			}),

			joinTheGame(gameId, player2),
		]);
	});

export const playerAndPlayerHaveJoinedTheGameWithId = (
	defineStepFunction: DefineStepFunction,
	player1Callback: () => PlayerGame,
	player2Callback: () => PlayerGame,
	gameIdCallback: () => GameId,
): void =>
	defineStepFunction(
		/^(.*) and (.*) have joined the game with ID '(.*)'$/,
		async () => {
			const player1 = player1Callback();
			const player2 = player2Callback();
			const gameId = gameIdCallback();

			player1.connect();
			player2.connect();

			return Promise.all([
				new Promise<void>((resolve) => {
					player1.onPlayerEvent(resolve);
				}),
				new Promise<void>((resolve) => {
					player1.onGameEvent(resolve);
					player1.emitJoinEvent(gameId);
				}),
				new Promise<void>((resolve) => {
					player2.onGameEvent(resolve);
					player2.emitJoinEvent(gameId);
				}),
			]);
		},
	);

export const playerCannotJoinTheGameWithId = (
	defineStepFunction: DefineStepFunction,
	playerCallback: () => PlayerGame,
	gameIdCallback: () => GameId,
): void =>
	defineStepFunction(/^(.*) cannot join the game with ID '(.*)'$/, () => {
		const player = playerCallback();
		const gameId = gameIdCallback();

		return new Promise<void>((resolve) => {
			player.onException(resolve);
			player.emitJoinEvent(gameId);
			player.connect();
		});
	});

export const playerGetsTheStateOfGameWithId = (
	defineStepFunction: DefineStepFunction,
	playerCallback: () => PlayerGame,
): void =>
	defineStepFunction(/^(.*) gets the state of game with ID '(.*)'$/, () => {
		const player = playerCallback();

		expect(player.gameData).toMatchSnapshot();
	});

export const playerUpdatesItsGameState = (
	defineStepFunction: DefineStepFunction,
	playerCallback: () => PlayerGame,
): void =>
	defineStepFunction(/^(.*) updates its game state$/, () => {
		const player = playerCallback();

		expect(player.gameData).toMatchSnapshot();
	});

export const theStateOfGameWithIdIsUnaffected = (
	defineStepFunction: DefineStepFunction,
	backendCallback: () => Nest,
	gameIdCallback: () => GameId,
): void =>
	defineStepFunction(
		/^the state of game with ID '(.*)' is unaffected$/,
		async () => {
			const backend = backendCallback();
			const gameId = gameIdCallback();

			const contextId = ContextIdFactory.create();

			jest
				.spyOn(ContextIdFactory, 'getByRequest')
				.mockImplementation(() => contextId);

			const service = await backend.app.resolve(GamesService, contextId);

			const game = service.getGameFromDatabase(gameId);

			expect(game).toMatchSnapshot();
		},
	);

export const playerAndPlayerUpdateTheirGameState = (
	defineStepFunction: DefineStepFunction,
	player1Callback: () => PlayerGame,
	player2Callback: () => PlayerGame,
): void =>
	defineStepFunction(/^(.*) and (.*) update their game state$/, () => {
		const player1 = player1Callback();
		const player2 = player2Callback();

		expect(player1.gameData).toMatchSnapshot();
		expect(player2.gameData).toMatchSnapshot();
	});

export const playerAndPlayerAreKickedFromGame = (
	defineStepFunction: DefineStepFunction,
	backendCallback: () => Nest,
	player1Callback: () => PlayerGame,
	player2Callback: () => PlayerGame,
	gameIdCallback: () => GameId,
): void =>
	defineStepFunction(
		/^(.*) and (.*) are kicked from game with ID '(.*)'$/,
		async () => {
			const gameId = gameIdCallback();
			const backend = backendCallback();

			const contextId = ContextIdFactory.create();

			jest
				.spyOn(ContextIdFactory, 'getByRequest')
				.mockImplementation(() => contextId);

			const service = await backend.app.resolve(GamesService, contextId);

			const game = service.getGameFromDatabase(gameId);

			expect(game).toMatchSnapshot();
		},
	);

export const playerChangesTheGameBackground = (
	defineStepFunction: DefineStepFunction,
	player1Callback: () => PlayerGame,
	player2Callback: () => PlayerGame,
): void =>
	defineStepFunction(
		/^(.*) changes the game background$/,
		async () =>
			new Promise<void>((resolve) => {
				const player1 = player1Callback();
				const player2 = player2Callback();

				player2.onBackgroundEvent(resolve);

				player1.emitBackgroundColorEvent('#123abc');
			}),
	);

const startAStroke = async (player1: PlayerGame, player2: PlayerGame) =>
	new Promise<void>((resolve) => {
		player2.onPlayerPointEvent(resolve);

		player1.emitPointEvent({
			first: true,
			x: 42,
			y: 24,
			size: 12,
			color: '#FFFFFF',
			timestamp: Date.now() - player1.timeDifference,
		});
	});

export const playerStartsAStroke = (
	defineStepFunction: DefineStepFunction,
	player1Callback: () => PlayerGame,
	player2Callback: () => PlayerGame,
): void =>
	defineStepFunction(/^(.*) starts a stroke$/, () => {
		const player1 = player1Callback();
		const player2 = player2Callback();

		return startAStroke(player1, player2);
	});

export const playerHasStartedAStroke = (
	defineStepFunction: DefineStepFunction,
	player1Callback: () => PlayerGame,
	player2Callback: () => PlayerGame,
): void =>
	defineStepFunction(/^(.*) has started a stroke$/, () => {
		const player1 = player1Callback();
		const player2 = player2Callback();

		return startAStroke(player1, player2);
	});

export const playerContinuesAStroke = (
	defineStepFunction: DefineStepFunction,
	player1Callback: () => PlayerGame,
	player2Callback: () => PlayerGame,
): void =>
	defineStepFunction(
		/^(.*) continues its stroke$/,
		async () =>
			new Promise<void>((resolve) => {
				const player1 = player1Callback();
				const player2 = player2Callback();

				player2.onPlayerPointEvent(resolve);
				player1.emitPointEvent({
					x: 42,
					y: 24,
					timestamp: Date.now() - player1.timeDifference,
				});
			}),
	);

export const playerCancelsAStroke = (
	defineStepFunction: DefineStepFunction,
	player1Callback: () => PlayerGame,
	player2Callback: () => PlayerGame,
): void =>
	defineStepFunction(
		/^(.*) cancels its stroke$/,
		async () =>
			new Promise<void>((resolve) => {
				const player1 = player1Callback();
				const player2 = player2Callback();

				player2.onPlayerUndoEvent(resolve);
				player1.emitUndoEvent();
			}),
	);

export const playerEndsItsStroke = (
	defineStepFunction: DefineStepFunction,
	player1Callback: () => PlayerGame,
	player2Callback: () => PlayerGame,
): void =>
	defineStepFunction(
		/^(.*) ends its stroke$/,
		async () =>
			new Promise<void>((resolve) => {
				const player1 = player1Callback();
				const player2 = player2Callback();

				player2.onPlayerEndStrokeEvent(resolve);
				player1.emitEndStrokeEvent();
			}),
	);

export const playerAsksForTheListOfGames = (
	defineStepFunction: DefineStepFunction,
	playerCallback: () => PlayerGame,
): void =>
	defineStepFunction(/^(.*) asks for the list of games$/, async () => {
		const player = playerCallback();

		return updateTheListOfGames(player);
	});

export const playerGetsTheListOfGames = (
	defineStepFunction: DefineStepFunction,
	playerCallback: () => PlayerGame,
): void =>
	defineStepFunction(/^(.*) gets the list of games$/, () => {
		const player = playerCallback();

		expect(player.availableGames).toMatchSnapshot();
	});

export const playerUpdatesTheListOfGames = (
	defineStepFunction: DefineStepFunction,
	playerCallback: () => PlayerGame,
): void =>
	defineStepFunction(/^(.*) updates the list of games$/, async () => {
		const player = playerCallback();

		await updateTheListOfGames(player);

		expect(player.availableGames).toMatchSnapshot();
	});

playerUpdatesTheListOfGames;
