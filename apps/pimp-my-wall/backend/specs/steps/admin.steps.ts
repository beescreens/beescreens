// TODO: Remove callbacks when this issue is closed: https://github.com/bencompton/jest-cucumber/issues/127
import request from 'supertest';
import { ContextIdFactory } from '@nestjs/core';
import { DefineStepFunction } from 'jest-cucumber';
import { Nest, PlayerGame } from '../helpers';
import { ConfigService } from '@nestjs/config';
import { GameId } from '@beescreens/pimp-my-wall';
import { API_KEY } from '@/config/config.constants';

export const adminHasTheApiKeyToAdministrateTheGames = (
	defineStepFunction: DefineStepFunction,
	backendCallback: () => Nest,
): void =>
	defineStepFunction(
		'an admin has the API key to administrate the games',
		async () => {
			const backend = backendCallback();

			const contextId = ContextIdFactory.create();

			jest
				.spyOn(ContextIdFactory, 'getByRequest')
				.mockImplementation(() => contextId);

			const config = await backend.app.resolve(ConfigService, contextId);

			expect(config.get<string>(API_KEY)).toBeDefined();
		},
	);

export const adminAsksToResetTheGame = (
	defineStepFunction: DefineStepFunction,
	backendCallback: () => Nest,
	player1Callback: () => PlayerGame,
	player2Callback: () => PlayerGame,
): void =>
	defineStepFunction(
		/^an admin asks to reset the game with ID '(.*)'$/,
		async (gameId: GameId) => {
			const backend = backendCallback();
			const player1 = player1Callback();
			const player2 = player2Callback();

			const contextId = ContextIdFactory.create();

			jest
				.spyOn(ContextIdFactory, 'getByRequest')
				.mockImplementation(() => contextId);

			const config = await backend.app.resolve(ConfigService, contextId);

			const promises = Promise.all([
				new Promise<void>((resolve) => player1.onResetEvent(resolve)),
				new Promise<void>((resolve) => player2.onResetEvent(resolve)),
			]);

			await request(backend.server)
				.post(`/api/v1/admin/reset-game/${gameId}`)
				.set('Authorization', config.get<string>(API_KEY) as string)
				.send();

			return promises;
		},
	);

export const adminAsksToKickAllPlayers = (
	defineStepFunction: DefineStepFunction,
	backendCallback: () => Nest,
	player1Callback: () => PlayerGame,
	player2Callback: () => PlayerGame,
): void =>
	defineStepFunction(
		/^an admin asks to kick all players from a game with ID '(.*)'$/,
		async (gameId: GameId) => {
			const backend = backendCallback();
			const player1 = player1Callback();
			const player2 = player2Callback();

			const contextId = ContextIdFactory.create();

			jest
				.spyOn(ContextIdFactory, 'getByRequest')
				.mockImplementation(() => contextId);

			const config = await backend.app.resolve(ConfigService, contextId);

			const promises = Promise.all([
				new Promise<void>((resolve) => player1.onKickEvent(resolve)),
				new Promise<void>((resolve) => player2.onKickEvent(resolve)),
			]);

			await request(backend.server)
				.post(`/api/v1/admin/kick-all/${gameId}`)
				.set('Authorization', config.get<string>(API_KEY) as string)
				.send();

			return promises;
		},
	);
