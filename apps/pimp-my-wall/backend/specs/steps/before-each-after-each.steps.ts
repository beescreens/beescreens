import * as jestDateMock from 'jest-date-mock';
import { Nest, PlayerGame, setupNest, teardownNest } from '../helpers';

export const setupOnePlayer = async (): Promise<{
	nest: Nest;
	playerGame: PlayerGame;
}> => {
	jestDateMock.advanceTo(new Date('2021-07-12T20:54:33.014Z'));

	const nest = await setupNest();
	const playerGame = new PlayerGame(nest);

	return {
		nest,
		playerGame,
	};
};

export const teardownOnePlayer = async (
	nest: Nest,
	playerGame: PlayerGame,
): Promise<void> => {
	playerGame.close();

	await teardownNest(nest);

	jestDateMock.clear();
};

export const setupTwoPlayers = async (): Promise<{
	nest: Nest;
	playerGame1: PlayerGame;
	playerGame2: PlayerGame;
}> => {
	jestDateMock.advanceTo(new Date('2021-07-12T20:54:33.014Z'));

	const nest = await setupNest();
	const playerGame1 = new PlayerGame(nest);
	const playerGame2 = new PlayerGame(nest);

	return {
		nest,
		playerGame1,
		playerGame2,
	};
};

export const teardownTwoPlayers = async (
	nest: Nest,
	playerGame1: PlayerGame,
	playerGame2: PlayerGame,
): Promise<void> => {
	playerGame1.close();
	playerGame2.close();

	await teardownNest(nest);

	jestDateMock.clear();
};

export const setupThreePlayers = async (): Promise<{
	nest: Nest;
	playerGame1: PlayerGame;
	playerGame2: PlayerGame;
	playerGame3: PlayerGame;
}> => {
	jestDateMock.advanceTo(new Date('2021-07-12T20:54:33.014Z'));

	const nest = await setupNest();
	const playerGame1 = new PlayerGame(nest);
	const playerGame2 = new PlayerGame(nest);
	const playerGame3 = new PlayerGame(nest);

	return {
		nest,
		playerGame1,
		playerGame2,
		playerGame3,
	};
};

export const teardownThreePlayers = async (
	nest: Nest,
	playerGame1: PlayerGame,
	playerGame2: PlayerGame,
	playerGame3: PlayerGame,
): Promise<void> => {
	playerGame1.close();
	playerGame2.close();
	playerGame3.close();

	await teardownNest(nest);

	jestDateMock.clear();
};
