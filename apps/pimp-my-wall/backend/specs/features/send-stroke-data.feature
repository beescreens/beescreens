Feature: Send stroke data

Background:
	Given the backend has a game ready with ID '1'
	And Alice and Bob have joined the game with ID '1'

Scenario: A user starts a new stroke
	When Alice starts a stroke
	Then Alice and Bob update their game state

Scenario: A user continues a stroke
	And Alice has started a stroke
	When Alice continues its stroke
	Then Alice and Bob update their game state
