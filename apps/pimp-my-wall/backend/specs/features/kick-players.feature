Feature: Kick Players

Background:
  Given the backend has a game ready with ID '1'
  And Alice and Bob have joined the game with ID '1'
  And an admin has the API key to administrate the games

Scenario: The admin asks to kick all players
  When an admin asks to kick all players from a game with ID '1'
  Then Alice and Bob are kicked from game with ID '1'
