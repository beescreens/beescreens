Feature: Send background data

Scenario: A user changes the game background
	Given the backend has a game ready with ID '1'
	And Alice and Bob have joined the game with ID '1'
	When Alice changes the game background
	Then Alice and Bob update their game state
