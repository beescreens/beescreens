Feature: Create Game

Background:
  Given the backend has a no game
	An admin who has the Api Key to administrate the game

Scenario: The admin want to create a game
  When an admin asks to create a game with name "Hello"
	Then a game with name "Hello" is created
