Feature: Games

Scenario: A user gets the list of games
	When Alice asks for the list of games
	Then Alice gets the list of games

Scenario: A user updates the list of games when another user joins a game
	Given the backend has two games ready with IDs '1' and '2'
	And Alice joins the game with ID '1'
	When Bob joins the game with ID '2'
	Then Alice updates the list of games
