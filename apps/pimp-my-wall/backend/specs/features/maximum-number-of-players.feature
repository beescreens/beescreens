Feature: Maximum number of players

Background:
	Given the backend has two games ready with IDs '1' and '2'

Scenario: The player cannot join a game that is full
	Given Alice joins the game with ID '1'
	And Bob joins the game with ID '1'
	Then Carol cannot join the game with ID '1'
