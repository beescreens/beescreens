Feature: Undo

Background:
	Given the backend has a game ready with ID '1'
	And Alice and Bob have joined the game with ID '1'
	And Alice has started a stroke

Scenario: A user cancels their stroke
	When Alice cancels its stroke
	Then Alice and Bob update their game state
