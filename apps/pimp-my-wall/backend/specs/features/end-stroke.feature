Feature: End Stroke

Background:
	Given the backend has a game ready with ID '1'
	And Alice and Bob have joined the game with ID '1'
	And Alice has started a stroke
	And Alice continues its stroke
	And Alice continues its stroke

Scenario: A user ends its stroke
	When Alice ends its stroke
	Then Alice and Bob update their game state
