Feature: Join a game

Background:
	Given the backend has two games ready with IDs '1' and '2'

Scenario: The player joins the game with ID '1' and gets the game state
	When Alice joins the game with ID '1'
	Then Alice gets the state of game with ID '1'
	And the state of game with ID '2' is unaffected

Scenario: A player joins a game with ID '1' and other players updates their game state
	And Alice has joined the game with ID '1'
	When Bob joins Alice in the game with ID '1'
	Then Alice and Bob update their game state
	And the state of game with ID '2' is unaffected
