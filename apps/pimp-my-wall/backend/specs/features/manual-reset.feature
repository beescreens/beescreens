Feature: Manual reset

Background:
	Given the backend has a game ready with ID '1'
	And Alice and Bob have joined the game with ID '1'
	And Alice has started a stroke
	And an admin has the API key to administrate the games

Scenario: The admin asks to reset the game
	When an admin asks to reset the game with ID '1'
	Then Alice and Bob update their game state
