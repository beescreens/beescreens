Feature: Reset

Background:
	Given the backend has a game ready with ID '1'
	And Alice and Bob have joined the game with ID '1'
	And Alice has started a stroke

Scenario: The backend asks to reset the game
	When the backend asks to reset the game
	Then Alice and Bob update their game state
