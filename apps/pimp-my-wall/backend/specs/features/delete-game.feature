Feature: Delete Game

Background:
  Given the backend has a no game
	An admin who has the Api Key to administrate the game

Scenario: The admin want to delete a game named "Destroy Me"
  When an admin asks to delete a game with name "Destroy Me"
	Then a game with name "Destroy Me" is deleted
