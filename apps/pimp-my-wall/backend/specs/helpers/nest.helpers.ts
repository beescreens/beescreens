import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { moduleMetadata } from '@/app.module';
import { bootstrap } from '@/bootstrap';

export interface Nest {
	app: INestApplication;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	server: any;
	serverAddress: string;
}

export const setupNest = async (): Promise<Nest> => {
	const moduleFixture: TestingModule = await Test.createTestingModule(
		moduleMetadata,
	).compile();

	let app = moduleFixture.createNestApplication();

	app = await bootstrap(app);

	const server = app.getHttpServer();

	const { address, port } = server.listen().address();

	const serverAddress = `http://[${address}]:${port}`;

	await app.init();

	return {
		app,
		server,
		serverAddress,
	};
};

export const teardownNest = async (nest: Nest): Promise<void> => {
	await nest.app.close();
};
