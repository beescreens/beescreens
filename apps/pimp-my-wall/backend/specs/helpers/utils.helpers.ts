export const getNextId = ((playerNumber) => () => {
	playerNumber += 1;
	return `${playerNumber}`;
})(0);
