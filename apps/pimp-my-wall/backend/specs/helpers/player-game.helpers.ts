import {
	HexColor,
	Event,
	GameData,
	optimizeStroke,
	PlayerId,
	PointFromPlayer,
	AbstractPoint,
	PlayerData,
	Milliseconds,
	GameDataSubset,
	Payload,
	GameId,
} from '@beescreens/pimp-my-wall';
import { deserialize } from 'serialize-anything';
import { io, Socket } from 'socket.io-client';
import { Nest } from './nest.helpers';

export class PlayerGame {
	socket: Socket;

	availableGames!: GameDataSubset[];

	yourself!: PlayerId;

	timeDifference!: number;

	gameData!: GameData;

	constructor(nest: Nest) {
		this.socket = io(nest.serverAddress, {
			forceNew: true,
			autoConnect: false,
		});
	}

	connect(): void {
		this.socket.connect();
	}

	close(): void {
		this.socket.close();
	}

	onGameEvent(resolve: (value: void | PromiseLike<void>) => void): void {
		this.socket.on(Event.GAME, async (payload: Payload) => {
			const gameData = deserialize(payload) as GameData;

			const yourself = gameData.yourself as PlayerId;

			const timestamp = (gameData.playersData.get(yourself) as PlayerData)
				.timestamp as Milliseconds;

			this.yourself = yourself;
			this.gameData = gameData;
			this.timeDifference = Date.now() - timestamp;

			resolve();
		});
	}

	onGamesEvent(resolve: (value: void | PromiseLike<void>) => void): void {
		this.socket.on(
			Event.AVAILABLE_GAMES,
			async (availableGames: GameDataSubset[]) => {
				this.availableGames = availableGames;

				resolve();
			},
		);
	}

	onPlayerEvent(resolve: (value: void | PromiseLike<void>) => void): void {
		this.socket.on(Event.NEW_PLAYER, async (playerId: PlayerId) => {
			this.gameData.playersData.set(playerId, {
				timestamp: Date.now(),
				strokes: [],
			});

			resolve();
		});
	}

	onBackgroundEvent(resolve: (value: void | PromiseLike<void>) => void): void {
		this.socket.on(
			Event.BACKGROUND_COLOR_FROM_PLAYER,
			async (backgroundColor: HexColor) => {
				this.gameData.backgroundColor = backgroundColor;

				resolve();
			},
		);
	}

	onPlayerPointEvent(resolve: (value: void | PromiseLike<void>) => void): void {
		this.socket.on(
			Event.NEW_POINT_FROM_PLAYER,
			async ({ playerId, point }: PointFromPlayer) => {
				const strokes = this.gameData.playersData.get(playerId)?.strokes;

				if (strokes) {
					if (point.first) {
						strokes.push([]);
					}

					const lastStroke = strokes[strokes.length - 1];

					lastStroke.push(point);
				}

				resolve();
			},
		);
	}

	emitGamesEvent(): void {
		this.socket.emit(Event.REQUEST_GAMES);
	}

	emitJoinEvent(gameId: GameId): void {
		this.socket.emit(Event.JOIN_GAME, gameId);
	}

	emitBackgroundColorEvent(backgroundColor: HexColor): void {
		this.gameData.backgroundColor = backgroundColor;

		this.socket.emit(Event.BACKGROUND_COLOR, backgroundColor);
	}

	emitPointEvent(point: AbstractPoint): void {
		const strokes = this.gameData.playersData.get(
			this.yourself as PlayerId,
		)?.strokes;

		if (strokes) {
			if (point.first) {
				strokes.push([]);
			}

			const lastStroke = strokes[strokes.length - 1];

			lastStroke.push(point);
		}

		this.socket.emit(Event.NEW_POINT, point);
	}

	onException(resolve: (value: void | PromiseLike<void>) => void): void {
		this.socket.on(Event.EXCEPTION, async () => {
			resolve();
		});
	}

	onPlayerUndoEvent(resolve: (value: void | PromiseLike<void>) => void): void {
		this.socket.on(Event.UNDO_FROM_PLAYER, async (playerId: PlayerId) => {
			const strokes = this.gameData.playersData.get(playerId)?.strokes;

			if (strokes && strokes.length) {
				strokes.pop();
			}

			resolve();
		});
	}

	emitUndoEvent(): void {
		const strokes = this.gameData.playersData.get(
			this.yourself as PlayerId,
		)?.strokes;

		if (strokes && strokes.length) {
			strokes.pop();
		}

		this.socket.emit(Event.UNDO);
	}

	onPlayerEndStrokeEvent(
		resolve: (value: void | PromiseLike<void>) => void,
	): void {
		this.socket.on(Event.END_STROKE_FROM_PLAYER, async (playerId: PlayerId) => {
			const strokes = this.gameData.playersData.get(playerId)?.strokes;

			if (strokes && strokes.length) {
				// Optimisation of the last stroke
				const lastStrokeIndex = strokes.length - 1;
				const lastStroke = strokes[lastStrokeIndex];
				strokes[lastStrokeIndex] = optimizeStroke(lastStroke, 4);
			}
			resolve();
		});
	}

	emitEndStrokeEvent(): void {
		const strokes = this.gameData.playersData.get(
			this.yourself as PlayerId,
		)?.strokes;

		if (strokes && strokes.length) {
			// Optimisation of the last stroke
			const lastStrokeIndex = strokes.length - 1;
			const lastStroke = strokes[lastStrokeIndex];
			strokes[lastStrokeIndex] = optimizeStroke(lastStroke, 4);
		}

		this.socket.emit(Event.END_STROKE);
	}

	onResetEvent(resolve: (value: PromiseLike<void> | void) => void): void {
		this.socket.on(Event.RESET, async (payload: Payload) => {
			const gameData = deserialize(payload) as GameData;

			this.gameData = gameData;

			resolve();
		});
	}

	onKickEvent(resolve: (value: PromiseLike<void> | void) => void): void {
		this.socket.on(Event.KICK, async () => {
			resolve();
		});
	}
}
