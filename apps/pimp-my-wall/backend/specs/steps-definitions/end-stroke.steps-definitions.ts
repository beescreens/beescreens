import { defineFeature, loadFeature } from 'jest-cucumber';
import { getNextId, Nest, PlayerGame } from '../helpers';
import {
	teardownTwoPlayers,
	setupTwoPlayers,
	playerAndPlayerHaveJoinedTheGameWithId,
	playerHasStartedAStroke,
	playerContinuesAStroke,
	playerEndsItsStroke,
	playerAndPlayerUpdateTheirGameState,
	theBackendHasAGameReadyWithIdOne,
} from '../steps';

jest.mock('nanoid', () => ({
	nanoid: jest.fn().mockImplementation(() => getNextId()),
}));

const feature = loadFeature('./specs/features/end-stroke.feature');

defineFeature(feature, (test) => {
	let backend: Nest;
	let alice: PlayerGame;
	let bob: PlayerGame;

	test('A user ends its stroke', ({ given, and, when, then }) => {
		theBackendHasAGameReadyWithIdOne(
			given,
			() => backend,
			() => '1',
		);
		playerAndPlayerHaveJoinedTheGameWithId(
			and,
			() => alice,
			() => bob,
			() => '1',
		);
		playerHasStartedAStroke(
			and,
			() => alice,
			() => bob,
		);
		playerContinuesAStroke(
			and,
			() => alice,
			() => bob,
		);
		playerContinuesAStroke(
			and,
			() => alice,
			() => bob,
		);
		playerEndsItsStroke(
			when,
			() => alice,
			() => bob,
		);
		playerAndPlayerUpdateTheirGameState(
			then,
			() => alice,
			() => bob,
		);
	});

	beforeEach(
		async () =>
			({
				nest: backend,
				playerGame1: alice,
				playerGame2: bob,
			} = await setupTwoPlayers()),
	);

	afterEach(async () => await teardownTwoPlayers(backend, alice, bob));
});
