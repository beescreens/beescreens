import { defineFeature, loadFeature } from 'jest-cucumber';
import {
	adminAsksToKickAllPlayers,
	adminHasTheApiKeyToAdministrateTheGames,
} from '../steps/admin.steps';
import { getNextId, Nest, PlayerGame } from '../helpers';
import {
	teardownTwoPlayers,
	setupTwoPlayers,
	playerAndPlayerHaveJoinedTheGameWithId,
	playerAndPlayerAreKickedFromGame,
	theBackendHasAGameReadyWithIdOne,
} from '../steps';

jest.mock('nanoid', () => ({
	nanoid: jest.fn().mockImplementation(() => getNextId()),
}));

const feature = loadFeature('./specs/features/kick-players.feature');

defineFeature(feature, async (test) => {
	let backend: Nest;
	let alice: PlayerGame;
	let bob: PlayerGame;

	test('The admin asks to kick all players', ({ given, and, when, then }) => {
		theBackendHasAGameReadyWithIdOne(
			given,
			() => backend,
			() => '1',
		);
		playerAndPlayerHaveJoinedTheGameWithId(
			and,
			() => alice,
			() => bob,
			() => '1',
		);
		adminHasTheApiKeyToAdministrateTheGames(and, () => backend);
		adminAsksToKickAllPlayers(
			when,
			() => backend,
			() => alice,
			() => bob,
		);
		playerAndPlayerAreKickedFromGame(
			then,
			() => backend,
			() => alice,
			() => bob,
			() => '1',
		);
	});

	beforeEach(
		async () =>
			({
				nest: backend,
				playerGame1: alice,
				playerGame2: bob,
			} = await setupTwoPlayers()),
	);

	afterEach(async () => await teardownTwoPlayers(backend, alice, bob));
});
