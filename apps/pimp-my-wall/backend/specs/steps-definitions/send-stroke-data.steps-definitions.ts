import { defineFeature, loadFeature } from 'jest-cucumber';
import { getNextId, Nest, PlayerGame } from '../helpers';
import {
	setupTwoPlayers,
	teardownTwoPlayers,
	playerAndPlayerHaveJoinedTheGameWithId,
	playerAndPlayerUpdateTheirGameState,
	playerStartsAStroke,
	playerHasStartedAStroke,
	playerContinuesAStroke,
	theBackendHasAGameReadyWithIdOne,
} from '../steps';

jest.mock('nanoid', () => ({
	nanoid: jest.fn().mockImplementation(() => getNextId()),
}));

const feature = loadFeature('./specs/features/send-stroke-data.feature');

defineFeature(feature, (test) => {
	let backend: Nest;
	let alice: PlayerGame;
	let bob: PlayerGame;

	test('A user starts a new stroke', ({ given, and, when, then }) => {
		theBackendHasAGameReadyWithIdOne(
			given,
			() => backend,
			() => '1',
		);
		playerAndPlayerHaveJoinedTheGameWithId(
			and,
			() => alice,
			() => bob,
			() => '1',
		);
		playerStartsAStroke(
			when,
			() => alice,
			() => bob,
		);
		playerAndPlayerUpdateTheirGameState(
			then,
			() => alice,
			() => bob,
		);
	});

	test('A user continues a stroke', ({ given, and, when, then }) => {
		theBackendHasAGameReadyWithIdOne(
			given,
			() => backend,
			() => '1',
		);
		playerAndPlayerHaveJoinedTheGameWithId(
			and,
			() => alice,
			() => bob,
			() => '1',
		);
		playerHasStartedAStroke(
			and,
			() => alice,
			() => bob,
		);
		playerContinuesAStroke(
			when,
			() => alice,
			() => bob,
		);
		playerAndPlayerUpdateTheirGameState(
			then,
			() => alice,
			() => bob,
		);
	});

	beforeEach(
		async () =>
			({
				nest: backend,
				playerGame1: alice,
				playerGame2: bob,
			} = await setupTwoPlayers()),
	);

	afterEach(async () => await teardownTwoPlayers(backend, alice, bob));
});
