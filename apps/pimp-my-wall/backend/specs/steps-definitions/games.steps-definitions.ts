import { defineFeature, loadFeature } from 'jest-cucumber';
import { getNextId, Nest, PlayerGame } from '../helpers';
import {
	playerAsksForTheListOfGames,
	playerGetsTheListOfGames,
	playerJoinsTheGameWithId,
	playerUpdatesTheListOfGames,
	setupTwoPlayers,
	teardownTwoPlayers,
	theBackendHasTwoGamesReadyWithIdsOneAndTwo,
} from '../steps';

jest.mock('nanoid', () => ({
	nanoid: jest.fn().mockImplementation(() => getNextId()),
}));

const feature = loadFeature('./specs/features/games.feature');

defineFeature(feature, async (test) => {
	let backend: Nest;
	let alice: PlayerGame;
	let bob: PlayerGame;

	test('A user gets the list of games', ({ when, then }) => {
		playerAsksForTheListOfGames(when, () => alice);
		playerGetsTheListOfGames(then, () => alice);
	});

	test('A user updates the list of games when another user joins a game', ({
		given,
		and,
		when,
		then,
	}) => {
		theBackendHasTwoGamesReadyWithIdsOneAndTwo(
			given,
			() => backend,
			() => '1',
			() => '2',
		);
		playerJoinsTheGameWithId(
			and,
			() => alice,
			() => '1',
		);
		playerJoinsTheGameWithId(
			when,
			() => bob,
			() => '2',
		);
		playerUpdatesTheListOfGames(then, () => alice);
	});

	beforeEach(
		async () =>
			({
				nest: backend,
				playerGame1: alice,
				playerGame2: bob,
			} = await setupTwoPlayers()),
	);

	afterEach(async () => await teardownTwoPlayers(backend, alice, bob));
});
