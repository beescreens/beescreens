import { defineFeature, loadFeature } from 'jest-cucumber';
import {
	adminAsksToResetTheGame,
	adminHasTheApiKeyToAdministrateTheGames,
} from '../steps/admin.steps';
import { getNextId, Nest, PlayerGame } from '../helpers';
import {
	teardownTwoPlayers,
	setupTwoPlayers,
	playerAndPlayerHaveJoinedTheGameWithId,
	playerHasStartedAStroke,
	playerAndPlayerUpdateTheirGameState,
	theBackendHasAGameReadyWithIdOne,
} from '../steps';

jest.mock('nanoid', () => ({
	nanoid: jest.fn().mockImplementation(() => getNextId()),
}));

const feature = loadFeature('./specs/features/manual-reset.feature');

defineFeature(feature, async (test) => {
	let backend: Nest;
	let alice: PlayerGame;
	let bob: PlayerGame;

	test('The admin asks to reset the game', ({ given, and, when, then }) => {
		theBackendHasAGameReadyWithIdOne(
			given,
			() => backend,
			() => '1',
		);
		playerAndPlayerHaveJoinedTheGameWithId(
			and,
			() => alice,
			() => bob,
			() => '1',
		);
		playerHasStartedAStroke(
			and,
			() => alice,
			() => bob,
		);
		adminHasTheApiKeyToAdministrateTheGames(and, () => backend);
		adminAsksToResetTheGame(
			when,
			() => backend,
			() => alice,
			() => bob,
		);
		playerAndPlayerUpdateTheirGameState(
			then,
			() => alice,
			() => bob,
		);
	});

	beforeEach(
		async () =>
			({
				nest: backend,
				playerGame1: alice,
				playerGame2: bob,
			} = await setupTwoPlayers()),
	);

	afterEach(async () => await teardownTwoPlayers(backend, alice, bob));
});
