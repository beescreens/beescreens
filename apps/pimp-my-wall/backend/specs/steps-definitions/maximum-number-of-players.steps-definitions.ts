import { defineFeature, loadFeature } from 'jest-cucumber';
import { getNextId, Nest, PlayerGame } from '../helpers';
import {
	theBackendHasTwoGamesReadyWithIdsOneAndTwo,
	playerJoinsTheGameWithId,
	setupThreePlayers,
	teardownThreePlayers,
	playerCannotJoinTheGameWithId,
} from '../steps';

jest.mock('nanoid', () => ({
	nanoid: jest.fn().mockImplementation(() => getNextId()),
}));

const feature = loadFeature(
	'./specs/features/maximum-number-of-players.feature',
);

defineFeature(feature, async (test) => {
	let backend: Nest;
	let alice: PlayerGame;
	let bob: PlayerGame;
	let carol: PlayerGame;

	test('The player cannot join a game that is full', ({ given, and, then }) => {
		theBackendHasTwoGamesReadyWithIdsOneAndTwo(
			given,
			() => backend,
			() => '1',
			() => '2',
		);
		playerJoinsTheGameWithId(
			given,
			() => alice,
			() => '1',
		);
		playerJoinsTheGameWithId(
			and,
			() => bob,
			() => '1',
		);
		playerCannotJoinTheGameWithId(
			then,
			() => carol,
			() => '1',
		);
	});

	beforeEach(
		async () =>
			({
				nest: backend,
				playerGame1: alice,
				playerGame2: bob,
				playerGame3: carol,
			} = await setupThreePlayers()),
	);

	afterEach(async () => await teardownThreePlayers(backend, alice, bob, carol));
});
