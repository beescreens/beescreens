import { defineFeature, loadFeature } from 'jest-cucumber';
import {
	teardownTwoPlayers,
	setupTwoPlayers,
	playerChangesTheGameBackground,
	playerAndPlayerUpdateTheirGameState,
	playerAndPlayerHaveJoinedTheGameWithId,
	theBackendHasAGameReadyWithIdOne,
} from '../steps';
import { getNextId, Nest, PlayerGame } from '../helpers';
import {} from 'specs/steps/players.steps';

jest.mock('nanoid', () => ({
	nanoid: jest.fn().mockImplementation(() => getNextId()),
}));

const feature = loadFeature('./specs/features/send-background-data.feature');

defineFeature(feature, (test) => {
	let backend: Nest;
	let alice: PlayerGame;
	let bob: PlayerGame;

	test('A user changes the game background', ({ given, and, when, then }) => {
		theBackendHasAGameReadyWithIdOne(
			given,
			() => backend,
			() => '1',
		);
		playerAndPlayerHaveJoinedTheGameWithId(
			and,
			() => alice,
			() => bob,
			() => '1',
		);
		playerChangesTheGameBackground(
			when,
			() => alice,
			() => bob,
		);
		playerAndPlayerUpdateTheirGameState(
			then,
			() => alice,
			() => bob,
		);
	});

	beforeEach(
		async () =>
			({
				nest: backend,
				playerGame1: alice,
				playerGame2: bob,
			} = await setupTwoPlayers()),
	);

	afterEach(async () => await teardownTwoPlayers(backend, alice, bob));
});
