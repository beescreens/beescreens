import { defineFeature, loadFeature } from 'jest-cucumber';
import {
	teardownTwoPlayers,
	setupTwoPlayers,
	playerCancelsAStroke,
	playerAndPlayerHaveJoinedTheGameWithId,
	playerHasStartedAStroke,
	playerAndPlayerUpdateTheirGameState,
	theBackendHasAGameReadyWithIdOne,
} from '../steps';
import { getNextId, Nest, PlayerGame } from '../helpers';

jest.mock('nanoid', () => ({
	nanoid: jest.fn().mockImplementation(() => getNextId()),
}));

const feature = loadFeature('./specs/features/undo.feature');

defineFeature(feature, (test) => {
	let backend: Nest;
	let alice: PlayerGame;
	let bob: PlayerGame;

	test('A user cancels their stroke', ({ given, and, when, then }) => {
		theBackendHasAGameReadyWithIdOne(
			given,
			() => backend,
			() => '1',
		);
		playerAndPlayerHaveJoinedTheGameWithId(
			and,
			() => alice,
			() => bob,
			() => '1',
		);
		playerHasStartedAStroke(
			and,
			() => alice,
			() => bob,
		);
		playerCancelsAStroke(
			when,
			() => alice,
			() => bob,
		);
		playerAndPlayerUpdateTheirGameState(
			then,
			() => alice,
			() => bob,
		);
	});

	beforeEach(
		async () =>
			({
				nest: backend,
				playerGame1: alice,
				playerGame2: bob,
			} = await setupTwoPlayers()),
	);

	afterEach(async () => await teardownTwoPlayers(backend, alice, bob));
});
