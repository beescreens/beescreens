import { defineFeature, loadFeature } from 'jest-cucumber';
import { getNextId, Nest, PlayerGame } from '../helpers';
import {
	teardownTwoPlayers,
	setupTwoPlayers,
	playerAndPlayerHaveJoinedTheGameWithId,
	playerHasStartedAStroke,
	backendAsksToResetTheGame,
	playerAndPlayerUpdateTheirGameState,
	theBackendHasAGameReadyWithIdOne,
} from '../steps';

jest.mock('nanoid', () => ({
	nanoid: jest.fn().mockImplementation(() => getNextId()),
}));

const feature = loadFeature('./specs/features/reset.feature');

defineFeature(feature, async (test) => {
	let backend: Nest;
	let alice: PlayerGame;
	let bob: PlayerGame;

	test('The backend asks to reset the game', ({ given, and, when, then }) => {
		theBackendHasAGameReadyWithIdOne(
			given,
			() => backend,
			() => '1',
		);
		playerAndPlayerHaveJoinedTheGameWithId(
			and,
			() => alice,
			() => bob,
			() => '1',
		);
		playerHasStartedAStroke(
			and,
			() => alice,
			() => bob,
		);
		backendAsksToResetTheGame(
			when,
			() => backend,
			() => alice,
			() => bob,
		);
		playerAndPlayerUpdateTheirGameState(
			then,
			() => alice,
			() => bob,
		);
	});

	beforeEach(
		async () =>
			({
				nest: backend,
				playerGame1: alice,
				playerGame2: bob,
			} = await setupTwoPlayers()),
	);

	afterEach(async () => await teardownTwoPlayers(backend, alice, bob));
});
