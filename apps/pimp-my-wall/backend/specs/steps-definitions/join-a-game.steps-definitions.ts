import { defineFeature, loadFeature } from 'jest-cucumber';
import { getNextId, Nest, PlayerGame } from '../helpers';
import {
	teardownTwoPlayers,
	setupTwoPlayers,
	theBackendHasTwoGamesReadyWithIdsOneAndTwo,
	playerJoinsTheGameWithId,
	playerHasJoinedTheGameWithId,
	playerGetsTheStateOfGameWithId,
	playerAndPlayerUpdateTheirGameState,
	playerJoinsPlayerInTheGameWithId,
	theStateOfGameWithIdIsUnaffected,
} from '../steps';

jest.mock('nanoid', () => ({
	nanoid: jest.fn().mockImplementation(() => getNextId()),
}));

const feature = loadFeature('./specs/features/join-a-game.feature');

defineFeature(feature, async (test) => {
	let backend: Nest;
	let alice: PlayerGame;
	let bob: PlayerGame;

	test("The player joins the game with ID '1' and gets the game state", ({
		given,
		when,
		then,
		and,
	}) => {
		theBackendHasTwoGamesReadyWithIdsOneAndTwo(
			given,
			() => backend,
			() => '1',
			() => '2',
		);
		playerJoinsTheGameWithId(
			when,
			() => alice,
			() => '1',
		);
		playerGetsTheStateOfGameWithId(then, () => alice);
		theStateOfGameWithIdIsUnaffected(
			and,
			() => backend,
			() => '2',
		);
	});

	test("A player joins a game with ID '1' and other players updates their game state", ({
		given,
		when,
		then,
		and,
	}) => {
		theBackendHasTwoGamesReadyWithIdsOneAndTwo(
			given,
			() => backend,
			() => '1',
			() => '2',
		);
		playerHasJoinedTheGameWithId(
			and,
			() => alice,
			() => '1',
		);
		playerJoinsPlayerInTheGameWithId(
			when,
			() => alice,
			() => bob,
			() => '1',
		);
		playerAndPlayerUpdateTheirGameState(
			then,
			() => alice,
			() => bob,
		);
		theStateOfGameWithIdIsUnaffected(
			and,
			() => backend,
			() => '2',
		);
	});

	beforeEach(
		async () =>
			({
				nest: backend,
				playerGame1: alice,
				playerGame2: bob,
			} = await setupTwoPlayers()),
	);

	afterEach(async () => await teardownTwoPlayers(backend, alice, bob));
});
