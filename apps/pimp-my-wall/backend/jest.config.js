module.exports = {
	testEnvironment: 'node',
	preset: 'ts-jest',
	testMatch: ['./**/?(*.)+(spec|steps-definitions|test).ts'],
	coverageDirectory: 'coverage',
	testTimeout: 15000,
	transform: {
		'^.+\\.(t|j)s$': [
			'ts-jest',
			{
				astTransformers: {
					before: ['jest-swagger.config.js'],
				},
			},
		],
	},
	setupFiles: ['jest-date-mock'],
};
