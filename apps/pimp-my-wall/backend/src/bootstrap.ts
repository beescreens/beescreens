import compression from 'compression';
import {
	INestApplication,
	ValidationPipe,
	VersioningType,
} from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { HttpAdapterHost } from '@nestjs/core';
import { PrismaClientExceptionFilter, PrismaService } from 'nestjs-prisma';
import { HTTP_API_KEY } from '@/auth/auth.constants';

export const bootstrap = async (
	app: INestApplication,
): Promise<INestApplication> => {
	const { httpAdapter } = app.get(HttpAdapterHost);

	app.useGlobalFilters(new PrismaClientExceptionFilter(httpAdapter));

	app.useGlobalPipes(
		new ValidationPipe({
			transform: true,
		}),
	);

	app.enableVersioning({
		type: VersioningType.URI,
		prefix: 'api/v',
	});

	app.enableCors();

	const prismaService = app.get(PrismaService);
	await prismaService.enableShutdownHooks(app);

	const swaggerConfig = new DocumentBuilder()
		.setTitle('Pimp My Wall API')
		.setDescription(
			'Pimp My Wall is a BeeScreens app allowing players to draw on a (remote) canvas together.',
		)
		.setContact(
			'BeeScreens',
			'https://gitlab.com/beescreens',
			'contact@beescreens.ch',
		)
		.setVersion(process.env.npm_package_version as string)
		.setLicense(
			'AGPL-3.0 license',
			'https://gitlab.com/beescreens/beescreens/-/blob/main/apps/pimp-my-wall/COPYING',
		)
		.setExternalDoc(
			'Additional Documentation',
			'https://gitlab.com/beescreens/beescreens/-/blob/main/apps/pimp-my-wall',
		)
		.addApiKey(
			{
				type: 'apiKey',
				description: 'Authentication by API key',
				name: 'Authorization',
				in: 'header',
			},
			HTTP_API_KEY,
		)
		.build();

	const document = SwaggerModule.createDocument(app, swaggerConfig);

	SwaggerModule.setup('api', app, document, {
		// https://swagger.io/docs/open-source-tools/swagger-ui/usage/configuration/
		swaggerOptions: {
			showExtensions: true,
			tagsSorter: 'alpha',
			persistAuthorization: true,
		},
	});

	app.use(compression());

	return app;
};
