import { IsDateString } from 'class-validator';
import { Player, PlayerId, ReadStroke } from '@beescreens/pimp-my-wall';
import { ApiProperty } from '@nestjs/swagger';
import { ReadStrokeDto } from '@/strokes/dtos/read-stroke.dto';

export class PlayerDto implements Player {
	/**
	 * id of the player
	 */
	id: PlayerId;

	/**
	 * strokes of the player
	 */
	@ApiProperty({ type: [ReadStrokeDto] })
	strokes: ReadStroke[];

	/**
	 * timestamp of the player
	 */
	@IsDateString()
	timestamp: Date;

	constructor(entity: Player) {
		this.id = entity.id;
		this.strokes = entity.strokes.map((stroke) => new ReadStrokeDto(stroke));
		this.timestamp = entity.timestamp;
	}
}
