import { PlayerDto } from '@/players/dtos/player.dto';
import { Player, ReadPlayer } from '@beescreens/pimp-my-wall';

export class ReadPlayerDto extends PlayerDto implements ReadPlayer {
	constructor(entity: Player) {
		super(entity);
	}
}
