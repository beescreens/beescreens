/* eslint-disable @typescript-eslint/ban-types */
import { HTTP_API_KEY } from '@/auth/auth.constants';
import { HttpApiKeyGuard } from '@/auth/guards/http-api-key.guard';
import { applyDecorators, UseGuards, CanActivate } from '@nestjs/common';
import { ApiSecurity, ApiUnauthorizedResponse } from '@nestjs/swagger';

export const HttpApiKeyAuth = (
	...guards: (Function | CanActivate)[]
): (<TFunction extends Function, Y>(
	target: object | TFunction,
	propertyKey?: string | symbol | undefined,
	descriptor?: TypedPropertyDescriptor<Y> | undefined,
) => void) =>
	applyDecorators(
		UseGuards(HttpApiKeyGuard, ...guards),
		ApiSecurity(HTTP_API_KEY),
		ApiUnauthorizedResponse({
			description: 'Wrong API key.',
		}),
	);
