import { AuthService } from '@/auth/auth.service';
import { HttpApiKeyStrategy } from '@/auth/strategies/http-api-key.strategy';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

@Module({
	imports: [ConfigModule],
	providers: [AuthService, HttpApiKeyStrategy],
})
export class AuthModule {}
