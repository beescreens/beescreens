import { API_KEY } from '@/config/config.constants';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
	constructor(readonly configService: ConfigService) {}

	async validate(apiKey: string): Promise<boolean> {
		return apiKey === this.configService.get<string>(API_KEY);
	}
}
