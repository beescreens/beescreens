import { AuthGuard } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { HTTP_API_KEY_STRATEGY } from '@/auth/auth.constants';

@Injectable()
export class HttpApiKeyGuard extends AuthGuard(HTTP_API_KEY_STRATEGY) {}
