import { HeaderAPIKeyStrategy as ApiKeyStrategy } from 'passport-headerapikey';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from '@/auth/auth.service';
import { HTTP_API_KEY_STRATEGY } from '@/auth/auth.constants';

@Injectable()
export class HttpApiKeyStrategy extends PassportStrategy(
	ApiKeyStrategy,
	HTTP_API_KEY_STRATEGY,
) {
	constructor(readonly auth: AuthService) {
		super(
			{ header: 'Authorization' },
			true,
			async (
				apiKey: string,
				done: (error: Error | null, validated: boolean) => void,
			) => this.validate(apiKey, done),
		);
	}

	async validate(
		apiKey: string,
		done: (error: Error | null, validated: boolean) => void,
	): Promise<void> {
		const validApiKey = await this.auth.validate(apiKey);

		if (validApiKey) {
			done(null, true);
		}

		done(new UnauthorizedException(), false);
	}
}
