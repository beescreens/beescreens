import {
	GameFullException,
	GameNotFoundException,
	StrokeNotFoundException,
} from '@beescreens/pimp-my-wall';
import { ArgumentsHost, Catch } from '@nestjs/common';
import { BaseWsExceptionFilter, WsException } from '@nestjs/websockets';

@Catch()
export class WsAllExceptionsFilter extends BaseWsExceptionFilter {
	catch(exception: unknown, host: ArgumentsHost): void {
		if (
			exception instanceof GameNotFoundException ||
			exception instanceof StrokeNotFoundException ||
			exception instanceof GameFullException
		) {
			super.catch(new WsException(exception.getErrorMessage()), host);
		} else {
			super.catch(exception, host);
		}
	}
}
