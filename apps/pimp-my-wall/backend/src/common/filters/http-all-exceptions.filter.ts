import {
	GameNotFoundException,
	StrokeNotFoundException,
} from '@beescreens/pimp-my-wall';
import { Catch, ArgumentsHost, NotFoundException } from '@nestjs/common';
import { BaseExceptionFilter } from '@nestjs/core';

@Catch()
export class HttpAllExceptionsFilter extends BaseExceptionFilter {
	catch(exception: unknown, host: ArgumentsHost): void {
		if (
			exception instanceof GameNotFoundException ||
			exception instanceof StrokeNotFoundException
		) {
			super.catch(new NotFoundException(exception.getErrorMessage()), host);
		} else {
			super.catch(exception, host);
		}
	}
}
