type Message = {
	message: string;
};

export class CannotSaveGameException extends Error {
	private description = 'The game cannot be saved';

	constructor(message: string) {
		super(message);

		Object.setPrototypeOf(this, CannotSaveGameException.prototype);
	}

	getErrorMessage(): Message {
		if (this.message) {
			return {
				message: `${this.description}. Message: ${this.message}`,
			};
		}

		return {
			message: this.description,
		};
	}
}
