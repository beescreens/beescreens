import { IsDateString, Min } from 'class-validator';
import { Point } from '@beescreens/pimp-my-wall';

export class PointDto implements Point {
	/**
	 * timestamp of the point
	 */
	@IsDateString()
	timestamp?: Date;

	/**
	 * x coordinate of the point
	 */
	@Min(0)
	x: number;

	/**
	 * y coordinate of the point
	 */
	@Min(0)
	y: number;

	constructor(entity: Point) {
		this.timestamp = entity.timestamp;
		this.x = entity.x;
		this.y = entity.y;
	}
}
