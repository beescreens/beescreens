import { OmitType } from '@nestjs/swagger';
import { CreatePoint } from '@beescreens/pimp-my-wall';
import { PointDto } from '@/points/dtos/point.dto';

export class CreatePointDto
	extends OmitType(PointDto, ['timestamp'] as const)
	implements CreatePoint {}
