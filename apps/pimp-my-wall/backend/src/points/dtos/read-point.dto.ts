import { PointDto } from '@/points/dtos/point.dto';
import { Point, ReadPoint } from '@beescreens/pimp-my-wall';

export class ReadPointDto extends PointDto implements ReadPoint {
	constructor(entity: Point) {
		super(entity);
	}
}
