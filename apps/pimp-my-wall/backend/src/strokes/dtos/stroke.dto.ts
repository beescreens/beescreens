import { IsDateString, IsHexColor, Min } from 'class-validator';
import { Stroke, HexColor } from '@beescreens/pimp-my-wall';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { ReadPointDto } from '@/points/dtos/read-point.dto';

export class StrokeDto implements Stroke {
	/**
	 * timestamp of the stroke
	 */
	@IsDateString()
	timestamp?: Date;

	/**
	 * points of the stroke
	 */
	@ApiPropertyOptional({ type: [ReadPointDto] })
	points?: ReadPointDto[];

	/**
	 * color of the point
	 */
	@IsHexColor()
	color: HexColor;

	/**
	 * size of the point
	 */
	@Min(1)
	size: number;

	constructor(entity: Stroke) {
		this.timestamp = entity.timestamp;
		this.points = entity.points?.map((point) => new ReadPointDto(point));
		this.color = entity.color;
		this.size = entity.size;
	}
}
