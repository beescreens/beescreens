import { StrokeDto } from '@/strokes/dtos/stroke.dto';
import { ReadStroke, Stroke } from '@beescreens/pimp-my-wall';

export class ReadStrokeDto extends StrokeDto implements ReadStroke {
	constructor(entity: Stroke) {
		super(entity);
	}
}
