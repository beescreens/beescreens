import { StrokeDto } from '@/strokes/dtos/stroke.dto';
import { OmitType } from '@nestjs/swagger';
import { CreateStroke } from '@beescreens/pimp-my-wall';

export class CreateStrokeDto
	extends OmitType(StrokeDto, ['timestamp', 'points'] as const)
	implements CreateStroke {}
