import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { AppModule } from '@/app.module';
import { bootstrap } from '@/bootstrap';
import { ConfigService } from '@nestjs/config';
import { EnvironmentVariables, PORT } from '@/config/config.constants';

const main = async () => {
	const instance = await NestFactory.create(AppModule);
	const app = await bootstrap(instance);

	const configService = app.get(ConfigService<EnvironmentVariables, true>);
	const port = configService.get(PORT, { infer: true });

	await app.listen(port, '0.0.0.0');

	Logger.log(`Listening on http://0.0.0.0:${port}`, main.name);
};

main();
