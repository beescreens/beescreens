import { AppController } from '@/app.controller';
import { AuthModule } from '@/auth/auth.module';
import { GamesModule } from '@/games/games.module';
import { Module, ModuleMetadata } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

export const moduleMetadata: ModuleMetadata = {
	imports: [AuthModule, ConfigModule, GamesModule],
	controllers: [AppController],
};

@Module(moduleMetadata)
export class AppModule {}
