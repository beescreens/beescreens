import {
	Controller,
	UseFilters,
	Body,
	Param,
	HttpCode,
	Delete,
} from '@nestjs/common';
import {
	ApiBadRequestResponse,
	ApiNoContentResponse,
	ApiNotFoundResponse,
	ApiOperation,
	ApiParam,
	ApiTags,
} from '@nestjs/swagger';
import { HttpApiKeyAuth } from '../auth/decorators/http-api-key-auth.decorator';
import { HttpAllExceptionsFilter } from '../common/filters/http-all-exceptions.filter';
import { CreateGameDto } from '@/games/dtos/create-game.dto';
import { GamesService } from '@/games/games.service';
import { ReadGameDto } from '@/games/dtos/read-game.dto';
import { CustomPost } from '@/common/decorators/custom-post.decorator';
import { GetMany } from '@/common/decorators/get-many.decorator';
import { GetOne } from '@/common/decorators/get-one.decorator';
import { CustomPatch } from '@/common/decorators/custom-patch.decorator';
import { CustomDelete } from '@/common/decorators/custom-delete.decorator';
import { UpdateGameDto } from '@/games/dtos/update-game.dto';
import { MissingOrIncorrectFieldsResponse } from '@/common/openapi/responses';
import { GameId } from '@beescreens/pimp-my-wall';
import { GamesGateway } from '@/games/games.gateway';
import { ReadGameWithPlayersDto } from '@/games/dtos/read-game-with-players.dto';

@ApiTags('Games')
@Controller({
	path: 'games',
	version: '1',
})
@UseFilters(HttpAllExceptionsFilter)
export class GamesController {
	constructor(
		readonly gamesService: GamesService,
		readonly gamesGateway: GamesGateway,
	) {}

	@GetMany({
		name: 'Games',
		summary: 'Get the games',
		operationId: 'getGames',
		responseType: [ReadGameDto],
	})
	async getGames(): Promise<ReadGameDto[]> {
		const games = await this.gamesService.getGames();

		const gamesDto = games.map((game) => new ReadGameDto(game));

		return gamesDto;
	}

	@GetOne({
		name: 'Game',
		summary: 'Get the specified game',
		operationId: 'getGame',
		responseType: ReadGameDto,
	})
	async getGame(@Param('id') id: GameId): Promise<ReadGameDto> {
		const game = await this.gamesService.getGame(id);

		return new ReadGameDto(game);
	}

	@CustomPost({
		name: 'Game',
		summary: 'Create a new game',
		operationId: 'createGame',
		bodyType: CreateGameDto,
		responseType: ReadGameDto,
	})
	@HttpApiKeyAuth()
	async createGame(@Body() createGameDto: CreateGameDto): Promise<ReadGameDto> {
		const game = await this.gamesService.create(createGameDto);

		await this.gamesService.createCurrentGame(game);

		await this.gamesGateway.broadcastAvailableGames();

		return new ReadGameDto(game);
	}

	@CustomPatch({
		name: 'Game',
		summary: 'Update the specified game',
		bodyType: UpdateGameDto,
		responseType: ReadGameDto,
		operationId: 'updateGame',
	})
	@HttpApiKeyAuth()
	async updateGame(
		@Param('id') id: GameId,
		@Body() updateGame: UpdateGameDto,
	): Promise<ReadGameDto> {
		const updatedGame = await this.gamesService.update(id, {
			...updateGame,
		});

		await this.gamesService.updateCurrentGame(id, updatedGame);

		await this.gamesGateway.broadcastAvailableGames();

		return new ReadGameDto(updatedGame);
	}

	@CustomDelete({
		name: 'Game',
		summary: 'Delete the specified game',
		operationId: 'deleteGame',
	})
	@HttpApiKeyAuth()
	async deleteGame(@Param('id') id: GameId): Promise<void> {
		await this.gamesGateway.reset(id);

		await this.gamesService.deleteGame(id);

		await this.gamesService.deleteCurrentGame(id);

		await this.gamesGateway.broadcastAvailableGames();
	}

	@Delete(':id/reset')
	@HttpCode(204)
	@HttpApiKeyAuth()
	@ApiOperation({
		summary: 'Reset the game',
		description: 'Reset the game.',
		operationId: 'resetGame',
	})
	@ApiParam({
		name: 'id',
		description: 'The game ID.',
		format: 'uuid',
	})
	@ApiNoContentResponse({
		description: 'Game has been successfully reset.',
	})
	@ApiNotFoundResponse({
		description: 'Game has not been found.',
	})
	@ApiBadRequestResponse(MissingOrIncorrectFieldsResponse)
	async resetGame(@Param('id') id: GameId): Promise<void> {
		const gameReset = await this.gamesService.resetCurrentGame(id);

		await this.gamesGateway.reset(id, new ReadGameWithPlayersDto(gameReset));
	}
}
