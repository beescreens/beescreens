import { GameDto } from '@/games/dtos/game.dto';
import { GameWithCurrentNumberOfPlayers } from '@beescreens/pimp-my-wall';
import { Min } from 'class-validator';

export class GameWithCurrentNumberOfPlayersDto
	extends GameDto
	implements GameWithCurrentNumberOfPlayers
{
	/**
	 * current number of players
	 */
	@Min(0)
	currentNumberOfPlayers: number;

	constructor(entity: GameWithCurrentNumberOfPlayers) {
		super(entity);

		this.currentNumberOfPlayers = entity.currentNumberOfPlayers;
	}
}
