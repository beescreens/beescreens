import { GameWithPlayersDto } from '@/games/dtos/game-with-players.dto';
import { GameWithPlayers, ReadGameWithPlayers } from '@beescreens/pimp-my-wall';

export class ReadGameWithPlayersDto
	extends GameWithPlayersDto
	implements ReadGameWithPlayers
{
	constructor(entity: GameWithPlayers) {
		super(entity);
	}
}
