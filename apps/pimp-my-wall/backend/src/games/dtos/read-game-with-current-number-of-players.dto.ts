import { GameWithCurrentNumberOfPlayersDto } from '@/games/dtos/game-with-current-number-of-players.dto';
import {
	GameWithCurrentNumberOfPlayers,
	ReadGameWithCurrentNumberOfPlayers,
} from '@beescreens/pimp-my-wall';

export class ReadGameWithCurrentNumberOfPlayersDto
	extends GameWithCurrentNumberOfPlayersDto
	implements ReadGameWithCurrentNumberOfPlayers
{
	constructor(entity: GameWithCurrentNumberOfPlayers) {
		super(entity);

		this.currentNumberOfPlayers = entity.currentNumberOfPlayers;
	}
}
