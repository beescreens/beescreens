import { GameWithCurrentNumberOfPlayersDto } from '@/games/dtos/game-with-current-number-of-players.dto';
import { ReadPlayerDto } from '@/players/dtos/read-player.dto';
import { GameWithPlayers, ReadGameWithPlayers } from '@beescreens/pimp-my-wall';
import { ApiProperty } from '@nestjs/swagger';

export class GameWithPlayersDto
	extends GameWithCurrentNumberOfPlayersDto
	implements ReadGameWithPlayers
{
	/**
	 * players
	 */
	@ApiProperty({ type: [ReadPlayerDto] })
	players: ReadPlayerDto[];

	constructor(entity: GameWithPlayers) {
		super(entity);

		this.players = [...entity.players.values()].map(
			(player) => new ReadPlayerDto(player),
		);
	}
}
