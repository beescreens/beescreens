import { GameDto } from '@/games/dtos/game.dto';
import { Game, ReadGame } from '@beescreens/pimp-my-wall';

export class ReadGameDto extends GameDto implements ReadGame {
	constructor(entity: Game) {
		super(entity);
	}
}
