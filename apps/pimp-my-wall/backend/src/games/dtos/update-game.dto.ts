import { GameDto } from '@/games/dtos/game.dto';
import { UpdateGame } from '@beescreens/pimp-my-wall';
import { OmitType, PartialType } from '@nestjs/swagger';

export class UpdateGameDto
	extends PartialType(
		OmitType(GameDto, ['id', 'createdAt', 'updatedAt'] as const),
	)
	implements UpdateGame {}
