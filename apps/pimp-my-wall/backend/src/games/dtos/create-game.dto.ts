import { OmitType } from '@nestjs/swagger';
import { CreateGame } from '@beescreens/pimp-my-wall';
import { GameDto } from '@/games/dtos/game.dto';

export class CreateGameDto
	extends OmitType(GameDto, ['id', 'createdAt', 'updatedAt'] as const)
	implements CreateGame {}
