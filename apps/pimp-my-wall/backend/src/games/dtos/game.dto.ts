import { IsDateString, IsHexColor, Min, MinLength } from 'class-validator';
import { Game, GameId, HexColor } from '@beescreens/pimp-my-wall';

export class GameDto implements Game {
	/**
	 * id of the game
	 */
	id: GameId;

	/**
	 * name of the game
	 */
	@MinLength(1)
	name: string;

	/**
	 * maximum number of allowed players
	 */
	@Min(1)
	maximumNumberOfPlayers: number;

	/**
	 * width of the canvas
	 */
	@Min(1)
	canvasWidth: number;

	/**
	 * height of the game
	 */
	@Min(1)
	canvasHeight: number;

	/**
	 * stroke color of the game
	 */
	@IsHexColor()
	strokeColor: HexColor = '#000000';

	/**
	 * background color of the game
	 */
	@IsHexColor()
	backgroundColor: HexColor = '#ffffff';

	/**
	 * date when the user was created
	 */
	@IsDateString()
	createdAt: Date;

	/**
	 * date when the user was updated
	 */
	@IsDateString()
	updatedAt: Date;

	constructor(entity: Game) {
		this.id = entity.id;
		this.name = entity.name;
		this.maximumNumberOfPlayers = entity.maximumNumberOfPlayers;
		this.canvasWidth = entity.canvasWidth;
		this.canvasHeight = entity.canvasHeight;
		this.strokeColor = entity.strokeColor;
		this.backgroundColor = entity.backgroundColor;
		this.createdAt = entity.createdAt;
		this.updatedAt = entity.updatedAt;
	}
}
