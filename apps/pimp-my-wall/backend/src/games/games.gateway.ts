import {
	ClientEvent,
	GameId,
	HexColor,
	JoinPayload,
	PimpMyWallBackendSocket,
	PimpMyWallSocketServer,
	Player,
	Point,
	ServerEvent,
	Stroke,
	customJsonParser,
} from '@beescreens/pimp-my-wall';
import {
	Logger,
	OnModuleDestroy,
	OnModuleInit,
	UseFilters,
} from '@nestjs/common';
import {
	ConnectedSocket,
	MessageBody,
	OnGatewayConnection,
	OnGatewayDisconnect,
	SubscribeMessage,
	WebSocketGateway,
	WebSocketServer,
} from '@nestjs/websockets';
import { randomUUID } from 'crypto';
import { WsAllExceptionsFilter } from '@/common/filters/ws-all-exceptions.filter';
import { GamesService } from '@/games/games.service';
import { ReadGameWithCurrentNumberOfPlayersDto } from '@/games/dtos/read-game-with-current-number-of-players.dto';
import { ReadGameWithPlayersDto } from '@/games/dtos/read-game-with-players.dto';
import { CreateStrokeDto } from '@/strokes/dtos/create-stroke.dto';
import { CreatePointDto } from '@/points/dtos/create-point.dto';

@WebSocketGateway({
	cors: {
		origin: '*',
	},
	transports: ['websocket'],
	parser: customJsonParser,
})
@UseFilters(WsAllExceptionsFilter)
export class GamesGateway
	implements
		OnModuleInit,
		OnModuleDestroy,
		OnGatewayConnection,
		OnGatewayDisconnect
{
	@WebSocketServer()
	private server!: PimpMyWallSocketServer;

	private logger: Logger = new Logger(GamesGateway.name);

	constructor(private gamesService: GamesService) {}

	async onModuleInit(): Promise<void> {
		this.logger.log('Initializing the module');

		await this.broadcastAvailableGames();
	}

	async onModuleDestroy(): Promise<void> {
		this.logger.log('Destroying the module');

		this.server.emit(ServerEvent.RESET);
	}

	async handleConnection(): Promise<void> {
		this.logger.log('A client has connected');
	}

	async handleDisconnect(
		@ConnectedSocket() socket: PimpMyWallBackendSocket,
	): Promise<void> {
		this.logger.log('A client has disconnected');

		const { gameId } = socket.data;

		if (gameId) {
			socket.leave(gameId);

			await this.gamesService.leaveGame(gameId);

			await this.broadcastAvailableGames();
		}
	}

	async broadcastAvailableGames(): Promise<void> {
		const games = await this.getAvailableGames();

		this.server.emit(ServerEvent.AVAILABLE_GAMES, games);
	}

	async reset(gameId: GameId, game?: ReadGameWithPlayersDto): Promise<void> {
		this.server.to(gameId).emit(ServerEvent.RESET, game);
	}

	@SubscribeMessage(ClientEvent.GET_AVAILABLE_GAMES)
	async getAvailableGames(): Promise<ReadGameWithCurrentNumberOfPlayersDto[]> {
		const currentGames = await this.gamesService.getCurrentGames();

		const currentGamesDto = currentGames.map(
			(game): ReadGameWithCurrentNumberOfPlayersDto =>
				new ReadGameWithCurrentNumberOfPlayersDto(game),
		);

		return currentGamesDto;
	}

	@SubscribeMessage(ClientEvent.JOIN)
	async join(
		@ConnectedSocket() socket: PimpMyWallBackendSocket,
		@MessageBody() { gameId, isDisplay }: JoinPayload,
	): Promise<ReadGameWithPlayersDto | undefined> {
		this.logger.log(`A client is joining game ${gameId}`);

		const player: Player = {
			id: randomUUID(),
			strokes: [],
			timestamp: new Date(),
		};

		if (!isDisplay) {
			await this.gamesService.joinGame(gameId, player);

			socket.data.playerId = player.id;
			socket.data.gameId = gameId;

			await this.broadcastAvailableGames();
		}

		socket.join(gameId);

		this.server.to(gameId).emit(ServerEvent.NEW_PLAYER, player);

		const currentGame = await this.gamesService.getCurrentGame(gameId);

		if (currentGame) {
			return new ReadGameWithPlayersDto(currentGame);
		}

		return undefined;
	}

	@SubscribeMessage(ClientEvent.STROKE_START)
	async strokeStart(
		@ConnectedSocket() socket: PimpMyWallBackendSocket,
		@MessageBody() { color, size }: CreateStrokeDto,
	): Promise<void> {
		const { gameId, playerId } = socket.data;

		if (gameId && playerId) {
			const stroke: Stroke = {
				timestamp: new Date(),
				color,
				size,
				points: [],
			};

			await this.gamesService.addStroke(gameId, playerId, stroke);

			this.server.to(gameId).emit(ServerEvent.STROKE_START_FROM_PLAYER, {
				playerId,
				data: {
					stroke,
				},
			});
		}
	}

	@SubscribeMessage(ClientEvent.STROKE_CONTINUE)
	async strokeContinue(
		@ConnectedSocket() socket: PimpMyWallBackendSocket,
		@MessageBody() { x, y }: CreatePointDto,
	): Promise<void> {
		const { gameId, playerId } = socket.data;

		if (gameId && playerId) {
			const point: Point = {
				timestamp: new Date(),
				x,
				y,
			};

			await this.gamesService.addPointToStroke(gameId, playerId, point);

			this.server.to(gameId).emit(ServerEvent.STROKE_CONTINUE_FROM_PLAYER, {
				playerId,
				data: {
					point,
				},
			});
		}
	}

	@SubscribeMessage(ClientEvent.STROKE_END)
	async strokeEnd(
		@ConnectedSocket() socket: PimpMyWallBackendSocket,
	): Promise<void> {
		const { gameId, playerId } = socket.data;

		if (gameId && playerId) {
			await this.gamesService.endStroke(gameId, playerId);

			this.server
				.to(gameId)
				.emit(ServerEvent.STROKE_END_FROM_PLAYER, { playerId });
		}
	}

	@SubscribeMessage(ClientEvent.UNDO)
	async undo(
		@ConnectedSocket() socket: PimpMyWallBackendSocket,
	): Promise<void> {
		const { gameId, playerId } = socket.data;

		if (gameId && playerId) {
			await this.gamesService.undo(gameId, playerId);

			this.server.to(gameId).emit(ServerEvent.UNDO_FROM_PLAYER, { playerId });
		}
	}

	@SubscribeMessage(ClientEvent.BACKGROUND_COLOR)
	async updateBackgroundColor(
		@ConnectedSocket() socket: PimpMyWallBackendSocket,
		@MessageBody() backgroundColor: HexColor,
	): Promise<void> {
		const { gameId, playerId } = socket.data;

		if (gameId && playerId) {
			await this.gamesService.updateBackgroundColor(gameId, backgroundColor);

			this.server.to(gameId).emit(ServerEvent.BACKGROUND_COLOR_FROM_PLAYER, {
				playerId,
				data: {
					backgroundColor,
				},
			});
		}
	}
}
