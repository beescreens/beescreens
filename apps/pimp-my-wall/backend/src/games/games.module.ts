import { GamesController } from '@/games/games.controller';
import { GamesGateway } from '@/games/games.gateway';
import { GamesService } from '@/games/games.service';
import { Module, ModuleMetadata } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { PrismaModule } from 'nestjs-prisma';

export const moduleMetadata: ModuleMetadata = {
	controllers: [GamesController],
	imports: [ConfigModule, PrismaModule],
	providers: [GamesService, GamesGateway],
	exports: [GamesService, GamesGateway],
};

@Module(moduleMetadata)
export class GamesModule {}
