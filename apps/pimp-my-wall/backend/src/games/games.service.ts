import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';
import {
	CreateGame,
	GameId,
	UpdateGame,
	PlayerId,
	Player,
	GameNotFoundException,
	GameFullException,
	Stroke,
	Point,
	GameWithPlayers,
	Game,
} from '@beescreens/pimp-my-wall';
import simplify from 'simplify-js';

@Injectable()
export class GamesService implements OnModuleInit {
	private logger: Logger = new Logger(GamesService.name);

	private currentGames = new Map<GameId, GameWithPlayers>();

	constructor(readonly prismaService: PrismaService) {}

	async onModuleInit(): Promise<void> {
		const games = await this.getGames();

		games.forEach((game) => this.createCurrentGame(game));

		this.logger.log('Module initialized');
	}

	async getGames(): Promise<Game[]> {
		return await this.prismaService.game.findMany();
	}

	async getGame(gameId: GameId): Promise<Game> {
		const game = await this.prismaService.game.findFirstOrThrow({
			where: {
				id: gameId,
			},
		});

		return game;
	}

	async create(createGame: CreateGame): Promise<Game> {
		const game = await this.prismaService.game.create({
			data: {
				...createGame,
			},
		});

		return game;
	}

	async update(gameId: GameId, updateGame: UpdateGame): Promise<Game> {
		this.logger.log(`Game update (${gameId})`);

		const updatedGame = await this.prismaService.game.update({
			where: {
				id: gameId,
			},
			data: {
				...updateGame,
			},
		});

		return updatedGame;
	}

	async deleteGame(gameId: GameId): Promise<void> {
		this.logger.log(`Game delete (${gameId})`);

		await this.prismaService.game.delete({
			where: {
				id: gameId,
			},
		});
	}

	async getCurrentGames(): Promise<GameWithPlayers[]> {
		const games = [...this.currentGames.values()];

		return games;
	}

	async getCurrentGame(gameId: GameId): Promise<GameWithPlayers | undefined> {
		const game = this.currentGames.get(gameId);

		return game;
	}

	async createCurrentGame(game: Game): Promise<GameWithPlayers> {
		const newCurrentGame: GameWithPlayers = {
			...game,
			players: new Map<PlayerId, Player>(),
			currentNumberOfPlayers: 0,
		};

		this.currentGames.set(game.id, newCurrentGame);

		return newCurrentGame;
	}

	async updateCurrentGame(
		gameId: GameId,
		game: Game,
	): Promise<GameWithPlayers | undefined> {
		this.logger.log(`Current game update (${gameId})`);

		const currentGame = this.currentGames.get(gameId);

		if (currentGame) {
			const updatedCurrentGame: GameWithPlayers = {
				...game,
				players: currentGame.players,
				currentNumberOfPlayers: currentGame.currentNumberOfPlayers,
			};

			this.currentGames.set(gameId, updatedCurrentGame);

			return updatedCurrentGame;
		}
	}

	async deleteCurrentGame(gameId: GameId): Promise<void> {
		this.logger.log(`Current game delete (${gameId})`);

		this.currentGames.delete(gameId);
	}

	async resetCurrentGame(gameId: GameId): Promise<GameWithPlayers> {
		const game = await this.getGame(gameId);

		const resetGame = await this.createCurrentGame(game);

		return resetGame;
	}

	async joinGame(gameId: GameId, player: Player): Promise<void> {
		const game = this.currentGames.get(gameId);

		if (!game) {
			throw new GameNotFoundException(gameId);
		}

		if (game.currentNumberOfPlayers >= game.maximumNumberOfPlayers) {
			throw new GameFullException(gameId);
		}

		game.players.set(player.id, player);

		game.currentNumberOfPlayers += 1;

		await this.updateCurrentGame(gameId, game);
	}

	async leaveGame(gameId: GameId): Promise<void> {
		const game = this.currentGames.get(gameId);

		if (!game) {
			throw new GameNotFoundException(gameId);
		}

		if (game.currentNumberOfPlayers > 0) {
			game.currentNumberOfPlayers -= 1;
		}

		await this.updateCurrentGame(gameId, game);
	}

	async addStroke(
		gameId: GameId,
		playerId: PlayerId,
		stroke: Stroke,
	): Promise<void> {
		this.currentGames.get(gameId)?.players.get(playerId)?.strokes.push(stroke);
	}

	async addPointToStroke(
		gameId: GameId,
		playerId: PlayerId,
		point: Point,
	): Promise<void> {
		this.currentGames
			.get(gameId)
			?.players.get(playerId)
			?.strokes.slice(-1)[0]
			.points?.push(point);
	}

	async endStroke(gameId: GameId, playerId: PlayerId): Promise<void> {
		const lastStroke = this.currentGames
			.get(gameId)
			?.players.get(playerId)
			?.strokes.pop();

		if (lastStroke?.points) {
			const optimizedStroke = {
				...lastStroke,
				points: simplify(lastStroke.points, 3),
			};

			this.currentGames
				.get(gameId)
				?.players.get(playerId)
				?.strokes.push(optimizedStroke);
		}
	}

	async undo(gameId: GameId, playerId: PlayerId): Promise<void> {
		this.currentGames.get(gameId)?.players.get(playerId)?.strokes.pop();
	}

	async updateBackgroundColor(
		gameId: GameId,
		backgroundColor: string,
	): Promise<void> {
		const game = this.currentGames.get(gameId);

		if (game) {
			game.backgroundColor = backgroundColor;

			await this.updateCurrentGame(gameId, game);
		}
	}
}
