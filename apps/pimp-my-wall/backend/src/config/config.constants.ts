export const PORT = 'PIMP_MY_WALL_PORT';
export const API_KEY = 'PIMP_MY_WALL_API_KEY';
export const DATABASE_URL = 'PIMP_MY_WALL_DATABASE_URL';

export interface EnvironmentVariables {
	[PORT]: number;
	[API_KEY]: string;
	[DATABASE_URL]: string;
}
