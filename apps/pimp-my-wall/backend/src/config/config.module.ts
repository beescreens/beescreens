import { PORT } from '@/app.constants';
import { API_KEY, DATABASE_URL } from '@/config/config.constants';
import { Module } from '@nestjs/common';
import { ConfigModule as ConfigurationModule } from '@nestjs/config';
import Joi from 'joi';

@Module({
	imports: [
		ConfigurationModule.forRoot({
			validationSchema: Joi.object({
				[PORT]: Joi.number().port().default(4000),
				[API_KEY]: Joi.string().required(),
				[DATABASE_URL]: Joi.string().required(),
			}),
			validationOptions: {
				allowUnknown: true,
				abortEarly: false,
			},
			expandVariables: true,
		}),
	],
	exports: [ConfigurationModule],
})
export class ConfigModule {}
