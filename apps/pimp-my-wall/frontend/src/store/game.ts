import {
	FromPlayer,
	GameWithPlayers,
	HexColor,
	Player,
	PointFromPlayer,
	StrokeFromPlayer,
} from '@beescreens/pimp-my-wall';
import { useStore } from 'zustand';
import { createStore } from 'zustand/vanilla';
import { immer } from 'zustand/middleware/immer';
import simplify from 'simplify-js';

interface GameStore {
	game: GameWithPlayers | null;
	setGame: (game: GameWithPlayers) => void;
	createNewPlayer: (player: Player) => void;
	strokeStart: (strokeFromOtherPlayer: StrokeFromPlayer) => void;
	strokeContinue: (pointFromOtherPlayer: PointFromPlayer) => void;
	strokeEnd: (fromPlayer: FromPlayer) => void;
	undo: (fromPlayer: FromPlayer) => void;
	setBackgroundColor: (color: HexColor) => void;
}

export const store = createStore(
	immer<GameStore>((set) => ({
		game: null,
		setGame: (game) => set(() => ({ game })),
		createNewPlayer: (player) =>
			set((state) => {
				if (state.game) {
					state.game.players.set(player.id, player);
				}
			}),
		strokeStart: ({ playerId, data: { stroke } }) =>
			set((state) => {
				if (state.game) {
					state.game.players.get(playerId)?.strokes.push(stroke);
				}
			}),
		strokeContinue: ({ playerId, data: { point } }) =>
			set((state) => {
				if (state.game) {
					state.game.players
						.get(playerId)
						?.strokes.slice(-1)[0]
						.points?.push(point);
				}
			}),
		strokeEnd: ({ playerId }) =>
			set((state) => {
				if (state.game) {
					const lastStroke = state.game.players.get(playerId)?.strokes.pop();

					if (lastStroke?.points) {
						const optimizedStroke = {
							...lastStroke,
							points: simplify(lastStroke.points, 3),
						};

						state.game.players.get(playerId)?.strokes.push(optimizedStroke);
					}
				}
			}),
		undo: ({ playerId }) =>
			set((state) => {
				if (state.game) {
					state.game.players.get(playerId)?.strokes.pop();
				}
			}),
		setBackgroundColor: (color) =>
			set((state) => {
				if (state.game) {
					state.game.backgroundColor = color;
				}
			}),
	})),
);

export function useGameStore(): GameStore;
export function useGameStore<T>(
	selector: (state: GameStore) => T,
	equals?: (a: T, b: T) => boolean,
): T;
export function useGameStore<T>(
	selector?: (state: GameStore) => T,
	equals?: (a: T, b: T) => boolean,
) {
	// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
	return useStore(store, selector!, equals);
}
