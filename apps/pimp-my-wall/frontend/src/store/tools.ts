import { HexColor } from '@beescreens/pimp-my-wall';
import { useStore } from 'zustand';
import { createStore } from 'zustand/vanilla';
import { immer } from 'zustand/middleware/immer';

interface ToolsStore {
	drawingMode: boolean;
	strokeMinSize: number;
	strokeSize: number;
	strokeMaxSize: number;
	strokeColor: HexColor;
	setDrawingMode: (drawingMode: boolean) => void;
	setStrokeMinSize: (strokeSize: number) => void;
	setStrokeSize: (strokeSize: number) => void;
	setStrokeMaxSize: (strokeSize: number) => void;
	setStrokeColor: (strokeColor: HexColor) => void;
}

export const store = createStore(
	immer<ToolsStore>((set) => ({
		game: null,
		drawingMode: false,
		strokeMinSize: 1,
		strokeSize: 1,
		strokeMaxSize: 100,
		strokeColor: '#000000',
		setDrawingMode: (drawingMode) => set(() => ({ drawingMode })),
		setStrokeMinSize: (strokeMinSize) => set(() => ({ strokeMinSize })),
		setStrokeSize: (strokeSize) => set(() => ({ strokeSize })),
		setStrokeMaxSize: (strokeMaxSize) => set(() => ({ strokeMaxSize })),
		setStrokeColor: (strokeColor) => set(() => ({ strokeColor })),
	})),
);

export function useToolsStore(): ToolsStore;
export function useToolsStore<T>(
	selector: (state: ToolsStore) => T,
	equals?: (a: T, b: T) => boolean,
): T;
export function useToolsStore<T>(
	selector?: (state: ToolsStore) => T,
	equals?: (a: T, b: T) => boolean,
) {
	// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
	return useStore(store, selector!, equals);
}
