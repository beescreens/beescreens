import { GameWithCurrentNumberOfPlayers } from '@beescreens/pimp-my-wall';
import { useStore } from 'zustand';
import { createStore } from 'zustand/vanilla';
import { immer } from 'zustand/middleware/immer';

interface GamesStore {
	games: GameWithCurrentNumberOfPlayers[] | null;
	setGames: (games: GameWithCurrentNumberOfPlayers[]) => void;
}

export const store = createStore(
	immer<GamesStore>((set) => ({
		games: null,
		setGames: (games) => set(() => ({ games })),
	})),
);

export function useGamesStore(): GamesStore;
export function useGamesStore<T>(
	selector: (state: GamesStore) => T,
	equals?: (a: T, b: T) => boolean,
): T;
export function useGamesStore<T>(
	selector?: (state: GamesStore) => T,
	equals?: (a: T, b: T) => boolean,
) {
	// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
	return useStore(store, selector!, equals);
}
