export const debounce = (
	// eslint-disable-next-line @typescript-eslint/ban-types
	func: Function,
	wait: number,
): ((...args: unknown[]) => void) => {
	let timeout: ReturnType<typeof setTimeout>;

	return (...args: unknown[]) => {
		const later = () => {
			func(...args);
			clearTimeout(timeout);
		};

		clearTimeout(timeout);

		timeout = setTimeout(later, wait);
	};
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const isNumeric = (n: any): boolean =>
	!isNaN(parseFloat(n)) && isFinite(n);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const isTrulyTrue = (b: any): boolean =>
	typeof b !== 'undefined' && b !== 'false';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const isHexColor = (c: any): boolean => /^#[0-9a-f]{3,6}$/i.test(c);

export const sleep = (ms: number): Promise<void> =>
	new Promise((r) => setTimeout(r, ms));
