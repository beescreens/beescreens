import useTranslation from 'next-translate/useTranslation';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import { useGamesStore } from '@/store/games';

export const GamesList = (): JSX.Element => {
	const { t } = useTranslation('common');

	const games = useGamesStore((state) => state.games) ?? [];

	return (
		<>
			{games.map(
				({ id, name, currentNumberOfPlayers, maximumNumberOfPlayers }) => {
					const isFull = currentNumberOfPlayers >= maximumNumberOfPlayers;

					return (
						<div
							key={id}
							className={`${isFull && 'pointer-events-none'} text-center`}
						>
							<Link
								href={`/play/${encodeURIComponent(id)}`}
								className="focus:outline-none"
							>
								<button
									type="button"
									disabled={isFull}
									className={`${
										isFull
											? 'disabled:opacity-25'
											: 'hover:bg-indigo-600 hover:text-white'
									} ease m-2 select-none rounded-md border border-indigo-500 px-4 py-2 text-indigo-500 transition duration-500 focus:outline-none focus:ring-2 focus:ring-indigo-600`}
								>
									<FontAwesomeIcon
										icon={['fas', isFull ? 'door-closed' : 'door-open']}
										className="mr-2"
									/>

									{t('play.joinGame', {
										name,
										currentNumberOfPlayers,
										maximumNumberOfPlayers,
									})}
								</button>
							</Link>
						</div>
					);
				},
			)}
		</>
	);
};
