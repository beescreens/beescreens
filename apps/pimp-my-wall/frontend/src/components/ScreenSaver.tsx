import { useCallback, useEffect, useState } from 'react';
import { Stage, Layer, Text, Image } from 'react-konva';
import useTranslation from 'next-translate/useTranslation';
import useImage from 'use-image';
import Konva from 'konva';

Konva.hitOnDragEnabled = true;

export type ScreenSaverProps = {
	className?: string;
};

export const ScreenSaver = ({ className }: ScreenSaverProps): JSX.Element => {
	const { t } = useTranslation('common');

	const [windowDimensions, setWindowDimensions] = useState({
		width: window.innerWidth,
		height: window.innerHeight,
	});

	const QR_CODE_LINK = process.env.PIMP_MY_WALL_QR_CODE_LINK as string;
	const QR_CODE_SIZE = parseInt(
		process.env.PIMP_MY_WALL_QR_CODE_SIZE as string,
		10,
	);
	const QR_CODE_MARGIN = parseInt(
		process.env.PIMP_MY_WALL_QR_CODE_MARGIN as string,
		10,
	);
	const QR_CODE_FOREGROUND_COLOR = process.env
		.QR_CODE_FOREGROUND_COLOR as string;
	const QR_CODE_BACKGROUND_COLOR = process.env
		.QR_CODE_BACKGROUND_COLOR as string;
	const SCREENSAVER_FONT_SIZE = parseInt(
		process.env.PIMP_MY_WALL_SCREENSAVER_FONT_SIZE as string,
		10,
	);

	const upperText = {
		text: t('display.upperText'),
		size: SCREENSAVER_FONT_SIZE,
		color: '#ffffff',
		position: -windowDimensions.height / 3,
	};

	const lowerText = {
		text: t('display.lowerText', { url: QR_CODE_LINK }),
		size: SCREENSAVER_FONT_SIZE,
		color: '#ffffff',
		position: windowDimensions.height / 3,
	};

	const [image] = useImage(
		`https://quickchart.io/qr?text=${encodeURI(
			QR_CODE_LINK,
		)}&light=${QR_CODE_BACKGROUND_COLOR.replace(
			'#',
			'',
		)}&dark=${QR_CODE_FOREGROUND_COLOR.replace(
			'#',
			'',
		)}&margin=${QR_CODE_MARGIN}&size=${QR_CODE_SIZE}`,
	);

	const resize = useCallback(() => {
		setWindowDimensions({
			width: window.innerWidth,
			height: window.innerHeight,
		});
	}, []);

	useEffect(() => {
		window.addEventListener('resize', resize);
		return () => window.removeEventListener('resize', resize);
	}, [resize]);

	return (
		<>
			<Stage
				width={windowDimensions.width}
				height={windowDimensions.height}
				onContextMenu={(e) => e.evt.preventDefault()}
				className={className}
			>
				<Layer>
					<Text
						width={windowDimensions.width}
						height={windowDimensions.height}
						y={upperText.position}
						fill={upperText.color}
						fontSize={upperText.size}
						text={upperText.text}
						verticalAlign="middle"
						align="center"
					/>
					<Image
						alt="QR Code"
						x={windowDimensions.width / 2 - QR_CODE_SIZE / 2}
						y={windowDimensions.height / 2 - QR_CODE_SIZE / 2}
						image={image}
					/>
					<Text
						width={windowDimensions.width}
						height={windowDimensions.height}
						y={lowerText.position}
						fill={lowerText.color}
						fontSize={lowerText.size}
						text={lowerText.text}
						verticalAlign="middle"
						align="center"
					/>
				</Layer>
			</Stage>
		</>
	);
};
