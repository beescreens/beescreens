import React from 'react';
import { Head } from '../meta/Head';

export type DisplayLayoutProps = {
	children: React.ReactNode;
	title: string;
};

export const DisplayLayout = ({
	children,
	title,
}: DisplayLayoutProps): JSX.Element => {
	return (
		<Head title={title}>
			<div className="flex h-full w-full flex-col bg-gray-800">
				<div className="grow">{children}</div>
			</div>
		</Head>
	);
};
