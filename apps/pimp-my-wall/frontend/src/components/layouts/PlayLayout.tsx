import React, { createContext, useRef } from 'react';
import { Head } from '../meta/Head';
import { BackgroundColorTool } from '../tools/BackgroundColorTool';
import { DrawTool } from '../tools/DrawTool';
import { MoveTool } from '../tools/MoveTool';
import { StrokeColorTool } from '../tools/StrokeColorTool';
import { StrokeSizeTool } from '../tools/StrokeSizeTool';
import { UndoTool } from '../tools/UndoTool';
import { QuitTool } from '../tools/QuitTool';
import { useToolsStore } from '@/store/tools';

interface PlayerContext {
	toolbarRef?: React.RefObject<HTMLDivElement>;
}

export const playerCTX = createContext<PlayerContext>({});

export type PlayLayoutProps = {
	children: React.ReactNode;
	title: string;
	version: string;
};

export const PlayLayout = ({
	children,
	title,
}: PlayLayoutProps): JSX.Element => {
	const toolbarRef = useRef<HTMLDivElement>(null);

	const drawingMode = useToolsStore((state) => state.drawingMode);

	return (
		<playerCTX.Provider value={{ toolbarRef }}>
			<Head title={title}>
				<div className="flex h-full w-full flex-col bg-gray-800">
					{children}
					<section
						ref={toolbarRef}
						className="fixed bottom-0 flex w-full shrink-0 flex-col bg-gray-700 shadow"
					>
						<div className="flex justify-between">
							{drawingMode ? <MoveTool /> : <DrawTool />}
							<UndoTool />
							<StrokeColorTool />
							<StrokeSizeTool />
							<BackgroundColorTool />
							<QuitTool />
						</div>
					</section>
				</div>
			</Head>
		</playerCTX.Provider>
	);
};
