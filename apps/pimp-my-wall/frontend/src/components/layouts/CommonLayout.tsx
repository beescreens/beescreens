import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { Head } from '../meta/Head';

export type CommonLayoutProps = {
	children: React.ReactNode;
	title: string;
	version: string;
};

export const CommonLayout = ({
	children,
	title,
	version,
}: CommonLayoutProps): JSX.Element => {
	const { t } = useTranslation('common');
	const router = useRouter();

	return (
		<Head title={title}>
			<div className="flex min-h-screen flex-col overflow-auto bg-gray-100">
				<div className="container mx-auto mb-auto px-10 text-justify leading-normal">
					{children}
				</div>
				<div className="flex justify-around border-t p-2 text-center text-sm text-gray-500">
					<div className="w-1/4">
						{t('version')} &apos;{version}&apos;
					</div>
					<div className="w-1/4">
						<Link href="/">{t('returnToHomepage')}</Link>
					</div>
					<div className="w-1/4">
						<Link href={router.pathname} locale="fr" className="uppercase">
							{t('locales.fr.key')}
						</Link>{' '}
						/{' '}
						<Link href={router.pathname} locale="en" className="uppercase">
							{t('locales.en.key')}
						</Link>
					</div>
				</div>
			</div>
		</Head>
	);
};
