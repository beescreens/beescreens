import useTranslation from 'next-translate/useTranslation';
import { Tool } from './Tool';
import { useToolsStore } from '@/store/tools';

export const MoveTool = (): JSX.Element => {
	const { t } = useTranslation('common');

	const setDrawingMode = useToolsStore((state) => state.setDrawingMode);

	return (
		<Tool
			name={t('play.tools.move.title')}
			iconStyle="fas"
			iconName="arrows-alt"
			iconColor="text-red-400"
			action={() => setDrawingMode(false)}
		/>
	);
};
