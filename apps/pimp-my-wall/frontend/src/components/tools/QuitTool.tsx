import useTranslation from 'next-translate/useTranslation';
import { Tool } from './Tool';
import { useRouter } from 'next/router';

export const QuitTool = (): JSX.Element => {
	const { t } = useTranslation('common');
	const router = useRouter();

	return (
		<Tool
			name={t('play.tools.quit.title')}
			iconStyle="fas"
			iconName="door-open"
			iconColor="text-gray-400"
			action={() => {
				router.push('/end');
			}}
		/>
	);
};
