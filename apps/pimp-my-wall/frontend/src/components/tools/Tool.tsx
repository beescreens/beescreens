import { IconName, IconPrefix } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export type ToolProps = {
	name: string;
	iconName: IconName;
	iconStyle: IconPrefix;
	iconColor?: string;
	action: () => void;
};

export const Tool = ({
	name,
	iconName,
	iconStyle,
	iconColor,
	action,
}: ToolProps): JSX.Element => {
	return (
		<button
			onClick={action}
			// eslint-disable-next-line tailwindcss/no-custom-classname
			className={
				'inline-block w-full justify-center p-2 text-center text-slate-300 hover:text-white focus:text-white'
			}
		>
			<div className="-mx-2 text-center">
				<FontAwesomeIcon icon={[iconStyle, iconName]} className={iconColor} />
				<span className="block text-xs">{name}</span>
			</div>
		</button>
	);
};
