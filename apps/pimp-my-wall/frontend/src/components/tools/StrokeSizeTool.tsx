import useTranslation from 'next-translate/useTranslation';
import React, { useState } from 'react';
import Slider from 'react-input-slider';
import { Circle, Layer, Rect, Stage } from 'react-konva';
import { Modal } from '../Modal';
import { Tool } from './Tool';
import { useToolsStore } from '@/store/tools';
import { useGameStore } from '@/store/game';

export const StrokeSizeTool = (): JSX.Element => {
	const { t } = useTranslation('common');

	const setDrawingMode = useToolsStore((state) => state.setDrawingMode);
	const setStrokeSize = useToolsStore((state) => state.setStrokeSize);

	const strokeMinSize = useToolsStore((state) => state.strokeMinSize);
	const strokeSize = useToolsStore((state) => state.strokeSize);
	const strokeMaxSize = useToolsStore((state) => state.strokeMaxSize);
	const strokeColor = useToolsStore((state) => state.strokeColor);
	const backgroundColor = useGameStore(
		(state) => state.game?.backgroundColor ?? '#ffffff',
	);

	const [isOpen, changeIsOpen] = useState(false);

	return (
		<>
			<Tool
				name={t('play.tools.strokeSize.title')}
				iconStyle="fas"
				iconName="signature"
				iconColor="text-yellow-400"
				action={() => changeIsOpen(!isOpen)}
			/>

			{isOpen && (
				<Modal
					title={t('play.tools.strokeSize.title')}
					close={() => changeIsOpen(!isOpen)}
				>
					<p className="text-sm text-gray-400">
						{t('play.tools.strokeSize.placeholder')}
					</p>
					<div className="mt-2 flex justify-center">
						<Stage width={200} height={200}>
							<Layer>
								<Rect
									width={window.innerWidth}
									height={window.innerHeight}
									fill={backgroundColor}
								/>
								<Circle
									x={100}
									y={100}
									radius={strokeSize}
									fill={strokeColor}
								/>
							</Layer>
						</Stage>
					</div>
					<div className="mt-2 flex justify-center">
						<Slider
							axis="x"
							xmin={strokeMinSize}
							xmax={strokeMaxSize}
							x={strokeSize}
							onChange={(newstrokeSize) => {
								setDrawingMode(true);
								setStrokeSize(newstrokeSize.x);
							}}
						/>
					</div>
				</Modal>
			)}
		</>
	);
};
