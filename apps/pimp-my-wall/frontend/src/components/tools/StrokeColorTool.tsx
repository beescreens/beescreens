import useTranslation from 'next-translate/useTranslation';
import { useState } from 'react';
import { HexColorInput, HexColorPicker } from 'react-colorful';
import { Modal } from '../Modal';
import { Tool } from './Tool';
import { useToolsStore } from '@/store/tools';
import { HexColor } from '@beescreens/pimp-my-wall';

export const StrokeColorTool = (): JSX.Element => {
	const { t } = useTranslation('common');

	const [isOpen, changeIsOpen] = useState(false);

	const setDrawingMode = useToolsStore((state) => state.setDrawingMode);
	const setStrokeColor = useToolsStore((state) => state.setStrokeColor);
	const strokeColor = useToolsStore((state) => state.strokeColor);

	return (
		<>
			<Tool
				name={t('play.tools.strokeColor.title')}
				iconStyle="fas"
				iconName="palette"
				iconColor="text-blue-400"
				action={() => changeIsOpen(!isOpen)}
			/>

			{isOpen && (
				<Modal
					title={t('play.tools.strokeColor.title')}
					close={() => changeIsOpen(!isOpen)}
				>
					<p className="text-sm text-gray-400">
						{t('play.tools.strokeColor.placeholder')}
					</p>
					<HexColorPicker
						className="mt-2 w-full"
						color={strokeColor}
						onChange={(newStrokeColor: HexColor) => {
							setDrawingMode(true);
							setStrokeColor(newStrokeColor);
						}}
					/>
					<HexColorInput
						className="mt-2 w-full rounded-lg text-center"
						color={strokeColor}
						onChange={(newStrokeColor: HexColor) => {
							setDrawingMode(true);
							setStrokeColor(newStrokeColor);
						}}
					/>
				</Modal>
			)}
		</>
	);
};
