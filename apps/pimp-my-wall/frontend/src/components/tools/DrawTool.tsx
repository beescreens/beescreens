import useTranslation from 'next-translate/useTranslation';
import { Tool } from './Tool';
import { useToolsStore } from '@/store/tools';

export const DrawTool = (): JSX.Element => {
	const { t } = useTranslation('common');

	const setDrawingMode = useToolsStore((state) => state.setDrawingMode);

	return (
		<Tool
			name={t('play.tools.draw.title')}
			iconStyle="fas"
			iconName="paint-brush"
			iconColor="text-green-400"
			action={() => setDrawingMode(true)}
		/>
	);
};
