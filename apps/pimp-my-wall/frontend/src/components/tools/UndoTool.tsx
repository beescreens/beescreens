import useTranslation from 'next-translate/useTranslation';
import { Tool } from './Tool';
import { useWebSocket } from '@/providers/WebsocketProvider';

export const UndoTool = (): JSX.Element => {
	const { t } = useTranslation('common');

	const { emitUndo } = useWebSocket();

	return (
		<Tool
			name={t('play.tools.undo.title')}
			iconStyle="fas"
			iconName="undo"
			iconColor="text-pink-400"
			action={() => emitUndo()}
		/>
	);
};
