import useTranslation from 'next-translate/useTranslation';
import { useEffect, useState } from 'react';
import { HexColorInput, HexColorPicker } from 'react-colorful';
import { Modal } from '../Modal';
import { Tool } from './Tool';
import { useWebSocket } from '@/providers/WebsocketProvider';
import { useGameStore } from '@/store/game';

export const BackgroundColorTool = (): JSX.Element => {
	const { t } = useTranslation('common');

	const { emitBackgroundColor } = useWebSocket();

	const setBackgroundColor = useGameStore((state) => state.setBackgroundColor);
	const backgroundColor = useGameStore(
		(state) => state.game?.backgroundColor ?? '#ffffff',
	);

	const [isOpen, changeIsOpen] = useState(false);

	useEffect(() => {
		setBackgroundColor(backgroundColor);
	}, [setBackgroundColor, backgroundColor]);

	return (
		<>
			<Tool
				name={t('play.tools.backgroundColor.title')}
				iconStyle="fas"
				iconName="brush"
				iconColor="text-indigo-400"
				action={() => changeIsOpen(!isOpen)}
			/>

			{isOpen && (
				<Modal
					title={t('play.tools.backgroundColor.title')}
					close={(accept) => {
						changeIsOpen(false);

						if (accept) {
							emitBackgroundColor(backgroundColor);
						}
					}}
				>
					<p className="text-sm text-gray-400">
						{t('play.tools.backgroundColor.placeholder')}
					</p>
					<HexColorPicker
						className="mt-2 w-full"
						color={backgroundColor}
						onChange={setBackgroundColor}
					/>
					<HexColorInput
						className="mt-2 w-full rounded-lg text-center"
						color={backgroundColor}
						onChange={setBackgroundColor}
					/>
				</Modal>
			)}
		</>
	);
};
