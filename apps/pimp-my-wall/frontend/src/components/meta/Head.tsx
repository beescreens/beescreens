import React from 'react';
import NextHead from 'next/head';

type HeadProps = {
	children: React.ReactNode;
	title: string;
};

export const Head = ({ children, title }: HeadProps): JSX.Element => {
	return (
		<>
			<NextHead>
				<meta
					name="viewport"
					content="initial-scale=1.0, width=device-width"
					key="viewport"
				/>
				<title>{title}</title>
				<link
					rel="icon"
					href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>🧑‍🎨</text></svg>"
				/>
			</NextHead>
			{children}
		</>
	);
};
