import { Stage, Layer, Line, Rect, Circle } from 'react-konva';
import Konva from 'konva';
import {
	useCallback,
	useContext,
	useEffect,
	useLayoutEffect,
	useRef,
	useState,
} from 'react';
import { useRouter } from 'next/router';
import { Vector2d } from 'konva/lib/types';
import { isNumeric } from '../utils';
import { playerCTX } from './layouts/PlayLayout';
import { Player, PlayerId } from '@beescreens/pimp-my-wall';
import { useGameStore } from '@/store/game';
import { useWebSocket } from '@/providers/WebsocketProvider';
import { useToolsStore } from '@/store/tools';

Konva.hitOnDragEnabled = true;

export type SketchProps = {
	className?: string;
};

export const Sketch = ({ className }: SketchProps): JSX.Element => {
	const { query } = useRouter();

	const { emitStrokeStart, emitStrokeContinue, emitStrokeEnd } = useWebSocket();

	const scaleBy = 1.1;

	const [windowDimensions, setWindowDimensions] = useState({
		width: window.innerWidth,
		height: window.innerHeight,
	});

	const { toolbarRef } = useContext(playerCTX);
	const containerRef = useRef<HTMLDivElement>(null);

	const canvasWidth = useGameStore((state) => state.game?.canvasWidth) ?? 1;
	const canvasHeight = useGameStore((state) => state.game?.canvasHeight) ?? 1;
	const backgroundColor = useGameStore((state) => state.game?.backgroundColor);
	const players =
		useGameStore((state) => state.game?.players) ?? new Map<PlayerId, Player>();
	const drawingMode = useToolsStore((state) => state.drawingMode);
	const strokeSize = useToolsStore((state) => state.strokeSize);
	const strokeColor = useToolsStore((state) => state.strokeColor);

	// Compute the scale and the coordinates to display on the current screen
	const {
		x1: queryX1,
		y1: queryY1,
		x2: queryX2,
		y2: queryY2,
		scale: queryScale,
	} = query;
	const { width: windowWidth, height: windowHeight } = windowDimensions;

	let scale;

	let x1: number;
	let y1: number;

	const canvasRatio = canvasHeight / canvasWidth;
	const windowRatio = windowHeight / windowWidth;

	if (
		(canvasWidth > canvasHeight && windowRatio > canvasRatio) ||
		(canvasWidth < canvasHeight && windowRatio > canvasRatio)
	) {
		scale = windowWidth / canvasWidth;
		x1 = 0;
		y1 = -(windowHeight / scale - canvasHeight) / 2;
	} else {
		scale = windowHeight / canvasHeight;
		x1 = -(windowWidth / scale - canvasWidth) / 2;
		y1 = 0;
	}

	if (isNumeric(queryX1)) {
		x1 = parseInt(queryX1 as string);
	}

	if (isNumeric(queryY1)) {
		y1 = parseInt(queryY1 as string);
	}

	if (isNumeric(queryX2) && isNumeric(queryY2)) {
		const x2 = parseInt(queryX2 as string);
		const y2 = parseInt(queryY2 as string);

		if (x2 - x1 > y2 - y1) {
			scale = windowWidth / (x2 - x1);
		} else {
			scale = windowHeight / (y2 - y1);
		}
	} else if (isNumeric(queryScale)) {
		scale = parseFloat(queryScale as string);
	}

	// canvas position is relative to scale
	x1 = x1 * scale;
	y1 = y1 * scale;

	const strokeStarted = useRef(false);
	let lastCenter: Vector2d | undefined = undefined;
	let lastDist = 0;

	const stayInBounds = (p: Konva.Vector2d): Konva.Vector2d => {
		let newX = p.x;
		let newY = p.y;

		// Can't go top
		if (p.y < 0) {
			newY = 0;
		}

		// Can't go left
		if (p.x < 0) {
			newX = 0;
		}

		// Can't go right
		if (p.x > canvasWidth) {
			newX = canvasWidth;
		}

		// Can't go bottom
		if (p.y > canvasHeight) {
			newY = canvasHeight;
		}

		return {
			x: newX,
			y: newY,
		};
	};

	const getRelativePointerPosition = (
		node: Konva.Node | Konva.Stage | undefined | null,
	): Konva.Vector2d | undefined => {
		// the function will return pointer position relative to the passed node
		const transform = node?.getAbsoluteTransform().copy();
		// to detect relative position we need to invert transform
		transform?.invert();

		// get pointer (say mouse or touch) position
		const pos = node?.getStage()?.getPointerPosition();

		// now we find a relative point
		if (pos) {
			return transform?.point(pos);
		}
	};

	const getDistance = (p1: Vector2d, p2: Vector2d) =>
		Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));

	const getCenter = (p1: Vector2d, p2: Vector2d) => ({
		x: (p1.x + p2.x) / 2,
		y: (p1.y + p2.y) / 2,
	});

	const startStroke = (e: Konva.KonvaEventObject<MouseEvent | TouchEvent>) => {
		if (!drawingMode) {
			return;
		}

		strokeStarted.current = true;

		const stage = e.target.getStage();

		if (stage) {
			emitStrokeStart({
				color: strokeColor,
				size: strokeSize,
			});

			stage.batchDraw();
		}
	};

	const continueStroke = (
		e: Konva.KonvaEventObject<MouseEvent | TouchEvent>,
	) => {
		if (!strokeStarted.current || !drawingMode) {
			return;
		}

		const stage = e.target.getStage();

		if (stage) {
			let pointPosition = getRelativePointerPosition(stage);

			pointPosition = stayInBounds(pointPosition as Vector2d);

			emitStrokeContinue({
				x: pointPosition.x,
				y: pointPosition.y,
			});
		}
	};

	const endStrokeDrawing = () => {
		if (strokeStarted.current) {
			strokeStarted.current = false;
			emitStrokeEnd();
		}
	};

	const zoom = (e: Konva.KonvaEventObject<WheelEvent>) => {
		e.evt.preventDefault();

		const stage = e.target.getStage();

		if (stage) {
			const oldScale = stage.scaleX();

			const pointer = stage.getPointerPosition();

			if (pointer) {
				const mousePointTo = {
					x: (pointer.x - stage.x()) / oldScale,
					y: (pointer.y - stage.y()) / oldScale,
				};

				const newScale =
					e.evt.deltaY < 0 ? oldScale * scaleBy : oldScale / scaleBy;

				stage.scale({ x: newScale, y: newScale });

				const newPos = {
					x: pointer.x - mousePointTo.x * newScale,
					y: pointer.y - mousePointTo.y * newScale,
				};

				stage.position(newPos);
			}

			stage.batchDraw();
		}
	};

	const pinchToZoom = (e: Konva.KonvaEventObject<TouchEvent>) => {
		e.evt.preventDefault();

		const stage = e.target.getStage();

		const touch1 = e.evt.touches[0];
		const touch2 = e.evt.touches[1];

		if (stage && touch1 && touch2) {
			// if the stage was under Konva's drag&drop
			// we need to stop it, and implement our own pan logic with two pointers
			if (stage.isDragging()) {
				stage.stopDrag();
			}

			const p1 = {
				x: touch1.clientX,
				y: touch1.clientY,
			};
			const p2 = {
				x: touch2.clientX,
				y: touch2.clientY,
			};

			if (!lastCenter) {
				lastCenter = getCenter(p1, p2);
				return;
			}
			const newCenter = getCenter(p1, p2);

			const dist = getDistance(p1, p2);

			if (!lastDist) {
				lastDist = dist;
			}

			// local coordinates of center point
			const pointTo = {
				x: (newCenter.x - stage.x()) / stage.scaleX(),
				y: (newCenter.y - stage.y()) / stage.scaleX(),
			};

			const newScale = stage.scaleX() * (dist / lastDist);

			stage.scaleX(newScale);
			stage.scaleY(newScale);

			// calculate new position of the stage
			const dx = newCenter.x - lastCenter.x;
			const dy = newCenter.y - lastCenter.y;

			const newPos = {
				x: newCenter.x - pointTo.x * newScale + dx,
				y: newCenter.y - pointTo.y * newScale + dy,
			};

			stage.position(newPos);

			lastDist = dist;
			lastCenter = newCenter;
		}
	};

	const resetPinchToZoom = () => {
		lastDist = 0;
		lastCenter = undefined;
	};

	const onMouseDown = (e: Konva.KonvaEventObject<MouseEvent>) => startStroke(e);
	const onMouseMove = (e: Konva.KonvaEventObject<MouseEvent>) =>
		continueStroke(e);
	const onMouseUp = () => endStrokeDrawing();

	const onTouchStart = (e: Konva.KonvaEventObject<TouchEvent>) => {
		e.evt.preventDefault();

		if (e.evt.touches.length === 1) {
			startStroke(e);
		} else {
			pinchToZoom(e);
		}
	};

	const onTouchMove = (e: Konva.KonvaEventObject<TouchEvent>) => {
		e.evt.preventDefault();

		if (e.evt.touches.length === 1) {
			continueStroke(e);
		} else {
			pinchToZoom(e);
		}
	};

	const onTouchEnd = (e: Konva.KonvaEventObject<TouchEvent>) => {
		e.evt.preventDefault();

		endStrokeDrawing();
		resetPinchToZoom();
	};

	const resize = useCallback(() => {
		setWindowDimensions({
			width:
				containerRef.current?.getBoundingClientRect().width ??
				window.innerWidth,
			height:
				(containerRef.current?.getBoundingClientRect().height ??
					window.innerHeight) -
				(toolbarRef?.current?.getBoundingClientRect().height || 0),
		});
	}, [toolbarRef]);

	// force resize at first render to expand the canvas in the parent div
	useLayoutEffect(() => resize(), [resize]);

	useEffect(() => {
		window.addEventListener('resize', resize);
		return () => window.removeEventListener('resize', resize);
	}, [resize]);

	const resizeObserverRef = useRef(
		new ResizeObserver(() => {
			resize();
		}),
	);

	useEffect(() => {
		const container = containerRef.current;
		const resizeObserver = resizeObserverRef.current;
		if (container != null) {
			resizeObserver.observe(container);
		}
		return () => {
			if (container) {
				resizeObserver.unobserve(container);
			}
			resizeObserver.disconnect();
		};
	}, []);

	return (
		<div ref={containerRef} className="static h-screen max-h-screen w-screen">
			<Stage
				width={windowDimensions.width}
				height={windowDimensions.height}
				draggable={!drawingMode}
				// dragBoundFunc={stayInBounds}
				// Common
				onContextMenu={(e) => e.evt.preventDefault()}
				onDragMove={() => void 0}
				onDragEnd={() => void 0}
				// Desktop
				onMouseDown={onMouseDown}
				onMouseMove={onMouseMove}
				onMouseUp={onMouseUp}
				onMouseLeave={onMouseUp}
				onWheel={zoom}
				// Mobile
				onTouchStart={onTouchStart}
				onTouchMove={onTouchMove}
				onTouchEnd={onTouchEnd}
				x={-x1}
				y={-y1}
				scaleX={scale}
				scaleY={scale}
				className={className}
			>
				<Layer listening={false}>
					<Rect
						width={canvasWidth}
						height={canvasHeight}
						fill={backgroundColor}
						listening={false}
						onDragMove={(e) => {
							e.target.absolutePosition({ x: 0, y: 0 });
						}}
					/>
					{[...players.values()]
						.flatMap((player) => player.strokes)
						.sort((a, b) => {
							if (a.timestamp && b.timestamp) {
								return a.timestamp.getTime() - b.timestamp.getTime();
							}

							return 0;
						})
						.map(({ points, color, size }, strokeId) => {
							const pointsAsArray = points?.flatMap(({ x, y }) => [x, y]) ?? [];

							if (pointsAsArray.length > 2) {
								return (
									<Line
										key={strokeId}
										points={pointsAsArray}
										stroke={color}
										strokeWidth={size}
										shadowForStrokeEnabled={false}
										lineCap="round"
										lineJoin="round"
										perfectDrawEnabled={false}
									/>
								);
							} else if (pointsAsArray.length === 2) {
								return (
									<Circle
										key={strokeId}
										x={pointsAsArray[0]}
										y={pointsAsArray[1]}
										radius={size / 2}
										fill={color}
										shadowForStrokeEnabled={false}
										perfectDrawEnabled={false}
									/>
								);
							}
						})}
					<Rect
						width={canvasWidth}
						height={canvasHeight}
						fill={backgroundColor}
						listening={false}
						onDragMove={(e) => {
							e.target.absolutePosition({ x: 0, y: 0 });
						}}
						globalCompositeOperation={'destination-in'}
					/>
				</Layer>
			</Stage>
		</div>
	);
};
