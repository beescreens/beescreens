import useTranslation from 'next-translate/useTranslation';

export type ModelProps = {
	children: React.ReactNode;
	title: string;
	close: (accept: boolean) => void;
};

export const Modal = ({ title, close, children }: ModelProps): JSX.Element => {
	const { t } = useTranslation('common');

	return (
		<>
			<div className="fixed inset-0 z-10 overflow-y-auto">
				<div className="flex min-h-screen items-end justify-center px-4 pb-20 pt-4 text-center sm:block sm:p-0">
					<div className="fixed inset-0 transition-opacity" aria-hidden="true">
						<div className="absolute inset-0 bg-gray-500 opacity-75"></div>
					</div>
					<span
						className="hidden sm:inline-block sm:h-screen sm:align-middle"
						aria-hidden="true"
					>
						&#8203;
					</span>

					<div
						className="inline-block translate-x-0 translate-y-0 overflow-hidden rounded-lg bg-gray-700 text-left align-bottom shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg sm:align-middle"
						role="dialog"
						aria-modal="true"
						aria-labelledby="modal-headline"
					>
						<div className="bg-gray-700 px-4 pb-4 pt-5 sm:p-6 sm:pb-4">
							<div className="sm:flex sm:items-start">
								<div className="mt-3 w-full text-center sm:mt-0 sm:text-left">
									<div className="relative">
										<div className="absolute right-0 top-0">
											<button
												onClick={() => close(false)}
												className="z-50 cursor-pointer"
											>
												<svg
													className="fill-current text-gray-100"
													xmlns="http://www.w3.org/2000/svg"
													width="18"
													height="18"
													viewBox="0 0 18 18"
												>
													<path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
												</svg>
											</button>
										</div>
									</div>
									<h3
										className="text-lg font-medium leading-6 text-gray-100"
										id="modal-headline"
									>
										{title}
									</h3>
									<div className="mb-6 mt-2">{children}</div>

									<button
										onClick={() => close(true)}
										className="w-full rounded bg-gray-800 px-4 py-2 font-bold text-white hover:bg-gray-900"
									>
										{t('play.tools.apply.title')}
									</button>
								</div>
							</div>
						</div>
						<div className="bg-gray-800 px-4 py-3 sm:flex sm:flex-row-reverse sm:px-6"></div>
					</div>
				</div>
			</div>
		</>
	);
};
