import { GetServerSideProps, InferGetServerSidePropsType } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
// import { useEffect } from 'react';
// import { useDispatch } from 'react-redux';
import { CommonLayout } from '../components/layouts/CommonLayout';
// import { requestGames } from '../store/actions';

export type AppProps = {
	version: string;
};

export const getServerSideProps: GetServerSideProps<AppProps> = async () => ({
	props: {
		version: process.env.npm_package_version as string,
	},
});

const App = ({
	version,
}: InferGetServerSidePropsType<typeof getServerSideProps>): JSX.Element => {
	const { t } = useTranslation('common');

	return (
		<CommonLayout
			title={`${t('homepage.title')} | ${t('title')}`}
			version={version}
		>
			<h1 className="m-4 mt-32 text-center text-4xl font-medium">
				{t('title')} - {t('homepage.title')}
			</h1>
			<div className="mb-4" />
			<div className="text-center">{t('homepage.welcome')}</div>
			<div className="mb-4" />
			<div className="text-center">
				<Link href="/play" className="focus:outline-none">
					<button
						type="button"
						className="ease m-6 select-none rounded-md border border-indigo-500 px-4 py-2 text-indigo-500 transition duration-500 hover:bg-indigo-600 hover:text-white focus:outline-none focus:ring-2 focus:ring-indigo-600"
					>
						{t('homepage.displayGames')}
					</button>
				</Link>
			</div>

			<div className="mb-4" />
			<div className="mb-auto text-center">
				<Link href="/about" className="focus:outline-none">
					<button
						type="button"
						className="ease m-2 select-none rounded-md border border-indigo-500 px-4 py-2 text-indigo-500 transition duration-500 hover:bg-indigo-600 hover:text-white focus:outline-none focus:ring-2 focus:ring-indigo-600"
					>
						{t('about.title')}
					</button>
				</Link>
			</div>
		</CommonLayout>
	);
};

export default App;
