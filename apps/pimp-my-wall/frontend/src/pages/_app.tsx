import 'tailwindcss/tailwind.css';
import '../styles.css';
import { AppProps } from 'next/app';
import { config, library } from '@fortawesome/fontawesome-svg-core';
import '@fortawesome/fontawesome-svg-core/styles.css';
import {
	faSignature,
	faPaintBrush,
	faArrowsAlt,
	faBrush,
	faPalette,
	faEllipsisV,
	faUndo,
	faDoorOpen,
	faDoorClosed,
	faRedo,
	faSignOutAlt,
	faTrash,
} from '@fortawesome/free-solid-svg-icons';
import { enableMapSet } from 'immer';

// See https://github.com/FortAwesome/react-fontawesome#integrating-with-other-tools-and-frameworks
config.autoAddCss = false;

// Import @fortawesome/free-brands-svg-icons
library.add();

// Import @fortawesome/free-regular-svg-icons
library.add();

// Import @fortawesome/free-solid-svg-icons
library.add(
	faSignature,
	faPaintBrush,
	faArrowsAlt,
	faBrush,
	faPalette,
	faEllipsisV,
	faUndo,
	faDoorOpen,
	faDoorClosed,
	faRedo,
	faSignOutAlt,
	faTrash,
);

enableMapSet();

const App = ({ Component, pageProps }: AppProps): JSX.Element => {
	return <Component {...pageProps} />;
};

export default App;
