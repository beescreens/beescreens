import { GetServerSideProps, InferGetServerSidePropsType } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import { CommonLayout } from '../components/layouts/CommonLayout';

export type AppProps = {
	version: string;
};

export const getServerSideProps: GetServerSideProps<AppProps> = async () => ({
	props: {
		version: process.env.PIMP_MY_WALL_VERSION as string,
	},
});

const App = ({
	version,
}: InferGetServerSidePropsType<typeof getServerSideProps>): JSX.Element => {
	const { t } = useTranslation('common');

	return (
		<CommonLayout
			title={`${t('reset.title')} | ${t('title')}`}
			version={version}
		>
			<h1 className="m-4 mt-36 text-center text-4xl font-medium">
				{t('title')} - {t('reset.title')}
			</h1>
			<p className="mb-4 whitespace-pre-line">{t('reset.message')}</p>
			<div className="text-center">
				<Link href="/" className="focus:outline-none">
					<button
						type="button"
						className="ease m-2 select-none rounded-md border border-indigo-500 px-4 py-2 text-indigo-500 transition duration-500 hover:bg-indigo-600 hover:text-white focus:outline-none focus:ring-2 focus:ring-indigo-600"
					>
						{t('returnToHomepage')}
					</button>
				</Link>
			</div>
		</CommonLayout>
	);
};

export default App;
