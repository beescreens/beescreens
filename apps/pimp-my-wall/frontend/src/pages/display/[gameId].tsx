import { GetServerSideProps, InferGetServerSidePropsType } from 'next';
import useTranslation from 'next-translate/useTranslation';
import dynamic from 'next/dynamic';
import { useEffect, useState } from 'react';
import { SketchProps } from '../../components/Sketch';
import { ScreenSaverProps } from '../../components/ScreenSaver';
import { useRouter } from 'next/router';
import { GameId } from '@beescreens/pimp-my-wall';
import { DisplayLayout } from '../../components/layouts/DisplayLayout';
import { isTrulyTrue, sleep } from '../../utils';
import { WebSocketProvider } from '@/providers/WebsocketProvider';

const Sketch = dynamic<SketchProps>(
	() => import('@/components/Sketch').then((mod) => mod.Sketch),
	{
		ssr: false,
	},
);

const ScreenSaver = dynamic<ScreenSaverProps>(
	() => import('@/components/ScreenSaver').then((mod) => mod.ScreenSaver),
	{
		ssr: false,
	},
);

export type AppProps = {
	backendSocketIoUrl: string;
	screensaverFrequency: number;
	screensaverDuration: number;
};

export const getServerSideProps: GetServerSideProps<AppProps> = async () => ({
	props: {
		backendSocketIoUrl: process.env
			.PIMP_MY_WALL_BACKEND_SOCKET_IO_URL as string,
		screensaverFrequency: parseInt(
			process.env.PIMP_MY_WALL_SCREENSAVER_FREQUENCY as string,
			10,
		),
		screensaverDuration: parseInt(
			process.env.PIMP_MY_WALL_SCREENSAVER_DURATION as string,
			10,
		),
	},
});

const App = ({
	backendSocketIoUrl,
	screensaverFrequency,
	screensaverDuration,
}: InferGetServerSidePropsType<typeof getServerSideProps>): JSX.Element => {
	const { t } = useTranslation('common');
	const { query } = useRouter();
	const gameId = query.gameId as GameId;
	const [showScreensaver, changeShowScreensaver] = useState(false);

	const screensaverMode = isTrulyTrue(query.screensaver);

	useEffect(() => {
		if (screensaverMode) {
			// Define the function to show/hide the screensaver
			const startScreensaver = async () => {
				await sleep(screensaverFrequency);
				changeShowScreensaver(true);

				await sleep(screensaverDuration);
				changeShowScreensaver(false);

				// Restart the timer
				await startScreensaver();
			};

			// Start the screensaver the first time
			startScreensaver();
		}
	}, [screensaverDuration, screensaverFrequency, screensaverMode]);

	if (screensaverMode) {
		return (
			<WebSocketProvider
				backendSocketIoUrl={backendSocketIoUrl}
				gameId={gameId}
				isDisplay={true}
			>
				<DisplayLayout title={`${t('display.title')} | ${t('title')}`}>
					<ScreenSaver
						className={`overflow-visible ${
							showScreensaver ? 'animate-fade-in' : 'animate-fade-out'
						}`}
					/>
					<Sketch
						className={`overflow-visible ${
							showScreensaver ? 'animate-fade-out' : 'animate-fade-in'
						}`}
					/>
				</DisplayLayout>
			</WebSocketProvider>
		);
	}

	return (
		<WebSocketProvider
			backendSocketIoUrl={backendSocketIoUrl}
			gameId={gameId}
			isDisplay={true}
		>
			<DisplayLayout title={`${t('display.title')} | ${t('title')}`}>
				<Sketch />
			</DisplayLayout>
		</WebSocketProvider>
	);
};

export default App;
