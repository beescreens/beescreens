import { GetServerSideProps, InferGetServerSidePropsType } from 'next';
import useTranslation from 'next-translate/useTranslation';
import { WebSocketProvider } from '@/providers/WebsocketProvider';
import { useGamesStore } from '@/store/games';
import { CommonLayout } from '@/components/layouts/CommonLayout';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export type AppProps = {
	version: string;
	backendSocketIoUrl: string;
};

export const getServerSideProps: GetServerSideProps<AppProps> = async () => ({
	props: {
		version: process.env.PIMP_MY_WALL_VERSION as string,
		backendSocketIoUrl: process.env
			.PIMP_MY_WALL_BACKEND_SOCKET_IO_URL as string,
	},
});

const App = ({
	version,
	backendSocketIoUrl,
}: InferGetServerSidePropsType<typeof getServerSideProps>): JSX.Element => {
	const { t } = useTranslation('common');

	const games = useGamesStore((state) => state.games) ?? [];

	return (
		<WebSocketProvider backendSocketIoUrl={backendSocketIoUrl}>
			<CommonLayout
				title={`${t('display.title')} | ${t('title')}`}
				version={version}
			>
				<h1 className="m-4 mt-36 text-center text-4xl font-medium">
					{t('title')} - {t('display.title')}
				</h1>
				<div className="mb-4" />
				<div className="text-center">{t('display.welcome')}</div>
				<div className="mb-4" />
				{games.map(({ id, name }) => {
					return (
						<div key={id} className="text-center">
							<Link
								href={`/display/${encodeURIComponent(id)}`}
								className="focus:outline-none"
							>
								<button
									type="button"
									className="ease m-2 select-none rounded-md border border-indigo-500 px-4 py-2 text-indigo-500 transition duration-500 focus:outline-none focus:ring-2 focus:ring-indigo-600"
								>
									<FontAwesomeIcon
										icon={['fas', 'door-open']}
										className="mr-2"
									/>
									{t('display.displayGame', {
										name,
									})}
								</button>
							</Link>
						</div>
					);
				})}
				<div className="mb-4" />
			</CommonLayout>
		</WebSocketProvider>
	);
};

export default App;
