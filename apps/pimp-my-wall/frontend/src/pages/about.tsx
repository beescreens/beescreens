import { GetServerSideProps, InferGetServerSidePropsType } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import { CommonLayout } from '../components/layouts/CommonLayout';

export type AppProps = {
	version: string;
};

export const getServerSideProps: GetServerSideProps<AppProps> = async () => ({
	props: {
		version: process.env.PIMP_MY_WALL_VERSION as string,
	},
});

const App = ({
	version,
}: InferGetServerSidePropsType<typeof getServerSideProps>): JSX.Element => {
	const { t } = useTranslation('common');

	return (
		<CommonLayout
			title={`${t('about.title')} | ${t('title')}`}
			version={version}
		>
			<h1 className="m-4 mt-36 text-center text-4xl font-medium">
				{t('title')} - {t('about.title')}
			</h1>
			<div className="mb-4">
				<h2 className="m-2 text-center text-2xl font-medium">
					{t('about.introduction.title')}
				</h2>
				<p className="mb-4 whitespace-pre-line">
					{t('about.introduction.text')}
				</p>
			</div>
			<div className="mb-4">
				<h2 className="m-2 text-center text-2xl font-medium">
					{t('about.tools.title')}
				</h2>
				<p className="mb-4 whitespace-pre-line">{t('about.tools.text')}</p>
				<ul className="mb-4 list-inside list-disc whitespace-pre-line">
					<li>
						{t('play.tools.move.title')}: {t('play.tools.move.placeholder')}
					</li>
					<li>
						{t('play.tools.cancel.title')}: {t('play.tools.cancel.placeholder')}
					</li>
					<li>
						{t('play.tools.pinchToZoom.title')}:{' '}
						{t('play.tools.pinchToZoom.placeholder')}
					</li>
					<li>
						{t('play.tools.draw.title')}: {t('play.tools.draw.placeholder')}
					</li>
					<li>
						{t('play.tools.strokeColor.title')}:{' '}
						{t('play.tools.strokeColor.placeholder')}
					</li>
					<li>
						{t('play.tools.strokeSize.title')}:{' '}
						{t('play.tools.strokeSize.placeholder')}
					</li>
					<li>
						{t('play.tools.backgroundColor.title')}:{' '}
						{t('play.tools.backgroundColor.placeholder')}
					</li>
				</ul>
			</div>
			<div className="mb-4">
				<h2 className="m-2 text-center text-2xl font-medium">
					{t('about.lastElements.title')}
				</h2>
				<p className="mb-4 whitespace-pre-line">
					{t('about.lastElements.text', { minutes: 3 })}
				</p>
			</div>
			<div className="mb-4">
				<h2 className="m-2 text-center text-2xl font-medium">
					{t('about.technologies.title')}
				</h2>
				<p className="mb-4 whitespace-pre-line">
					{t('about.technologies.text')}
				</p>
				<div className="text-center underline">
					<a href="https://gitlab.com/beescreens/beescreens">
						{t('about.goToSourceCode')}
					</a>
				</div>
			</div>
			<div className="mb-36">
				<div className="text-center">
					<Link href="/" className="focus:outline-none">
						<button
							type="button"
							className="ease m-2 select-none rounded-md border border-indigo-500 px-4 py-2 text-indigo-500 transition duration-500 hover:bg-indigo-600 hover:text-white focus:outline-none focus:ring-2 focus:ring-indigo-600"
						>
							{t('returnToHomepage')}
						</button>
					</Link>
				</div>
			</div>
		</CommonLayout>
	);
};

export default App;
