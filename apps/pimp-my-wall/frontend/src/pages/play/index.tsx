import { GetServerSideProps, InferGetServerSidePropsType } from 'next';
import useTranslation from 'next-translate/useTranslation';
import { CommonLayout } from '../../components/layouts/CommonLayout';
import { WebSocketProvider } from '@/providers/WebsocketProvider';
import { GamesList } from '@/components/GamesList';

export type AppProps = {
	version: string;
	backendSocketIoUrl: string;
};

export const getServerSideProps: GetServerSideProps<AppProps> = async () => ({
	props: {
		version: process.env.npm_package_version as string,
		backendSocketIoUrl: process.env
			.PIMP_MY_WALL_BACKEND_SOCKET_IO_URL as string,
	},
});

const App = ({
	version,
	backendSocketIoUrl,
}: InferGetServerSidePropsType<typeof getServerSideProps>): JSX.Element => {
	const { t } = useTranslation('common');

	return (
		<WebSocketProvider backendSocketIoUrl={backendSocketIoUrl}>
			<CommonLayout
				title={`${t('play.title')} | ${t('title')}`}
				version={version}
			>
				<h1 className="m-4 mt-36 text-center text-4xl font-medium">
					{t('title')} - {t('play.title')}
				</h1>
				<div className="mb-4" />
				<div className="text-center">{t('play.welcome')}</div>
				<div className="mb-4" />
				<GamesList />
				<div className="mb-4" />
			</CommonLayout>
		</WebSocketProvider>
	);
};

export default App;
