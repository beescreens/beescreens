import { GetServerSideProps, InferGetServerSidePropsType } from 'next';
import useTranslation from 'next-translate/useTranslation';
import dynamic from 'next/dynamic';
import { useCallback, useEffect } from 'react';
import { PlayLayoutProps } from '../../components/layouts/PlayLayout';
import { SketchProps } from '../../components/Sketch';
import router, { useRouter } from 'next/router';
import { GameId } from '@beescreens/pimp-my-wall';
import { WebSocketProvider } from '@/providers/WebsocketProvider';
import { useToolsStore } from '@/store/tools';
import { useGameStore } from '@/store/game';

const PlayLayout = dynamic<PlayLayoutProps>(
	() => import('@/components/layouts/PlayLayout').then((mod) => mod.PlayLayout),
	{ ssr: false },
);

const Sketch = dynamic<SketchProps>(
	() => import('@/components/Sketch').then((mod) => mod.Sketch),
	{
		ssr: false,
	},
);

export type AppProps = {
	version: string;
	backendSocketIoUrl: string;
	idleTimeout: number;
	strokeColor: string;
	strokeMinSize: number;
	strokeSize: number;
	strokeMaxSize: number;
};

export const getServerSideProps: GetServerSideProps<AppProps> = async () => ({
	props: {
		version: process.env.npm_package_version as string,
		backendSocketIoUrl: process.env
			.PIMP_MY_WALL_BACKEND_SOCKET_IO_URL as string,
		idleTimeout: parseInt(process.env.PIMP_MY_WALL_IDLE_TIMEOUT as string, 10),
		strokeColor: process.env.PIMP_MY_WALL_STROKE_COLOR as string,
		strokeMinSize: parseInt(
			process.env.PIMP_MY_WALL_STROKE_MIN_SIZE as string,
			10,
		),
		strokeSize: parseInt(process.env.PIMP_MY_WALL_STROKE_SIZE as string, 10),
		strokeMaxSize: parseInt(
			process.env.PIMP_MY_WALL_STROKE_MAX_SIZE as string,
			10,
		),
	},
});

const App = ({
	version,
	backendSocketIoUrl,
	idleTimeout,
	strokeColor,
	strokeMinSize,
	strokeSize,
	strokeMaxSize,
}: InferGetServerSidePropsType<typeof getServerSideProps>): JSX.Element => {
	const { t } = useTranslation('common');
	const { query } = useRouter();
	const gameId = query.gameId as GameId;

	const gameStrokeColor = useGameStore((state) => state.game?.strokeColor);

	const setStrokeColor = useToolsStore((state) => state.setStrokeColor);
	const setStrokeMinSize = useToolsStore((state) => state.setStrokeMinSize);
	const setStrokeSize = useToolsStore((state) => state.setStrokeSize);
	const setStrokeMaxSize = useToolsStore((state) => state.setStrokeMaxSize);

	const timeOutListener = useCallback(() => {
		if (document.hidden) {
			setTimeout(() => {
				console.log(`Idle check after: ${idleTimeout} seconds`);
				if (document.hidden) {
					router.push('/end');
				}
			}, idleTimeout * 1000);
		}
	}, [idleTimeout]);

	useEffect(() => {
		setStrokeColor(gameStrokeColor ?? strokeColor);
		setStrokeMinSize(strokeMinSize);
		setStrokeSize(strokeSize);
		setStrokeMaxSize(strokeMaxSize);
	});

	useEffect(() => {
		if (typeof window && document.hidden !== undefined) {
			document.addEventListener('visibilitychange', timeOutListener, true);
		}
		return () => {
			document.removeEventListener('visibilitychange', timeOutListener, true);
		};
	}, [timeOutListener]);

	return (
		<WebSocketProvider backendSocketIoUrl={backendSocketIoUrl} gameId={gameId}>
			<PlayLayout
				title={`${t('play.title')} | ${t('title')}`}
				version={version}
			>
				<Sketch />
			</PlayLayout>
		</WebSocketProvider>
	);
};

export default App;
