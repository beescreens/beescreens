import { GetServerSideProps, InferGetServerSidePropsType } from 'next';
import useTranslation from 'next-translate/useTranslation';
import { useState } from 'react';
import { CommonLayout } from '../components/layouts/CommonLayout';
import { GameId } from '@beescreens/pimp-my-wall';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useRouter } from 'next/router';
import { useGamesStore } from '@/store/games';
import { WebSocketProvider } from '@/providers/WebsocketProvider';

export type AppProps = {
	backendApiUrl: string;
	backendSocketIoUrl: string;
	version: string;
};

export const getServerSideProps: GetServerSideProps<AppProps> = async ({
	query,
}) => {
	const apiKey = process.env.PIMP_MY_WALL_API_KEY;
	const backendApiUrl = process.env.PIMP_MY_WALL_BACKEND_API_URL as string;
	const backendSocketIoUrl = process.env
		.PIMP_MY_WALL_BACKEND_SOCKET_IO_URL as string;
	const version = process.env.PIMP_MY_WALL_VERSION as string;

	const apiKeyQuery = query.apiKey;

	if (apiKeyQuery != apiKey) {
		return {
			redirect: {
				destination: '/',
				permanent: false,
			},
		};
	}

	return {
		props: {
			backendApiUrl,
			backendSocketIoUrl,
			version,
		},
	};
};

const App = ({
	backendApiUrl,
	backendSocketIoUrl,
	version,
}: InferGetServerSidePropsType<typeof getServerSideProps>): JSX.Element => {
	const { t } = useTranslation('common');

	const games = useGamesStore((state) => state.games) ?? [];

	const { query } = useRouter();

	const apiKey = query.apiKey as string;

	const [isCreatingGame, setIsCreatingGame] = useState(false);

	const [formData, setFormData] = useState({
		name: '',
		maximumNumberOfPlayers: '',
		canvasWidth: '',
		canvasHeight: '',
		strokeColor: '',
		backgroundColor: '',
	});

	const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
		const { name, value } = event.currentTarget;
		setFormData({ ...formData, [name]: value });
	};

	const cancelForm = () => {
		setIsCreatingGame(false);
	};

	const submitForm = async () => {
		const name = formData.name;
		const maximumNumberOfPlayers = parseInt(
			formData.maximumNumberOfPlayers,
			10,
		);
		const canvasWidth = parseInt(formData.canvasWidth, 10);
		const canvasHeight = parseInt(formData.canvasHeight, 10);
		const strokeColor = formData.strokeColor;
		const backgroundColor = formData.backgroundColor;

		const response = await fetch(`${backendApiUrl}/v1/games`, {
			method: 'POST',
			headers: {
				Authorization: apiKey,
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				name,
				maximumNumberOfPlayers,
				canvasWidth,
				canvasHeight,
				strokeColor,
				backgroundColor,
			}),
		});

		if (response.status === 201) {
			setIsCreatingGame(false);
		}
	};

	const deleteGame = async (id: GameId) => {
		await fetch(`${backendApiUrl}/v1/games/${id}`, {
			method: 'DELETE',
			headers: {
				Authorization: apiKey,
			},
		});
	};

	const resetGame = async (id: GameId) => {
		await fetch(`${backendApiUrl}/v1/games/${id}/reset`, {
			method: 'DELETE',
			headers: {
				Authorization: apiKey,
			},
		});
	};

	return (
		<WebSocketProvider backendSocketIoUrl={backendSocketIoUrl}>
			<CommonLayout
				title={`${t('admin.title')} | ${t('title')}`}
				version={version}
			>
				<h1 className="m-4 mt-36 text-center text-4xl font-medium">
					{t('title')} - {t('admin.title')}
				</h1>
				<div className="mb-4" />
				<div className="text-center">{t('homepage.welcome')}</div>
				<div className="mb-4" />
				<ul className="mx-auto mt-16 w-96 rounded-lg border border-gray-200 bg-white text-sm font-medium text-gray-900 dark:border-gray-600 dark:bg-gray-700 dark:text-white">
					{games.map(
						({ id, name, currentNumberOfPlayers, maximumNumberOfPlayers }) => {
							return (
								<li
									key={id}
									className="flex w-full items-center justify-between border-b border-gray-200 px-4 py-2 first:rounded-t-lg last:rounded-b-lg dark:border-gray-600"
								>
									<>{t('admin.game', { name })}</>
									<span>
										{t('admin.players', {
											currentNumberOfPlayers,
											maximumNumberOfPlayers,
										})}
									</span>
									<div>
										<button
											type="button"
											onClick={() => resetGame(id)}
											className="ease m-2 select-none rounded-md border bg-red-700 px-4 py-2 text-white transition duration-500 hover:border-red-600 hover:text-red-600 focus:outline-none focus:ring-2 focus:ring-red-700"
										>
											<FontAwesomeIcon icon={['fas', 'redo']} />
										</button>
										<button
											type="button"
											onClick={() => deleteGame(id)}
											className="ease m-2 select-none rounded-md border bg-red-700 px-4 py-2 text-white transition duration-500 hover:border-red-600 hover:text-red-600 focus:outline-none focus:ring-2 focus:ring-red-700"
										>
											<FontAwesomeIcon icon={['fas', 'trash']} />
										</button>
									</div>
								</li>
							);
						},
					)}
					{isCreatingGame ? (
						<div className="grid justify-items-center py-2 ">
							<div className="">
								<label htmlFor="name">{t('admin.form.name')}</label>
								<input
									value={formData.name}
									onChange={handleChange}
									className="m-2 rounded-lg outline"
									type="text"
									name="name"
									id="name"
								/>
							</div>
							<div>
								<label htmlFor="maximumNumberOfPlayers">
									{t('admin.form.maximumNumberOfPlayers')}
								</label>
								<input
									onChange={handleChange}
									value={formData.maximumNumberOfPlayers}
									className="m-2 rounded-lg outline"
									type="number"
									name="maximumNumberOfPlayers"
									id="maximumNumberOfPlayers"
								/>
							</div>
							<div>
								<label htmlFor="canvasWidth">
									{t('admin.form.canvasWidth')}
								</label>
								<input
									value={formData.canvasWidth}
									onChange={handleChange}
									className="m-2 rounded-lg outline"
									type="number"
									name="canvasWidth"
									id="canvasWidth"
								/>
							</div>
							<div>
								<label htmlFor="canvasHeight">
									{t('admin.form.canvasHeight')}
								</label>
								<input
									value={formData.canvasHeight}
									onChange={handleChange}
									className="m-2 rounded-lg outline"
									type="number"
									name="canvasHeight"
									id="canvasHeight"
								/>
							</div>
							<div>
								<label htmlFor="strokeColor">
									{t('admin.form.strokeColor')}
								</label>
								<input
									value={formData.strokeColor}
									onChange={handleChange}
									className="m-2 rounded-lg outline"
									type="color"
									name="strokeColor"
									id="strokeColor"
								/>
							</div>
							<div>
								<label htmlFor="backgroundColor">
									{t('admin.form.backgroundColor')}
								</label>
								<input
									value={formData.backgroundColor}
									onChange={handleChange}
									className="m-2 rounded-lg outline"
									type="color"
									name="backgroundColor"
									id="backgroundColor"
								/>
							</div>
							<div>
								<button
									className="mx-2 rounded-lg border-2 border-solid px-2"
									onClick={() => cancelForm()}
								>
									{t('admin.form.cancel')}
								</button>
								<button
									onClick={() => submitForm()}
									className="rounded-lg border-2 border-solid px-2"
								>
									{t('admin.form.submit')}
								</button>
							</div>
						</div>
					) : (
						<div className="mx-auto grid place-items-center py-2">
							<button
								type="button"
								onClick={() => setIsCreatingGame(true)}
								className="h-10 w-10 rounded-full bg-blue-500 text-white hover:bg-red-500"
							>
								+
							</button>
						</div>
					)}
				</ul>
			</CommonLayout>
		</WebSocketProvider>
	);
};

export default App;
