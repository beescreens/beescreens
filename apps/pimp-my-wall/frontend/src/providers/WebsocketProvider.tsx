import {
	createContext,
	useCallback,
	useContext,
	useEffect,
	useMemo,
} from 'react';
import { io } from 'socket.io-client';
import {
	ClientEvent,
	CreatePoint,
	CreateStroke,
	GameId,
	HexColor,
	PimpMyWallFrontendSocket,
	ServerEvent,
	customJsonParser,
} from '@beescreens/pimp-my-wall';
import { useGameStore } from '@/store/game';
import { useGamesStore } from '@/store/games';

interface WebSocketContextType {
	emitStrokeContinue: (point: CreatePoint) => void;
	emitStrokeEnd: () => void;
	emitStrokeStart: (stroke: CreateStroke) => void;
	emitUndo: () => void;
	emitBackgroundColor: (color: HexColor) => void;
}

interface WebSocketProviderProps {
	backendSocketIoUrl: string;
	gameId?: GameId;
	isDisplay?: boolean;
	children: React.ReactNode;
}

const WebSocketContext = createContext<WebSocketContextType | undefined>(
	undefined,
);

function WebSocketProvider({
	backendSocketIoUrl,
	gameId,
	isDisplay = false,
	children,
}: WebSocketProviderProps): JSX.Element {
	const socket: PimpMyWallFrontendSocket = useMemo(
		() =>
			io(backendSocketIoUrl, {
				autoConnect: false,
				transports: ['websocket'],
				parser: customJsonParser,
			}),
		[backendSocketIoUrl],
	);

	const setGames = useGamesStore((state) => state.setGames);
	const setGame = useGameStore((state) => state.setGame);
	const setBackgroundColor = useGameStore((state) => state.setBackgroundColor);
	const createNewPlayer = useGameStore((state) => state.createNewPlayer);
	const strokeStart = useGameStore((state) => state.strokeStart);
	const strokeContinue = useGameStore((state) => state.strokeContinue);
	const strokeEnd = useGameStore((state) => state.strokeEnd);
	const undo = useGameStore((state) => state.undo);

	const join = useCallback(
		async (socket: PimpMyWallFrontendSocket, gameId: GameId) => {
			console.log('Joining game', gameId);

			socket.emit(ClientEvent.JOIN, { gameId, isDisplay }, (game) => {
				console.log('Joined game', game);
				setGame({
					...game,
					players: new Map(game.players.map((player) => [player.id, player])),
				});
			});
		},
		[isDisplay, setGame],
	);

	useEffect(() => {
		if (!socket.connected) {
			socket.connect();

			if (gameId) {
				join(socket, gameId);
			}
		}

		return () => {
			socket.disconnect();
		};
	}, [gameId, join, socket]);

	useEffect(() => {
		if (!socket) return;

		socket.on(ServerEvent.CONNECT, () => {
			console.log('Connected to the server');

			socket.emit(ClientEvent.GET_AVAILABLE_GAMES, undefined, (games) => {
				setGames(games);
			});
		});

		socket.on(ServerEvent.NEW_PLAYER, (player) => {
			createNewPlayer(player);
		});

		socket.on(ServerEvent.AVAILABLE_GAMES, (games) => {
			setGames(games);
		});

		socket.on(ServerEvent.STROKE_START_FROM_PLAYER, (stroke) => {
			strokeStart(stroke);
		});

		socket.on(ServerEvent.STROKE_CONTINUE_FROM_PLAYER, (point) => {
			strokeContinue(point);
		});

		socket.on(ServerEvent.STROKE_END_FROM_PLAYER, (fromPlayer) => {
			strokeEnd(fromPlayer);
		});

		socket.on(ServerEvent.UNDO_FROM_PLAYER, (fromPlayer) => {
			undo(fromPlayer);
		});

		socket.on(ServerEvent.BACKGROUND_COLOR_FROM_PLAYER, ({ data }) => {
			setBackgroundColor(data.backgroundColor);
		});

		socket.on(ServerEvent.RESET, (game) => {
			if (isDisplay && game) {
				setGame({
					...game,
					players: new Map(game.players.map((player) => [player.id, player])),
				});
			} else {
				// Force a refresh to restart the socket connection
				window.location.href = '/reset';
			}
		});

		return () => {
			socket.off(ServerEvent.CONNECT);
			socket.off(ServerEvent.AVAILABLE_GAMES);
			socket.off(ServerEvent.STROKE_START_FROM_PLAYER);
			socket.off(ServerEvent.STROKE_CONTINUE_FROM_PLAYER);
			socket.off(ServerEvent.STROKE_END_FROM_PLAYER);
			socket.off(ServerEvent.UNDO_FROM_PLAYER);
			socket.off(ServerEvent.NEW_PLAYER);
			socket.off(ServerEvent.BACKGROUND_COLOR_FROM_PLAYER);
			socket.off(ServerEvent.RESET);
		};
	}, [
		createNewPlayer,
		setBackgroundColor,
		setGames,
		socket,
		strokeContinue,
		strokeEnd,
		strokeStart,
		undo,
	]);

	const emitStrokeStart = (stroke: CreateStroke) => {
		socket.emit(ClientEvent.STROKE_START, stroke);
	};

	const emitStrokeContinue = (point: CreatePoint) => {
		socket.emit(ClientEvent.STROKE_CONTINUE, point);
	};

	const emitStrokeEnd = () => {
		socket.emit(ClientEvent.STROKE_END);
	};

	const emitUndo = () => {
		socket.emit(ClientEvent.UNDO);
	};

	const emitBackgroundColor = (color: HexColor) => {
		socket.emit(ClientEvent.BACKGROUND_COLOR, color);
	};

	return (
		<WebSocketContext.Provider
			value={{
				emitStrokeContinue,
				emitStrokeEnd,
				emitStrokeStart,
				emitUndo,
				emitBackgroundColor,
			}}
		>
			{children}
		</WebSocketContext.Provider>
	);
}

function useWebSocket() {
	const context = useContext(WebSocketContext);

	if (context === undefined) {
		throw new Error('useWebSocket must be used within a WebSocketProvider');
	}

	return context;
}

export { WebSocketProvider, useWebSocket };
