module.exports = {
	ignorePatterns: ['!.eslintrc.js', '!.prettierrc.js'],
	extends: [
		'next/core-web-vitals',
		'plugin:tailwindcss/recommended',
		'plugin:@typescript-eslint/recommended',
	],
};
