/** @type {import('next').NextConfig} */
// eslint-disable-next-line @typescript-eslint/no-var-requires
const nextTranslate = require('next-translate-plugin');

const nextConfig = {
	poweredByHeader: false,
	reactStrictMode: true,
	experimental: {
		externalDir: true,
	},
};

module.exports = nextTranslate(nextConfig);
