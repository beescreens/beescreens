module.exports = {
	locales: ['en', 'fr'],
	defaultLocale: 'fr',
	pages: {
		'*': ['common'],
	},
	loadLocaleFrom: (locale, namespace) =>
		import(`./src/i18n/${locale}/${namespace}`).then((m) => m.default),
};
