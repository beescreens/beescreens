# Pimp My Wall frontend

## Prerequisites

The following prerequisites must be filled to run this service:

- [Node.js](../../../docs/explanations/about-nodejs/index.md) must be installed.
- [pnpm](../../../docs/explanations/about-pnpm/index.md) must be installed.
- [Docker](../../../docs/explanations/about-docker/index.md) must be installed (optional, only for deployment testing).
- [Docker Compose](../../../docs/explanations/about-docker/index.md) must be installed (optional, only for deployment testing).

## Set up the service

```sh
# Install the dependencies
pnpm install

# Copy the .env.defaults file to a .env file.
cp .env.defaults .env

# Optional: edit the environment variables file
vim .env
```

## Start the service in development mode

```sh
# Start the service in development mode
pnpm dev
```

The service should be now accessible on [http://localhost:3000](http://localhost:3000)

## Build the service

```sh
# Build the service
pnpm build
```

## Start the service in production mode

```sh
# Start the service in production mode
pnpm start
```

## Test with Docker Compose

In order to test with Docker Compose, execute the following steps:

```sh
# Switch to the deployment directory
cd ../../../deployment/pimp-my-wall

# Copy the `docker-compose.override.development.yaml` to `docker-compose.override.yaml`
cp docker-compose.override.development.yaml docker-compose.override.yaml

# Build the service
pnpm --filter @beescreens/pimp-my-wall-frontend build

# Bundle the service
pnpm --filter @beescreens/pimp-my-wall-frontend bundle

# Build the Docker image for this service
docker compose build frontend

# Start the Docker container for this service
docker compose up frontend
```
