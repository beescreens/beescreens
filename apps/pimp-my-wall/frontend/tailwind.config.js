module.exports = {
	content: ['./src/pages/**/*.tsx', './src/components/**/*.tsx'],
	important: true,
	theme: {
		container: {
			screens: {
				sm: '640px',
				md: '768px',
				lg: '1024px',
			},
		},
		extend: {
			// https://tailwindcss.com/docs/animation
			animation: {
				'fade-in': 'fadeIn 1s linear forwards',
				'fade-out': 'fadeOut 1s linear forwards',
			},
			keyframes: () => ({
				fadeIn: {
					from: { opacity: '0', position: 'absolute', visibility: 'hidden' },
					to: { opacity: '1', position: 'static', visibility: 'visible' },
				},
				fadeOut: {
					from: { opacity: '1', position: 'static', visibility: 'visible' },
					to: { opacity: '0', position: 'absolute', visibility: 'hidden' },
				},
			}),
		},
	},
	plugins: [],
};
