# Pimp My Wall

Check the online reference **Pimp My Wall** at <https://docs.beescreens.ch/reference/pimp-my-wall/>.

You can also check the offline reference in this repository at [`../../docs/reference/pimp-my-wall.md`](../../docs/reference/pimp-my-wall.md).
