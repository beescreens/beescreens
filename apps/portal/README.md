# Portal

Portal to choose an interactive application to use.

Based on the [React Three Fiber Portal demo](https://codesandbox.io/p/sandbox/enter-portals-9m4tpc?).

## Prerequisites

The following prerequisites must be filled to run this service:

- [Node.js](../../docs/explanations/about-nodejs/index.md) must be installed.
- [pnpm](../../docs/explanations/about-pnpm/index.md) must be installed.

## Set up the service

```sh
# Install the dependencies
pnpm install

# Copy the .env.defaults file to a .env file.
pnpm setup:env

# Edit the environment variables file if needed
vim .env
```

## Start the service in development mode

```sh
# Start the service in development mode
pnpm dev
```

The service should be now accessible on [http://localhost:3000](http://localhost:3000)

## Build the service

```sh
# Build the service
pnpm build
```

## Start the service in production mode

```sh
# Start the service in production mode
pnpm start
```
