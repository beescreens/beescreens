import { Portal } from '@/components/portal';
import { unstable_noStore as noStore } from 'next/cache';

export default function Home() {
	// Force Next.js to dynamically generate the page (Dynamic server-side rendering)
	// See :
	// - https://nextjs.org/docs/app/building-your-application/configuring/environment-variables#runtime-environment-variables
	// - https://nextjs.org/docs/app/building-your-application/configuring/environment-variables#dynamic-rendering
	noStore();

	const portalProps = {
		beeplaceUrl: process.env.BEEPLACE_URL!,
		pimpMyWallUrl: process.env.PIMP_MY_WALL_URL!,
	};

	return <Portal {...portalProps} />;
}
