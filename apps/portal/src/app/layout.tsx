import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import './globals.css';
import clsx from 'clsx';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
	title: 'Portal of BeeScreens',
	description: 'Portal to choose between all the available BeeScreens apps.',
};

export default function RootLayout({
	children,
}: Readonly<{
	children: React.ReactNode;
}>) {
	return (
		<html lang="en" className="w-full h-full">
			<body className={clsx('w-full h-full', inter.className)}>{children}</body>
		</html>
	);
}
