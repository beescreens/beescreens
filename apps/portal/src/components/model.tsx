import { Block } from '@/components/models/block';
import { Brush } from '@/components/models/brush';
import * as THREE from 'three';

const zPlane = new THREE.Plane(new THREE.Vector3(0, 0, 1), 0);
const yPlane = new THREE.Plane(new THREE.Vector3(0, 1, 0), 1);

type ModelProps = {
	clip?: boolean;
	name: string;
};

export const Model = ({ clip, name, ...props }: ModelProps) => {
	return name === 'BeePlace' ? (
		<Block
			clippingPlanes={clip ? [zPlane, yPlane] : undefined}
			scale={2}
			position={[0, 0, 0]}
			{...props}
		/>
	) : (
		<Brush
			clippingPlanes={clip ? [zPlane, yPlane] : undefined}
			scale={0.5}
			position={[0, 0, 0]}
			rotation={[0.5, 0, 0]}
			{...props}
		/>
	);
};
