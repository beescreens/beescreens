// Code created with https://gltf.pmnd.rs/
// Model from https://poly.pizza/m/bl-Ofb3hGIp

import React, { useRef } from 'react';
import { useGLTF } from '@react-three/drei';
import { GLTF } from 'three-stdlib';
import { useFrame } from '@react-three/fiber';
import { Group, Mesh, MeshStandardMaterial, Plane } from 'three';

type GLTFResult = GLTF & {
	nodes: {
		['Node-Mesh']: Mesh;
		['Node-Mesh_1']: Mesh;
		['Node-Mesh_2']: Mesh;
		['Node-Mesh_3']: Mesh;
	};
	materials: {
		mat15: MeshStandardMaterial;
		mat20: MeshStandardMaterial;
		mat4: MeshStandardMaterial;
		mat13: MeshStandardMaterial;
	};
};

type BrushProps = JSX.IntrinsicElements['group'] & {
	clippingPlanes?: Plane[];
};

export const Brush = (props: BrushProps) => {
	const { clippingPlanes } = props;
	const { nodes, materials } = useGLTF('/brush.glb') as GLTFResult;
	const brushRef = useRef<Group>(null);
	const materialsClone = useRef<Record<string, MeshStandardMaterial>>({});

	useFrame(() => {
		if (!brushRef.current) return;
		brushRef.current.rotation.y += 0.01;
	});

	Object.keys(materials).forEach((key) => {
		const keyName = key as keyof typeof materials;
		materialsClone.current[keyName] = materials[keyName].clone();
		if (clippingPlanes) {
			materialsClone.current[keyName].clippingPlanes = clippingPlanes;
		}
	});

	return (
		<group {...props} dispose={null} ref={brushRef}>
			<mesh
				geometry={nodes['Node-Mesh'].geometry}
				material={materialsClone.current.mat15}
			/>
			<mesh
				geometry={nodes['Node-Mesh_1'].geometry}
				material={materialsClone.current.mat20}
			/>
			<mesh
				geometry={nodes['Node-Mesh_2'].geometry}
				material={materialsClone.current.mat4}
			/>
			<mesh
				geometry={nodes['Node-Mesh_3'].geometry}
				material={materialsClone.current.mat13}
			/>
		</group>
	);
};

useGLTF.preload('/brush.glb');
