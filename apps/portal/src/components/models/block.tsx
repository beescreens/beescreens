// Code created with https://gltf.pmnd.rs/
// Model from https://poly.pizza/m/9-TrEDeCFmf

import React, { useRef } from 'react';
import { useGLTF } from '@react-three/drei';
import { GLTF } from 'three-stdlib';
import { useFrame } from '@react-three/fiber';
import { Mesh, MeshStandardMaterial, Group, Plane } from 'three';

type GLTFResult = GLTF & {
	nodes: {
		['Node-Mesh']: Mesh;
		['Node-Mesh_1']: Mesh;
		['Node-Mesh_2']: Mesh;
		['Node-Mesh_3']: Mesh;
	};
	materials: {
		mat8: MeshStandardMaterial;
		mat5: MeshStandardMaterial;
		mat12: MeshStandardMaterial;
		mat7: MeshStandardMaterial;
	};
};

type BlockProps = JSX.IntrinsicElements['group'] & {
	clippingPlanes?: Plane[];
};

export const Block = (props: BlockProps) => {
	const { clippingPlanes } = props;
	const { nodes, materials } = useGLTF('/block.glb') as GLTFResult;
	const blockRef = useRef<Group>(null);
	const materialsClone = useRef<Record<string, MeshStandardMaterial>>({});

	useFrame(() => {
		if (!blockRef.current) return;
		blockRef.current.rotation.y += 0.01;
		blockRef.current.rotation.x += 0.01;
	});

	Object.keys(materials).forEach((key) => {
		const keyName = key as keyof typeof materials;
		materialsClone.current[keyName] = materials[keyName].clone();
		if (clippingPlanes) {
			materialsClone.current[keyName].clippingPlanes = clippingPlanes;
		}
	});

	return (
		<group {...props} dispose={null} ref={blockRef}>
			<mesh
				geometry={nodes['Node-Mesh'].geometry}
				material={materialsClone.current.mat8}
			/>
			<mesh
				geometry={nodes['Node-Mesh_1'].geometry}
				material={materialsClone.current.mat5}
			/>
			<mesh
				geometry={nodes['Node-Mesh_2'].geometry}
				material={materialsClone.current.mat12}
			/>
			<mesh
				geometry={nodes['Node-Mesh_3'].geometry}
				material={materialsClone.current.mat7}
			/>
		</group>
	);
};

useGLTF.preload('/block.glb');
