import * as THREE from 'three';
import {
	MeshPortalMaterial,
	PortalMaterialType,
	Text,
	useCursor,
	useTexture,
} from '@react-three/drei';
import { ThreeEvent, useFrame } from '@react-three/fiber';
import { ComponentProps, useRef, useState } from 'react';
import { easing } from 'maath';
import { suspend } from 'suspend-react';
import { Model } from '@/components/model';

type FrameProps = {
	value: string;
	name: string;
	link: string;
	texture: string;
	active: string | null;
	width?: number;
	height?: number;
	setActive: (id: string | null) => void;
} & ComponentProps<'group'>;

// @ts-expect-error - no types for this font
const medium = import('@pmndrs/assets/fonts/inter_medium.woff');

export const Frame = ({
	value,
	name,
	link,
	texture,
	active,
	setActive,
	width = 1,
	height = 1.61803398875,
	...props
}: FrameProps) => {
	const portal = useRef<PortalMaterialType>(null);
	const [hovered, hover] = useState(false);
	const colorMap = useTexture(texture);

	function choosePortal(e: ThreeEvent<MouseEvent>, id: string) {
		e.stopPropagation();
		setActive(id);

		setTimeout(() => {
			location.href = link;
			setActive(null);
		}, 1000);
	}

	useCursor(hovered);
	useFrame((_, dt) =>
		easing.damp(portal.current!, 'blend', active === value ? 1 : 0, 0.2, dt),
	);
	return (
		<group {...props}>
			<Text
				font={(suspend(medium) as any).default}
				fontSize={0.1}
				anchorX="right"
				position={[0.4, -0.659, 0.01]}
				material-toneMapped={false}
				outlineColor="#404040"
				outlineWidth={0.001}
				outlineBlur={0.03}
				outlineOpacity={hovered ? 0.8 : 0.4}
			>
				{name}
			</Text>
			<mesh
				name={value}
				onClick={(e) => choosePortal(e, value)}
				onPointerOver={(e) => hover(true)}
				onPointerOut={() => hover(false)}
			>
				{/* @ts-ignore */}
				<roundedPlaneGeometry args={[width, height, 0.1]} />

				<MeshPortalMaterial
					ref={portal}
					events={active === value}
					side={THREE.DoubleSide}
				>
					<mesh position={[0, 0, -0.4]} scale={2}>
						<planeGeometry />
						<meshBasicMaterial toneMapped={false} map={colorMap} />
					</mesh>
					<Model clip={false} name={name} />
					<ambientLight intensity={1} />
				</MeshPortalMaterial>
				<Model clip={true} name={name} />
			</mesh>
		</group>
	);
};
