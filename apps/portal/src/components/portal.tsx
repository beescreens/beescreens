'use client';

import * as THREE from 'three';
import { useState } from 'react';
import { Canvas, extend } from '@react-three/fiber';
import { Cloud, Clouds, Sky } from '@react-three/drei';
import { geometry } from 'maath';
import { Frame } from '@/components/frame';
import { Rig } from '@/components/rig';

extend(geometry);

export type PortalProps = {
	beeplaceUrl: string;
	pimpMyWallUrl: string;
};

export const Portal = (props: PortalProps) => {
	const { beeplaceUrl, pimpMyWallUrl } = props;
	const [active, setActive] = useState<string | null>(null);

	return (
		<>
			<Canvas
				gl={{ localClippingEnabled: true }}
				camera={{ fov: 75, position: [0, 0, 20] }}
			>
				<Clouds material={THREE.MeshBasicMaterial}>
					<Cloud seed={10} bounds={50} volume={80} position={[40, 0, -80]} />
					<Cloud seed={10} bounds={50} volume={80} position={[-40, 10, -80]} />
				</Clouds>
				<Sky />
				<Frame
					value="01"
					name="BeePlace"
					link={beeplaceUrl}
					texture="/beeplace.png"
					active={active}
					setActive={setActive}
					position={[-0.6, 0, 0]}
					rotation={[0, 0.3, 0]}
				></Frame>
				<Frame
					value="02"
					name="Pimp My Wall"
					link={pimpMyWallUrl}
					texture="/pmw.png"
					active={active}
					setActive={setActive}
					position={[0.6, 0, 0]}
					rotation={[0, -0.3, 0]}
				></Frame>
				<Rig active={active} />
				<ambientLight intensity={1} />
			</Canvas>
		</>
	);
};
