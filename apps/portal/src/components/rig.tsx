import * as THREE from 'three';
import { CameraControls } from '@react-three/drei';
import { useThree } from '@react-three/fiber';
import { useEffect } from 'react';

type RigProps = {
	active: string | null;
};

export const Rig = ({ active }: RigProps) => {
	const { controls, scene } = useThree();

	useEffect(() => {
		const position = new THREE.Vector3(0, 0, 3);
		const focus = new THREE.Vector3(0, 0, 0);

		if (active) {
			const activeFrame = scene.getObjectByName(active);
			if (activeFrame) {
				activeFrame.parent?.localToWorld(position.set(0, 1.25, 1.5));
				activeFrame.parent?.localToWorld(focus.set(0, 0, -2));
			}
		}

		// @ts-ignore
		controls?.setLookAt(...position.toArray(), ...focus.toArray(), true);
	}, [active, controls, scene]);
	return (
		<CameraControls
			minDistance={1}
			maxDistance={5}
			makeDefault
			minAzimuthAngle={-Math.PI / 3}
			maxAzimuthAngle={Math.PI / 3}
			minPolarAngle={0.5}
			maxPolarAngle={Math.PI / 2}
		/>
	);
};
