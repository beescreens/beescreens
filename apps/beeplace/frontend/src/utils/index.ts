export function isClickInsideRect(
	rect: DOMRect,
	x: number,
	y: number,
): boolean {
	return (
		x >= rect.x &&
		x <= rect.x + rect.width &&
		y >= rect.y &&
		y <= rect.y + rect.height
	);
}

export function hexToRGBA(hex: string) {
	const r = parseInt(hex.substring(1, 3), 16);
	const g = parseInt(hex.substring(3, 5), 16);
	const b = parseInt(hex.substring(5, 7), 16);
	return [r, g, b, 255];
}

export function getContrastColor(r: number, g: number, b: number, a: number) {
	const alpha = a / 255;
	const brightness = r * 0.299 + g * 0.587 + b * 0.114 + (1 - alpha) * 255;

	return brightness > 50 ? '#000000' : '#FFFFFF';
}

export function formatMs(ms: number) {
	const rounded = Math.round(ms / 1000);
	const minutes = Math.floor(rounded / 60);
	const seconds = rounded % 60;

	return `${minutes}:${seconds.toString().padStart(2, '0')}`;
}

export function assertIsDefined<T>(value: T): asserts value is NonNullable<T> {
	if (value === undefined || value === null) {
		throw new Error(`${value} is not defined`);
	}
}
