import FingerprintJS from '@fingerprintjs/fingerprintjs';

export async function getFingerprint() {
	const fp = await FingerprintJS.load({ monitoring: false });
	const { visitorId } = await fp.get();

	return visitorId;
}
