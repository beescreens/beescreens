import { PinchZoomHandle } from '@/components/PinchZoom';
import { hexToRGBA } from '@/utils';
import { RefObject } from 'react';

export function centerCanvas(
	canvasRef: RefObject<PinchZoomHandle>,
	canvasSize: number,
	initialScale: number,
) {
	if (!canvasRef.current) return;

	const cameraBounds = document.documentElement.getBoundingClientRect();
	const scaledSize = canvasSize * initialScale;

	canvasRef.current.setTransform({
		scale: initialScale,
		x: cameraBounds.width / 2 - scaledSize / 2,
		y: cameraBounds.height / 2 - scaledSize / 2,
	});
}

export function getImageData(
	board: number[],
	colors: string[],
	canvasSize: number,
) {
	const pixels = board.map((color) => hexToRGBA(colors[color]));
	const data = new Uint8ClampedArray(pixels.flat());
	return new ImageData(data, canvasSize, canvasSize);
}

function upscaleCanvas(canvas: HTMLCanvasElement, targetResolution: number) {
	const size = canvas.width;
	const factor = targetResolution / size;

	if (!Number.isInteger(factor)) return canvas;

	const scaled = document.createElement('canvas');
	scaled.width = targetResolution;
	scaled.height = targetResolution;

	const image = canvas.getContext('2d')?.getImageData(0, 0, size, size);
	const ctx = scaled.getContext('2d');
	if (!image || !ctx) return canvas;

	for (let i = 0; i < size; i++) {
		for (let j = 0; j < size; j++) {
			const index = (i + j * size) * 4;
			const pixel = image.data.slice(index, index + 4);
			ctx.fillStyle = `rgba(${pixel[0]}, ${pixel[1]}, ${pixel[2]}, ${pixel[3]})`;
			ctx.fillRect(i * factor, j * factor, factor, factor);
		}
	}

	return scaled;
}

export function downloadImage(
	filename = 'beeplace.png',
	targetResolution = 4096,
) {
	const canvas = document.querySelector('canvas');
	if (!canvas) return;

	const scaled = upscaleCanvas(canvas, targetResolution);
	const link = document.createElement('a');
	link.download = filename;
	link.href = scaled.toDataURL('image/png', 1);
	link.click();
}
