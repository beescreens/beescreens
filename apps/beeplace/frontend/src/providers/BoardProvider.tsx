import {
	createContext,
	MutableRefObject,
	ReactNode,
	useContext,
	useRef,
} from 'react';
import { useBoardStore } from '@/store/board';
import { Pixel } from '@beescreens/beeplace';

interface BoardContextType {
	canvasRef: MutableRefObject<HTMLCanvasElement | null>;
	clearBoard: () => void;
	get2dContext: () => CanvasRenderingContext2D;
	getBounds: () => DOMRect;
	placePixel: (pixel: Pixel) => void;
	tryToPlacePixel: (pixel: Pixel) => boolean;
}

const BoardContext = createContext<BoardContextType | undefined>(undefined);

function BoardProvider({ children }: { children: ReactNode }) {
	const canvasRef = useRef<HTMLCanvasElement | null>(null);
	const nextPixel = useBoardStore((state) => state.nextPixel);
	const setNextPixel = useBoardStore((state) => state.setNextPixel);
	const addPlacedPixel = useBoardStore((state) => state.addPlacedPixel);
	const resetPlacedPixels = useBoardStore((state) => state.resetPlacedPixels);
	const initialState = useBoardStore((state) => state.initialState);
	const canPlacePixel = useBoardStore((state) => state.canPlacePixel);

	function clearBoard() {
		const ctx = get2dContext();
		ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
	}

	function getCanvas() {
		if (!canvasRef.current) {
			throw new Error('Canvas ref is not set');
		}
		return canvasRef.current;
	}

	function get2dContext() {
		const context = getCanvas().getContext('2d', { willReadFrequently: true });
		if (!context) {
			throw new Error('Could not get 2d context');
		}
		return context;
	}

	function getBounds() {
		return getCanvas().getBoundingClientRect();
	}

	function tryToPlacePixel(pixel: Pixel) {
		if (!canPlacePixel() || !initialState) {
			return false;
		}

		placePixel(pixel);

		if (!nextPixel || nextPixel.getTime() < Date.now()) {
			setNextPixel(new Date(Date.now() + initialState.cooldown * 1000));
			setTimeout(() => {
				resetPlacedPixels();
			}, initialState.cooldown * 1000);
		}
		addPlacedPixel();

		return true;
	}

	function placePixel(pixel: Pixel) {
		const ctx = get2dContext();

		if (!initialState || pixel.color >= initialState.colors.length) {
			throw new Error('Invalid color');
		}

		ctx.fillStyle = initialState.colors[pixel.color];
		ctx.fillRect(pixel.x, pixel.y, 1, 1);
	}

	return (
		<BoardContext.Provider
			value={{
				canvasRef,
				clearBoard,
				get2dContext,
				getBounds,
				placePixel,
				tryToPlacePixel,
			}}
		>
			{children}
		</BoardContext.Provider>
	);
}

function useBoard() {
	const context = useContext(BoardContext);
	if (context === undefined) {
		throw new Error('useBoard must be used within a BoardProvider');
	}
	return context;
}

export { BoardProvider, useBoard };
