import {
	createContext,
	useCallback,
	useContext,
	useEffect,
	useMemo,
} from 'react';
import { io } from 'socket.io-client';
import { useBoard } from '@/providers/BoardProvider';
import { useBoardStore } from '@/store/board';
import {
	BeePlaceFrontendSocket,
	ClientEvent,
	Pixel,
	ServerEvent,
	testFunction,
} from '@beescreens/beeplace';
import { getFingerprint } from '@/utils/fingerprint';

interface WebSocketContextType {
	emitPixelFromPlayer: (pixel: Pixel) => void;
}

interface WebSocketProviderProps {
	backendSocketIoUrl: string;
	isDisplay?: boolean;
	children: React.ReactNode;
}

const WebSocketContext = createContext<WebSocketContextType | undefined>(
	undefined,
);

function WebSocketProvider({
	backendSocketIoUrl,
	isDisplay = false,
	children,
}: WebSocketProviderProps): JSX.Element {
	const socket: BeePlaceFrontendSocket = useMemo(
		() =>
			io(backendSocketIoUrl, { autoConnect: false, transports: ['websocket'] }),
		[backendSocketIoUrl],
	);

	const { clearBoard, placePixel } = useBoard();
	const setInitialState = useBoardStore((state) => state.setInitialState);
	const setNextPixel = useBoardStore((state) => state.setNextPixel);
	const setReadOnly = useBoardStore((state) => state.setReadOnly);
	const setUserCount = useBoardStore((state) => state.setUserCount);
	const addPlacedPixel = useBoardStore((state) => state.addPlacedPixel);
	const resetPlacedPixels = useBoardStore((state) => state.resetPlacedPixels);

	const joinBoard = useCallback(
		async (socket: BeePlaceFrontendSocket) => {
			const fingerprint = await getFingerprint();

			socket.emit(ClientEvent.JOIN, { fingerprint, isDisplay }, (payload) => {
				console.log('JOIN', payload);
				const { initialState, cooldown, placedPixels, userCount } = payload;
				setInitialState(initialState);
				setUserCount(userCount);

				if (cooldown > 0) {
					setNextPixel(new Date(Date.now() + cooldown * 1000));
					addPlacedPixel(placedPixels);
					setTimeout(() => {
						resetPlacedPixels();
					}, cooldown * 1000);
				}
			});
		},
		[
			addPlacedPixel,
			isDisplay,
			resetPlacedPixels,
			setInitialState,
			setNextPixel,
			setUserCount,
		],
	);

	useEffect(() => {
		if (!socket.connected) {
			socket.connect();
			joinBoard(socket);
		}

		console.log(testFunction());

		return () => {
			socket.close();
		};
	}, [socket, joinBoard]);

	useEffect(() => {
		if (!socket) return;

		socket.on(ServerEvent.READ_ONLY, (readOnly) => {
			console.log('READ ONLY', readOnly);
			setReadOnly(readOnly);
		});

		socket.on(ServerEvent.RESET_BOARD, () => {
			console.log('RESET BOARD');
			clearBoard();
		});

		socket.on(ServerEvent.UPDATE_BOARD, (data) => {
			const array = data.split(',').map(Number);
			console.log('UPDATE', array.length / 3);

			// Place all pixels, each pixel is stored as 3 numbers in the array: [x, y, color]
			for (let i = 0; i < array.length; i += 3) {
				placePixel({ x: array[i], y: array[i + 1], color: array[i + 2] });
			}
		});

		socket.on(ServerEvent.USER_COUNT, (count) => {
			setUserCount(count);
		});

		return () => {
			socket.off(ServerEvent.READ_ONLY);
			socket.off(ServerEvent.RESET_BOARD);
			socket.off(ServerEvent.UPDATE_BOARD);
			socket.off(ServerEvent.USER_COUNT);
		};
	}, [clearBoard, placePixel, socket, setReadOnly, setUserCount]);

	const emitPixelFromPlayer = (pixel: Pixel) => {
		socket.emit(ClientEvent.PIXEL_FROM_PLAYER, pixel);
	};

	return (
		<WebSocketContext.Provider value={{ emitPixelFromPlayer }}>
			{children}
		</WebSocketContext.Provider>
	);
}

function useWebSocket() {
	const context = useContext(WebSocketContext);
	if (context === undefined) {
		throw new Error('useWebSocket must be used within a WebSocketProvider');
	}
	return context;
}

export { WebSocketProvider, useWebSocket };
