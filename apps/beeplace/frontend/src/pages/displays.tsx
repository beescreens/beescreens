import { GetServerSideProps, InferGetServerSidePropsType } from 'next';
import dynamic from 'next/dynamic';
const DisplaysConfiguration = dynamic(
	() =>
		import('@/components/display/DisplaysConfiguration').then(
			(mod) => mod.DisplaysConfiguration,
		),
	{
		ssr: false,
	},
);

interface AppProps {
	displayWidth: number;
	displayHeight: number;
	displayColumns: number;
	displayRows: number;
}

export const getServerSideProps: GetServerSideProps<AppProps> = async () => ({
	props: {
		displayWidth: parseInt(process.env.DISPLAY_WIDTH as string, 10),
		displayHeight: parseInt(process.env.DISPLAY_HEIGHT as string, 10),
		displayColumns: parseInt(process.env.DISPLAY_COLUMNS as string, 10),
		displayRows: parseInt(process.env.DISPLAY_ROWS as string, 10),
	},
});

const App = (props: InferGetServerSidePropsType<typeof getServerSideProps>) => {
	return <DisplaysConfiguration {...props} />;
};

export default App;
