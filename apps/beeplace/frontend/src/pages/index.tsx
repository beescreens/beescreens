import { BoardContainer } from '@/components/BoardContainer';
import { BoardProvider } from '@/providers/BoardProvider';
import { WebSocketProvider } from '@/providers/WebsocketProvider';
import { GetServerSideProps, InferGetServerSidePropsType } from 'next';

interface AppProps {
	backendSocketIoUrl: string;
	minScale: number;
	maxScale: number;
}

export const getServerSideProps: GetServerSideProps<AppProps> = async () => ({
	props: {
		backendSocketIoUrl: process.env.BACKEND_SOCKET_IO_URL as string,
		minScale: parseFloat(process.env.MIN_SCALE as string),
		maxScale: parseFloat(process.env.MAX_SCALE as string),
	},
});

const App = ({
	backendSocketIoUrl,
	minScale,
	maxScale,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
	return (
		<BoardProvider>
			<WebSocketProvider backendSocketIoUrl={backendSocketIoUrl}>
				<BoardContainer minScale={minScale} maxScale={maxScale} />
			</WebSocketProvider>
		</BoardProvider>
	);
};

export default App;
