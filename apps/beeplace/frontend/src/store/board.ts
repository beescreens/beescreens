import { InitialState } from '@beescreens/beeplace';
import { useStore } from 'zustand';
import { createStore } from 'zustand/vanilla';
import { immer } from 'zustand/middleware/immer';

interface BoardState {
	initialState: InitialState | null;
	nextPixel: Date | null;
	placedPixels: number;
	userCount: number;
	setInitialState: (initial: InitialState) => void;
	setNextPixel: (next: Date) => void;
	setReadOnly: (readOnly: boolean) => void;
	setUserCount: (userCount: number) => void;
	addPlacedPixel: (number?: number) => void;
	resetPlacedPixels: () => void;
	canPlacePixel: () => boolean;
}

export const store = createStore(
	immer<BoardState>((set, get) => ({
		initialState: null,
		nextPixel: null,
		placedPixels: 0,
		userCount: 0,
		setInitialState: (initialState) => set(() => ({ initialState })),
		setNextPixel: (nextPixel) => set(() => ({ nextPixel })),
		setReadOnly: (readOnly) =>
			set((state) => {
				if (state.initialState) {
					state.initialState.readOnly = readOnly;
				}
			}),
		setUserCount: (userCount) => set(() => ({ userCount })),
		addPlacedPixel: (number) =>
			set((state) => ({ placedPixels: number ?? state.placedPixels + 1 })),
		resetPlacedPixels: () => set(() => ({ placedPixels: 0 })),
		canPlacePixel: () => {
			const { nextPixel, placedPixels, initialState } = get();
			if (!nextPixel || !initialState) return true;
			if (placedPixels < initialState?.maxPixels) return true;

			return nextPixel.getTime() < Date.now();
		},
	})),
);

export function useBoardStore(): BoardState;
export function useBoardStore<T>(
	selector: (state: BoardState) => T,
	equals?: (a: T, b: T) => boolean,
): T;
export function useBoardStore<T>(
	selector?: (state: BoardState) => T,
	equals?: (a: T, b: T) => boolean,
) {
	return useStore(store, selector!, equals);
}
