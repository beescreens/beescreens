import clsx from 'clsx';
import React, { forwardRef, ForwardRefRenderFunction } from 'react';

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
	children: React.ReactNode;
}

const MyButton: ForwardRefRenderFunction<HTMLButtonElement, ButtonProps> = (
	{ children, className, ...props },
	ref,
) => {
	return (
		<button
			className={clsx(
				'gradient hover:glow-button focus-visible:glow-button group relative cursor-pointer select-none rounded-xl bg-gradient-to-b from-stone-900 to-neutral-900 px-6 py-3 text-xl font-bold text-white transition-all duration-200 ease-in hover:to-zinc-900 focus:outline-none active:brightness-75',
				className,
			)}
			ref={ref}
			{...props}
		>
			<div className="flex items-center justify-center transition-transform duration-200 ease-in group-active:scale-90">
				{children}
			</div>
		</button>
	);
};

export const Button = forwardRef(MyButton);
