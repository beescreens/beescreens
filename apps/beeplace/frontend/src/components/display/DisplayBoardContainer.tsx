import { DisplayBoard } from '@/components/display/DisplayBoard';
import { useBoardStore } from '@/store/board';

interface DisplayBoardContainerProps {
	minScale: number;
	maxScale: number;
}

export const DisplayBoardContainer = (props: DisplayBoardContainerProps) => {
	const initialState = useBoardStore((state) => state.initialState);

	return <>{initialState && <DisplayBoard {...initialState} {...props} />}</>;
};
