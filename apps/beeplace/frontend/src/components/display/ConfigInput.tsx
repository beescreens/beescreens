export interface ConfigInputProps {
	id: string;
	label: string;
	min: number;
	max: number;
	value?: number;
	onChange: (value: number | undefined) => void;
}

export const ConfigInput = (props: ConfigInputProps) => {
	const { id, label, min, max, value, onChange } = props;

	function handleChange(e: React.ChangeEvent<HTMLInputElement>) {
		const newValue = parseInt(e.target.value);
		if (isNaN(newValue)) {
			onChange(min);
			return;
		}
		if (newValue >= min && newValue <= max) {
			onChange(newValue);
		}
	}

	return (
		<div>
			<label htmlFor={id} className="block text-sm font-medium text-white">
				{label}
			</label>
			<input
				value={value === min ? '' : value}
				onChange={handleChange}
				type="number"
				name={id}
				id={id}
				min={min}
				max={max}
				placeholder={`Min: ${min}, max: ${max}`}
				className="mt-2 block w-full rounded-lg border border-gray-600 bg-gray-700 p-2.5 text-sm text-white placeholder-gray-400"
			/>
		</div>
	);
};
