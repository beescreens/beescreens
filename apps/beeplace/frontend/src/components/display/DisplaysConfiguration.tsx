import { ConfigInput } from '@/components/display/ConfigInput';
import { InfoIcon } from '@/components/icons/InfoIcon';
import { assertIsDefined } from '@/utils';
import clsx from 'clsx';
import Link from 'next/link';
import { useState } from 'react';
import { useLocalStorage } from 'react-use';

export interface DisplaysConfigurationProps {
	displayWidth: number;
	displayHeight: number;
	displayColumns: number;
	displayRows: number;
}

export const DisplaysConfiguration = (props: DisplaysConfigurationProps) => {
	const { displayWidth, displayHeight, displayColumns, displayRows } = props;

	const [selected, setSelected] = useState<number | null>(null);
	const [width, setWidth] = useLocalStorage('width', displayWidth);
	const [height, setHeight] = useLocalStorage('height', displayHeight);
	const [nbCols, setNbCols] = useLocalStorage('nbCols', displayColumns);
	const [nbRows, setNbRows] = useLocalStorage('nbRows', displayRows);
	assertIsDefined(width);
	assertIsDefined(height);
	assertIsDefined(nbCols);
	assertIsDefined(nbRows);

	const columns = [...Array(nbCols).keys()];
	const rows = [...Array(nbRows).keys()];

	return (
		<div className="flex w-screen flex-col items-center justify-center space-y-4 px-8 py-8 md:h-screen md:flex-row md:space-x-4 md:space-y-0 md:py-0">
			<div className="w-full md:w-1/3">
				<div className="rounded-xl border border-gray-500 bg-gray-700 px-4 py-3 shadow-xl">
					<h1 className="flex items-center justify-center space-x-6 text-xl text-white">
						<div className="-rotate-6 transform">🛠️</div>
						<span className="flex items-center space-x-2">
							<div className="flex h-6 w-6 items-center justify-center rounded-full border border-indigo-400 bg-indigo-300 p-2 text-sm">
								1
							</div>
							<span>Configuration</span>
						</span>
						<div className="rotate-6 transform">🛠️</div>
					</h1>
					<div className="mt-3 space-y-4 rounded-md border border-gray-500 bg-gray-600 px-4 py-4">
						<div className="grid gap-4 sm:grid-cols-2 sm:gap-6">
							<ConfigInput
								id="width"
								value={width}
								label="Display width"
								min={0}
								max={9999}
								onChange={setWidth}
							/>
							<ConfigInput
								id="height"
								value={height}
								label="Display height"
								min={0}
								max={9999}
								onChange={setHeight}
							/>
							<ConfigInput
								id="nbCols"
								value={nbCols}
								label="Number of columns"
								min={0}
								max={5}
								onChange={setNbCols}
							/>
							<ConfigInput
								id="nbRows"
								value={nbRows}
								label="Number of rows"
								min={0}
								max={5}
								onChange={setNbRows}
							/>
						</div>
					</div>
				</div>
			</div>
			<div className="w-full md:w-2/3">
				<div className="rounded-xl border border-gray-500 bg-gray-700 px-4 py-3 shadow-xl">
					<h1 className="flex items-center justify-center space-x-6 text-xl text-white">
						<div className="-rotate-6 transform">🎊</div>
						<span className="flex items-center space-x-2">
							<div className="flex h-6 w-6 items-center justify-center rounded-full border border-indigo-400 bg-indigo-300 p-2 text-sm">
								2
							</div>
							<span>Choose a display</span>
						</span>
						<div className="rotate-6 transform">🎊</div>
					</h1>
					<div className="mt-3 space-y-4 rounded-md border border-gray-500 bg-gray-600 px-4 py-4">
						{rows.map((row) => (
							<div key={row} className="flex space-x-4">
								{columns.map((column) => {
									const index = row * nbCols + column;
									return (
										<Link
											key={column}
											href={`/display?i=${encodeURIComponent(
												index + 1,
											)}&w=${encodeURIComponent(width)}&h=${encodeURIComponent(
												height,
											)}&c=${encodeURIComponent(nbCols)}&r=${encodeURIComponent(
												nbRows,
											)}`}
											className={clsx('overflow-hidden focus:outline-none', {
												'w-full': nbCols === 1,
												'w-1/2': nbCols === 2,
												'w-1/3': nbCols === 3,
												'w-1/4': nbCols === 4,
												'w-1/5': nbCols === 5,
											})}
											onMouseEnter={() => setSelected(index)}
											onMouseLeave={() => setSelected(null)}
										>
											<button
												type="button"
												className="w-full select-none whitespace-nowrap rounded-xl border border-indigo-300 px-4 py-3 text-indigo-300 transition duration-300 ease-in-out hover:bg-indigo-300 hover:text-white focus:outline-none"
											>
												<span>
													{nbCols * nbRows < 10 ? 'Show display ' : 'Display '}
												</span>
												<span className="border-b-2 border-indigo-300 font-semibold">
													{index + 1}
												</span>
											</button>
										</Link>
									);
								})}
							</div>
						))}
						<div className="text-center">
							<div className="mt-8 inline-flex flex-col items-center justify-center space-y-1 rounded-xl border border-gray-700 bg-gray-800 px-8 py-2 text-gray-200">
								<InfoIcon />
								<div className="space-y-3">
									<span>Display resolution: </span>
									<span className="font-semibold text-white">
										{width}x{height}
									</span>

									<div className="flex flex-col items-center space-y-[2px]">
										{rows.map((row) => (
											<ul key={row} className="flex space-x-[2px]">
												{columns.map((column) => {
													const index = row * nbCols + column;
													return (
														<li
															key={column}
															className={clsx('h-6 w-6 bg-gray-600', {
																'rounded-tl': index === 0,
																'rounded-tr': index === nbCols - 1,
																'rounded-bl':
																	row === nbRows - 1 && column === 0,
																'rounded-br': index === nbRows * nbCols - 1,
																'bg-indigo-300': index === selected,
															})}
														></li>
													);
												})}
											</ul>
										))}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};
