import { useRef, useCallback } from 'react';
import { useEffectOnce } from 'react-use';
import { useBoard } from '@/providers/BoardProvider';
import {
	applyCssProperties,
	OnPinchZoomEvent,
	PinchZoom,
	PinchZoomHandle,
} from '@/components/PinchZoom';
import { InitialState } from '@beescreens/beeplace';
import { centerCanvas, getImageData } from '@/utils/board';
import { useSearchParams } from 'next/navigation';

interface DisplayBoardProps extends InitialState {
	minScale: number;
	maxScale: number;
}

export const DisplayBoard = (props: DisplayBoardProps) => {
	const { board, canvasSize, colors, minScale, maxScale } = props;
	const { canvasRef, get2dContext } = useBoard();
	const pinchZoomRef = useRef<PinchZoomHandle>(null);
	const params = useSearchParams();

	// Position canvas according to index in URL
	useEffectOnce(() => {
		if (!pinchZoomRef.current) return;

		// If no index is provided or if index is incorrect, center the canvas
		const index = parseInt(params.get('i') || '1') - 1;
		const displayWidth = parseInt(params.get('w') || '1');
		const displayHeight = parseInt(params.get('h') || '1');
		const displayColumns = parseInt(params.get('c') || '1');
		const displayRows = parseInt(params.get('r') || '1');
		if (
			!params.has('i') ||
			!params.has('w') ||
			!params.has('h') ||
			!params.has('c') ||
			!params.has('r') ||
			index < 0 ||
			index >= displayColumns * displayRows
		) {
			const windowSize = Math.min(window.innerWidth, window.innerHeight);
			const scale = windowSize / canvasSize;
			centerCanvas(pinchZoomRef, canvasSize, scale);
			return;
		}

		const canvasWidth = Math.min(
			displayWidth * displayColumns,
			displayHeight * displayRows,
		);
		const canvasHeight = canvasWidth;

		const realWidth = displayWidth * displayColumns;
		const realHeight = displayHeight * displayRows;

		const paddingLeft = (realWidth - canvasWidth) / 2;
		const paddingTop = (realHeight - canvasHeight) / 2;

		const indexCol = index % displayColumns;
		const indexRow = Math.floor(index / displayColumns);

		pinchZoomRef.current.setTransform({
			x: paddingLeft - indexCol * displayWidth,
			y: paddingTop - indexRow * displayHeight,
			scale: canvasWidth / canvasSize,
		});
	});

	// Display initial state
	useEffectOnce(() => {
		if (!board || !board.length) return;
		get2dContext().putImageData(getImageData(board, colors, canvasSize), 0, 0);
	});

	const onTransform = useCallback(
		({ x, y, scale }: OnPinchZoomEvent) => {
			applyCssProperties(canvasRef.current, { x, y, scale });
		},
		[canvasRef],
	);

	return (
		<>
			<PinchZoom
				ref={pinchZoomRef}
				onTransform={onTransform}
				minScale={minScale}
				maxScale={maxScale}
			>
				<canvas
					ref={canvasRef}
					width={canvasSize}
					height={canvasSize}
					className="select-none bg-white"
					style={{
						imageRendering: 'pixelated',
					}}
				/>
			</PinchZoom>
		</>
	);
};
