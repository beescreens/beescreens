import { Pointer } from './Pointer';

export const isPointerEvent = (event: unknown): event is PointerEvent =>
	self.PointerEvent && event instanceof PointerEvent;

export const noop = () => {};

export const getDistance = (a: Pointer, b?: Pointer): number =>
	!b
		? 0
		: Math.sqrt((b.clientX - a.clientX) ** 2 + (b.clientY - a.clientY) ** 2);

export const getMidpoint = (
	a: Pointer,
	b?: Pointer,
): {
	clientX: number;
	clientY: number;
} => {
	if (!b) {
		return a;
	}

	return {
		clientX: (a.clientX + b.clientX) / 2,
		clientY: (a.clientY + b.clientY) / 2,
	};
};

export const getAbsoluteValue = (
	value: number | string,
	max: number,
): number => {
	if (typeof value === 'number') {
		return value;
	}

	if (value.trimEnd().endsWith('%')) {
		return (max * parseFloat(value)) / 100;
	}

	return parseFloat(value);
};

export const applyCssProperties = (
	element: HTMLElement | null,
	{ x, y, scale }: { x: number; y: number; scale: number },
) => {
	if (!element) {
		return;
	}

	element.style.setProperty('--x', x + 'px');
	element.style.setProperty('--y', y + 'px');
	element.style.setProperty('--scale', scale + '');
};

export const getTransform2DValue = ({
	x,
	y,
	scale,
}: {
	x: number;
	y: number;
	scale: number;
}) => `translateX(${x}px) translateY(${y}px) scale(${scale})`;

export const getTransform3DValue = ({
	x,
	y,
	scale,
}: {
	x: number;
	y: number;
	scale: number;
}) => `translate3d(${x}px, ${y}px, 0) scale3d(${scale},${scale}, 1)`;
