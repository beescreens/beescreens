import {
	ComponentPropsWithoutRef,
	useRef,
	forwardRef,
	useImperativeHandle,
} from 'react';
import { PointerTracker } from './PointerTracker';
import { getMidpoint, getDistance, getAbsoluteValue } from './utils';
import { Pointer } from './Pointer';
import type { OwnProps, PinchZoomHandle } from './types';
import clsx from 'clsx';
import { useEffectOnce } from 'react-use';
import { store } from '@/store/board';

type PinchZoomProps = OwnProps & ComponentPropsWithoutRef<'div'>;

const PinchZoomComponent: React.ForwardRefRenderFunction<
	PinchZoomHandle,
	PinchZoomProps
> = (props, ref) => {
	const _root = useRef<HTMLDivElement>(null);
	const transform = useRef<DOMMatrix>(new DOMMatrix());
	const pointerTracker = useRef<PointerTracker>();

	const root = () => {
		if (!_root.current) return document.createElement('div');

		return _root.current;
	};
	const _x = () => transform.current.e;
	const _y = () => transform.current.f;
	const _scale = () => transform.current.a;

	const rootBoundingClientRect = () => root().getBoundingClientRect();
	const childBoundingClientRect = () =>
		root().children[0].getBoundingClientRect();

	const createNewPointerTracker = () => {
		const { handleClick } = props;
		const startPos = new Pointer(new MouseEvent('click'));

		return new PointerTracker(root(), {
			start: (pointer, event) => {
				// We only want to track 2 pointers at most
				if (pointerTracker.current?.currentPointers.length === 2) {
					return false;
				}

				startPos.clientX = pointer.clientX;
				startPos.clientY = pointer.clientY;

				event.preventDefault();

				return true;
			},
			move: (previousPointers) => {
				if (!pointerTracker.current) return;

				_onPointerMove(
					previousPointers,
					pointerTracker.current.currentPointers,
				);
			},
			end: (pointer) => {
				// If we're not dragging, we're clicking
				if (
					getDistance(startPos, pointer) < 10 &&
					store.getState().canPlacePixel()
				) {
					handleClick?.({
						x: pointer.clientX,
						y: pointer.clientY,
					});
				}
			},
		});
	};

	const _visibilityListener = () => {
		if (document.visibilityState === 'hidden') {
			pointerTracker.current?.unsubscribe();
			pointerTracker.current = createNewPointerTracker();
		}
	};

	useEffectOnce(() => {
		root().children[0].classList.add('pinchzoom-child');

		// Watch for pointers
		pointerTracker.current = createNewPointerTracker();

		root().addEventListener('wheel', _onWheel);

		// To remove event listeners when the user leaves the app
		document.addEventListener('visibilitychange', _visibilityListener);

		return () => {
			root().removeEventListener('wheel', _onWheel);
			document.removeEventListener('visibilitychange', _visibilityListener);
			pointerTracker.current?.unsubscribe();
		};
	});

	/**
	 * Update the stage with a given scale/x/y.
	 */
	const setTransform = (
		opts: {
			x?: number;
			y?: number;
			scale?: number;
		} = {},
	) => {
		const { scale = _scale() } = opts;
		let { x = _x(), y = _y() } = opts;

		// Get current layout
		const thisBounds = rootBoundingClientRect();
		const positioningElBounds = childBoundingClientRect();
		// Not displayed. May be disconnected or display:none.
		// Just take the values, and we'll check bounds later.
		if (!thisBounds.width || !thisBounds.height) {
			return _updateTransform(scale, x, y);
		}
		// Create points for _positioningEl.
		let topLeft = new DOMPoint(
			positioningElBounds.left - thisBounds.left,
			positioningElBounds.top - thisBounds.top,
		);

		let bottomRight = new DOMPoint(
			positioningElBounds.width + topLeft.x,
			positioningElBounds.height + topLeft.y,
		);
		// Calculate the intended position of _positioningEl.
		const matrix = new DOMMatrix()
			.translate(x, y)
			.scale(scale)
			// Undo current transform
			.multiply(transform.current.inverse());
		topLeft = topLeft.matrixTransform(matrix);
		bottomRight = bottomRight.matrixTransform(matrix);
		// Ensure _positioningEl can't move beyond out-of-bounds.
		// Correct for x
		if (topLeft.x > thisBounds.width) {
			x += thisBounds.width - topLeft.x;
		} else if (bottomRight.x < 0) {
			x += -bottomRight.x;
		}
		// Correct for y
		if (topLeft.y > thisBounds.height) {
			y += thisBounds.height - topLeft.y;
		} else if (bottomRight.y < 0) {
			y += -bottomRight.y;
		}

		return _updateTransform(scale, x, y);
	};
	/**
	 * Update transform values without checking bounds. This is only called in setTransform.
	 */
	const _updateTransform = (scale: number, x: number, y: number) => {
		const { minScale, maxScale, onTransform } = props;
		// Avoid scaling to zero
		if (scale < minScale || (maxScale && scale > maxScale)) {
			return;
		}

		// Return if there's no change
		if (scale === _scale() && x === _x() && y === _y()) {
			return;
		}

		transform.current.e = x;
		transform.current.f = y;
		transform.current.d = transform.current.a = scale;

		onTransform({ x, y, scale });
	};

	const _onWheel = (event: WheelEvent) => {
		event.preventDefault();
		const currentRect = childBoundingClientRect();
		const { ctrlKey, deltaMode } = event;
		let { deltaY } = event;

		if (deltaMode === 1) {
			// 1 is "lines", 0 is "pixels"
			// Firefox uses "lines" for some types of mouse
			deltaY *= 15;
		}

		// ctrlKey is true when pinch-zooming on a trackpad.
		const divisor = ctrlKey ? 100 : 300;
		const scaleDiff = 1 - deltaY / divisor;

		_applyChange({
			scaleDiff,
			originX: event.clientX - currentRect.left,
			originY: event.clientY - currentRect.top,
		});
	};

	const _onPointerMove = (
		previousPointers: Array<Pointer>,
		currentPointers: Array<Pointer>,
	) => {
		// Combine next points with previous points
		const currentRect = childBoundingClientRect();
		// For calculating panning movement
		const prevMidpoint = getMidpoint(previousPointers[0], previousPointers[1]);
		const newMidpoint = getMidpoint(currentPointers[0], currentPointers[1]);
		// Midpoint within the element
		const originX = prevMidpoint.clientX - currentRect.left;
		const originY = prevMidpoint.clientY - currentRect.top;
		// Calculate the desired change in scale
		const prevDistance = getDistance(previousPointers[0], previousPointers[1]);
		const newDistance = getDistance(currentPointers[0], currentPointers[1]);
		const scaleDiff = prevDistance ? newDistance / prevDistance : 1;
		_applyChange({
			originX,
			originY,
			scaleDiff,
			panX: newMidpoint.clientX - prevMidpoint.clientX,
			panY: newMidpoint.clientY - prevMidpoint.clientY,
		});
	};
	/** Transform the view & fire a change event */
	const _applyChange = (
		opts: {
			panX?: number;
			panY?: number;
			originX?: number;
			originY?: number;
			scaleDiff?: number;
		} = {},
	) => {
		const {
			panX = 0,
			panY = 0,
			originX = 0,
			originY = 0,
			scaleDiff = 1,
		} = opts;
		const matrix = new DOMMatrix()
			// Translate according to panning.
			.translate(panX, panY)
			// Scale about the origin.
			.translate(originX, originY)
			// Apply current translate
			.translate(_x(), _y())
			.scale(scaleDiff)
			.translate(-originX, -originY)
			// Apply current scale.
			.scale(_scale());

		// Convert the transform into basic translate & scale.
		setTransform({ x: matrix.e, y: matrix.f, scale: matrix.a });
	};

	/**
	 * Change the scale, adjusting x/y by a given transform origin.
	 */
	const scaleTo = (
		scale: number,
		opts: {
			originX?: number;
			originY?: number;
			relativeTo?: 'content' | 'container';
		} = {},
	) => {
		let { originX = 0, originY = 0 } = opts;
		const { relativeTo = 'content' } = opts;
		const isRelativeToContent = relativeTo === 'content';

		const rect = isRelativeToContent
			? childBoundingClientRect()
			: rootBoundingClientRect();

		originX = getAbsoluteValue(originX, rect.width);
		originY = getAbsoluteValue(originY, rect.height);

		if (isRelativeToContent) {
			originX += _x();
			originY += _y();
		} else {
			const currentRect = childBoundingClientRect();
			originX -= currentRect.left;
			originY -= currentRect.top;
		}

		_applyChange({ originX, originY, scaleDiff: scale / _scale() });
	};

	const { children, onTransform, handleClick, minScale, maxScale, ...rest } =
		props;

	useImperativeHandle(
		ref,
		() => {
			return {
				x() {
					return _x();
				},
				y() {
					return _y();
				},
				scale() {
					return _scale();
				},
				scaleTo(
					scale: number,
					options?: {
						originX?: number;
						originY?: number;
						relativeTo?: 'content' | 'container';
					},
				) {
					scaleTo(scale, options);
				},
				setTransform(options?: { x?: number; y?: number; scale?: number }) {
					setTransform(options);
				},
			};
		},
		// eslint-disable-next-line react-hooks/exhaustive-deps
		[],
	);

	return (
		<div
			{...rest}
			ref={_root}
			className={clsx(
				'block h-screen w-screen touch-none overflow-hidden',
				props.className,
			)}
		>
			{children}
		</div>
	);
};

export const PinchZoom = forwardRef(PinchZoomComponent);
