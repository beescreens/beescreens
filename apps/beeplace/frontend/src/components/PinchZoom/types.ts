export interface OnPinchZoomEvent {
	x: number;
	y: number;
	scale: number;
}

export type OnPinchZoomClickEvent = Omit<OnPinchZoomEvent, 'scale'>;

export interface OwnProps {
	onTransform: (event: OnPinchZoomEvent) => void;
	handleClick?: (event: OnPinchZoomClickEvent) => void;
	minScale: number;
	maxScale: number;
}

export interface PinchZoomHandle {
	x(): number;
	y(): number;
	scale(): number;
	scaleTo(
		scale: number,
		options?: {
			originX?: number;
			originY?: number;
			relativeTo?: 'content' | 'container';
		},
	): void;
	setTransform(options?: { x?: number; y?: number; scale?: number }): void;
}
