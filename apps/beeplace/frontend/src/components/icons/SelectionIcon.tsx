import { SVGProps } from 'react';

export const SelectionIcon = (props: SVGProps<SVGSVGElement>) => (
	<svg
		width={102}
		height={102}
		viewBox="0 0 300 300"
		xmlns="http://www.w3.org/2000/svg"
		{...props}
	>
		<path
			style={{
				fill: 'none',
				strokeWidth: 50,
				stroke: 'currentColor',
				strokeLinecap: 'butt',
				strokeLinejoin: 'miter',
				strokeMiterlimit: 4,
				strokeDasharray: 'none',
				strokeOpacity: 1,
			}}
			d="M100 0H0v100M200 0h100v100M100 300H0V200m200 100h100V200"
		/>
	</svg>
);
