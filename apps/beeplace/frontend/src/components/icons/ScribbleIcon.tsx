import { SVGProps } from 'react';

export const ScribbleIcon = (props: SVGProps<SVGSVGElement>) => (
	<svg
		xmlns="http://www.w3.org/2000/svg"
		viewBox="0 0 19 19"
		fill="none"
		{...props}
	>
		<path
			stroke="currentColor"
			strokeLinecap="round"
			strokeLinejoin="round"
			strokeWidth={1.856}
			d="M6.482 1.27c0 1.095 5.97-.976 5.97 0C12.452 3.583 1 6.02 1 8.58c0 3.045 16.813-2.56 16.813 0 0 2.924-13.402 4.872-13.402 7.43 0 1.706 9.868-1.826 9.868 0"
		/>
	</svg>
);
