import { useEffect, useMemo, useState } from 'react';
import { formatMs } from '@/utils';

export const Timer = ({ deadline }: { deadline: Date }) => {
	const deadlineTime = useMemo(() => deadline.getTime(), [deadline]);
	const [ms, setMs] = useState(deadlineTime - Date.now());

	useEffect(() => {
		const interval = setInterval(() => setMs(deadlineTime - Date.now()), 1000);

		return () => clearInterval(interval);
	}, [deadlineTime]);

	return <>{ms > 0 && <div className="w-9">{formatMs(ms)}</div>}</>;
};
