import { Timer } from '@/components/Timer';
import { ClockIcon } from '@/components/icons/ClockIcon';
import { CubeIcon } from '@/components/icons/CubeIcon';
import { DownloadIcon } from '@/components/icons/DownloadIcon';
import { UsersIcon } from '@/components/icons/UsersIcon';
import { useBoardStore } from '@/store/board';
import { downloadImage } from '@/utils/board';
import { Coordinate } from '@beescreens/beeplace';
import clsx from 'clsx';

interface InfoPanelProps {
	className?: string;
	selection: Coordinate;
	showSelection: boolean;
}

export const InfosPanel = (props: InfoPanelProps) => {
	const { className, selection, showSelection } = props;
	const nextPixel = useBoardStore((state) => state.nextPixel);
	const userCount = useBoardStore((state) => state.userCount);

	return (
		<div
			className={clsx(
				className,
				'w-full max-w-[150px] space-y-2 rounded-xl border border-gray-500 bg-gray-700 px-3 py-2 text-sm text-white md:max-w-[165px] md:text-base',
			)}
		>
			{/* User count */}
			<div className="flex items-center justify-between">
				<div className="flex items-center space-x-2 md:space-x-3">
					<UsersIcon className="h-5 w-5 md:h-6 md:w-6" />
					<span className="min-w-[20px]">{userCount}</span>
				</div>
				<div
					onClick={() => downloadImage('beeplace.png')}
					className="group cursor-pointer rounded-lg border border-transparent px-2 pb-1 pt-0.5 text-gray-300 transition duration-200 ease-in hover:border-gray-400 hover:bg-gray-500 hover:text-white focus:outline-none"
				>
					<DownloadIcon className="tr h-5 w-5 transition duration-200 ease-in group-hover:translate-y-px" />
				</div>
			</div>
			{/* Timer */}
			<div className="flex items-center space-x-2 md:space-x-3">
				<ClockIcon className="h-5 w-5 md:h-6 md:w-6" />
				{nextPixel && nextPixel.getTime() > Date.now() ? (
					<Timer deadline={nextPixel} />
				) : (
					<div>-</div>
				)}
			</div>
			{/* Selection coordinates */}
			<div className="flex items-center space-x-2 md:space-x-3">
				<CubeIcon className="h-5 w-5 md:h-6 md:w-6" />
				<div className="flex justify-between space-x-2">
					{showSelection ? (
						<>
							<div className="min-w-[38px]">
								<span className="font-semibold">x: </span>
								{selection.x}
							</div>
							<div className="min-w-[38px]">
								<span className="font-semibold">y: </span>
								{selection.y}
							</div>
						</>
					) : (
						<div>-</div>
					)}
				</div>
			</div>
		</div>
	);
};
