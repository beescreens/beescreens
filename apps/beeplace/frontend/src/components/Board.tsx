import { useRef, useCallback, useState } from 'react';
import { useEffectOnce, useLatest, useLocalStorage } from 'react-use';
import { useBoard } from '@/providers/BoardProvider';
import {
	applyCssProperties,
	OnPinchZoomEvent,
	PinchZoom,
	PinchZoomHandle,
} from '@/components/PinchZoom';
import { isClickInsideRect } from '@/utils';
import { SelectionIcon } from '@/components/icons/SelectionIcon';
import { useWebSocket } from '@/providers/WebsocketProvider';
import { useBoardStore } from '@/store/board';
import { PlacePixelStatus } from '@/components/PlacePixelStatus';
import { Coordinate, InitialState, Pixel } from '@beescreens/beeplace';
import { centerCanvas, getImageData } from '@/utils/board';
import clsx from 'clsx';
import { InfosPanel } from '@/components/InfosPanel';
import { PlacePixel } from '@/components/PlacePixel';
import { OnPinchZoomClickEvent } from '@/components/PinchZoom/types';

interface BoardProps extends InitialState {
	minScale: number;
	maxScale: number;
}

const Y_OFFSET = 200;

export const Board = ({
	board,
	canvasSize,
	colors,
	initialScale,
	initialColor,
	maxPixels,
	minScale,
	maxScale,
}: BoardProps) => {
	const { canvasRef, tryToPlacePixel, getBounds, get2dContext } = useBoard();
	const { emitPixelFromPlayer } = useWebSocket();
	const pinchZoomRef = useRef<PinchZoomHandle>(null);
	const selectionIconRef = useRef<HTMLDivElement>(null);
	const [showPlacePixel, setShowPlacePixel] = useState(false);
	const [showSelection, setShowSelection] = useState(false);
	const [selection, setSelection] = useState<Coordinate>({ x: 0, y: 0 });
	const [selectionTransition, setSelectionTransition] = useState(false);
	const latestSelection = useLatest(selection);
	const [color, setColor] = useLocalStorage('color', initialColor);
	const canPlacePixel = useBoardStore((state) => state.canPlacePixel);

	// Center canvas on page load
	useEffectOnce(() => {
		if (!pinchZoomRef.current) return;
		centerCanvas(pinchZoomRef, canvasSize, initialScale);
	});

	// Display initial state
	useEffectOnce(() => {
		if (!board?.length) return;
		get2dContext().putImageData(getImageData(board, colors, canvasSize), 0, 0);
	});

	function getColor() {
		return color ?? initialColor;
	}

	const onTransform = useCallback(
		({ x, y, scale }: OnPinchZoomEvent) => {
			applyCssProperties(canvasRef.current, { x, y, scale });
			applyCssProperties(selectionIconRef.current, {
				x: x + latestSelection.current.x * scale,
				y: y + latestSelection.current.y * scale,
				scale: scale / 100,
			});
			setSelectionTransition(false);
		},
		[canvasRef, latestSelection],
	);

	function onClick(e: OnPinchZoomClickEvent) {
		if (!canPlacePixel() || !pinchZoomRef.current) return;

		toggleUI(true);
		const bound = getBounds();

		if (!isClickInsideRect(bound, e.x, e.y)) {
			toggleUI(false);
			return;
		}

		const scale = pinchZoomRef.current.scale();
		const x = Math.round((e.x - bound.x) / scale - 0.5);
		const y = Math.round((e.y - bound.y) / scale - 0.5);

		setSelection({ x, y });
		setSelectionTransition(true);

		const pinch = pinchZoomRef.current;
		const cameraWidth = document.documentElement.scrollWidth;
		const cameraHeight = document.documentElement.scrollHeight;

		// Selected point top left
		pinch.setTransform({
			scale: scale,
			x: pinch.x() - bound.x - x * scale,
			y: pinch.y() - bound.y - y * scale,
		});
		// Zooming
		pinch.scaleTo(maxScale, { relativeTo: 'container' });
		// Center on selected point
		pinch.setTransform({
			scale: maxScale,
			x: pinch.x() + cameraWidth / 2 - maxScale / 2,
			y: pinch.y() + cameraHeight / 2 - maxScale / 2 - Y_OFFSET,
		});

		applyCssProperties(selectionIconRef.current, {
			x: pinch.x() + x * maxScale,
			y: pinch.y() + y * maxScale,
			scale: maxScale / 100,
		});
	}

	function onPlace() {
		const pixel: Pixel = {
			x: selection.x,
			y: selection.y,
			color: getColor(),
		};
		if (tryToPlacePixel(pixel)) {
			emitPixelFromPlayer(pixel);
		}
		toggleUI(false);
	}

	function toggleUI(value: boolean) {
		setShowSelection(value);
		setShowPlacePixel(value);
	}

	return (
		<>
			{/* Infos Panel */}
			<InfosPanel
				className="fixed left-0 top-0 z-50 ml-3 mt-3 select-none"
				showSelection={showSelection}
				selection={selection}
			/>
			{/* Place pixel status message */}
			<div className="fixed bottom-0 left-1/2 z-20 mb-3 -translate-x-1/2 transform select-none">
				<PlacePixelStatus max={maxPixels} />
			</div>
			{/* Drawer to select pixels */}
			<PlacePixel
				isOpen={showPlacePixel}
				onOpenChange={toggleUI}
				colors={colors}
				color={getColor()}
				maxPixels={maxPixels}
				setColor={setColor}
				onPlace={onPlace}
				onCanvasClick={onClick}
			/>
			{/* Selection */}
			<div
				ref={selectionIconRef}
				className={clsx(
					'pointer-events-none absolute z-50 select-none bg-transparent',
					{
						'transition-transform duration-200': selectionTransition,
					},
				)}
				style={{
					transform:
						'translate3d(var(--x,0),var(--y,0),0) scale3d(var(--scale,1),var(--scale,1),1)',
					transformOrigin: '0 0',
				}}
			>
				{showSelection && <SelectionIcon className="text-white" />}
			</div>
			<PinchZoom
				ref={pinchZoomRef}
				onTransform={onTransform}
				handleClick={onClick}
				minScale={minScale}
				maxScale={maxScale}
			>
				<canvas
					ref={canvasRef}
					width={canvasSize}
					height={canvasSize}
					className="select-none bg-white"
					style={{
						imageRendering: 'pixelated',
					}}
				/>
			</PinchZoom>
		</>
	);
};
