import { Button } from '@/components/Button';
import { OnPinchZoomClickEvent } from '@/components/PinchZoom/types';
import { Timer } from '@/components/Timer';
import { EnterIcon } from '@/components/icons/EnterIcon';
import { useBoardStore } from '@/store/board';
import { useMemo, useRef, useState } from 'react';
import { useKey } from 'react-use';
import { Drawer } from 'vaul';

interface PlacePixelProps {
	isOpen: boolean;
	colors: string[];
	color: number;
	maxPixels: number;
	onOpenChange: (isOpen: boolean) => void;
	setColor: (color: number) => void;
	onPlace: () => void;
	onCanvasClick: (e: OnPinchZoomClickEvent) => void;
}

export const PlacePixel = (props: PlacePixelProps) => {
	const {
		isOpen,
		colors,
		color,
		maxPixels,
		onOpenChange,
		setColor,
		onPlace,
		onCanvasClick,
	} = props;
	const placeButtonRef = useRef<HTMLButtonElement>(null);
	const [showColorPicker, setShowColorPicker] = useState(false);
	const nextPixel = useBoardStore((state) => state.nextPixel);
	const placedPixels = useBoardStore((state) => state.placedPixels);
	const remaining = useMemo(
		() => maxPixels - placedPixels,
		[maxPixels, placedPixels],
	);

	useKey('Enter', () => {
		placeButtonRef.current?.click();
	});

	function updateColor(color: number) {
		setColor(color);
		setShowColorPicker(false);
	}

	function handleCanvasClick(e: CustomEvent<{ originalEvent: PointerEvent }>) {
		e.preventDefault();
		const { clientX, clientY } = e.detail.originalEvent;
		onCanvasClick({
			x: clientX,
			y: clientY,
		});
	}

	return (
		<Drawer.Root
			shouldScaleBackground={false}
			open={isOpen}
			onOpenChange={onOpenChange}
		>
			<Drawer.Portal>
				<Drawer.Overlay className="!pointer-events-none fixed inset-0 z-40 bg-black/40" />
				<Drawer.Content
					onPointerDownOutside={(e) => handleCanvasClick(e)}
					className="fixed bottom-0 left-0 right-0 z-50 mt-24 flex flex-col rounded-t-[10px] border-gray-500 bg-gray-700 text-white outline-none"
				>
					<div className="flex-1 rounded-t-[10px] p-4">
						<div className="mx-auto mb-4 h-1.5 w-12 flex-shrink-0 rounded-full bg-gray-400" />
						<div className="mx-auto max-w-md">
							<div className="mb-4 flex flex-col">
								<div>
									<h2 className="text-lg font-medium">Select your color</h2>
									<div className="mt-4">
										<Drawer.NestedRoot
											open={showColorPicker}
											onOpenChange={setShowColorPicker}
										>
											<button
												onClick={() => setShowColorPicker(true)}
												className="relative z-10 h-24 w-full rounded border-4 border-dashed border-black/40"
												style={{
													backgroundColor: colors[color],
												}}
											>
												<span
													className="font-semibold"
													style={{
														color: colors[color],
														filter:
															'saturate(0) grayscale(1) brightness(.7) contrast(1000%) invert(1)',
													}}
												>
													Pick a color
												</span>
											</button>
											<Drawer.Portal>
												<Drawer.Overlay className="fixed inset-0 z-[60] bg-black/40" />
												<Drawer.Content className="fixed bottom-0 left-0 right-0 z-[60] mt-24 flex flex-col rounded-t-[10px] border-gray-500 bg-gray-700 text-white">
													<div className="flex-1 rounded-t-[10px] p-4">
														<div className="mx-auto mb-4 h-1.5 w-12 flex-shrink-0 rounded-full bg-gray-400" />
														<div className="mx-auto mb-4 max-w-md">
															<Drawer.Title className="font-medium">
																Pick a color
															</Drawer.Title>
															<ul className="mt-2 grid grid-cols-4 gap-2">
																{colors.map((color, index) => (
																	<li key={index}>
																		<button
																			onClick={() => updateColor(index)}
																			className="block aspect-square h-full w-full rounded transition-transform duration-200 ease-in active:scale-90"
																			style={{
																				backgroundColor: color,
																			}}
																		></button>
																	</li>
																))}
															</ul>
														</div>
													</div>
												</Drawer.Content>
											</Drawer.Portal>
										</Drawer.NestedRoot>
									</div>
								</div>
								<Button
									ref={placeButtonRef}
									onClick={onPlace}
									className="mt-6 h-16"
								>
									Place
									<div className="ml-4 rounded-lg border border-neutral-800 bg-neutral-900 p-2 transition duration-200 ease-in group-hover:-translate-y-[2px] group-hover:border-neutral-700">
										<EnterIcon className="h-3 w-3" />
									</div>
								</Button>
							</div>
						</div>
					</div>
					<div className="mt-auto border-t border-gray-500 bg-gray-600 p-4">
						<div className="mx-auto flex max-w-md items-center justify-between gap-12 text-xs text-gray-200">
							<div className="flex items-center">
								<span className="mr-1 font-semibold">{remaining}</span>
								<span>{remaining > 1 ? 'pixels' : 'pixel'} remaining</span>
							</div>
							{nextPixel && nextPixel.getTime() > Date.now() && (
								<div className="flex items-center gap-1">
									<span>More pixels in</span>
									<div className="font-semibold">
										<Timer deadline={nextPixel} />
									</div>
								</div>
							)}
						</div>
					</div>
				</Drawer.Content>
			</Drawer.Portal>
		</Drawer.Root>
	);
};
