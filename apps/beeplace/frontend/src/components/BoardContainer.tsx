import { Board } from '@/components/Board';
import { DisplayBoard } from '@/components/display/DisplayBoard';
import { InfoIcon } from '@/components/icons/InfoIcon';
import { useBoardStore } from '@/store/board';

interface BoardContainerProps {
	minScale: number;
	maxScale: number;
}

export const BoardContainer = (props: BoardContainerProps) => {
	const initialState = useBoardStore((state) => state.initialState);

	if (!initialState) {
		return <></>;
	}

	if (initialState.readOnly) {
		return (
			<>
				<div className="pointer-events-none fixed z-50 mt-2 flex w-full select-none justify-center">
					<div className="flex items-center space-x-3 rounded-lg border border-indigo-300 bg-indigo-400 px-8 py-2 text-white">
						<InfoIcon className="h-8 w-8" />
						<span>BeePlace is currently in read-only mode.</span>
					</div>
				</div>
				<DisplayBoard {...initialState} {...props} />
			</>
		);
	}

	return <Board {...initialState} {...props} />;
};
