import { Timer } from '@/components/Timer';
import { useBoardStore } from '@/store/board';
import clsx from 'clsx';
import { useMemo } from 'react';

export const PlacePixelStatus = ({ max }: { max: number }) => {
	const nextPixel = useBoardStore((state) => state.nextPixel);
	const placedPixels = useBoardStore((state) => state.placedPixels);
	const remaining = useMemo(() => max - placedPixels, [max, placedPixels]);

	return (
		<>
			<div className="flex space-x-1 rounded-xl border border-gray-500 bg-gray-700 px-3 py-2 text-white">
				{remaining > 0 ? (
					<>
						<span>Place your </span>
						{remaining > 1 && (
							<div
								className={clsx(
									'flex justify-center border-b border-white font-bold',
									{
										'w-3': max < 10,
										'w-6': max >= 10,
									},
								)}
							>
								{remaining}
							</div>
						)}
						<div className="w-10">{remaining > 1 ? 'pixels' : 'pixel'}</div>
					</>
				) : (
					<div className="flex items-center gap-1">
						<span>More pixels in</span>
						<div className="font-semibold">
							<Timer deadline={nextPixel!} />
						</div>
					</div>
				)}
			</div>
		</>
	);
};
