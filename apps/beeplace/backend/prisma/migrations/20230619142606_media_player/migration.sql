/*
  Warnings:

  - You are about to drop the column `user` on the `pixels` table. All the data in the column will be lost.
  - Added the required column `user_id` to the `pixels` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "pixels" DROP COLUMN "user",
ADD COLUMN     "is_admin" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "user_id" TEXT NOT NULL,
ALTER COLUMN "color" SET DATA TYPE TEXT;
