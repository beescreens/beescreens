/*
  Warnings:

  - You are about to drop the column `is_admin` on the `pixels` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "pixels" DROP COLUMN "is_admin";
