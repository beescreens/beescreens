# BeePlace backend

## Prerequisites

The following prerequisites must be filled to run this service:

- [Node.js](../../../docs/explanations/about-nodejs/index.md) must be installed.
- [pnpm](../../../docs/explanations/about-pnpm/index.md) must be installed.
- [Docker](../../../docs/explanations/about-docker/index.md) must be installed.
- [Docker Compose](../../../docs/explanations/about-docker/index.md) must be installed.

## Set up the service

```sh
# Install the dependencies
pnpm install

# Copy the .env.defaults file to a .env file.
pnpm setup:env

# Edit the environment variables file if needed
vim .env
```

## Start the service in development mode

```sh
# Start Redis and PostgreSQL in the background
docker compose up -d

# Start the service in development mode
pnpm dev
```

The service should be now accessible on [http://localhost:4000](http://localhost:4000)

## Build the service

```sh
# Build the service
pnpm build
```

## Test the service

```sh
# Test the service
pnpm test
```

## Start the service in production mode

```sh
# Start the service in production mode
pnpm start
```

## Profiling

The app can be started in profiling mode to generate different type of graphs when it's stopped.

It's using Clinic.js to generate the graphs. You can learn more about the different types of profiling tests available [here](https://clinicjs.org/).

```sh
# Here are the different types of profiling tests available
pnpm profile:bubble
pnpm profile:doctor
pnpm profile:flame
```

The idea is to profile the app while running a load test in local with k6, see [here](../tests/load-tests).
