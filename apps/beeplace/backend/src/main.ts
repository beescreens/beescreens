import { NestFactory } from '@nestjs/core';
import {
	FastifyAdapter,
	NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { AppModule } from '@/app.module';
import { ConfigService } from '@nestjs/config';
import { PORT } from '@/config/config.constants';
import { bootstrap } from '@/bootstrap';

const main = async () => {
	const instance = await NestFactory.create<NestFastifyApplication>(
		AppModule,
		new FastifyAdapter(),
	);

	const app = await bootstrap(instance);

	const configService = app.get(ConfigService);
	const port = configService.get<number>(PORT);

	await app.listen(port as number, '0.0.0.0');
};

main();
