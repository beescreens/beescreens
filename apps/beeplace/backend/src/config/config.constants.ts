export const API_KEY = 'apiKey';
export const BOARD_REFRESH_RATE = 'boardRefreshRate';
export const BOARD_SIZE = 'boardSize';
export const COLORS = 'colors';
export const COOLDOWN = 'cooldown';
export const INITIAL_COLOR = 'initialColor';
export const INITIAL_SCALE = 'initialScale';
export const MAX_PIXELS = 'maxPixels';
export const PORT = 'port';
export const REDIS_HOST = 'redisHost';
export const REDIS_PORT = 'redisPort';
export const USER_COUNT_REFRESH_RATE = 'userCountRefreshRate';

export interface EnvironmentVariables {
	[API_KEY]: string;
	[BOARD_REFRESH_RATE]: number;
	[BOARD_SIZE]: number;
	[COLORS]: string[];
	[COOLDOWN]: number;
	[INITIAL_COLOR]: number;
	[INITIAL_SCALE]: number;
	[MAX_PIXELS]: number;
	[PORT]: number;
	[REDIS_HOST]: string;
	[REDIS_PORT]: number;
	[USER_COUNT_REFRESH_RATE]: number;
}
