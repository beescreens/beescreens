import {
	API_KEY,
	BOARD_REFRESH_RATE,
	BOARD_SIZE,
	COLORS,
	COOLDOWN,
	INITIAL_COLOR,
	INITIAL_SCALE,
	MAX_PIXELS,
	PORT,
	REDIS_HOST,
	REDIS_PORT,
	USER_COUNT_REFRESH_RATE,
} from '@/config/config.constants';

export default () => ({
	[API_KEY]: process.env.API_KEY as string,
	[BOARD_REFRESH_RATE]: parseInt(process.env.BOARD_REFRESH_RATE as string),
	[BOARD_SIZE]: parseInt(process.env.BOARD_SIZE as string),
	[COLORS]: JSON.parse(process.env.COLORS as string),
	[COOLDOWN]: parseInt(process.env.COOLDOWN as string),
	[INITIAL_COLOR]: parseInt(process.env.INITIAL_COLOR_INDEX as string),
	[INITIAL_SCALE]: parseInt(process.env.INITIAL_SCALE as string),
	[MAX_PIXELS]: parseInt(process.env.MAX_PIXELS as string),
	[PORT]: parseInt(process.env.PORT as string),
	[REDIS_HOST]: process.env.REDIS_HOST as string,
	[REDIS_PORT]: parseInt(process.env.REDIS_PORT as string),
	[USER_COUNT_REFRESH_RATE]: parseInt(
		process.env.USER_COUNT_REFRESH_RATE as string,
	),
});
