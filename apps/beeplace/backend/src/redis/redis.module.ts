import {
	DynamicModule,
	FactoryProvider,
	Logger,
	Module,
	ModuleMetadata,
} from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import IORedis, { Redis, RedisOptions } from 'ioredis';
import {
	EnvironmentVariables,
	REDIS_HOST,
	REDIS_PORT,
} from '@/config/config.constants';

export const IO_REDIS_KEY = 'IORedis';

type RedisModuleOptions = {
	connectionOptions: RedisOptions;
	onClientReady: (client: Redis) => void;
};

type RedisAsyncModuleOptions = {
	useFactory: (
		...args: any[]
	) => Promise<RedisModuleOptions> | RedisModuleOptions;
} & Pick<ModuleMetadata, 'imports'> &
	Pick<FactoryProvider, 'inject'>;

class CustomRedisModule {
	static async registerAsync(
		options: RedisAsyncModuleOptions,
	): Promise<DynamicModule> {
		const { useFactory, imports, inject } = options;

		const redisProvider = {
			provide: IO_REDIS_KEY,
			useFactory: async (...args: any[]) => {
				const { connectionOptions, onClientReady } = await useFactory(...args);

				const client = await new IORedis(connectionOptions);

				onClientReady(client);

				return client;
			},
			inject,
		};

		return {
			module: CustomRedisModule,
			imports,
			providers: [redisProvider],
			exports: [redisProvider],
		};
	}
}

@Module({
	imports: [
		CustomRedisModule.registerAsync({
			imports: [ConfigModule],
			useFactory: async (
				configService: ConfigService<EnvironmentVariables, true>,
			) => {
				const logger = new Logger('RedisModule');

				return {
					connectionOptions: {
						host: configService.get(REDIS_HOST),
						port: configService.get(REDIS_PORT),
					},
					onClientReady: (client) => {
						logger.log('Redis client ready');

						client.on('error', (err) => {
							logger.error('Redis Client Error: ', err);
						});

						client.on('connect', () => {
							logger.log(
								`Connected to redis on ${client.options.host}:${client.options.port}`,
							);
						});
					},
				};
			},
			inject: [ConfigService],
		}),
	],
	exports: [CustomRedisModule],
})
export class RedisModule {}
