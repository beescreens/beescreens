import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { BoardController } from '@/board/board.controller';
import { BoardService } from '@/board/board.service';
import { PlayerService } from '@/player/player.service';
import { BoardGateway } from '@/board/board.gateway';
import { RedisModule } from '@/redis/redis.module';
import { PrismaService } from 'nestjs-prisma';

@Module({
	imports: [ConfigModule, RedisModule],
	controllers: [BoardController],
	providers: [BoardService, BoardGateway, PlayerService, PrismaService],
})
export class BoardModule {}
