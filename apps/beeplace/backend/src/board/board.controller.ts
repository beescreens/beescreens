import {
	Body,
	Controller,
	HttpCode,
	HttpException,
	HttpStatus,
	Post,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ApiKeyAuth } from '@/auth/api-key/api-key-auth.decorator';
import { BoardGateway } from '@/board/board.gateway';
import { ReadOnlyDto } from '@/board/dtos/read-only.dto';
import { ResetAreaDto } from '@/board/dtos/reset-area.dto';
import {
	BOARD_SIZE,
	COLORS,
	EnvironmentVariables,
} from '@/config/config.constants';
import { ApiBody, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { RestoreFromDatabaseDto } from '@/board/dtos/restore-from-database.dto';

@ApiTags('Board')
@Controller({
	path: 'board',
	version: '1',
})
export class BoardController {
	constructor(
		private readonly boardGateway: BoardGateway,
		private readonly configService: ConfigService<EnvironmentVariables, true>,
	) {}

	@Post('read-only')
	@HttpCode(HttpStatus.OK)
	@ApiOperation({
		summary: 'Activate or deactivate read only mode',
		description: 'Activate or deactivate read only mode.',
		operationId: 'readOnly',
	})
	@ApiBody({
		description: 'Activate or deactivate read only mode',
		type: ReadOnlyDto,
	})
	@ApiOkResponse({
		description: 'Read only mode activated or deactivated.',
	})
	@ApiKeyAuth()
	async readOnly(@Body() body: ReadOnlyDto) {
		await this.boardGateway.setReadOnly(body.activate);

		return {
			statusCode: HttpStatus.OK,
			message: `Read only mode ${
				body.activate ? 'activated' : 'deactivated'
			}}}`,
		};
	}

	@Post('reset')
	@HttpCode(HttpStatus.OK)
	@ApiOperation({
		summary: 'Reset the board',
		description: 'Reset the board to its initial state.',
		operationId: 'reset',
	})
	@ApiOkResponse({
		description: 'Board reset.',
	})
	@ApiKeyAuth()
	async reset() {
		await this.boardGateway.resetBoard();

		return {
			statusCode: HttpStatus.OK,
			message: 'Board reset',
		};
	}

	@Post('reset-area')
	@HttpCode(HttpStatus.OK)
	@ApiOperation({
		summary: 'Reset an area of the board',
		description: 'Reset an area of the board to a given color.',
		operationId: 'resetArea',
	})
	@ApiBody({
		description: 'Area to reset and color to set.',
		type: ResetAreaDto,
	})
	@ApiOkResponse({
		description: 'Board area reset.',
	})
	@ApiKeyAuth()
	async resetArea(@Body() body: ResetAreaDto) {
		const { topLeft, bottomRight, color = 0 } = body;

		const colors = this.configService.get(COLORS, { infer: true });
		if (color >= colors.length) {
			throw new HttpException(
				`Invalid color, should be in the range [0, ${colors.length - 1}]`,
				HttpStatus.BAD_REQUEST,
			);
		}

		const boardSize = this.configService.get(BOARD_SIZE, { infer: true });
		if (
			topLeft[0] > bottomRight[0] ||
			topLeft[1] > bottomRight[1] ||
			bottomRight[0] >= boardSize ||
			bottomRight[1] >= boardSize
		) {
			throw new HttpException(
				`Invalid coordinates for a board of size ${boardSize}`,
				HttpStatus.BAD_REQUEST,
			);
		}

		await this.boardGateway.resetArea(topLeft, bottomRight, color);

		return {
			statusCode: HttpStatus.OK,
			message: 'Board area reset',
		};
	}

	@Post('restore-from-database')
	@HttpCode(HttpStatus.OK)
	@ApiOperation({
		summary: 'Restore board state from the database',
		description: 'Restore board state from the database at a given time.',
		operationId: 'restoreFromDatabase',
	})
	@ApiBody({
		description: 'Date of the state to restore.',
		type: RestoreFromDatabaseDto,
	})
	@ApiOkResponse({
		description: 'Board restored',
	})
	@ApiKeyAuth()
	async restoreFromDatabase(@Body() body: RestoreFromDatabaseDto) {
		await this.boardGateway.restoreFromDatabase(body.date);

		return {
			statusCode: HttpStatus.OK,
			message: 'Board restored',
		};
	}
}
