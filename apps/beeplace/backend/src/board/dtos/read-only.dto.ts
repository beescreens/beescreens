import { IsBoolean } from 'class-validator';

export class ReadOnlyDto {
	/**
	 * Activate the read-only mode
	 */
	@IsBoolean()
	activate: boolean;
}
