import { IsDateString } from 'class-validator';

export class RestoreFromDatabaseDto {
	/**
	 * Date from which to restore from the database.
	 */
	@IsDateString()
	date: Date;
}
