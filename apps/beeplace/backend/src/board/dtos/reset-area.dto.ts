import {
	IsArray,
	IsInt,
	ArrayMinSize,
	ArrayMaxSize,
	Min,
} from 'class-validator';

export class ResetAreaDto {
	/**
	 * The top left corner of the area to reset
	 */
	@IsArray()
	@ArrayMinSize(2)
	@ArrayMaxSize(2)
	@IsInt({ each: true })
	@Min(0, { each: true })
	topLeft: number[];

	/**
	 * The bottom right corner of the area to reset
	 */
	@IsArray()
	@ArrayMinSize(2)
	@ArrayMaxSize(2)
	@IsInt({ each: true })
	@Min(0, { each: true })
	bottomRight: number[];

	/**
	 * The color index to reset the area to
	 */
	@IsInt()
	@Min(0)
	color?: number;
}
