import { ADMIN_USERNAME } from '@/auth/auth.constants';
import {
	BOARD_SIZE,
	COLORS,
	EnvironmentVariables,
} from '@/config/config.constants';
import { IO_REDIS_KEY } from '@/redis/redis.module';
import { Pixel } from '@beescreens/beeplace';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import Redis from 'ioredis';
import { PrismaService } from 'nestjs-prisma';

const BOARD_KEY = 'board';
const UINT_8 = 'u8';
const UTF8 = 'utf-8';
const WHITE = 0;

@Injectable()
export class BoardService {
	private readonly logger = new Logger(BoardService.name);

	constructor(
		private readonly configService: ConfigService<EnvironmentVariables, true>,
		private prisma: PrismaService,
		@Inject(IO_REDIS_KEY) private readonly redisClient: Redis,
	) {}

	private async initBoard(size: number) {
		await this.redisClient.bitfield(
			BOARD_KEY,
			'SET',
			UINT_8,
			`#${size * size - 1}`,
			WHITE,
		);
	}

	private async storePixelInBitfield(pixel: Pixel) {
		const { x, y, color } = pixel;
		const offset = y * this.configService.get(BOARD_SIZE) + x;
		return this.redisClient.bitfield(
			BOARD_KEY,
			'SET',
			UINT_8,
			`#${offset}`,
			color,
		);
	}

	async setupBoard() {
		// Set the board size
		const size = this.configService.get(BOARD_SIZE);
		const bitfield = await this.redisClient.get(BOARD_KEY);
		if (!bitfield) {
			await this.initBoard(size);
			return;
		}
		const buffer = Buffer.from(bitfield, UTF8);
		const array = Array.from(buffer);
		if (array.length < size * size) {
			await this.initBoard(size);
		}
		this.logger.log('Board size has been set.');
	}

	async getBoard() {
		const bitfield = await this.redisClient.get(BOARD_KEY);
		if (!bitfield) {
			return [];
		}
		const buffer = Buffer.from(bitfield, UTF8);
		const size = this.configService.get(BOARD_SIZE, { infer: true });
		return Array.from(buffer.subarray(0, size * size));
	}

	async placePixel(pixel: Pixel, fingerprint: string) {
		// Store pixel in the Redis Cache
		await this.storePixelInBitfield(pixel);

		// Store pixel in the Database
		// TODO: can be done in a Queue to improve performance
		const colors = this.configService.get(COLORS, { infer: true });
		if (pixel.color >= colors.length) {
			return;
		}
		await this.prisma.pixel.create({
			data: {
				x: pixel.x,
				y: pixel.y,
				color: colors[pixel.color],
				userId: fingerprint,
			},
		});
	}

	async resetBoard() {
		await this.redisClient.del(BOARD_KEY);
	}

	async resetArea(pixels: Pixel[]) {
		// Store in Redis
		const promises = pixels.map((pixel) => this.storePixelInBitfield(pixel));
		await Promise.all(promises);

		// Store in Database
		const colors = this.configService.get(COLORS, { infer: true });
		await this.prisma.pixel.createMany({
			data: pixels.map(({ x, y, color }) => ({
				x,
				y,
				color: colors[color],
				userId: ADMIN_USERNAME,
			})),
		});
	}

	generatePixelsInArea(
		topLeft: number[],
		bottomRight: number[],
		color: number,
	) {
		const pixels: Pixel[] = [];

		for (let x = topLeft[0]; x <= bottomRight[0]; x++) {
			for (let y = topLeft[1]; y <= bottomRight[1]; y++) {
				pixels.push({ x, y, color });
			}
		}

		return pixels;
	}

	async restoreFromDatabase(date: Date): Promise<Pixel[]> {
		const colors = this.configService.get(COLORS, { infer: true });
		const colorsMap = new Map(colors.map((color, index) => [color, index]));

		const pixelsFromDB = await this.prisma.pixel.findMany({
			select: {
				x: true,
				y: true,
				color: true,
				createdAt: true,
			},
			orderBy: [{ x: 'asc' }, { y: 'asc' }, { createdAt: 'desc' }],
			distinct: ['x', 'y'],
			where: {
				createdAt: {
					lte: date,
				},
			},
		});
		const latestPixels = pixelsFromDB.map(({ x, y, color }) => ({
			x,
			y,
			color: colorsMap.get(color) ?? 0,
		}));

		// Redis
		const promises = latestPixels.map((pixel) =>
			this.storePixelInBitfield(pixel),
		);
		await Promise.all(promises);

		// Database
		await this.prisma.pixel.createMany({
			data: pixelsFromDB.map(({ x, y, color }) => ({
				x,
				y,
				color,
				userId: ADMIN_USERNAME,
			})),
		});

		return latestPixels;
	}
}
