import { ConfigService } from '@nestjs/config';
import {
	ConnectedSocket,
	MessageBody,
	OnGatewayDisconnect,
	SubscribeMessage,
	WebSocketGateway,
	WebSocketServer,
} from '@nestjs/websockets';
import {
	BOARD_REFRESH_RATE,
	BOARD_SIZE,
	COLORS,
	COOLDOWN,
	EnvironmentVariables,
	INITIAL_COLOR,
	INITIAL_SCALE,
	MAX_PIXELS,
	USER_COUNT_REFRESH_RATE,
} from '@/config/config.constants';
import { BoardService } from '@/board/board.service';
import { PlayerService } from '@/player/player.service';
import {
	BeePlaceBackendSocket,
	BeePlaceSocketServer,
	ClientEvent,
	ServerEvent,
	Pixel,
	JoinResponse,
	Coordinate,
	JoinPayload,
} from '@beescreens/beeplace';
import { OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { SchedulerRegistry } from '@nestjs/schedule';

const BOARD_UPDATE_INTERVAL = 'boardUpdate';
const USER_COUNT_INTERVAL = 'userCount';

@WebSocketGateway({
	cors: {
		origin: '*',
	},
	transports: ['websocket'],
})
export class BoardGateway
	implements OnModuleInit, OnModuleDestroy, OnGatewayDisconnect
{
	@WebSocketServer()
	private server!: BeePlaceSocketServer;
	private pixels: Map<Coordinate, Pixel> = new Map();
	private board: number[] = [];
	private readOnly = false;
	private players = new Set<string>();
	private userCountUpdated = false;

	constructor(
		private readonly configService: ConfigService<EnvironmentVariables, true>,
		private readonly boardService: BoardService,
		private readonly playerService: PlayerService,
		private readonly schedulerRegistry: SchedulerRegistry,
	) {}

	private pixelsToString(pixels: Pixel[]) {
		return pixels
			.map((pixel) => [pixel.x, pixel.y, pixel.color])
			.flat()
			.toString();
	}

	private broadcastBoardUpdates = () => {
		// Don't broadcast if there are no pixels to update
		if (this.pixels.size === 0) return;

		const pixels = Array.from(this.pixels.values());
		const buffer = this.pixelsToString(pixels);

		this.server.emit(ServerEvent.UPDATE_BOARD, buffer);
		this.pixels.clear();
	};

	private broadcastUserCount = () => {
		// Don't broadcast if no user joined or left
		if (!this.userCountUpdated) return;

		this.server.emit(ServerEvent.USER_COUNT, this.players.size);
		this.userCountUpdated = false;
	};

	async onModuleInit() {
		// Update board interval
		const boardMs = this.configService.get(BOARD_REFRESH_RATE, {
			infer: true,
		});
		const boardInterval = setInterval(this.broadcastBoardUpdates, boardMs);
		this.schedulerRegistry.addInterval(BOARD_UPDATE_INTERVAL, boardInterval);

		// User count interval
		const userCountMs = this.configService.get(USER_COUNT_REFRESH_RATE, {
			infer: true,
		});
		const userCountInterval = setInterval(this.broadcastUserCount, userCountMs);
		this.schedulerRegistry.addInterval(USER_COUNT_INTERVAL, userCountInterval);

		// Init board
		await this.boardService.setupBoard();
		this.board = await this.boardService.getBoard();
	}

	onModuleDestroy() {
		this.schedulerRegistry.deleteInterval(BOARD_UPDATE_INTERVAL);
		this.schedulerRegistry.deleteInterval(USER_COUNT_INTERVAL);
	}

	handleDisconnect(@ConnectedSocket() socket: BeePlaceBackendSocket) {
		if (socket.data.fingerprint) {
			this.players.delete(socket.data.fingerprint);
			this.userCountUpdated = true;
		}
	}

	@SubscribeMessage(ClientEvent.JOIN)
	async join(
		@ConnectedSocket()
		socket: BeePlaceBackendSocket,
		@MessageBody() { fingerprint, isDisplay }: JoinPayload,
	): Promise<JoinResponse> {
		socket.data.fingerprint = fingerprint;

		if (!isDisplay) {
			this.players.add(fingerprint);
			this.userCountUpdated = true;
		}

		return {
			initialState: {
				board: this.board,
				canvasSize: this.configService.get(BOARD_SIZE, { infer: true }),
				colors: this.configService.get(COLORS, { infer: true }),
				cooldown: this.configService.get(COOLDOWN, { infer: true }),
				initialColor: this.configService.get(INITIAL_COLOR, { infer: true }),
				initialScale: this.configService.get(INITIAL_SCALE, { infer: true }),
				maxPixels: this.configService.get(MAX_PIXELS, { infer: true }),
				readOnly: this.readOnly,
			},
			cooldown: await this.playerService.getCooldown(fingerprint),
			placedPixels: await this.playerService.getPlacedPixels(fingerprint),
			userCount: this.players.size,
		};
	}

	@SubscribeMessage(ClientEvent.PIXEL_FROM_PLAYER)
	async addPixel(
		@ConnectedSocket() socket: BeePlaceBackendSocket,
		@MessageBody() pixel: Pixel,
	): Promise<void> {
		if (!socket.data.fingerprint || this.readOnly) {
			return;
		}

		const maxPixels = this.configService.get(MAX_PIXELS, { infer: true });
		const placedPixel = await this.playerService.getPlacedPixels(
			socket.data.fingerprint,
		);

		if (placedPixel >= maxPixels) {
			console.log(`Already placed ${placedPixel} pixels`);
			return;
		}

		await this.boardService.placePixel(pixel, socket.data.fingerprint);
		const isFirstPixel = placedPixel === 0;
		await this.playerService.storePlayer(socket.data.fingerprint, isFirstPixel);

		// Add pixel to map for future broadcast
		this.pixels.set({ x: pixel.x, y: pixel.y }, pixel);

		// Add to the board cache to prevent Redis calls
		const offset = pixel.y * this.configService.get(BOARD_SIZE) + pixel.x;
		this.board[offset] = pixel.color;
	}

	async setReadOnly(readOnly: boolean) {
		this.readOnly = readOnly;
		this.server.emit(ServerEvent.READ_ONLY, readOnly);
	}

	async resetBoard(): Promise<void> {
		await this.boardService.resetBoard();
		this.server.emit(ServerEvent.RESET_BOARD);
	}

	async resetArea(
		topLeft: number[],
		bottomRight: number[],
		color: number,
	): Promise<void> {
		const pixels = this.boardService.generatePixelsInArea(
			topLeft,
			bottomRight,
			color,
		);

		await this.boardService.resetArea(pixels);

		// Update board cache
		const size = this.configService.get(BOARD_SIZE, { infer: true });
		pixels.forEach(
			(pixel) => (this.board[pixel.y * size + pixel.x] = pixel.color),
		);

		this.server.emit(ServerEvent.UPDATE_BOARD, this.pixelsToString(pixels));
	}

	async restoreFromDatabase(date: Date): Promise<void> {
		const pixels = await this.boardService.restoreFromDatabase(date);

		this.board = await this.boardService.getBoard();

		this.server.emit(ServerEvent.UPDATE_BOARD, this.pixelsToString(pixels));
	}
}
