import { COOLDOWN, EnvironmentVariables } from '@/config/config.constants';
import { IO_REDIS_KEY } from '@/redis/redis.module';
import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import Redis from 'ioredis';

const EXPIRE = 'EX';
const PLAYERS_KEY = 'users';

@Injectable()
export class PlayerService {
	constructor(
		private readonly configService: ConfigService<EnvironmentVariables, true>,
		@Inject(IO_REDIS_KEY) private readonly redisClient: Redis,
	) {}

	private makeKey(fingerprint: string) {
		return `${PLAYERS_KEY}:${fingerprint}`;
	}

	private async getPlayer(fingerprint: string) {
		return this.redisClient.get(this.makeKey(fingerprint));
	}

	async storePlayer(fingerprint: string, isFirstPixel: boolean) {
		if (isFirstPixel) {
			const cooldown = this.configService.get(COOLDOWN, { infer: true });
			const placedPixel = 1;

			await this.redisClient.set(
				this.makeKey(fingerprint),
				placedPixel,
				EXPIRE,
				cooldown,
			);
		} else {
			await this.redisClient.incr(this.makeKey(fingerprint));
		}
	}

	async updatePlayer(fingerprint: string) {
		await this.redisClient.incr(this.makeKey(fingerprint));
	}

	async getPlacedPixels(fingerprint: string) {
		const player = await this.getPlayer(fingerprint);
		return player ? parseInt(player) : 0;
	}

	async getCooldown(fingerprint: string) {
		return this.redisClient.ttl(this.makeKey(fingerprint));
	}
}
