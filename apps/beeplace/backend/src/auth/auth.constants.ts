export enum PASSPORT_STRATEGY_NAME {
	API_KEY = 'API_KEY',
}

export const API_KEY_HEADER_NAME = 'apiKey';

export const ADMIN_USERNAME = 'ADMIN';
