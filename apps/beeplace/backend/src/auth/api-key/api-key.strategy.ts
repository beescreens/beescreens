import { HeaderAPIKeyStrategy } from 'passport-headerapikey';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { AuthService } from '@/auth/auth.service';
import {
	API_KEY_HEADER_NAME,
	PASSPORT_STRATEGY_NAME,
} from '@/auth/auth.constants';

type DoneCallback = (err: Error | null, verified?: boolean) => void;

@Injectable()
export class ApiKeyStrategy extends PassportStrategy(
	HeaderAPIKeyStrategy,
	PASSPORT_STRATEGY_NAME.API_KEY,
) {
	constructor(private authService: AuthService) {
		super(
			{ header: API_KEY_HEADER_NAME },
			false,
			async (apiKey: string, done: DoneCallback) => {
				try {
					await this.authService.validateApiKey(apiKey);

					done(null, true);
				} catch (error) {
					done(null, false);
				}
			},
		);
	}
}
