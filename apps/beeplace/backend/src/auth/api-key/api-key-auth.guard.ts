import { PASSPORT_STRATEGY_NAME } from '@/auth/auth.constants';
import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class ApiKeyAuthGuard extends AuthGuard(
	PASSPORT_STRATEGY_NAME.API_KEY,
) {}
