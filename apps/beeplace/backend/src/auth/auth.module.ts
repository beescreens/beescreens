import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { ConfigModule } from '@nestjs/config';
import { AuthService } from '@/auth/auth.service';
import { ApiKeyStrategy } from './api-key/api-key.strategy';

@Module({
	imports: [ConfigModule, PassportModule],
	providers: [AuthService, ApiKeyStrategy],
	exports: [AuthService],
})
export class AuthModule {}
