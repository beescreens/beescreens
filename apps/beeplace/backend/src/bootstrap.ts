import { ValidationPipe, VersioningType } from '@nestjs/common';
import { NestFastifyApplication } from '@nestjs/platform-fastify';
import { PrismaService } from 'nestjs-prisma';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import {
	API_KEY_HEADER_NAME,
	PASSPORT_STRATEGY_NAME,
} from '@/auth/auth.constants';

export async function bootstrap(
	app: NestFastifyApplication,
): Promise<NestFastifyApplication> {
	app.useGlobalPipes(
		new ValidationPipe({
			whitelist: true,
			transform: true,
		}),
	);

	app.enableVersioning({
		type: VersioningType.URI,
		prefix: 'api/v',
	});

	const prismaService = app.get(PrismaService);
	await prismaService.enableShutdownHooks(app);

	const config = new DocumentBuilder()
		.setTitle('BeePlace API')
		.setDescription('The BeePlace API description')
		.setVersion(process.env.npm_package_version as string)
		.addApiKey(
			{
				type: 'apiKey',
				description: 'The API key to access protected endpoints',
				name: API_KEY_HEADER_NAME,
			},
			PASSPORT_STRATEGY_NAME.API_KEY,
		)
		.build();

	const document = SwaggerModule.createDocument(app, config);

	SwaggerModule.setup('api', app, document, {
		swaggerOptions: {
			showExtensions: true,
			tagsSorter: 'alpha',
			operationsSorter: 'alpha',
			persistAuthorization: true,
		},
	});

	return app;
}
