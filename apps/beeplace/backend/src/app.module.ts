import { Module } from '@nestjs/common';
import { ConfigModule } from '@/config/config.module';
import { BoardModule } from '@/board/board.module';
import { AuthModule } from '@/auth/auth.module';
import { RedisModule } from '@/redis/redis.module';
import { PrismaModule } from 'nestjs-prisma';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from '@/app.controller';

@Module({
	imports: [
		AuthModule,
		BoardModule,
		ConfigModule,
		PrismaModule,
		RedisModule,
		ScheduleModule.forRoot(),
	],
	controllers: [AppController],
})
export class AppModule {}
