export interface Coordinate {
	x: number;
	y: number;
}

export interface Pixel extends Coordinate {
	color: number;
}
