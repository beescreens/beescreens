export interface InitialState {
	board: number[];
	canvasSize: number;
	colors: string[];
	cooldown: number;
	initialColor: number;
	initialScale: number;
	maxPixels: number;
	readOnly: boolean;
}
