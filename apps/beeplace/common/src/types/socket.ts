import { Server, Socket } from 'socket.io';
import { Socket as ClientSocket } from 'socket.io-client';
import { Pixel } from '../types';
import { ClientEvent, ServerEvent } from '../enums/events';
import { JoinResponse } from './join-response';
import { JoinPayload } from './join-payload';

interface ServerToClientEvents {
	[ServerEvent.READ_ONLY]: (readOnly: boolean) => void;
	[ServerEvent.RESET_BOARD]: () => void;
	[ServerEvent.UPDATE_BOARD]: (data: string) => void;
	[ServerEvent.USER_COUNT]: (count: number) => void;
}

interface ClientToServerEvents {
	[ClientEvent.JOIN]: (
		data: JoinPayload,
		callback: (response: JoinResponse) => void,
	) => void;
	[ClientEvent.PIXEL_FROM_PLAYER]: (pixel: Pixel) => void;
}

interface InterServerEvents {
	ping: () => void;
}

interface SocketData {
	fingerprint: string;
}

export type BeePlaceFrontendSocket = ClientSocket<
	ServerToClientEvents,
	ClientToServerEvents
>;
export type BeePlaceBackendSocket = Socket<
	ClientToServerEvents,
	ServerToClientEvents,
	InterServerEvents,
	SocketData
>;
export type BeePlaceSocketServer = Server<
	ClientToServerEvents,
	ServerToClientEvents,
	InterServerEvents,
	SocketData
>;
