export interface JoinPayload {
	fingerprint: string;
	isDisplay?: boolean;
}
