import { InitialState } from './initial-state';

export interface JoinResponse {
	initialState: InitialState;
	cooldown: number;
	placedPixels: number;
	userCount: number;
}
