export enum ServerEvent {
	READ_ONLY = 'readOnly',
	RESET_BOARD = 'resetBoard',
	UPDATE_BOARD = 'u', // shortest possible name to reduce bandwidth usage for end users
	USER_COUNT = 'userCount',
}

export enum ClientEvent {
	JOIN = 'join',
	PIXEL_FROM_PLAYER = 'pixelFromPlayer',
}
