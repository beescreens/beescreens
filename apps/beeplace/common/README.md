# `@beescreens/beeplace` common code

## Development

These instructions are related to the development of the common code.

### Prerequisites

The following prerequisites must be filled to run this project:

- [Node.js](../../../../docs/explanations/about-nodejs/index.md) must be installed.
- [pnpm](../../../../docs/explanations/about-pnpm/index.md) must be installed.

### Set up the project

```sh
# Install the dependencies
pnpm install
```

### Build the code

Nothing has to be done, the frontend and the backend apps of BeePlace will build the common code automatically.
