module.exports = {
	root: true,
	env: {
		es6: true,
		jest: true,
		node: true,
	},
	parser: '@typescript-eslint/parser',
	parserOptions: {
		project: 'tsconfig.json',
		sourceType: 'module',
	},
	ignorePatterns: ['!.eslintrc.js', '!.prettierrc.js'],
	extends: [
		'eslint:recommended',
		'plugin:@typescript-eslint/eslint-recommended',
		'plugin:@typescript-eslint/recommended',
		'prettier',
	],
	plugins: ['@typescript-eslint/eslint-plugin', 'prettier'],
	rules: {
		'@typescript-eslint/explicit-module-boundary-types': [
			'error',
			{
				allowArgumentsExplicitlyTypedAsAny: true,
			},
		],
		'@typescript-eslint/no-shadow': ['error'],
		'@typescript-eslint/no-unused-vars': ['error'],
		'prettier/prettier': ['error', {}, { usePrettierrc: true }],
	},
};
