export default {
	noExternal: [/^((?!(k6)).)*$/],
	external: ['k6'],
};
