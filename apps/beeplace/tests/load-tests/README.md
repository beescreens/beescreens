# `@beescreens/beeplace-load-tests`

## Development

These instructions are related to the development of the load tests.

### Prerequisites

The following prerequisites must be filled to run this project:

- [Node.js](../../../../docs/explanations/about-nodejs/index.md) must be installed.
- [pnpm](../../../../docs/explanations/about-pnpm/index.md) must be installed.
- [k6](../../../../docs/explanations/about-k6/index.md) must be installed.

### Set up the project

```sh
# Install the dependencies
pnpm install

# Copy the .env.local.defaults and .env.prod.defaults files
pnpm setup:env
```

### Install the k6 dashboard plugin (optional)

It's already done if you are in the dev container.

The dashboard is currently not used due to an open issue: https://github.com/szkiba/xk6-dashboard/issues/35

```bash
go install go.k6.io/xk6/cmd/xk6@latest

xk6 build --with github.com/szkiba/xk6-dashboard@latest
```

### Build the code

```sh
pnpm build
```

### Add a new test

To create a new test, you have to create a new file in the `tests` folder. You can copy an existing test to have an idea of the needed content. Then, you just have to add the new entry to the `LOAD_TESTS` Map in the `main.ts` file.

```ts
const LOAD_TESTS = new Map<string, string>([
	['breakpoint', 'breakpoint-test.js'],
	['spike', 'spike-test.js'],
	// Add your tests here ! [name, filename]
]);
```

## Run the tests

You don't have to build the code before running the tests, it will be done automatically.

### Available tests

- **breakpoint**: it will add more and more users until it fails (`ws_connecting` latency threshold is reached). The goal is to find the maximum number of users that can be connected to the server and see if the code optimizations are working.
- **frontend**: small test that just do some HTTP requests to the frontend homepage.
- **spike**: it will add a lot of users at the same time and see if the server can handle it (no more than 10 errors). This test doesn't check the latency.

### Available flags

The flags need to be use with the `pnpm start:local` or `pnpm start:prod` commands (see below).

**Required**

- `--test [test name]`: the name of the test to run. The available tests are listed above.

**Optional**

- `--nbTests [number]`: the number of times the test will be run. Default: 1
- `--export [folderName]`: if set, the summary of the tests will be exported in the specified folder as JSON. The folder will be created in the `results` folder at the root of the `load-tests` folder. The entire `results` folder is gitignored.

### In Local

In local, the `.env.local` file is used to set the environment variables. Feel free to change the values.

```sh
pnpm start:local --test breakpoint
```

### In Production

In production, the `.env.prod` file is used to set the environment variables. Feel free to change the values.

```sh
pnpm start:prod --test breakpoint
```

## Analyse the results

If you used the `--export` flag to store results in JSON files, you can do a quick analyse the results with the `analyse.ts` script.

```sh
pnpm analyse --path [path to the results folder]
```

### Additional resources

- [k6](https://k6.io/) - A developer-centric open-source load testing tool.
- [Node.js](https://nodejs.org/) - A JavaScript runtime built on Chrome's V8 JavaScript engine.
- [pnpm](https://pnpm.js.org/) - Fast, disk space efficient package manager.
- [TypeScript](https://www.typescriptlang.org/) - An open-source language which builds on JavaScript by adding static type definitions.
