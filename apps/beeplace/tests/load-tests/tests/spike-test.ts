import { K6SocketIo } from '@beescreens/benchmarks';
import { Counter } from 'k6/metrics';
import { Options } from 'k6/options';
import { getSocketUrl } from '../utils/socket';
import { drawMaxRandomPixels } from '../utils/scenarios';
import { createSummaries } from '../utils/summary';

const CounterPixels = new Counter('drawnPixels');
const CounterErrors = new Counter('Errors');

export const options: Options = {
	stages: [
		{ duration: '1m', target: 2000 }, // fast ramp-up to a high point
		// No plateau
		{ duration: '30s', target: 0 }, // quick ramp-down to 0 users
	],
	thresholds: {
		Errors: [{ threshold: 'count<10', abortOnFail: true }],
	},
};

export default function () {
	const socket = new K6SocketIo(getSocketUrl());
	socket.setOnConnect(() => drawMaxRandomPixels(socket, CounterPixels));
	socket.connect();

	socket.on('error', (error) => {
		console.log(error);
		CounterErrors.add(1);
	});
}

export function handleSummary(data: any) {
	return createSummaries(data);
}
