import { K6SocketIo } from '@beescreens/benchmarks';
import { Counter } from 'k6/metrics';
import { Options } from 'k6/options';
import { drawMaxRandomPixels } from '../utils/scenarios';
import { getSocketUrl } from '../utils/socket';
import { createSummaries } from '../utils/summary';

const CounterPixels = new Counter('drawnPixels');
const CounterErrors = new Counter('Errors');

export const options: Options = {
	scenarios: {
		// Stress_test: {
		// 	executor: 'ramping-vus',
		// 	stages: [
		// 		{ duration: '10m', target: 200 }, // traffic ramp-up from 1 to a higher 200 users over 10 minutes.
		// 		{ duration: '30m', target: 200 }, // stay at higher 200 users for 10 minutes
		// 		{ duration: '5m', target: 0 }, // ramp-down to 0 users
		// 	],
		// },
		Breakpoint_test: {
			executor: 'ramping-arrival-rate',
			preAllocatedVUs: 10000,
			gracefulStop: '0s',
			stages: [
				{ duration: '5m', target: 1000 }, // just slowly ramp-up to a HUGE load
			],
		},
	},
	thresholds: {
		// checks: ["rate>0.95"],
		Errors: [{ threshold: 'count<10', abortOnFail: true }],
		ws_connecting: [{ threshold: 'p(95)<1200', abortOnFail: true }],
	},
};

export default function () {
	const socket = new K6SocketIo(getSocketUrl());
	socket.setOnConnect(() => drawMaxRandomPixels(socket, CounterPixels, 3, 30));
	socket.connect();

	socket.on('error', (_) => {
		CounterErrors.add(1);
	});
}

export function handleSummary(data: any) {
	return createSummaries(data);
}
