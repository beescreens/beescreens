import http from 'k6/http';
import { Options } from 'k6/options';
import { createSummaries } from '../utils/summary';

export const options: Options = {
	stages: [
		{ duration: '1m', target: 1000 }, // fast ramp-up to a high point
		// No plateau
		{ duration: '30s', target: 0 }, // quick ramp-down to 0 users
	],
};

export default function () {
	const protocol = __ENV.PROD === 'true' ? 'https' : 'http';
	http.get(`${protocol}://${__ENV.FRONTEND}`);
}

export function handleSummary(data: any) {
	return createSummaries(data);
}
