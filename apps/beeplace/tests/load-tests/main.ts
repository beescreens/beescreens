import { execSync } from 'node:child_process';
import fs from 'node:fs';
import minimist from 'minimist';
import dotenv from 'dotenv';

const DIST_PATH = 'dist/tests/';

// Available tests
const LOAD_TESTS = new Map<string, string>([
	['breakpoint', 'breakpoint-test.js'],
	['frontend', 'frontend-test.js'],
	['spike', 'spike-test.js'],
]);

const args = minimist(process.argv.slice(2));

// Load environment variables
if (!args.env || !['local', 'prod'].includes(args.env)) {
	throw new Error(
		'No correct env specified, use the --env flag with local or prod',
	);
}
const { parsed: env } = dotenv.config({ path: `.env.${args.env}` });
if (!env) {
	throw new Error(`No .env.${args.env} file found`);
}

// Flag "--test" not used
if (!args.test) {
	throw new Error('No test specified, use the --test flag');
}

// Test not found
if (!LOAD_TESTS.has(args.test)) {
	throw new Error(
		`Test "${args.test}" not found. Available tests: ${Array.from(
			LOAD_TESTS.keys(),
		).join(', ')}`,
	);
}

// Create export folder if it doesn't exist
if (args.export) {
	const path = `results/${args.export}`;
	if (!fs.existsSync(path)) {
		fs.mkdirSync(path, { recursive: true });
	}
}

// Flags to pass to k6
const k6Flags = Object.entries(env)
	.map(([key, value]) => `-e ${key}=${value}`)
	.join(' ');

const nbTests = args.nbTests ?? 1;

// Run the load test nbTests times
for (let i = 0; i < nbTests; i++) {
	console.log(`Running test ${i + 1}/${nbTests}`);

	const exportFlag = args.export
		? `-e EXPORT=${args.export}/${i + 1}.json`
		: '';

	try {
		execSync(
			`k6 run ${DIST_PATH}${LOAD_TESTS.get(
				args.test,
			)} ${k6Flags} ${exportFlag}`,
			{
				stdio: 'inherit',
			},
		);
	} catch (ignored) {
		/* errors from the load test are ignored, it's okay if they fail (ex: breakpoint test)*/
	}
}
