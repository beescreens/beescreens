import { readdir, readFile } from 'fs/promises';
import minimist from 'minimist';
import { Result } from './utils/summary';

const args = minimist(process.argv.slice(2));

if (!args.path) {
	throw new Error('No path specified, use the --path flag');
}

function mean(arr: number[]) {
	return arr.reduce((acc, val) => acc + val, 0) / arr.length;
}

function median(arr: number[]) {
	const sorted = arr.sort((a, b) => a - b);
	const mid = Math.floor(sorted.length / 2);
	return sorted.length % 2 !== 0
		? sorted[mid]
		: (sorted[mid - 1] + sorted[mid]) / 2;
}

function printResults(results: Result[]) {
	const durations = results.map((result) => result.duration);
	const vusers = results.map((result) => result.vusers);
	const pixels = results.map((result) => result.pixels);

	console.log(`
    Mean duration: ${mean(durations)} ms
    Median duration: ${median(durations)} ms

    Mean vusers: ${mean(vusers)}
    Median vusers: ${median(vusers)}

    Mean pixels: ${mean(pixels)}
    Median pixels: ${median(pixels)}
  `);
}

async function main() {
	try {
		const files = await readdir(args.path);
		console.log(`${files.length} test results found:`);

		const promises = files
			.filter((file) => file.endsWith('.json'))
			.map((file) => readFile(`${args.path}/${file}`, 'utf-8'));

		const data = await Promise.all(promises);
		const results: Result[] = data.map((d) => JSON.parse(d));
		printResults(results);
	} catch (e) {
		throw new Error(`Invalid path "${args.path}"`);
	}
}

main();
