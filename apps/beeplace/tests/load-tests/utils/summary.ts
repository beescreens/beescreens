// @ts-expect-error TS doesn't know about k6 remote modules
import { textSummary } from 'https://jslib.k6.io/k6-summary/0.0.3/index.js';

export interface Result {
	duration: number;
	vusers: number;
	pixels: number;
}

export function createSummaries(data: any) {
	const summaries: any = {
		stdout: textSummary(data, { indent: ' ', enableColors: true }),
	};

	// Export the results to a JSON file
	if (__ENV.EXPORT) {
		const result: Result = {
			duration: data.state.testRunDurationMs,
			vusers: data.metrics.vus.values.value,
			pixels: data.metrics.drawnPixels.values.count,
		};
		summaries[`results/${__ENV.EXPORT}`] = JSON.stringify(result);
	}

	return summaries;
}
