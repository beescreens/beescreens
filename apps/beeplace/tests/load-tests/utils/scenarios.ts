import { ClientEvent, ServerEvent } from '@beescreens/beeplace';
import { K6SocketIo } from '@beescreens/benchmarks';
import { joinRoom } from './socket';
import { sleep } from 'k6';
import { random, randomPixel } from './pixel';
import { Counter } from 'k6/metrics';
import http from 'k6/http';

export async function drawMaxRandomPixels(
	socket: K6SocketIo,
	CounterPixels: Counter,
	sleepBetweenPixels = 3,
	sleepAfter = 10,
	serverEventsHandler = () => {},
) {
	socket.setEventMessageHandle(ServerEvent.UPDATE_BOARD, serverEventsHandler);
	socket.setEventMessageHandle('pixelToPlayers', serverEventsHandler);
	socket.setEventMessageHandle('updateBoard', serverEventsHandler);
	socket.setEventMessageHandle(ServerEvent.USER_COUNT, serverEventsHandler);

	const { initialState } = await joinRoom(socket);
	const { canvasSize, colors, maxPixels } = initialState;

	for (let i = 0; i < maxPixels; i++) {
		sleep(random(sleepBetweenPixels));
		CounterPixels.add(1);
		socket.send(
			ClientEvent.PIXEL_FROM_PLAYER,
			randomPixel(canvasSize, colors.length),
			null,
		);
	}

	const protocol = __ENV.PROD === 'true' ? 'https' : 'http';
	http.get(`${protocol}://${__ENV.FRONTEND}`);
	sleep(sleepAfter);

	socket.close();
}
