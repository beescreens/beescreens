// @ts-expect-error TS doesn't know about k6 remote modules
import { uuidv4 } from 'https://jslib.k6.io/k6-utils/1.4.0/index.js';
import { ClientEvent, JoinResponse } from '@beescreens/beeplace';
import { K6SocketIo } from '@beescreens/benchmarks';

export function getSocketUrl(): string {
	const protocol = __ENV.PROD === 'true' ? 'wss' : 'ws';
	return `${protocol}://${__ENV.BACKEND}/socket.io/?EIO=4&transport=websocket`;
}

export async function joinRoom(socket: K6SocketIo): Promise<JoinResponse> {
	const { data } = await socket.sendWithAck<JoinResponse>(ClientEvent.JOIN, {
		fingerprint: uuidv4(),
	});

	return data;
}
