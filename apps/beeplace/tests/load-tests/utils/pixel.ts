export function random(max: number) {
	return Math.floor(Math.random() * max);
}

export function randomPixel(canvasSize: number, maxColor: number) {
	return {
		x: random(canvasSize),
		y: random(canvasSize),
		color: random(maxColor),
	};
}
