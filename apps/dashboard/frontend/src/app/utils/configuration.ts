import {
	filterDevicesConfigurationsFromConfiguration,
	filterSuccessDevicesConfigurationsFromConfiguration,
} from "@/app/utils/devices-configurations";
import { Configuration } from "@/types/configuration";
import { DeviceConfiguration } from "@/types/device-configuration";

// Check if a configuration is ready (all devices applied the configuration in a success state)
export const isConfigurationReady = (
	configuration: Configuration,
	devicesConfigurations: DeviceConfiguration[]
) => {
	return (
		filterSuccessDevicesConfigurationsFromConfiguration(
			configuration.id,
			devicesConfigurations
		).length ===
		filterDevicesConfigurationsFromConfiguration(
			configuration.id,
			devicesConfigurations
		).length
	);
};

// Get all configurations that are in a success state
export const filterReadyConfigurations = (
	configurations: Configuration[],
	devicesConfigurations: DeviceConfiguration[]
) => {
	return configurations.filter((configuration: Configuration) => {
		isConfigurationReady(configuration, devicesConfigurations);
	});
};
