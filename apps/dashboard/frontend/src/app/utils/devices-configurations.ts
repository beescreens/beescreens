import {
	DeviceConfiguration,
	DeviceConfigurationState,
} from "@/types/device-configuration";

// Get all devices-configurations applied to a specific device
export const filterDevicesConfigurationsFromDevice = (
	deviceId: string,
	devicesConfigurations: DeviceConfiguration[]
) => {
	return devicesConfigurations.filter(
		(deviceConfiguration: DeviceConfiguration) => {
			return deviceConfiguration.deviceId === deviceId;
		}
	);
};

// Get all devices-configurations applying a specific configuration
export const filterDevicesConfigurationsFromConfiguration = (
	configurationId: string,
	devicesConfigurations: DeviceConfiguration[]
) => {
	return devicesConfigurations.filter(
		(deviceConfiguration: DeviceConfiguration) => {
			return deviceConfiguration.configurationId === configurationId;
		}
	);
};

// Get all devices-configurations applied to a specific device and are in a success state
export const filterSuccessDevicesConfigurationsFromDevice = (
	deviceId: string,
	devicesConfigurations: DeviceConfiguration[]
) => {
	return devicesConfigurations.filter(
		(deviceConfiguration: DeviceConfiguration) => {
			return (
				deviceConfiguration.state == DeviceConfigurationState.SUCCESS &&
				deviceConfiguration.deviceId === deviceId
			);
		}
	);
};

// Get all devices-configurations with a specific configuration applied and are in a success state
export const filterSuccessDevicesConfigurationsFromConfiguration = (
	configurationId: string,
	devicesConfigurations: DeviceConfiguration[]
) => {
	return filterDevicesConfigurationsFromConfiguration(
		configurationId,
		devicesConfigurations
	).filter((deviceConfiguration: DeviceConfiguration) => {
		return (
			deviceConfiguration.state === DeviceConfigurationState.SUCCESS &&
			deviceConfiguration.configurationId === configurationId
		);
	});
};
