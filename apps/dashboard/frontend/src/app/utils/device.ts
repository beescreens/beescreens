import {
	filterDevicesConfigurationsFromDevice,
	filterSuccessDevicesConfigurationsFromDevice,
} from "@/app/utils/devices-configurations";
import { Device } from "@/types/device";
import { DeviceConfiguration } from "@/types/device-configuration";

export const isDeviceOnline = (device: Device) => {
	const thresholdMinutes = 5;
	const now: Date = new Date();
	const lastSeen = new Date(device.lastSeenAt);
	const diffMinutes = Math.round((now.getTime() - lastSeen.getTime()) / 60000);
	return diffMinutes < thresholdMinutes;
};

// Get all devices that are online (last seen less than 5 minutes ago)
export const filterDevices = (devices: Device[], isOnline: boolean) => {
	return devices.filter((device: Device) => {
		return isDeviceOnline(device) === isOnline;
	});
};

// Check if a device is ready (online and all configurations in a success state)
export const isDeviceReady = (
	device: Device,
	devicesConfigurations: DeviceConfiguration[]
) => {
	return (
		filterSuccessDevicesConfigurationsFromDevice(
			device.id,
			devicesConfigurations
		).length ===
		filterDevicesConfigurationsFromDevice(device.id, devicesConfigurations)
			.length
	);
};

// Get all devices (online or offline) with all configurations in a success state
export const filterReadyDevices = (
	devices: Device[],
	devicesConfigurations: DeviceConfiguration[]
) => {
	return devices.filter((device: Device) => {
		return isDeviceReady(device, devicesConfigurations);
	});
};
