"use client";

import { BaseForm } from "@/app/components/configurations/BaseForm";
import { NotificationContext } from "@/app/context/notificationContext";
import { CreateConfiguration } from "@/types/configuration";
import { useRouter } from "next/navigation";
import React, { useContext, useState } from "react";

interface CreateFormProps {
	createConfiguration: (configuration: CreateConfiguration) => void;
}

export const CreateForm = ({ createConfiguration }: CreateFormProps) => {
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [repositoryUrl, setRepositoryUrl] = useState("");
	const [repositoryPath, setRepositoryPath] = useState("");
	const [repositoryBranch, setRepositoryBranch] = useState("");

	const notificationCtx = useContext(NotificationContext);
	const router = useRouter();

	async function handleCreate() {
		const configuration: CreateConfiguration = {
			name: name,
			description: description,
			repositoryUrl: repositoryUrl,
			repositoryPath: repositoryPath,
			repositoryBranch: repositoryBranch,
		};

		try {
			await createConfiguration(configuration);
		} catch (error: any) {
			notificationCtx.error(error.message);
			return;
		}

		notificationCtx.success("Configuration created");
		router.replace("/configurations");
		router.refresh();
	}

	return (
		<BaseForm
			name={name}
			setName={setName}
			description={description}
			setDescription={setDescription}
			repositoryUrl={repositoryUrl}
			setRepositoryUrl={setRepositoryUrl}
			repositoryPath={repositoryPath}
			setRepositoryPath={setRepositoryPath}
			repositoryBranch={repositoryBranch}
			setRepositoryBranch={setRepositoryBranch}
			action={handleCreate}
			isCreate={true}
		/>
	);
};
