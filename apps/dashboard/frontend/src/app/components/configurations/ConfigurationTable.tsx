"use client";

import DynamicLink from "@/app/components/DynamicLink";
import { GreenBadge } from "@/app/components/badges/GreenBadge";
import { YellowBadge } from "@/app/components/badges/YellowBadge";
import { NotificationContext } from "@/app/context/notificationContext";
import { isConfigurationReady } from "@/app/utils/configuration";
import {
	filterDevicesConfigurationsFromConfiguration,
	filterSuccessDevicesConfigurationsFromConfiguration,
} from "@/app/utils/devices-configurations";
import { Configuration } from "@/types/configuration";
import { DeviceConfiguration } from "@/types/device-configuration";
import {
	ServerStackIcon,
	TrashIcon,
	WrenchScrewdriverIcon,
} from "@heroicons/react/24/outline";
import React, { useContext, useState } from "react";

interface ConfigurationsTableProps {
	allConfigurations: Configuration[];
	allDevicesConfigurations: DeviceConfiguration[];
	deleteConfiguration: (uuid: string) => void;
}

export const ConfigurationTable = ({
	allConfigurations,
	deleteConfiguration,
	allDevicesConfigurations,
}: ConfigurationsTableProps) => {
	const notificationCtx = useContext(NotificationContext);
	const [configurations, setConfigurations] = useState(allConfigurations);

	async function handleDelete(uuid: string) {
		try {
			await deleteConfiguration(uuid);
		} catch (error: any) {
			notificationCtx.error(error.message);
			return;
		}
		notificationCtx.success("Configuration deleted");
		setConfigurations(
			configurations.filter((configuration) => configuration.id !== uuid)
		);
	}

	return (
		<>
			<div className="mx-auto w-full p-2">
				<div className="relative flex flex-col max-w-7xl min-w-0 break-words bg-white w-full drop-shadow-md rounded">
					<div className="rounded-t mb-0 px-4 py-3 border-0">
						<div className="flex flex-wrap items-center">
							<div className="relative w-full px-4 max-w-full flex-grow flex-1">
								<h3 className="font-semibold text-base text-blueGray-700">
									Configurations
								</h3>
							</div>
						</div>
					</div>
					<div className="block w-full overflow-x-auto">
						<table className="items-center w-full bg-transparent border-collapse">
							<thead>
								<tr>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										Name
									</th>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										Description
									</th>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										Completion
									</th>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										Status
									</th>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										Actions
									</th>
								</tr>
							</thead>
							<tbody>
								{configurations.map((configuration: Configuration) => (
									<tr key={configuration.id}>
										<th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
											{configuration.name}
										</th>
										<th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
											{configuration.description}
										</th>
										<td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
											{
												filterSuccessDevicesConfigurationsFromConfiguration(
													configuration.id,
													allDevicesConfigurations
												).length
											}
											<span className="text-gray-500"> / </span>
											{
												filterDevicesConfigurationsFromConfiguration(
													configuration.id,
													allDevicesConfigurations
												).length
											}
										</td>
										<td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
											{
												isConfigurationReady(
													configuration,
													allDevicesConfigurations
												) ? (
													<GreenBadge text="Done" />
												) : (
													<YellowBadge text="Pending" />
												)
											}
										</td>
										<td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap">
											<div className="flex flex-row h-5 justify-between text-indigo-600 px-2">
												<DynamicLink
													className="text-sm font-bold leading-6 text-indigo-600 w-5"
													href={`/configurations/${configuration.id}`}
												>
													<WrenchScrewdriverIcon className="hover:cursor-pointer hover:text-indigo-950"></WrenchScrewdriverIcon>
												</DynamicLink>
												<a
													href="#"
													className="text-sm font-bold leading-6 text-indigo-600 cursor-pointer w-5"
													onClick={() => handleDelete(configuration.id)}
												>
													<TrashIcon className="hover:cursor-pointer hover:text-indigo-950"></TrashIcon>
												</a>
												<DynamicLink
													className="text-sm font-bold leading-6 text-indigo-600 w-5"
													href={`/configurations/${configuration.id}/devices`}
												>
													<ServerStackIcon className="hover:cursor-pointer hover:text-indigo-950"></ServerStackIcon>
												</DynamicLink>
											</div>
										</td>
									</tr>
								))}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</>
	);
};
