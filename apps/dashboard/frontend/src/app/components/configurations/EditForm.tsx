"use client";

import { BaseForm } from "@/app/components/configurations/BaseForm";
import { NotificationContext } from "@/app/context/notificationContext";
import { Configuration, EditConfiguration } from "@/types/configuration";
import { useRouter } from "next/navigation";
import { useContext, useState } from "react";

interface EditFormProps {
	configuration: Configuration;
	editConfiguration: (
		uuid: string,
		configuration: EditConfiguration
	) => void;
}

export const EditForm = ({
	configuration,
	editConfiguration,
}: EditFormProps) => {
	const [name, setName] = useState(configuration.name ?? "");
	const [description, setDescription] = useState(
		configuration.description ?? ""
	);
	const [repositoryUrl, setRepositoryUrl] = useState(
		configuration.repositoryUrl ?? ""
	);
	const [repositoryBranch, setRepositoryBranch] = useState(
		configuration.repositoryBranch ?? ""
	);
	const [repositoryPath, setRepositoryPath] = useState(
		configuration.repositoryPath ?? ""
	);

	const notificationCtx = useContext(NotificationContext);
	const router = useRouter();

	async function handleEdit() {
		const editedConfiguration: EditConfiguration = {
			name: name,
			description: description,
			repositoryUrl: repositoryUrl,
			repositoryBranch: repositoryBranch,
			repositoryPath: repositoryPath,
		};

		try {
			editConfiguration(configuration.id, editedConfiguration);
		} catch (error: any) {
			notificationCtx.error(error.message);
			return;
		}

		notificationCtx.success("Configuration edited");
		router.replace("/configurations");
		router.refresh();
	}

	return (
		<BaseForm
			name={name}
			setName={setName}
			description={description}
			setDescription={setDescription}
			repositoryUrl={repositoryUrl}
			setRepositoryUrl={setRepositoryUrl}
			repositoryBranch={repositoryBranch}
			setRepositoryBranch={setRepositoryBranch}
			repositoryPath={repositoryPath}
			setRepositoryPath={setRepositoryPath}
			action={handleEdit}
			isCreate={false}
		></BaseForm>
	);
};
