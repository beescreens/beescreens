"use client";

import {
	deleteDeviceConfiguration,
	patchDeviceConfiguration,
} from "@/app/api/devices-configurations/devices-configurations";
import ConfirmDelete from "@/app/components/ConfirmDelete";
import LogsDisplayer from "@/app/components/LogsDisplayer";
import { NotificationContext } from "@/app/context/notificationContext";
import {
	DeviceConfiguration,
	DeviceConfigurationState,
} from "@/types/device-configuration";
import { DisplayBy } from "@/types/display-by";
import {
	ArrowPathRoundedSquareIcon,
	TrashIcon,
} from "@heroicons/react/24/outline";
import React, { useContext, useState } from "react";

interface DeviceConfigurationsTableProps {
	devicesConfigurations: DeviceConfiguration[];
	displayBy: DisplayBy;
}

export const DeviceConfigurationsTable = ({
	devicesConfigurations,
	displayBy,
}: DeviceConfigurationsTableProps) => {
	const notificationCtx = useContext(NotificationContext);
	const [deviceConfigurations, setDeviceConfigurations] = useState(
		devicesConfigurations
	);
	const [displayLogs, setDisplayLogs] = useState(false);
	const [confirmDelete, setConfirmDelete] = useState(false);
	const [logs, setLogs] = useState("");

	const [selectedDeviceConfiguration, setSelectedDeviceConfiguration] =
		useState<DeviceConfiguration>();

	async function handleDelete(deviceConfiguration: DeviceConfiguration) {
		try {
			await deleteDeviceConfiguration(deviceConfiguration);
		} catch (error: any) {
			notificationCtx.error(error.message);
			return;
		}
		notificationCtx.success("Configuration cancelled");
		setDeviceConfigurations(
			deviceConfigurations.filter(
				(dc) =>
					dc.deviceId !== deviceConfiguration.deviceId ||
					dc.configurationId !== deviceConfiguration.configurationId
			)
		);
	}

	async function handleApplyAgain(deviceConfiguration: DeviceConfiguration) {
		try {
			await patchDeviceConfiguration(deviceConfiguration);
		} catch (error: any) {
			notificationCtx.error(error.message);
			return;
		}
		notificationCtx.success("Configuration applied");
		setDeviceConfigurations(
			deviceConfigurations.map((dc) => {
				if (
					dc.deviceId === deviceConfiguration.deviceId &&
					dc.configurationId === deviceConfiguration.configurationId
				) {
					dc.state = DeviceConfigurationState.NEW;
				}
				return dc;
			})
		);
	}

	return (
		<>
			{displayLogs && (
				<LogsDisplayer
					logs={logs}
					displayLogs={displayLogs}
					setDisplayLogs={setDisplayLogs}
				/>
			)}
			<ConfirmDelete
				confirmDelete={confirmDelete}
				setConfirmDelete={setConfirmDelete}
				deviceConfiguration={selectedDeviceConfiguration}
				handleDelete={handleDelete}
			/>

			<div className="mx-auto w-full p-2">
				<div className="relative flex flex-col max-w-7xl min-w-0 break-words bg-white w-full drop-shadow-md rounded">
					<div className="rounded-t mb-0 px-4 py-3 border-0">
						<div className="flex flex-wrap items-center">
							<div className="relative w-full px-4 max-w-full flex-grow flex-1">
								<h3 className="font-semibold text-base text-blueGray-700">
									{displayBy === DisplayBy.DEVICE
										? "Devices"
										: "Configurations"}
								</h3>
							</div>
						</div>
					</div>
					<div className="block w-full overflow-x-auto">
						<table className="items-center w-full bg-transparent border-collapse">
							<thead>
								<tr>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										Name
									</th>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										State
									</th>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										Logs
									</th>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										Actions
									</th>
								</tr>
							</thead>
							<tbody>
								{deviceConfigurations.map(
									(deviceConfiguration: DeviceConfiguration) => (
										<tr
											key={
												deviceConfiguration.deviceId +
												deviceConfiguration.configurationId
											}
										>
											<th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
												{displayBy === DisplayBy.DEVICE
													? deviceConfiguration.device.name
													: deviceConfiguration.configuration.name}
											</th>
											<th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
												{deviceConfiguration.state}
											</th>
											<th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
												<a
													href="#"
													className="text-sm font-bold leading-6 text-indigo-600 cursor-pointer"
													onClick={() => {
														setDisplayLogs(true);
														setLogs(deviceConfiguration.log);
													}}
												>
													View
												</a>
											</th>
											<td className="border-t-0 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap">
												<div className="flex flex-row h-5 text-indigo-600 px-5">
													<a
														href="#"
														className="text-sm font-bold leading-6 text-indigo-600 cursor-pointer w-5 mx-2"
														onClick={() => {
															setConfirmDelete(true);
															setSelectedDeviceConfiguration(
																deviceConfiguration
															);
														}}
													>
														<TrashIcon className="hover:cursor-pointer hover:text-indigo-950"></TrashIcon>
													</a>
													<a
														href="#"
														className="text-sm font-bold leading-6 text-indigo-600 cursor-pointer w-5 mx-2"
														onClick={() => {
															handleApplyAgain(deviceConfiguration);
														}}
													>
														<ArrowPathRoundedSquareIcon className="hover:cursor-pointer hover:text-indigo-950"></ArrowPathRoundedSquareIcon>
													</a>
												</div>
											</td>
										</tr>
									)
								)}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</>
	);
};
