"use client";

import DynamicLink from "@/app/components/DynamicLink";
import { GreenBadge } from "@/app/components/badges/GreenBadge";
import { RedBadge } from "@/app/components/badges/RedBadge";
import { YellowBadge } from "@/app/components/badges/YellowBadge";
import { NotificationContext } from "@/app/context/notificationContext";
import { isDeviceOnline, isDeviceReady } from "@/app/utils/device";
import {
	filterDevicesConfigurationsFromDevice,
	filterSuccessDevicesConfigurationsFromDevice,
} from "@/app/utils/devices-configurations";
import { Device } from "@/types/device";
import { DeviceConfiguration } from "@/types/device-configuration";
import {
	CubeIcon,
	TrashIcon,
	WrenchScrewdriverIcon,
} from "@heroicons/react/24/outline";
import React, { useContext, useState } from "react";

interface DeviceTableProps {
	allDevices: Device[];
	allDevicesConfigurations: DeviceConfiguration[];
	deleteDevice: (uuid: string) => void;
}

export const DeviceTable = ({
	allDevices,
	deleteDevice,
	allDevicesConfigurations,
}: DeviceTableProps) => {
	const notificationCtx = useContext(NotificationContext);
	const [devices, setDevices] = useState(allDevices);

	async function handleDelete(uuid: string) {
		try {
			await deleteDevice(uuid);
		} catch (error: any) {
			notificationCtx.error(error.message);
			return;
		}
		notificationCtx.success("Device deleted");
		setDevices(devices.filter((device) => device.id !== uuid));
	}

	return (
		<div className="mx-auto w-full p-2">
			<div className="relative flex flex-col max-w-7xl min-w-0 break-words bg-white w-full drop-shadow-md rounded">
				<div className="rounded-t mb-0 px-4 py-3 border-0">
					<div className="flex flex-wrap items-center">
						<div className="relative w-full px-4 max-w-full flex-grow flex-1">
							<h3 className="font-semibold text-base text-blueGray-700">
								Devices
							</h3>
						</div>
					</div>
				</div>
				<div className="block w-full overflow-x-auto">
					<table className="items-center w-full bg-transparent border-collapse">
						<thead>
							<tr>
								<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
									Name
								</th>
								<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
									Description
								</th>
								<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
									Last seen
								</th>
								<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
									Completion
								</th>
								<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
									Status
								</th>
								<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
									Actions
								</th>
							</tr>
						</thead>
						<tbody>
							{devices
								.sort((a, b) => {
									return (
										new Date(b.lastSeenAt).getTime() -
										new Date(a.lastSeenAt).getTime()
									);
								})
								.map((device: Device) => (
									<tr key={device.id}>
										<th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
											{device.name}
										</th>
										<th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
											{device.description}
										</th>
										<td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
											{device.lastSeenAt == null
												? "Never"
												: new Date(device.lastSeenAt).toLocaleString("fr-CH")}
										</td>
										<td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
											{
												filterSuccessDevicesConfigurationsFromDevice(
													device.id,
													allDevicesConfigurations
												).length
											}
											<span className="text-gray-500"> / </span>
											{
												filterDevicesConfigurationsFromDevice(
													device.id,
													allDevicesConfigurations
												).length
											}
										</td>
										<td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
											{isDeviceOnline(device) ? (
												isDeviceReady(device, allDevicesConfigurations) ? (
													<GreenBadge text="Ready" />
												) : (
													<YellowBadge text="Pending" />
												)
											) : (
												<RedBadge text="Offline" />
											)}
										</td>
										<td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap">
											<div className="flex flex-row h-5 justify-between text-indigo-600 px-2">
												<DynamicLink
													className="text-sm font-bold leading-6 text-indigo-600 w-5"
													href={`/devices/${device.id}`}
												>
													<WrenchScrewdriverIcon className="hover:cursor-pointer hover:text-indigo-950"></WrenchScrewdriverIcon>
												</DynamicLink>
												<a
													href="#"
													className="text-sm font-bold leading-6 text-indigo-600 cursor-pointer w-5"
													onClick={() => handleDelete(device.id)}
												>
													<TrashIcon className="hover:cursor-pointer hover:text-indigo-950"></TrashIcon>
												</a>
												<DynamicLink
													className="text-sm font-bold leading-6 text-indigo-600 w-5"
													href={`/devices/${device.id}/configurations`}
												>
													<CubeIcon className="hover:cursor-pointer hover:text-indigo-950"></CubeIcon>
												</DynamicLink>
											</div>
										</td>
									</tr>
								))}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	);
};
