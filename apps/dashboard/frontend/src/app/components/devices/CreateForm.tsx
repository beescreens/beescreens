"use client";

import { NotificationContext } from "@/app/context/notificationContext";
import { BaseForm } from "@/app/components/devices/BaseForm";
import { useRouter } from "next/navigation";
import React, { useContext, useState } from "react";
import { CreateDevice } from "@/types/device";

interface CreateFormProps {
	createDevice: (device: CreateDevice) => void;
}

export const CreateForm = ({ createDevice }: CreateFormProps) => {
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [apiKey, setApiKey] = useState("");

	const notificationCtx = useContext(NotificationContext);
	const router = useRouter();

	async function handleCreate() {
		const device: CreateDevice = {
			name: name,
			description: description,
			apiKey: apiKey,
		};

		try {
			await createDevice(device);
		} catch (error: any) {
			notificationCtx.error(error.message);
			return;
		}

		notificationCtx.success("Device created");
		router.replace("/devices");
		router.refresh();
	}

	return (
		<BaseForm
			name={name}
			setName={setName}
			description={description}
			setDescription={setDescription}
			apiKey={apiKey}
			setApiKey={setApiKey}
			action={handleCreate}
			isCreate={true}
		/>
	);
};
