"use client";

import { BaseForm } from "@/app/components/devices/BaseForm";
import { NotificationContext } from "@/app/context/notificationContext";
import { Device, EditDevice } from "@/types/device";
import { useRouter } from "next/navigation";
import { useContext, useState } from "react";

interface EditFormProps {
	device: Device;
	editDevice: (uuid: string, device: EditDevice) => void;
}

export const EditForm = ({ device, editDevice }: EditFormProps) => {
	const [name, setName] = useState(device.name ?? "");
	const [description, setDescription] = useState(device.description ?? "");
	const [apiKey, setApiKey] = useState("");

	const notificationCtx = useContext(NotificationContext);
	const router = useRouter();

	async function handleEdit() {
		const editedDevice: EditDevice = {
			name: name,
			description: description,
		};

		try {
			editDevice(device.id, editedDevice);
		} catch (error: any) {
			notificationCtx.error(error.message);
			return;
		}

		notificationCtx.success("Device edited");
		router.replace("/devices");
		router.refresh();
	}

	return (
		<BaseForm
			name={name}
			setName={setName}
			description={description}
			setDescription={setDescription}
			apiKey={apiKey}
			setApiKey={setApiKey}
			action={handleEdit}
			isCreate={false}
		></BaseForm>
	);
};
