"use client";

import Link from "next/link";

interface BaseFormProps {
	name: string;
	setName: (name: string) => void;
	description: string;
	setDescription: (description: string) => void;
	apiKey: string;
	setApiKey: (apiKey: string) => void;
	action: () => void;
	isCreate: boolean;
}

export const BaseForm = ({
	name,
	setName,
	description,
	setDescription,
	apiKey,
	setApiKey,
	action,
	isCreate,
}: BaseFormProps) => {
	return (
		// @ts-ignore
		<form action={action}>
			<div className="space-y-12">
				<div className="border-b border-gray-900/10 pb-12">
					<h1 className="text-3xl font-bold tracking-tight text-gray-900 py-6">
						{isCreate ? "Create a new device" : "Edit device"}
					</h1>
					<div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
						<div className="sm:col-span-3">
							<label
								htmlFor="name"
								className="block text-sm font-medium leading-6 text-gray-900"
							>
								Name
							</label>
							<div className="mt-2">
								<input
									type="text"
									name="name"
									id="name"
									autoComplete="given-name"
									className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
									value={name}
									onChange={(e) => setName(e.target.value)}
								/>
							</div>
						</div>

						<div className="sm:col-span-3">
							<label
								htmlFor="description"
								className="block text-sm font-medium leading-6 text-gray-900"
							>
								Description
							</label>
							<div className="mt-2">
								<input
									type="text"
									name="description"
									id="description"
									autoComplete="family-name"
									className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
									value={description}
									onChange={(e) => setDescription(e.target.value)}
								/>
							</div>
						</div>

						{isCreate ? (
							<div className="sm:col-span-4">
								<label
									htmlFor="api-key"
									className="block text-sm font-medium leading-6 text-gray-900"
								>
									API Key
								</label>
								<div className="mt-2">
									<input
										id="api-key"
										name="api-key"
										type="text"
										autoComplete="off"
										className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
										value={apiKey}
										onChange={(e) => setApiKey(e.target.value)}
									/>
								</div>
							</div>
						) : null}
					</div>
				</div>
			</div>

			<div className="mt-6 flex items-center justify-end gap-x-6">
				<Link
					href="/devices"
					className="text-sm font-semibold leading-6 text-gray-900"
				>
					Cancel
				</Link>
				<button
					type="submit"
					className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
				>
					Save
				</button>
			</div>
		</form>
	);
};
