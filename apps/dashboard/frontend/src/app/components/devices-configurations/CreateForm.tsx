"use client";

import Loading from "@/app/(details)/devices/loading";
import { NotificationContext } from "@/app/context/notificationContext";
import { Configuration } from "@/types/configuration";
import { Device } from "@/types/device";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { Suspense, useContext, useState } from "react";

interface CreateFormProps {
	device: Device;
	configurations: Configuration[];
	applyDeviceConfiguration: (
		deviceId: string,
		configurationIds: string
	) => void;
}

export const CreateForm = ({
	applyDeviceConfiguration,
	device,
	configurations,
}: CreateFormProps) => {
	const notificationCtx = useContext(NotificationContext);
	const router = useRouter();

	const [checked, setChecked] = useState([] as string[]);

	const handleCheck = (event: React.ChangeEvent<HTMLInputElement>) => {
		var updatedList: string[] = [...checked];
		if (event.target.checked) {
			updatedList = [...checked, event.target.value];
		} else {
			updatedList.splice(checked.indexOf(event.target.value), 1);
		}
		setChecked(updatedList);
	};

	async function handleCreate() {
		checked.forEach(async (configurationId) => {
			try {
				await applyDeviceConfiguration(device.id, configurationId);
			} catch (error: any) {
				notificationCtx.error(error.message);
				return;
			}
		});

		notificationCtx.success("Configuration(s) applied");
		router.replace(`/devices/${device.id}/configurations`);
		router.refresh();
	}

	return (
		// @ts-ignore
		<form action={handleCreate}>
			<div className="flex flex-col">
				<div className="flex">
					<div className="w-full mx-auto max-w-7xl py-6">
						<h1 className="text-3xl font-bold tracking-tight text-gray-900">
							Apply configurations to {device.name}
						</h1>
					</div>
				</div>

				<div className="mx-auto w-full p-2">
					<div className="relative flex flex-col max-w-7xl min-w-0 break-words bg-white w-full drop-shadow-md rounded">
						<div className="rounded-t mb-0 px-4 py-3 border-0">
							<div className="flex flex-wrap items-center">
								<div className="relative w-full max-w-full flex-grow flex-1">
									<h3 className="font-semibold text-base text-blueGray-700">
										Configurations
									</h3>
								</div>
							</div>
							<Suspense fallback={<Loading />}>
								<ul role="list" className="divide-y divide-gray-100">
									{configurations.map((configuration) => (
										<li
											key={configuration.id}
											className="flex justify-between gap-x-6 py-5"
										>
											<div className="flex gap-x-4">
												<div className="min-w-0 flex-auto">
													<p className="text-base font-semibold leading-6 text-gray-900">
														{configuration.name}
													</p>
													<p className="mt-1 truncate text-sm leading-5 text-gray-500">
														{configuration.description}
													</p>
												</div>
											</div>
											<div className="hidden sm:flex sm:flex-col sm:items-end pr-2">
												<input
													id={configuration.id}
													type="checkbox"
													value={configuration.id}
													onChange={handleCheck}
													className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 focus:ring-2 dark:border-gray-600 hover:cursor-pointer"
												/>
											</div>
										</li>
									))}
								</ul>
							</Suspense>
						</div>
					</div>
				</div>
				<div className="flex">
					<div className="flex w-full justify-start mx-auto max-w-7xl py-6">
						<Link
							href={`/devices/${device.id}/configurations`}
							className="inline-flex items-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm hover:bg-gray-50 ring-gray-300 ring-1 ring-inset focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
						>
							Go back
						</Link>
					</div>
					<div className="flex w-full justify-end mx-auto max-w-7xl py-6">
						<button
							type="submit"
							className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
						>
							Apply
						</button>
					</div>
				</div>
			</div>
		</form>
	);
};
