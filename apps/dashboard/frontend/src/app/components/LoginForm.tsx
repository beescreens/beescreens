"use client";

import { ExclamationCircleIcon } from "@heroicons/react/24/outline";
import Image from "next/image";
import { useRouter } from "next/navigation";
import { useState } from "react";

interface LoginFormProps {
	login: (username: string, password: string) => Promise<boolean>;
}

export const LoginForm = ({ login }: LoginFormProps) => {
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [isError, setIsError] = useState(false);
	const router = useRouter();

	async function handleLogin() {
		const loggedIn = await login(username, password);
		if (!loggedIn) {
			setIsError(true);
			return;
		}

		setIsError(false);
		router.replace("/");
		router.refresh();
	}

	return (
		// @ts-ignore
		<form action={handleLogin} className="space-y-6">
			<div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
				<div className="sm:mx-auto sm:w-full sm:max-w-sm">
					<Image
						className="mx-auto h-10 w-auto"
						src="/logo.svg"
						alt="Logo"
						width={32}
						height={32}
					/>
					<h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
						Sign in to your account
					</h2>
				</div>

				<div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
					<div>
						<label
							htmlFor="username"
							className="block text-sm font-medium leading-6 text-gray-900"
						>
							Username
						</label>
						<div className="relative mt-2 rounded-md shadow-sm">
							<input
								id="username"
								name="username"
								type="text"
								autoComplete="username"
								required
								className={
									(!isError
										? `text-gray-900 ring-gray-300 placeholder:text-gray-400 focus:ring-indigo-600`
										: `text-red-900 placeholder:text-red-300 ring-red-300 focus:ring-red-500`) &&
									`block w-full rounded-md border-0 py-1.5  shadow-sm ring-1 ring-inset focus:ring-2 focus:ring-inset sm:text-sm sm:leading-6`
								}
								onChange={(e) => setUsername(e.target.value)}
							/>
							{isError && (
								<div className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-3">
									<ExclamationCircleIcon
										className="h-5 w-5 text-red-500"
										aria-hidden="true"
									/>
								</div>
							)}
						</div>
						{isError && (
							<p className="my-2 text-sm text-red-600" id="username-error">
								Verify the username is correct.
							</p>
						)}
					</div>

					<div className="mt-4">
						<div className="flex items-center justify-between">
							<label
								htmlFor="password"
								className="block text-sm font-medium leading-6 text-gray-900"
							>
								Password
							</label>
						</div>
						<div className="relative mt-2 rounded-md shadow-sm">
							<input
								id="password"
								name="password"
								type="password"
								autoComplete="current-password"
								required
								className={
									(!isError
										? `text-gray-900 ring-gray-300 placeholder:text-gray-400 focus:ring-indigo-600`
										: `text-red-900 placeholder:text-red-300 ring-red-300 focus:ring-red-500`) &&
									`block w-full rounded-md border-0 py-1.5  shadow-sm ring-1 ring-inset focus:ring-2 focus:ring-inset sm:text-sm sm:leading-6`
								}
								onChange={(e) => setPassword(e.target.value)}
							/>
							{isError && (
								<div className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-3">
									<ExclamationCircleIcon
										className="h-5 w-5 text-red-500"
										aria-hidden="true"
									/>
								</div>
							)}
						</div>
						{isError && (
							<p className="my-2 text-sm text-red-600" id="username-error">
								Verify the password is correct.
							</p>
						)}
					</div>

					<div className="mt-4">
						<button
							type="submit"
							className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
						>
							Sign in
						</button>
					</div>
				</div>
			</div>
		</form>
	);
};
