"use client";

import { useRouter } from "next/navigation";
import { forwardRef } from "react";

// Inspired from : https://github.com/vercel/next.js/issues/42991#issuecomment-1367466954

const DynamicLink = forwardRef<
	HTMLAnchorElement,
	Omit<React.HTMLProps<HTMLAnchorElement>, "ref">
>(({ href, children, ...props }, ref) => {
	const router = useRouter();

	return (
		<a
			{...props}
			ref={ref}
			href={href}
			onClick={(e) => {
				e.preventDefault();
				router.replace(href as string);
				router.refresh();
			}}
		>
			{children}
		</a>
	);
});

DynamicLink.displayName = "DynamicLink";

export default DynamicLink;
