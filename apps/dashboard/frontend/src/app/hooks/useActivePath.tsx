import { usePathname } from 'next/navigation'

// Code inspired from : https://nikolasbarwicki.com/articles/highlight-currently-active-link-in-nextjs-13-with-app-router

export function useActivePath(): (path: string) => boolean {
  const pathname = usePathname()

  const checkActivePath = (path: string) => {
    if (path === '/' && pathname !== path) {
      return false
    }
    return pathname.startsWith(path)
  }

  return checkActivePath
}
