"use client";

import { UnauthorizedException } from "@/types/exceptions";
import { redirect } from "next/navigation";

export default function ErrorHandler({ error }: { error: Error }) {
	// TODO: Improve this error handling with an instanceOf
	// Does not work at the moment
	if (error.message === UnauthorizedException.name) {
		redirect("/auth/login");
	}
}
