import { login } from "@/app/api/auth/auth";
import { LoginForm } from "@/app/components/LoginForm";

export default async function Page() {
	return <LoginForm login={login} />;
}
