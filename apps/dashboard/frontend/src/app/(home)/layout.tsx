"use client";

import "@/styles/globals.css";
import { NotificationProvider } from "@/app/context/notificationContext";
import Navbar from "@/app/components/Navbar";

export default function HomeLayout({
	children,
}: {
	children: React.ReactNode;
}) {
	return (
		<div className="min-h-full bg-gray-100">
			<NotificationProvider>
				<Navbar isHomePath={true} />
				<main>
					<div className="mx-auto text-black">{children}</div>
				</main>
			</NotificationProvider>
		</div>
	);
}
