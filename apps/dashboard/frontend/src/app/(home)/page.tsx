import { getConfigurations } from "@/app/api/configurations/configurations";
import { getAllDevicesConfigurations } from "@/app/api/devices-configurations/devices-configurations";
import { getDevices } from "@/app/api/devices/devices";
import DynamicLink from "@/app/components/DynamicLink";
import { GreenBadge } from "@/app/components/badges/GreenBadge";
import { YellowBadge } from "@/app/components/badges/YellowBadge";
import { isConfigurationReady } from "@/app/utils/configuration";
import {
	filterDevices,
	filterReadyDevices,
	isDeviceReady,
} from "@/app/utils/device";
import {
	filterDevicesConfigurationsFromConfiguration,
	filterDevicesConfigurationsFromDevice,
	filterSuccessDevicesConfigurationsFromConfiguration,
	filterSuccessDevicesConfigurationsFromDevice,
} from "@/app/utils/devices-configurations";
import { Configuration } from "@/types/configuration";
import { Device } from "@/types/device";
import { DeviceConfiguration } from "@/types/device-configuration";
import {
	CubeIcon,
	EyeIcon,
	EyeSlashIcon,
	ServerStackIcon,
} from "@heroicons/react/24/outline";

export default async function Page() {
	const devices: Device[] = await getDevices();
	const configurations: Configuration[] = await getConfigurations();
	const devicesConfigurations: DeviceConfiguration[] =
		await getAllDevicesConfigurations();

	const numberOfReadyOnlineDevices = filterReadyDevices(
		devices,
		devicesConfigurations
	).length;

	return (
		<div className="flex flex-col ">
			<div className="bg-gray-800 md:pt-16 pb-32 pt-12">
				<div className="px-4 md:px-10 mx-auto w-full max-w-7xl py-6 sm:px-6 lg:px-8">
					<h1 className="text-5xl font-bold tracking-tight text-white pb-12">
						Dashboard
					</h1>
					<div className="flex flex-wrap justify-between">
						<div className="w-full lg:w-6/12 xl:w-1/5">
							<div className="relative flex flex-col min-w-0 break-words bg-white rounded mb-6 xl:mb-0 shadow-lg mx-2 xl:mx-0">
								<div className="flex-auto p-4">
									<div className="flex flex-wrap">
										<div className="relative w-full pr-4 max-w-full flex-grow flex-1">
											<h5 className="text-blueGray-400 uppercase font-bold text-xs">
												Total Devices
											</h5>
											<span className="font-semibold text-3xl text-blueGray-700">
												{devices.length}
											</span>
										</div>
										<div className="relative w-auto pl-4 flex-initial">
											<div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 shadow-lg rounded-full bg-purple-500">
												<ServerStackIcon></ServerStackIcon>
											</div>
										</div>
									</div>
									<p className="text-sm text-blueGray-400 mt-4">
										<span className="whitespace-nowrap">
											Registered inside the database
										</span>
									</p>
								</div>
							</div>
						</div>

						<div className="w-full lg:w-6/12 xl:w-1/5">
							<div className="relative flex flex-col min-w-0 break-words bg-white rounded mb-6 xl:mb-0 shadow-lg mx-2 xl:mx-0">
								<div className="flex-auto p-4">
									<div className="flex flex-wrap">
										<div className="relative w-full pr-4 max-w-full flex-grow flex-1">
											<h5 className="text-blueGray-400 uppercase font-bold text-xs">
												Online devices
											</h5>
											<span className="font-semibold text-3xl text-blueGray-700 mr-4">
												{filterDevices(devices, true).length}
											</span>
										</div>
										<div className="relative w-auto pl-4 flex-initial">
											<div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 shadow-lg rounded-full bg-green-500">
												<EyeIcon></EyeIcon>
											</div>
										</div>
									</div>
									<p className="text-sm text-blueGray-400 mt-4">
										<span className="text-emerald-500 mr-2">
											{ 
												numberOfReadyOnlineDevices === 0 ? 0 : 
												Math.round((numberOfReadyOnlineDevices / devices.length) * 100)
											}
											%
										</span>
										<span className="whitespace-nowrap">are configured </span>
										<span className="text-emerald-500 mr-2">
											{numberOfReadyOnlineDevices} / {devices.length}
										</span>
									</p>
								</div>
							</div>
						</div>

						<div className="w-full lg:w-6/12 xl:w-1/5">
							<div className="relative flex flex-col min-w-0 break-words bg-white rounded mb-6 xl:mb-0 shadow-lg mx-2 xl:mx-0">
								<div className="flex-auto p-4">
									<div className="flex flex-wrap">
										<div className="relative w-full pr-4 max-w-full flex-grow flex-1">
											<h5 className="text-blueGray-400 uppercase font-bold text-xs">
												Offline devices
											</h5>
											<span className="font-semibold text-3xl text-blueGray-700">
												{filterDevices(devices, false).length}
											</span>
										</div>
										<div className="relative w-auto pl-4 flex-initial">
											<div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 shadow-lg rounded-full bg-red-500">
												<EyeSlashIcon></EyeSlashIcon>
											</div>
										</div>
									</div>
									<p className="text-sm text-blueGray-400 mt-4">
										<span className="whitespace-nowrap">
											Not seen in the last 5 minutes
										</span>
									</p>
								</div>
							</div>
						</div>

						<div className="w-full lg:w-6/12 xl:w-1/5">
							<div className="relative flex flex-col min-w-0 break-words bg-white rounded mb-6 xl:mb-0 shadow-lg mx-2 xl:mx-0">
								<div className="flex-auto p-4">
									<div className="flex flex-wrap">
										<div className="relative w-full pr-4 max-w-full flex-grow flex-1">
											<h5 className="text-blueGray-400 uppercase font-bold text-xs">
												Configurations
											</h5>
											<span className="font-semibold text-3xl text-blueGray-700">
												{configurations.length}
											</span>
										</div>
										<div className="relative w-auto pl-4 flex-initial">
											<div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 shadow-lg rounded-full bg-blue-500">
												<CubeIcon></CubeIcon>
											</div>
										</div>
									</div>
									<p className="text-sm text-blueGray-400 mt-4">
										<span className="whitespace-nowrap">
											Registered inside the database
										</span>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div className="px-4 md:px-10 mx-auto w-full max-w-7xl py-6 sm:px-6 lg:px-8 -mt-24">
				<div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
					<div className="rounded-t mb-0 px-4 py-3 border-0">
						<div className="flex flex-wrap items-center">
							<div className="relative w-full px-4 max-w-full flex-grow flex-1">
								<h3 className="font-semibold text-base text-blueGray-700">
									Last seen devices
								</h3>
							</div>
							<div className="relative w-full px-4 max-w-full flex-grow flex-1 text-right">
								<DynamicLink
									className="bg-indigo-500 text-white active:bg-indigo-600 text-xs font-bold uppercase px-3 py-1 rounded outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-indigo-600"
									type="button"
									href="/devices"
								>
									See all
								</DynamicLink>
							</div>
						</div>
					</div>
					<div className="block w-full overflow-x-auto">
						<table className="items-center w-full bg-transparent border-collapse">
							<thead>
								<tr>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										Name
									</th>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										Last seen
									</th>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										Completion
									</th>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										Status
									</th>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										Actions
									</th>
								</tr>
							</thead>
							<tbody>
								{devices
									.sort((a, b) => {
										return (
											new Date(b.lastSeenAt).getTime() -
											new Date(a.lastSeenAt).getTime()
										);
									})
									.map((device: Device) => (
										<tr key={device.id}>
											<th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
												{device.name}
											</th>
											<td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
												{device.lastSeenAt == null ? (
													"Never"
												) : (
													new Date(device.lastSeenAt).toLocaleString("fr-CH")
												)}
											</td>
											<td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
												{
													filterSuccessDevicesConfigurationsFromDevice(
														device.id,
														devicesConfigurations
													).length
												}
												<span className="text-gray-500"> / </span>
												{
													filterDevicesConfigurationsFromDevice(
														device.id,
														devicesConfigurations
													).length
												}
											</td>
											<td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
												{isDeviceReady(device, devicesConfigurations) ? (
													<GreenBadge text="Done" />
												) : (
													<YellowBadge text="Pending" />
												)}
											</td>
											<td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap">
												<div className="flex flex-row h-5 justify-between text-indigo-600 px-2">
													<DynamicLink
														className="text-sm font-bold leading-6 text-indigo-600 w-5"
														href={`/devices/${device.id}/configurations`}
													>
														<CubeIcon className="hover:cursor-pointer hover:text-indigo-950"></CubeIcon>
													</DynamicLink>
												</div>
											</td>
										</tr>
									))}
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div className="px-4 md:px-10 mx-auto w-full max-w-7xl py-6 sm:px-6 lg:px-8">
				<div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
					<div className="rounded-t mb-0 px-4 py-3 border-0">
						<div className="flex flex-wrap items-center">
							<div className="relative w-full px-4 max-w-full flex-grow flex-1">
								<h3 className="font-semibold text-base text-blueGray-700">
									Available configurations
								</h3>
							</div>
							<div className="relative w-full px-4 max-w-full flex-grow flex-1 text-right">
								<DynamicLink
									className="bg-indigo-500 text-white active:bg-indigo-600 text-xs font-bold uppercase px-3 py-1 rounded outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 hover:bg-indigo-600"
									type="button"
									href="/configurations"
								>
									See all
								</DynamicLink>
							</div>
						</div>
					</div>
					<div className="block w-full overflow-x-auto">
						<table className="items-center w-full bg-transparent border-collapse">
							<thead>
								<tr>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										Name
									</th>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										Completion
									</th>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										Status
									</th>
									<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
										Actions
									</th>
								</tr>
							</thead>
							<tbody>
								{configurations.map((configuration: Configuration) => (
									<tr key={configuration.id}>
										<th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
											{configuration.name}
										</th>
										<td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
											{
												filterSuccessDevicesConfigurationsFromConfiguration(
													configuration.id,
													devicesConfigurations
												).length
											}
											<span className="text-gray-500"> / </span>
											{
												filterDevicesConfigurationsFromConfiguration(
													configuration.id,
													devicesConfigurations
												).length
											}
										</td>
										<td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
											{isConfigurationReady(
												configuration,
												devicesConfigurations
											) ? (
												<GreenBadge text="Done" />
											) : (
												<YellowBadge text="Pending" />
											)}
										</td>
										<td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap">
											<div className="flex flex-row h-5 justify-between text-indigo-600 px-2">
												<DynamicLink
													className="text-sm font-bold leading-6 text-indigo-600 w-5"
													href={`/configurations/${configuration.id}/devices`}
												>
													<ServerStackIcon className="hover:cursor-pointer hover:text-indigo-950"></ServerStackIcon>
												</DynamicLink>
											</div>
										</td>
									</tr>
								))}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	);
}
