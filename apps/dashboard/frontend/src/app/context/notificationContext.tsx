import React, { useState } from "react";

// Code inspired from https://jujuontheweb.medium.com/react-usecontext-hook-to-make-an-alert-notification-system-for-your-entire-application-721b4c6b7d0f

interface NotificationProviderProps {
	children: React.ReactNode;
}

interface NotificationContextValue {
	success: (text: string) => void;
	error: (text: string) => void;
	clear: () => void;
	notification: string | null;
	notificationText: string | null;
}

export const NOTIFICATION_STATUS = {
	SUCCESS: "success",
	ERROR: "error",
};

const NOTIFICATION_DURATION = 6000;

export const NotificationContext =
	React.createContext<NotificationContextValue>({
		success: () => {},
		error: () => {},
		clear: () => {},
		notification: null,
		notificationText: null,
	});

export const NotificationProvider = (props: NotificationProviderProps) => {
	const [notification, setNotification] = useState<string | null>(null);
	const [notificationText, setNotificationText] = useState<string | null>(null);

	const success = (text: string) => {
		window.scrollTo(0, 0);
		setNotificationText(text);
		setNotification(NOTIFICATION_STATUS.SUCCESS);

		setTimeout(() => {
			clear();
		}, NOTIFICATION_DURATION);
	};

	const error = (text: string) => {
		window.scrollTo(0, 0);
		setNotificationText(text);
		setNotification(NOTIFICATION_STATUS.ERROR);

		setTimeout(() => {
			clear();
		}, NOTIFICATION_DURATION);
	};

	const clear = () => {
		setNotificationText(null);
		setNotification(null);
	};

	return (
		<NotificationContext.Provider
			value={{
				success,
				error,
				clear,
				notification,
				notificationText,
			}}
		>
			{props.children}
		</NotificationContext.Provider>
	);
};
