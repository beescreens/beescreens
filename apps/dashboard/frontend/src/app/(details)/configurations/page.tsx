import {
	deleteConfiguration,
	getConfigurations,
} from "@/app/api/configurations/configurations";
import { ConfigurationTable } from "@/app/components/configurations/ConfigurationTable";
import Loading from "@/app/(details)/configurations/create/loading";
import Link from "next/link";
import { Suspense } from "react";
import { getAllDevicesConfigurations } from "@/app/api/devices-configurations/devices-configurations";

export default async function Page() {
	const configurations = await getConfigurations();
	const devicesConfigurations = await getAllDevicesConfigurations();

	return (
		<div className="flex flex-col">
			<div className="flex">
				<div className="w-full mx-auto max-w-7xl py-6">
					<h1 className="text-3xl font-bold tracking-tight text-gray-900">
						List of all configurations
					</h1>
				</div>
				<div className="flex w-full justify-end mx-auto max-w-7xl py-6">
					<Link
						href="/configurations/create"
						className="inline-flex items-center rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
					>
						Add new configuration
					</Link>
				</div>
			</div>
			<div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
				<div className="inline-block min-w-full py-2 sm:px-6 lg:px-8">
					<div className="overflow-hidden">
						<Suspense fallback={<Loading />}>
							<ConfigurationTable
								allConfigurations={configurations}
								allDevicesConfigurations={devicesConfigurations}
								deleteConfiguration={deleteConfiguration}
							/>
						</Suspense>
					</div>
				</div>
			</div>
		</div>
	);
}
