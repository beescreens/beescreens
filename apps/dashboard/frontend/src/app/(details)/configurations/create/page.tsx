import { createConfiguration } from "@/app/api/configurations/configurations";
import { CreateForm } from "@/app/components/configurations/CreateForm";

export default function Page() {
	return <CreateForm createConfiguration={createConfiguration} />;
}
