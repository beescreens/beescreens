import Loading from "@/app/(details)/devices/loading";
import { getConfiguration } from "@/app/api/configurations/configurations";
import { getDevicesFromConfiguration } from "@/app/api/devices-configurations/devices-configurations";
import { DevicesConfigurationsTable } from "@/app/components/devices-configurations/DevicesConfigurationsTable";
import { DeviceConfiguration } from "@/types/device-configuration";
import { DisplayBy } from "@/types/display-by";
import Link from "next/link";
import { Suspense } from "react";

export default async function Page({ params }: { params: { id: string } }) {
	const devicesConfigurations: DeviceConfiguration[] =
		await getDevicesFromConfiguration(params.id as string);

	const configuration = await getConfiguration(params.id as string);

	return (
		<div className="flex flex-col">
			<div className="flex">
				<div className="w-full mx-auto max-w-7xl py-6">
					<h1 className="text-3xl font-bold tracking-tight text-gray-900">
						Devices applying configuration {configuration.name}
					</h1>
				</div>
			</div>
			<div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
				<div className="inline-block min-w-full py-2 sm:px-6 lg:px-8">
					<div className="overflow-hidden">
						<Suspense fallback={<Loading />}>
							<DevicesConfigurationsTable
								devicesConfigurations={devicesConfigurations}
								displayBy={DisplayBy.DEVICE}
							/>
						</Suspense>
					</div>
				</div>
			</div>
			<div className="flex w-full justify-start mx-auto max-w-7xl py-6">
				<Link
					href="/configurations"
					className="inline-flex items-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm hover:bg-gray-50 ring-gray-300 ring-1 ring-inset focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
				>
					Go back
				</Link>
			</div>
		</div>
	);
}
