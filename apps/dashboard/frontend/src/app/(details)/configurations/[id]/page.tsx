import {
	editConfiguration,
	getConfiguration,
} from "@/app/api/configurations/configurations";
import { EditForm } from "@/app/components/configurations/EditForm";
import { Configuration } from "@/types/configuration";

export default async function Page({ params }: { params: { id: string } }) {
	const configuration: Configuration = await getConfiguration(params.id);

	return (
		<EditForm
			configuration={configuration}
			editConfiguration={editConfiguration}
		/>
	);
}
