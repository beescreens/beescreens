"use client";

import "@/styles/globals.css";
import { NotificationProvider } from "@/app/context/notificationContext";
import Navbar from "@/app/components/Navbar";

export default function DetailsLayout({
	children,
}: {
	children: React.ReactNode;
}) {
	return (
		<div className="min-h-full bg-gray-100">
			<NotificationProvider>
				<Navbar isHomePath={false} />
				<main>
					<div className="mx-auto max-w-7xl py-6 sm:px-6 lg:px-8 text-black">
						{children}
					</div>
				</main>
			</NotificationProvider>
		</div>
	);
}
