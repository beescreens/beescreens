export default function Loading() {
	const numberOfRows = 3;
	return (
		<>
			<div className="flex flex-col">
				<div className="flex">
					<div className="w-full mx-auto max-w-7xl py-6">
						<h1 className="text-3xl font-bold tracking-tight text-gray-900">
							<div className="w-16 h-4 bg-gray-300 rounded"></div>
						</h1>
					</div>
					<div className="flex w-full justify-end mx-auto max-w-7xl py-6">
						<div className="w-16 h-4 bg-gray-300 rounded"></div>
					</div>
				</div>
				<div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
					<div className="inline-block min-w-full py-2 sm:px-6 lg:px-8">
						<div className="overflow-hidden">
							<div className="mx-auto w-full p-2">
								<div className="relative flex flex-col max-w-7xl min-w-0 break-words bg-white w-full drop-shadow-md rounded">
									<div className="rounded-t mb-0 px-4 py-3 border-0">
										<div className="flex flex-wrap items-center">
											<div className="relative w-full px-4 max-w-full flex-grow flex-1">
												<h3 className="font-semibold text-base text-blueGray-700">
													<div className="w-16 h-4 bg-gray-300 rounded"></div>
												</h3>
											</div>
										</div>
									</div>
									<div className="block w-full overflow-x-auto">
										<table className="items-center w-full bg-transparent border-collapse">
											<thead>
												<tr>
													<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
														<div className="w-16 h-4 bg-gray-300 rounded"></div>
													</th>
													<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
														<div className="w-16 h-4 bg-gray-300 rounded"></div>
													</th>
													<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
														<div className="w-16 h-4 bg-gray-300 rounded"></div>
													</th>
													<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
														<div className="w-16 h-4 bg-gray-300 rounded"></div>
													</th>
													<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
														<div className="w-16 h-4 bg-gray-300 rounded"></div>
													</th>
													<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
														<div className="w-16 h-4 bg-gray-300 rounded"></div>
													</th>
													<th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
														<div className="w-16 h-4 bg-gray-300 rounded"></div>
													</th>
												</tr>
											</thead>
											<tbody>
												{Array.from(Array(numberOfRows).keys()).map((i) => (
													<tr key={i}>
														<th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
															<div className="w-16 h-4 bg-gray-300 rounded"></div>
														</th>
														<th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
															<div className="w-16 h-4 bg-gray-300 rounded"></div>
														</th>
														<th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
															<div className="w-16 h-4 bg-gray-300 rounded"></div>
														</th>
														<td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
															<div className="w-16 h-4 bg-gray-300 rounded"></div>
														</td>
														<td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
															<div className="w-16 h-4 bg-gray-300 rounded"></div>
														</td>
														<td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
															<div className="w-16 h-4 bg-gray-300 rounded"></div>
														</td>
														<td className="border-t-0 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap">
															<div className="w-16 h-4 bg-gray-300 rounded"></div>
														</td>
													</tr>
												))}
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
}
