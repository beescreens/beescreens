import Loading from "@/app/(details)/devices/loading";
import { getAllDevicesConfigurations } from "@/app/api/devices-configurations/devices-configurations";
import { deleteDevice, getDevices } from "@/app/api/devices/devices";
import { DeviceTable } from "@/app/components/devices/DeviceTable";
import { Device } from "@/types/device";
import { DeviceConfiguration } from "@/types/device-configuration";
import Link from "next/link";
import { Suspense } from "react";

export default async function Page() {
	const devices: Device[] = await getDevices();
	const devicesConfigurations: DeviceConfiguration[] =
		await getAllDevicesConfigurations();

	return (
		<div className="flex flex-col">
			<div className="flex">
				<div className="w-full mx-auto max-w-7xl py-6">
					<h1 className="text-3xl font-bold tracking-tight text-gray-900">
						List of all devices
					</h1>
				</div>
				<div className="flex w-full justify-end mx-auto max-w-7xl py-6">
					<Link
						href="/devices/create"
						className="inline-flex items-center rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
					>
						Add new device
					</Link>
				</div>
			</div>
			<div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
				<div className="inline-block min-w-full py-2 sm:px-6 lg:px-8">
					<div className="overflow-hidden">
						<Suspense fallback={<Loading />}>
							<DeviceTable
								allDevices={devices}
								allDevicesConfigurations={devicesConfigurations}
								deleteDevice={deleteDevice}
							/>
						</Suspense>
					</div>
				</div>
			</div>
		</div>
	);
}
