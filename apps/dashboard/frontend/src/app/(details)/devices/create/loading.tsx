export default function Loading() {
	return (
		<div className="space-y-12">
			<div className="border-b border-gray-900/10 pb-12">
				<h1 className="text-3xl font-bold tracking-tight text-gray-900 py-6">
					<div className="w-24 h-4 bg-gray-300 rounded"></div>
				</h1>

				<div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
					<div className="sm:col-span-3">
						<div className="w-24 h-4 bg-gray-300 rounded"></div>
					</div>

					<div className="sm:col-span-3">
						<div className="w-24 h-4 bg-gray-300 rounded"></div>
					</div>
				</div>
			</div>
		</div>
	);
}
