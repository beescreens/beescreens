import { createDevice } from "@/app/api/devices/devices";
import { CreateForm } from "@/app/components/devices/CreateForm";

export default function Page() {
	return <CreateForm createDevice={createDevice} />;
}
