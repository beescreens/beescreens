import { getConfigurations } from "@/app/api/configurations/configurations";
import { applyDeviceConfiguration } from "@/app/api/devices-configurations/devices-configurations";
import { getDevice } from "@/app/api/devices/devices";
import { CreateForm } from "@/app/components/devices-configurations/CreateForm";
import { Configuration } from "@/types/configuration";
import { Device } from "@/types/device";

export default async function Page({ params }: { params: { id: string } }) {
	const device: Device = await getDevice(params.id as string);
	const configurations: Configuration[] = await getConfigurations();

	return (
		<CreateForm
			device={device}
			configurations={configurations}
			applyDeviceConfiguration={applyDeviceConfiguration}
		/>
	);
}
