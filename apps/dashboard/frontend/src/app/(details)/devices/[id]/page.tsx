import { editDevice, getDevice } from "@/app/api/devices/devices";
import { EditForm } from "@/app/components/devices/EditForm";
import { Device } from "@/types/device";

export default async function Page({ params }: { params: { id: string } }) {
	const device: Device = await getDevice(params.id);

	return <EditForm device={device} editDevice={editDevice} />;
}
