"use server";

import { CreateDevice, EditDevice } from "@/types/device";
import { cookies } from "next/headers";
import { HTTP_UNAUTHORIZED } from "@/types/constants";
import "server-only";
import { UnauthorizedException } from "@/types/exceptions";

export async function createDevice(device: CreateDevice) {
	const res = await fetch(`${process.env.BACKEND_URL}/v1/devices`, {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${cookies().get("jwt")?.value}`,
		},
		body: JSON.stringify(device),
	});

	const json = await res.json();

	if (!res.ok) {
		if (res.status === HTTP_UNAUTHORIZED) {
			throw new UnauthorizedException();
		} else {
			throw new Error("Failed to create device");
		}
	}

	return json;
}

export async function getDevices() {
	const res = await fetch(`${process.env.BACKEND_URL}/v1/devices`, {
		cache: "no-store",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${cookies().get("jwt")?.value}`,
		},
	});

	if (!res.ok) {
		if (res.status === HTTP_UNAUTHORIZED) {
			throw new UnauthorizedException();
		} else {
			throw new Error("Failed to fetch devices");
		}
	}
	return res.json();
}

export async function getDevice(id: string) {
	const res = await fetch(`${process.env.BACKEND_URL}/v1/devices/${id}`, {
		cache: "no-store",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${cookies().get("jwt")?.value}`,
		},
	});

	if (!res.ok) {
		if (res.status === HTTP_UNAUTHORIZED) {
			throw new UnauthorizedException();
		} else {
			throw new Error("Failed to fetch device");
		}
	}

	return res.json();
}

export async function deleteDevice(id: string) {
	const res = await fetch(`${process.env.BACKEND_URL}/v1/devices/${id}`, {
		method: "DELETE",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${cookies().get("jwt")?.value}`,
		},
	});

	if (!res.ok) {
		if (res.status === HTTP_UNAUTHORIZED) {
			throw new UnauthorizedException();
		} else {
			throw new Error("Failed to delete device");
		}
	}

	return;
}

export async function editDevice(id: string, device: EditDevice) {
	const res = await fetch(`${process.env.BACKEND_URL}/v1/devices/${id}`, {
		method: "PATCH",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${cookies().get("jwt")?.value}`,
		},
		cache: "no-store",
		body: JSON.stringify(device),
	});

	if (!res.ok) {
		if (res.status === HTTP_UNAUTHORIZED) {
			throw new UnauthorizedException();
		} else {
			throw new Error("Failed to edit device");
		}
	}

	return;
}
