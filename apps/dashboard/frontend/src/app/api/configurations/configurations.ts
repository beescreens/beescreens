"use server";

import { CreateConfiguration, EditConfiguration } from "@/types/configuration";
import { HTTP_UNAUTHORIZED } from "@/types/constants";
import { UnauthorizedException } from "@/types/exceptions";
import { cookies } from "next/headers";
import "server-only";

export async function createConfiguration(configuration: CreateConfiguration) {
	const res = await fetch(`${process.env.BACKEND_URL}/v1/configurations`, {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${cookies().get("jwt")?.value}`,
		},
		body: JSON.stringify(configuration),
	});

	const json = await res.json();

	if (!res.ok) {
		if (res.status === HTTP_UNAUTHORIZED) {
			throw new UnauthorizedException();
		} else {
			throw new Error("Failed to create configuration");
		}
	}

	return json;
}

export async function getConfigurations() {
	const res = await fetch(`${process.env.BACKEND_URL}/v1/configurations`, {
		cache: "no-store",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${cookies().get("jwt")?.value}`,
		},
	});
	if (!res.ok) {
		if (res.status === HTTP_UNAUTHORIZED) {
			throw new UnauthorizedException();
		} else {
			throw new Error("Failed to fetch configurations");
		}
	}
	return res.json();
}

export async function getConfiguration(id: string) {
	const res = await fetch(
		`${process.env.BACKEND_URL}/v1/configurations/${id}`,
		{
			cache: "no-store",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${cookies().get("jwt")?.value}`,
			},
		}
	);

	if (!res.ok) {
		if (res.status === HTTP_UNAUTHORIZED) {
			throw new UnauthorizedException();
		} else {
			throw new Error("Failed to fetch configuration");
		}
	}

	return res.json();
}

export async function deleteConfiguration(id: string) {
	const res = await fetch(
		`${process.env.BACKEND_URL}/v1/configurations/${id}`,
		{
			method: "DELETE",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${cookies().get("jwt")?.value}`,
			},
		}
	);

	if (!res.ok) {
		if (res.status === HTTP_UNAUTHORIZED) {
			throw new UnauthorizedException();
		} else {
			throw new Error("Failed to delete configuration");
		}
	}

	return;
}

export async function editConfiguration(
	id: string,
	configuration: EditConfiguration
) {
	const res = await fetch(
		`${process.env.BACKEND_URL}/v1/configurations/${id}`,
		{
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${cookies().get("jwt")?.value}`,
			},
			cache: "no-store",
			body: JSON.stringify(configuration),
		}
	);

	if (!res.ok) {
		if (res.status === HTTP_UNAUTHORIZED) {
			throw new UnauthorizedException();
		} else {
			throw new Error("Failed to edit configuration");
		}
	}

	return;
}
