"use server";

import "server-only";

import {
	EditDeviceConfiguration,
	RemoveDeviceConfiguration,
} from "@/types/device-configuration";
import { HTTP_UNAUTHORIZED } from "@/types/constants";
import { cookies } from "next/headers";
import { UnauthorizedException } from "@/types/exceptions";

export async function getAllDevicesConfigurations() {
	const res = await fetch(
		`${process.env.BACKEND_URL}/v1/devices-configurations`,
		{
			cache: "no-store",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${cookies().get("jwt")?.value}`,
			},
		}
	);

	if (!res.ok) {
		if (res.status === HTTP_UNAUTHORIZED) {
			throw new UnauthorizedException();
		} else {
			throw new Error("Failed to fetch all devices configurations");
		}
	}
	return res.json();
}

export async function getConfigurationsFromDevice(deviceId: string) {
	const res = await fetch(
		`${process.env.BACKEND_URL}/v1/devices-configurations?` +
			new URLSearchParams({
				deviceId,
			}),
		{
			cache: "no-store",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${cookies().get("jwt")?.value}`,
			},
		}
	);

	if (!res.ok) {
		if (res.status === HTTP_UNAUTHORIZED) {
			throw new UnauthorizedException();
		} else {
			throw new Error(
				"Failed to fetch all configurations for a specific device"
			);
		}
	}
	return res.json();
}

export async function getDevicesFromConfiguration(configurationId: string) {
	const res = await fetch(
		`${process.env.BACKEND_URL}/v1/devices-configurations?` +
			new URLSearchParams({
				configurationId,
			}),
		{
			cache: "no-store",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${cookies().get("jwt")?.value}`,
			},
		}
	);

	if (!res.ok) {
		if (res.status === HTTP_UNAUTHORIZED) {
			throw new UnauthorizedException();
		} else {
			throw new Error(
				"Failed to fetch all devices for a specific configuration"
			);
		}
	}
	return res.json();
}

export async function deleteDeviceConfiguration(
	deviceConfiguration: RemoveDeviceConfiguration
) {
	const res = await fetch(
		`${process.env.BACKEND_URL}/v1/devices-configurations`,
		{
			method: "DELETE",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${cookies().get("jwt")?.value}`,
			},
			body: JSON.stringify({
				deviceId: deviceConfiguration.deviceId,
				configurationId: deviceConfiguration.configurationId,
			}),
		}
	);

	if (!res.ok) {
		if (res.status === HTTP_UNAUTHORIZED) {
			throw new UnauthorizedException();
		} else {
			throw new Error("Failed to delete device configuration");
		}
	}

	return;
}

export async function applyDeviceConfiguration(
	deviceId: string,
	configurationId: string
) {
	const res = await fetch(
		`${process.env.BACKEND_URL}/v1/devices-configurations`,
		{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${cookies().get("jwt")?.value}`,
			},
			cache: "no-store",
			body: JSON.stringify({
				deviceId,
				configurationId,
			}),
		}
	);

	if (!res.ok) {
		if (res.status === HTTP_UNAUTHORIZED) {
			throw new UnauthorizedException();
		} else {
			throw new Error("Failed to apply device configuration");
		}
	}

	return;
}

export async function patchDeviceConfiguration(
	deviceConfiguration: EditDeviceConfiguration
) {
	const res = await fetch(
		`${process.env.BACKEND_URL}/v1/devices-configurations`,
		{
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${cookies().get("jwt")?.value}`,
			},
			body: JSON.stringify({
				...deviceConfiguration,
			}),
			cache: "no-store",
		}
	);

	if (!res.ok) {
		if (res.status === HTTP_UNAUTHORIZED) {
			throw new UnauthorizedException();
		} else {
			throw new Error("Failed to patch device configuration");
		}
	}

	return;
}
