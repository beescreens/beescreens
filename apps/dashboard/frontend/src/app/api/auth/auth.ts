"use server";

import { cookies } from "next/headers";
import "server-only";

export async function login(
	username: string,
	password: string
): Promise<boolean> {
	const res = await fetch(`${process.env.BACKEND_URL}/v1/auth/login`, {
		method: "POST",
		cache: "no-store",
		headers: {
			"Content-Type": "application/json",
		},
		body: JSON.stringify({ username, password }),
	});

	if (!res.ok) {
		return false;
	}

	const json = await res.json();

	cookies().set({
		name: "jwt",
		value: json.jwt,
		httpOnly: true,
		secure: true,
		sameSite: true,
	});

	return true;
}

export async function logout(): Promise<boolean> {
	cookies().set({
		name: "jwt",
		value: "",
		maxAge: 0,
	});

	return true;
}
