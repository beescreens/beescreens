import { Configuration } from "@/types/configuration";
import { Device } from "@/types/device";

export type DeviceConfiguration = {
	configurationId: string;
	deviceId: string;
	state: string;
	log: string;
	createdAt: string;
	updatedAt: string;
	configuration: Configuration;
	device: Device;
};

export type ReadDeviceConfiguration = DeviceConfiguration;

export type CreateDeviceConfiguration = Pick<
	DeviceConfiguration,
	"configurationId" | "deviceId"
>;

export type EditDeviceConfiguration = Pick<
	DeviceConfiguration,
	"state" | "log" | "configurationId" | "deviceId"
>;

export type RemoveDeviceConfiguration = Pick<
	DeviceConfiguration,
	"deviceId" | "configurationId"
>;

export enum DeviceConfigurationState {
	NEW = "NEW",
	PENDING = "PENDING",
	ERROR = "ERROR",
	SUCCESS = "SUCCESS",
}
