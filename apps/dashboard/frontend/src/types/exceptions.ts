export class UnauthorizedException extends Error {
	constructor() {
		super("UnauthorizedException");

		this.name = "UnauthorizedException";
	}
}
