export type Device = {
	id: string;
	apiKey: string;
	name: string;
	description: string;
	lastSeenAt: string;
	createdAt: string;
	updatedAt: string;
};

export type ReadDevice = Omit<Device, "apiKey">;

export type CreateDevice = Omit<
	Device,
	"id" | "lastSeenAt" | "createdAt" | "updatedAt"
>;

export type EditDevice = Omit<
	Device,
	"id" | "apiKey" | "lastSeenAt" | "createdAt" | "updatedAt"
>;
