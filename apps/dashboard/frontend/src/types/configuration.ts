export type Configuration = {
	id: string;
	name: string;
	description: string;
	repositoryUrl: string;
	repositoryPath: string;
	repositoryBranch: string;
	createdAt: string;
	updatedAt: string;
};

export type ReadConfiguration = Configuration;

export type CreateConfiguration = Omit<
	Configuration,
	"id" | "createdAt" | "updatedAt"
>;

export type EditConfiguration = Omit<
	Configuration,
	"id" | "createdAt" | "updatedAt"
>;
