import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";

export const config = {
	matcher: ["/((?!auth/login|_next/static|_next/image|favicon.ico).*)"],
};

export function middleware(request: NextRequest) {
	const jwt = request.cookies.get("jwt");

	if (jwt) {
		return NextResponse.next();
	}

	return NextResponse.redirect(new URL("/auth/login", request.url));
}
