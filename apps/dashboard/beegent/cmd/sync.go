package cmd

import (
	"beegent/enums/status"
	"beegent/utils"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	apiUrl             string
	apiKey             string
	ansiblePullCommand string
)

var syncCmd = &cobra.Command{
	Use:   "sync",
	Short: "This command synchronizes the agent with the BeeScreens dashboard",
	Long: `This command calls the BeeScreens Dashboard API to fetch the new configurations
	for this specific device. This command should be run within a cron task.`,
	PreRun: func(cmd *cobra.Command, args []string) {
		apiUrl = viper.GetString("api-url")
		apiKey = viper.GetString("api-key")
		ansiblePullCommand = viper.GetString("ansible-pull-command")
	},
	Run: func(cmd *cobra.Command, args []string) {
		log.Info("Running sync command")

		log.Info("Fetching new configurations...")

		// Fetch new configurations
		devicesConfigurations, err := utils.GetDevicesConfigurations(apiUrl)
		if err != nil {
			fmt.Printf("Error: error while fetching new configurations, %s\n", err.Error())
			os.Exit(1)
		}

		fmt.Printf("Fetched %d configuration(s)\n", len(devicesConfigurations))

		if len(devicesConfigurations) == 0 {
			return
		}

		if dryRun {
			fmt.Println("Dry run mode, no changes applied")
			return
		}

		// Apply all configurations
		for _, deviceConfiguration := range devicesConfigurations {
			// Update device-configuration to PENDING
			err = utils.PatchConfigurationsForCurrentDevice(deviceConfiguration.ConfigurationId, status.PENDING, "", apiUrl, apiKey)
			if err != nil {
				fmt.Printf("Error: error while updating status to pending, %s\n", err.Error())
				os.Exit(1)
			}
			log.Info("Device-configuration updated to PENDING")

			updatedStatus := status.PENDING
			log.Info(fmt.Sprintf("Applying configuration : %s", deviceConfiguration.Configuration.Name))

			// Apply the configuration with ansible
			logs, err := utils.ApplyAnsibleConfiguration(&deviceConfiguration.Configuration)
			if err != nil {
				fmt.Printf("Error: error while applying the configuration, %s\n", err.Error())
				logs += "\n" + err.Error()
				updatedStatus = status.ERROR
			}

			// Update the dashboard with the result of the execution
			err = utils.PatchConfigurationsForCurrentDevice(deviceConfiguration.ConfigurationId, updatedStatus, logs, apiUrl, apiKey)
			if err != nil {
				fmt.Printf("Error: cannot update configuration status and log on dashboard, %s\n", err.Error())
				continue
			}

			log.Info(fmt.Sprintf("Configuration %s applied with status %s", deviceConfiguration.Configuration.Name, updatedStatus))
			log.Info("Dashboard updated with the result of the execution")
		}

		fmt.Printf("%d configuration(s) applied\n", len(devicesConfigurations))
	},
}

func init() {
	rootCmd.AddCommand(syncCmd)

	syncCmd.PersistentFlags().StringVar(&ansiblePullCommand, "ansible-pull-command", "ansible-pull", "ansible-pull command (check in $PATH as well)")
	syncCmd.PersistentFlags().StringVarP(&apiUrl, "api-url", "u", "", "API url to the dashboard")
	syncCmd.PersistentFlags().StringVarP(&apiKey, "api-key", "k", "", "API key to use to authenticate to the dashboard")

	viper.BindPFlags(syncCmd.PersistentFlags())
}
