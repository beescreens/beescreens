package cmd

import (
	"beegent/utils"
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	configFile string
	dryRun     bool
	verbose    bool
)

var rootCmd = &cobra.Command{
	Use:     "beegent",
	Version: "0.2.0",
	Short:   "Synchronize this device with the BeeScreens Dashboard",
	Long: `This tool is in charge of synchronizing this device
with the Beescreens dashboard to apply the right configurations`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig, initViper, initLogger)

	rootCmd.CompletionOptions.HiddenDefaultCmd = true
	rootCmd.SetHelpCommand(&cobra.Command{Hidden: true})
	rootCmd.SetVersionTemplate("beegent v{{.Version}}\n")

	rootCmd.PersistentFlags().StringVarP(&configFile, "config-file", "C", "", "config file (default is config.yaml or $HOME/.beegent/config.yaml)")
	rootCmd.PersistentFlags().BoolVarP(&dryRun, "dry-run", "D", false, "dry run mode, do not apply any changes")
	rootCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "V", false, "verbose logging")
	rootCmd.PersistentFlags().StringP("api-key", "k", "", "API key to use to authenticate to the dashboard")

	viper.BindPFlags(rootCmd.PersistentFlags())
}

func initConfig() {
	if configFile != "" {
		viper.SetConfigFile(configFile)
	} else {
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config.yaml in current directory or $HOME/.beegent
		viper.AddConfigPath(".")
		viper.AddConfigPath(path.Join(home, ".beegent"))
		viper.SetConfigName("config")
		viper.SetConfigType("yaml")
	}

	viper.SetEnvPrefix("beegent")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_", ".", "_"))
	viper.AutomaticEnv()

	err := viper.ReadInConfig()

	if err == nil {
		fmt.Fprintf(os.Stdout, "Using config file: %v\n", viper.ConfigFileUsed())
	} else {
		fmt.Fprintf(os.Stdout, "No config file found, using default values from environment variables and command flags\n")
	}
}

func initViper() {
	// Get command line arguments from Viper
	configFile = viper.GetString("config-file")
	dryRun = viper.GetBool("dry-run")
	verbose = viper.GetBool("verbose")
}

func initLogger() {
	utils.EnableLogging(verbose)
}
