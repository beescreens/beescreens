package utils

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
)

/**
 * Code inspired from : https://droctothorpe.github.io/posts/2020/07/leveled-logs-with-cobra-and-logrus/
 */

type PlainFormatter struct{}

func (f *PlainFormatter) Format(entry *log.Entry) ([]byte, error) {
	return []byte(fmt.Sprintf("%s\n", entry.Message)), nil
}

func EnableLogging(enable bool) {
	if enable {
		log.SetLevel(log.DebugLevel)
		customFormatter := new(log.TextFormatter)
		customFormatter.TimestampFormat = "2006-01-02 15:04:05"
		customFormatter.FullTimestamp = true
		log.SetFormatter(customFormatter)
		log.SetOutput(os.Stdout)
		log.Info("Debug logs enabled")
	} else {
		// Display no logs
		log.SetLevel(log.PanicLevel)
	}
}
