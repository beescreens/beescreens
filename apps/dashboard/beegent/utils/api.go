package utils

import (
	"beegent/enums/status"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/spf13/viper"
)

type DeviceReadDto struct {
	Id          string `json:"id"`
	Description string `json:"description"`
	LastSeenAt  string `json:"lastSeenAt"`
	ApiKey      string `json:"apiKey"`
	Name        string `json:"name"`
	CreatedAt   string `json:"createdAt"`
	UpdatedAt   string `json:"updatedAt"`
}

type ConfigurationReadDto struct {
	Id               string `json:"id"`
	Name             string `json:"name"`
	Description      string `json:"description"`
	RepositoryUrl    string `json:"repositoryUrl"`
	RepositoryPath   string `json:"repositoryPath"`
	RepositoryBranch string `json:"repositoryBranch"`
	CreatedAt        string `json:"createdAt"`
	UpdatedAt        string `json:"updatedAt"`
}

type DeviceConfigurationReadDto struct {
	ConfigurationId string               `json:"configurationId"`
	DeviceId        string               `json:"deviceId"`
	State           string               `json:"state"`
	Log             string               `json:"log"`
	Device          DeviceReadDto        `json:"device"`
	Configuration   ConfigurationReadDto `json:"configuration"`
	CreatedAt       string               `json:"createdAt"`
	UpdatedAt       string               `json:"updatedAt"`
}

type DeviceConfigurationPatchDto struct {
	State           string  `json:"state"`
	Log             string  `json:"log"`
	ConfigurationId *string `json:"configurationId"`
}

/**
 * Get the configurations for the current device (only configurations with state NEW are returned)
 *
 * @return The configurations for the current device
 */
func GetDevicesConfigurations(apiUrl string) ([]DeviceConfigurationReadDto, error) {
	req, err := http.NewRequest(http.MethodGet, apiUrl+"/devices-configurations", nil)
	q := req.URL.Query()
	q.Add("state", "NEW")
	req.URL.RawQuery = q.Encode()

	req.Header.Set("apiKey", viper.GetString("api-key"))
	req.Header.Set("Content-Type", "application/json")

	if err != nil {
		return nil, fmt.Errorf("failed to create request: %s", err.Error())
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed requesting devices configurations: %s", err.Error())
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("http request failed with status code : %s", resp.Status)
	}

	var configsDevices []DeviceConfigurationReadDto
	err = json.NewDecoder(resp.Body).Decode(&configsDevices)
	if err != nil {
		return nil, fmt.Errorf("failed to decode response: %s", err.Error())
	}

	return configsDevices, nil
}

/**
 * Patch the status and log of a device configuration
 * If no configurationId is provided, all the configurations for the current device will be patched
 *
 * @param configId The id of the configuration to patch (can be empty)
 * @param state The new state of the configuration
 * @param log The log of the execution
 */
func PatchConfigurationsForCurrentDevice(configurationId string, state status.Status, log string, apiUrl string, apiKey string) error {
	var data = DeviceConfigurationPatchDto{
		ConfigurationId: &configurationId,
		State:           string(state),
		Log:             log,
	}

	if configurationId == "" {
		data.ConfigurationId = nil
	}

	payload, err := json.Marshal(data)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(http.MethodPatch, apiUrl+"/devices-configurations", bytes.NewBuffer(payload))

	if err != nil {
		return fmt.Errorf("failed to create http request: %s", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("apiKey", apiKey)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("failed patching configuration-device: %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("http request failed with status code : %s", resp.Status)
	}

	return nil
}
