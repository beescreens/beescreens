package utils

import (
	"bytes"
	"fmt"
	"net/url"
	"os/exec"
	"path/filepath"
)

func ApplyAnsibleConfiguration(configuration *ConfigurationReadDto) (string, error) {
	var stdout bytes.Buffer

	_, err := url.ParseRequestURI(configuration.RepositoryUrl)
	if err != nil {
		return "", fmt.Errorf("repository url is not valid : '%s'", configuration.RepositoryUrl)
	}

	if filepath.Ext(configuration.RepositoryPath) != ".yml" && filepath.Ext(configuration.RepositoryPath) != ".yaml" {
		return "", fmt.Errorf("repository path is not valid : '%s'", configuration.RepositoryPath)
	}

	var cmd *exec.Cmd
	if configuration.RepositoryBranch == "" {
		cmd = exec.Command("ansible-pull", "-U", configuration.RepositoryUrl, configuration.RepositoryPath)
	} else {
		cmd = exec.Command("ansible-pull", "-U", configuration.RepositoryUrl, "-C", configuration.RepositoryBranch, configuration.RepositoryPath)
	}
	cmd.Stdout = &stdout

	err = cmd.Run()
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			// Command failed with non-zero exit code
			resultCode := exitError.ExitCode()
			return stdout.String(), fmt.Errorf("ansible-pull execution failed with result code: %d, %s", resultCode, stdout.String())
		} else {
			// Failed to run command
			return "", err
		}
	}

	return stdout.String(), nil
}
