## Prerequisites

The following prerequisites must be filled to run this service:

- [Node.js](../../../docs/explanations/about-nodejs/index.md) must be installed.
- [pnpm](../../../docs/explanations/about-pnpm/index.md) must be installed.
- [Golang](../../../docs/explanations/about-golang/index.md) must be installed.

## Build the service

```sh
# Build the service
pnpm build
```

## Start the service in production mode

```sh
# Start the service in production mode
pnpm start
```
