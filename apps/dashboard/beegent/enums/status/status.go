package status

type Status string

const (
	ERROR   Status = "ERROR"
	SUCCESS Status = "SUCCESS"
	PENDING Status = "PENDING"
)
