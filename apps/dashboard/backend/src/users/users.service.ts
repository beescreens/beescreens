import { Injectable } from '@nestjs/common';
import { User } from '@prisma/client';
import { PrismaService } from 'nestjs-prisma';

@Injectable()
export class UsersService {
	constructor(private readonly prisma: PrismaService) {}

	async getUserByName(username: string): Promise<User | null> {
		return await this.prisma.user.findFirst({
			where: {
				username,
			},
		});
	}

	async getUserById(id: string): Promise<User | null> {
		return await this.prisma.user.findUnique({
			where: {
				id,
			},
		});
	}
}
