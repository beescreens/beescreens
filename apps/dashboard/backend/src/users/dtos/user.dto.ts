import { ApiProperty } from '@nestjs/swagger';
import { User } from '@prisma/client';
import { IsDateString, IsNotEmpty, IsString, IsUUID } from 'class-validator';

export class UserDto implements User {
	/**
	 * The ID of the user
	 */
	@ApiProperty({ format: 'uuid' })
	@IsUUID()
	id: string;

	/**
	 * The username of the user
	 */
	@IsString()
	@IsNotEmpty()
	username: string;

	/**
	 * The password of the user
	 */
	@IsString()
	@IsNotEmpty()
	password: string;

	/**
	 * The date when the user was created
	 */
	@IsDateString()
	createdAt: Date;

	/**
	 * The date when the user was last updated
	 */
	@IsDateString()
	updatedAt: Date;
}
