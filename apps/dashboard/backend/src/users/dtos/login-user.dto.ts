import { UserDto } from '@/users/dtos/user.dto';
import { OmitType } from '@nestjs/swagger';

export class LoginUserDto extends OmitType(UserDto, [
	'id',
	'createdAt',
	'updatedAt',
] as const) {}
