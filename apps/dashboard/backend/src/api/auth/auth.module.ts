import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { UsersModule } from '@/users/users.module';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from '@/api/auth/jwt/jwt.strategy';
import { DevicesModule } from '@/api/devices/devices.module';
import { AuthController } from '@/api/auth/auth.controller';
import { LocalStrategy } from '@/api/auth/local/local.strategy';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JWT_EXPIRATION_TIME, JWT_SECRET } from '@/config/config.constants';
import { ApiKeyStrategy } from '@/api/auth/api-key/api-key.strategy';

@Module({
	imports: [
		PassportModule,
		UsersModule,
		DevicesModule,
		ConfigModule,
		JwtModule.registerAsync({
			imports: [ConfigModule],
			inject: [ConfigService],
			useFactory: async (configService: ConfigService) => ({
				secret: configService.get(JWT_SECRET, { infer: true }),
				signOptions: {
					expiresIn: configService.get(JWT_EXPIRATION_TIME, { infer: true }),
				},
			}),
		}),
	],
	controllers: [AuthController],
	providers: [AuthService, ApiKeyStrategy, JwtStrategy, LocalStrategy],
	exports: [AuthService],
})
export class AuthModule {}
