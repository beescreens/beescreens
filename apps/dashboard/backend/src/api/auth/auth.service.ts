import { UsersService } from '@/users/users.service';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Device, User } from '@prisma/client';
import * as argon2id from 'argon2';
import { DevicesService } from '@/api/devices/devices.service';
import { LoginUserDto } from '@/users/dtos/login-user.dto';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from '@/api/auth/types/jwt-payload.type';
import { Jwt } from '@/api/auth/types/jwt.type';

@Injectable()
export class AuthService {
	constructor(
		private readonly devicesService: DevicesService,
		private readonly usersService: UsersService,
		private readonly jwtService: JwtService,
	) {}

	async validateApiKey(apiKey: string): Promise<Device> {
		return await this.devicesService.findOneByApiKey(apiKey);
	}

	async validateUser(userDto: LoginUserDto): Promise<User> {
		const user = await this.usersService.getUserByName(userDto.username);

		if (!user) {
			throw new UnauthorizedException();
		}

		const passwordValid = await argon2id.verify(
			user.password as string,
			userDto.password,
		);

		if (!passwordValid) {
			throw new UnauthorizedException();
		}

		return user;
	}

	async validateJwtPayload({ sub }: JwtPayload): Promise<User> {
		const user = (await this.usersService.getUserById(sub)) as User;

		if (!user) {
			throw new UnauthorizedException();
		}

		return user;
	}

	async generateJwt(user: User): Promise<Jwt> {
		const payload: JwtPayload = { username: user.username, sub: user.id };

		return {
			jwt: await this.jwtService.signAsync(payload),
		};
	}
}
