/* eslint-disable @typescript-eslint/ban-types */
import { ApiKeyAuthGuard } from '@/api/auth/api-key/api-key-auth.guard';
import { PASSPORT_STRATEGY_NAME } from '@/api/auth/auth.constants';
import { applyDecorators, UseGuards, CanActivate } from '@nestjs/common';
import {
	ApiForbiddenResponse,
	ApiSecurity,
	ApiUnauthorizedResponse,
} from '@nestjs/swagger';

export const ApiKeyAuth = (...guards: (Function | CanActivate)[]) =>
	applyDecorators(
		UseGuards(ApiKeyAuthGuard, ...guards),
		ApiSecurity(PASSPORT_STRATEGY_NAME.API_KEY),
		ApiUnauthorizedResponse({
			description: 'Wrong API key.',
		}),
		ApiForbiddenResponse({
			description: 'Insufficient roles or permissions.',
		}),
	);
