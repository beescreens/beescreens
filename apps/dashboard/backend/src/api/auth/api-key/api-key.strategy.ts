import { HeaderAPIKeyStrategy } from 'passport-headerapikey';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { AuthService } from '@/api/auth/auth.service';
import { Device } from '@prisma/client';
import {
	API_KEY_HEADER_NAME,
	PASSPORT_STRATEGY_NAME,
} from '@/api/auth/auth.constants';
import { ExpressAuthInfo } from '@/api/auth/types/express-auth-info.type';

type DoneCallback = (
	err: Error | null,
	device?: Device | false,
	info?: ExpressAuthInfo,
) => void;

const authInfo: ExpressAuthInfo = {
	strategyName: PASSPORT_STRATEGY_NAME.API_KEY,
};

@Injectable()
export class ApiKeyStrategy extends PassportStrategy(
	HeaderAPIKeyStrategy,
	PASSPORT_STRATEGY_NAME.API_KEY,
) {
	constructor(private authService: AuthService) {
		// Signature from http://www.passportjs.org/packages/passport-headerapikey/
		super(
			{ header: API_KEY_HEADER_NAME },
			false,
			async (apiKey: string, done: DoneCallback) => {
				try {
					const device = await this.authService.validateApiKey(apiKey);

					done(null, device, authInfo);
				} catch (error) {
					done(null, false, authInfo);
				}
			},
		);
	}
}
