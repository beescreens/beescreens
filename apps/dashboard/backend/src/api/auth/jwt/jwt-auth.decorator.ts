/* eslint-disable @typescript-eslint/ban-types */
import { PASSPORT_STRATEGY_NAME } from '@/api/auth/auth.constants';
import { JwtAuthGuard } from '@/api/auth/jwt/jwt-auth.guards';
import { applyDecorators, UseGuards, CanActivate } from '@nestjs/common';
import {
	ApiBearerAuth,
	ApiForbiddenResponse,
	ApiUnauthorizedResponse,
} from '@nestjs/swagger';

export const JwtAuth = (...guards: (Function | CanActivate)[]) =>
	applyDecorators(
		UseGuards(JwtAuthGuard, ...guards),
		ApiBearerAuth(PASSPORT_STRATEGY_NAME.JWT),
		ApiUnauthorizedResponse({
			description: 'Wrong JWT.',
		}),
		ApiForbiddenResponse({
			description: 'Insufficient roles or permissions.',
		}),
	);
