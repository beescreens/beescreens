import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { AuthService } from '@/api/auth/auth.service';
import { User } from '@prisma/client';
import { ConfigService } from '@nestjs/config';
import { Request } from 'express';
import { JWT_SECRET } from '@/config/config.constants';
import { PASSPORT_STRATEGY_NAME } from '@/api/auth/auth.constants';
import { ExpressAuthInfo } from '@/api/auth/types/express-auth-info.type';
import { JwtPayload } from '@/api/auth/types/jwt-payload.type';

type DoneCallback = (
	err: Error | null,
	user?: User | false,
	info?: ExpressAuthInfo,
) => void;

const authInfo: ExpressAuthInfo = {
	strategyName: PASSPORT_STRATEGY_NAME.JWT,
};

@Injectable()
export class JwtStrategy extends PassportStrategy(
	Strategy,
	PASSPORT_STRATEGY_NAME.JWT,
) {
	constructor(private authService: AuthService, configService: ConfigService) {
		// Signature from https://www.passportjs.org/packages/passport-jwt/
		super(
			{
				jwtFromRequest: ExtractJwt.fromExtractors([
					ExtractJwt.fromAuthHeaderAsBearerToken(),
					(request: Request) => request.cookies.jwt,
				]),
				ignoreExpiration: false,
				secretOrKey: configService.get(JWT_SECRET, { infer: true }),
			},
			async (payload: JwtPayload, done: DoneCallback) => {
				try {
					const user = await this.authService.validateJwtPayload(payload);

					done(null, user, authInfo);
				} catch (error) {
					done(null, false, authInfo);
				}
			},
		);
	}
}
