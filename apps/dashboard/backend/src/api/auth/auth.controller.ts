import { AuthService } from '@/api/auth/auth.service';
import { LoginUserDto } from '@/users/dtos/login-user.dto';
import { Controller, HttpCode, Post, Res } from '@nestjs/common';
import { Response } from 'express';
import {
	ApiBadRequestResponse,
	ApiBody,
	ApiCreatedResponse,
	ApiOperation,
	ApiTags,
} from '@nestjs/swagger';
import { User } from '@prisma/client';
import { LocalAuth } from '@/api/auth/local/local-auth.decorator';
import { AuthUser } from '@/api/auth/decorators/auth-user.decorator';

@ApiTags('Auth')
@Controller({
	path: 'auth',
	version: '1',
})
export class AuthController {
	constructor(private readonly authService: AuthService) {}

	@Post('login')
	@LocalAuth()
	@HttpCode(200)
	@ApiOperation({
		summary: 'Login',
		description: 'Login with username and password.',
		operationId: 'login',
	})
	@ApiBody({
		description: `The user's credentials.`,
		type: LoginUserDto,
	})
	@ApiCreatedResponse({
		description: `The user has been successfully logged in.`,
	})
	@ApiBadRequestResponse({
		description: `The username or password is incorrect.`,
	})
	async login(@AuthUser() user: User, @Res() res: Response) {
		const jwt = await this.authService.generateJwt(user);

		return res.json(jwt);
	}
}
