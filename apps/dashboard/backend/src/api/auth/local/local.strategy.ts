import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { AuthService } from '@/api/auth/auth.service';
import { PASSPORT_STRATEGY_NAME } from '@/api/auth/auth.constants';
import { User } from '@prisma/client';
import { ExpressAuthInfo } from '@/api/auth/types/express-auth-info.type';

type DoneCallback = (
	err: Error | null,
	user?: User | false,
	info?: ExpressAuthInfo,
) => void;

const authInfo: ExpressAuthInfo = {
	strategyName: PASSPORT_STRATEGY_NAME.LOCAL,
};

@Injectable()
export class LocalStrategy extends PassportStrategy(
	Strategy,
	PASSPORT_STRATEGY_NAME.LOCAL,
) {
	constructor(private authService: AuthService) {
		// Signature from https://www.passportjs.org/packages/passport-local/
		super(async (username: string, password: string, done: DoneCallback) => {
			try {
				const user = await this.authService.validateUser({
					username,
					password,
				});

				done(null, user, authInfo);
			} catch (error) {
				done(null, false, authInfo);
			}
		});
	}
}
