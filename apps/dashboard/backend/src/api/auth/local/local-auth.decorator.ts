/* eslint-disable @typescript-eslint/ban-types */
import { PASSPORT_STRATEGY_NAME } from '@/api/auth/auth.constants';
import { LocalAuthGuard } from '@/api/auth/local/local-auth.guards';
import { applyDecorators, UseGuards, CanActivate } from '@nestjs/common';
import {
	ApiForbiddenResponse,
	ApiSecurity,
	ApiUnauthorizedResponse,
} from '@nestjs/swagger';

export const LocalAuth = (...guards: (Function | CanActivate)[]) =>
	applyDecorators(
		UseGuards(LocalAuthGuard, ...guards),
		ApiSecurity(PASSPORT_STRATEGY_NAME.LOCAL),
		ApiUnauthorizedResponse({
			description: 'Wrong username/password.',
		}),
		ApiForbiddenResponse({
			description: 'Insufficient roles or permissions.',
		}),
	);
