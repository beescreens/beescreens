export enum PASSPORT_STRATEGY_NAME {
	API_KEY = 'API_KEY',
	JWT = 'JWT',
	LOCAL = 'LOCAL',
}

export const API_KEY_HEADER_NAME = 'apiKey';
