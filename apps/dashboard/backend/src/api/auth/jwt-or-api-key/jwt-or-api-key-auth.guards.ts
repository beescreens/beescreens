import { PASSPORT_STRATEGY_NAME } from '@/api/auth/auth.constants';
import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class JwtOrApiKeyAuthGuard extends AuthGuard([
	PASSPORT_STRATEGY_NAME.JWT,
	PASSPORT_STRATEGY_NAME.API_KEY,
]) {}
