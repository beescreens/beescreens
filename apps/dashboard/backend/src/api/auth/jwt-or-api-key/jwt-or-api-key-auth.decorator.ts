/* eslint-disable @typescript-eslint/ban-types */
import { PASSPORT_STRATEGY_NAME } from '@/api/auth/auth.constants';
import { JwtOrApiKeyAuthGuard } from '@/api/auth/jwt-or-api-key/jwt-or-api-key-auth.guards';
import { applyDecorators, UseGuards, CanActivate } from '@nestjs/common';
import {
	ApiCookieAuth,
	ApiForbiddenResponse,
	ApiSecurity,
	ApiUnauthorizedResponse,
} from '@nestjs/swagger';

export const JwtOrApiKeyAuth = (...guards: (Function | CanActivate)[]) =>
	applyDecorators(
		UseGuards(JwtOrApiKeyAuthGuard, ...guards),
		ApiCookieAuth(PASSPORT_STRATEGY_NAME.JWT),
		ApiSecurity(PASSPORT_STRATEGY_NAME.API_KEY),
		ApiUnauthorizedResponse({
			description: 'Wrong JWT or API key.',
		}),
		ApiForbiddenResponse({
			description: 'Insufficient roles or permissions.',
		}),
	);
