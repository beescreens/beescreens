import { PASSPORT_STRATEGY_NAME } from '@/api/auth/auth.constants';

export type ExpressAuthInfo = {
	strategyName: PASSPORT_STRATEGY_NAME;
};
