import { PASSPORT_STRATEGY_NAME } from '@/api/auth/auth.constants';
import { ExpressAuthInfo } from '@/api/auth/types/express-auth-info.type';
import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { User } from '@prisma/client';
import { Request } from 'express';

export const AuthUser = createParamDecorator(
	(_: string, ctx: ExecutionContext): User | undefined => {
		const request: Request = ctx.switchToHttp().getRequest();

		const authInfo = request.authInfo as ExpressAuthInfo | undefined;

		if (
			authInfo?.strategyName === PASSPORT_STRATEGY_NAME.JWT ||
			authInfo?.strategyName === PASSPORT_STRATEGY_NAME.LOCAL
		) {
			return request.user as User;
		}

		return undefined;
	},
);
