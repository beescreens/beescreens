import { PASSPORT_STRATEGY_NAME } from '@/api/auth/auth.constants';
import { ExpressAuthInfo } from '@/api/auth/types/express-auth-info.type';
import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { Device } from '@prisma/client';
import { Request } from 'express';

export const AuthDevice = createParamDecorator(
	(_: string, ctx: ExecutionContext): Device | undefined => {
		const request: Request = ctx.switchToHttp().getRequest();

		const authInfo = request.authInfo as ExpressAuthInfo | undefined;

		if (authInfo?.strategyName === PASSPORT_STRATEGY_NAME.API_KEY) {
			return request.user as Device;
		}

		return undefined;
	},
);
