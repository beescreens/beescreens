import { ConfigurationsController } from '@/api/configurations/configurations.controller';
import { ConfigurationsService } from '@/api/configurations/configurations.service';
import { Test, TestingModule } from '@nestjs/testing';
import { PrismaModule } from 'nestjs-prisma';

describe('DeviceConfigController', () => {
	let controller: ConfigurationsController;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			imports: [PrismaModule],
			controllers: [ConfigurationsController],
			providers: [ConfigurationsService],
		}).compile();

		controller = module.get<ConfigurationsController>(ConfigurationsController);
	});

	it('should be defined', () => {
		expect(controller).toBeDefined();
	});
});
