import { Test, TestingModule } from '@nestjs/testing';
import { PrismaModule } from 'nestjs-prisma';
import { ConfigurationsService } from '@/api/configurations/configurations.service';

describe('ConfigurationService', () => {
	let service: ConfigurationsService;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			imports: [PrismaModule],
			providers: [ConfigurationsService],
		}).compile();

		service = module.get<ConfigurationsService>(ConfigurationsService);
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});
});
