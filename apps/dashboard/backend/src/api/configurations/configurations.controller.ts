import { Controller, Body, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CustomDelete } from '@/common/decorators/custom-delete.decorator';
import { CustomPatch } from '@/common/decorators/custom-patch.decorator';
import { CustomPost } from '@/common/decorators/custom-post.decorator';
import { GetMany } from '@/common/decorators/get-many.decorator';
import { GetOne } from '@/common/decorators/get-one.decorator';
import { ConfigurationsService } from '@/api/configurations/configurations.service';
import { CreateConfigurationDto } from '@/api/configurations/dtos/create-configuration.dto';
import { ReadConfigurationDto } from '@/api/configurations/dtos/read-configuration.dto';
import { UpdateConfigurationDto } from '@/api/configurations/dtos/update-configuration.dto';
import { JwtAuth } from '@/api/auth/jwt/jwt-auth.decorator';

@JwtAuth()
@ApiTags('Configurations')
@Controller({
	path: 'configurations',
	version: '1',
})
export class ConfigurationsController {
	constructor(private readonly configurationsService: ConfigurationsService) {}

	@CustomPost({
		name: 'Configuration',
		summary: 'Create a new configuration',
		bodyType: CreateConfigurationDto,
		responseType: ReadConfigurationDto,
		operationId: 'create',
	})
	async create(@Body() createConfig: CreateConfigurationDto) {
		const newConfig = await this.configurationsService.create(createConfig);
		return new ReadConfigurationDto(newConfig);
	}

	@GetMany({
		name: 'Configurations',
		summary: 'Get the configurations',
		operationId: 'findAll',
		responseType: [ReadConfigurationDto],
	})
	async findAll() {
		const configs = await this.configurationsService.findAll();
		const configsDto = configs.map(
			(config) => new ReadConfigurationDto(config),
		);
		return configsDto;
	}

	@GetOne({
		name: 'Configuration',
		summary: 'Get a configuration by id',
		operationId: 'findOne',
		responseType: ReadConfigurationDto,
	})
	async findOne(@Param('id') id: string) {
		const config = await this.configurationsService.findOne(id);
		return new ReadConfigurationDto(config);
	}

	@CustomPatch({
		name: 'Configuration',
		summary: 'Update a configuration by id',
		bodyType: UpdateConfigurationDto,
		responseType: ReadConfigurationDto,
		operationId: 'update',
	})
	async update(
		@Param('id') id: string,
		@Body() updateConfig: UpdateConfigurationDto,
	) {
		const updatedConfig = await this.configurationsService.update(
			id,
			updateConfig,
		);
		return new ReadConfigurationDto(updatedConfig);
	}

	@CustomDelete({
		name: 'Configuration',
		summary: 'Delete a configuration by id',
		operationId: 'remove',
	})
	async remove(@Param('id') id: string) {
		await this.configurationsService.remove(id);
	}
}
