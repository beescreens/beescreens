import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Configuration } from '@prisma/client';
import {
	IsDateString,
	IsNotEmpty,
	IsOptional,
	IsString,
	IsUUID,
	IsUrl,
} from 'class-validator';

export class ConfigurationDto implements Configuration {
	/**
	 * The ID of the configuration
	 */
	@ApiProperty({ format: 'uuid' })
	@IsUUID()
	id: string;

	/**
	 * The name of the configuration
	 */
	@IsString()
	@IsNotEmpty()
	name: string;

	/**
	 * The description of the configuration
	 */
	@ApiPropertyOptional()
	@IsOptional()
	@IsString()
	description: string | null;

	/**
	 * The URL to the repository
	 */
	@ApiProperty({ format: 'url' })
	@IsUrl()
	@IsNotEmpty()
	repositoryUrl: string;

	/**
	 * The path to the configuration file
	 */
	@IsString()
	@IsNotEmpty()
	repositoryPath: string;

	/**
	 * The branch of the repository to use
	 */
	@ApiPropertyOptional()
	@IsOptional()
	@IsString()
	repositoryBranch: string | null;

	/**
	 * The date when the configuration was last updated
	 */
	@IsDateString()
	createdAt: Date;

	/**
	 * The date when the configuration was last updated
	 */
	@IsDateString()
	updatedAt: Date;
}
