import { ConfigurationDto } from '@/api/configurations/dtos/configuration.dto';

export class ReadConfigurationDto extends ConfigurationDto {
	constructor(partial: Partial<ConfigurationDto>) {
		super();
		Object.assign(this, partial);
	}
}
