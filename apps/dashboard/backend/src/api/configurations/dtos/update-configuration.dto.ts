import { ConfigurationDto } from '@/api/configurations/dtos/configuration.dto';
import { OmitType, PartialType } from '@nestjs/swagger';

export class UpdateConfigurationDto extends PartialType(
	OmitType(ConfigurationDto, ['id', 'createdAt', 'updatedAt'] as const),
) {}
