import { ConfigurationDto } from '@/api/configurations/dtos/configuration.dto';
import { OmitType } from '@nestjs/swagger';

export class CreateConfigurationDto extends OmitType(ConfigurationDto, [
	'id',
	'createdAt',
	'updatedAt',
] as const) {}
