import { ConfigurationsController } from '@/api/configurations/configurations.controller';
import { ConfigurationsService } from '@/api/configurations/configurations.service';
import { Module } from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';

@Module({
	controllers: [ConfigurationsController],
	providers: [ConfigurationsService, PrismaService],
	exports: [ConfigurationsService],
})
export class ConfigurationsModule {}
