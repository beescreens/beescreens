import { Injectable } from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';
import { Prisma } from '@prisma/client';

@Injectable()
export class ConfigurationsService {
	constructor(private readonly prisma: PrismaService) {}

	async create(createConfig: Prisma.ConfigurationCreateInput) {
		return await this.prisma.configuration.create({
			data: createConfig,
		});
	}

	async findAll() {
		return await this.prisma.configuration.findMany();
	}

	async findOne(id: string) {
		return await this.prisma.configuration.findUniqueOrThrow({
			where: {
				id,
			},
		});
	}

	async update(id: string, updateConfig: Prisma.ConfigurationUpdateInput) {
		return await this.prisma.configuration.update({
			where: {
				id,
			},
			data: updateConfig,
		});
	}

	async remove(id: string) {
		await this.prisma.configuration.delete({
			where: {
				id,
			},
		});
	}
}
