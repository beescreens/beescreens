import { DeviceConfigurationDto } from '@/api/devices-configurations/dtos/device-configuration.dto';
import { ApiPropertyOptional, OmitType } from '@nestjs/swagger';
import { IsOptional, IsUUID } from 'class-validator';

export class UpdateDeviceConfigurationDto extends OmitType(
	DeviceConfigurationDto,
	[
		'createdAt',
		'updatedAt',
		'configuration',
		'device',
		'configurationId',
		'deviceId',
	] as const,
) {
	/**
	 * The id of the configuration
	 */
	@ApiPropertyOptional({ format: 'uuid' })
	@IsUUID()
	@IsOptional()
	configurationId: string | null;

	/**
	 * The id of the device
	 */
	@ApiPropertyOptional({ format: 'uuid' })
	@IsUUID()
	@IsOptional()
	deviceId: string | null;
}
