import { ReadConfigurationDto } from '@/api/configurations/dtos/read-configuration.dto';
import { ReadDeviceDto } from '@/api/devices/dtos/read-device.dto';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { DeviceConfiguration, DeviceConfigurationState } from '@prisma/client';
import {
	IsDateString,
	IsEnum,
	IsOptional,
	IsString,
	IsUUID,
} from 'class-validator';

export class DeviceConfigurationDto implements DeviceConfiguration {
	/**
	 * The id of the configuration
	 */
	@ApiProperty({ format: 'uuid' })
	@IsUUID()
	configurationId: string;

	/**
	 * The id of the device
	 */
	@ApiProperty({ format: 'uuid' })
	@IsUUID()
	deviceId: string;

	/**
	 * The state of the device
	 */
	@ApiProperty({ enum: DeviceConfigurationState })
	@IsEnum(DeviceConfigurationState)
	state: DeviceConfigurationState;

	/**
	 * The log of the configuration applied to the device
	 */
	@ApiPropertyOptional()
	@IsOptional()
	@IsString()
	log: string | null;

	/**
	 * The date when the device was created
	 */
	@IsDateString()
	createdAt: Date;

	/**
	 * The date when the device was last updated
	 */
	@IsDateString()
	updatedAt: Date;

	/**
	 * The configuration to apply to the device
	 */
	@ApiPropertyOptional({ type: () => ReadConfigurationDto })
	@IsOptional()
	configuration: ReadConfigurationDto;

	/**
	 * The device to apply the configuration to
	 */
	@ApiPropertyOptional({ type: () => ReadDeviceDto })
	@IsOptional()
	device: ReadDeviceDto;
}
