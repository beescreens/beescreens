import { DeviceConfigurationDto } from '@/api/devices-configurations/dtos/device-configuration.dto';
import { OmitType } from '@nestjs/swagger';

export class CreateDeviceConfigurationDto extends OmitType(
	DeviceConfigurationDto,
	[
		'state',
		'log',
		'createdAt',
		'updatedAt',
		'device',
		'configuration',
	] as const,
) {}
