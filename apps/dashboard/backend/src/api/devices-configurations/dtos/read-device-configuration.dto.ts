import { ReadConfigurationDto } from '@/api/configurations/dtos/read-configuration.dto';
import { DeviceConfigurationDto } from '@/api/devices-configurations/dtos/device-configuration.dto';
import { ReadDeviceDto } from '@/api/devices/dtos/read-device.dto';
import { Configuration, Device } from '@prisma/client';

export class ReadDeviceConfigurationDto extends DeviceConfigurationDto {
	constructor(partial: Partial<DeviceConfigurationDto>) {
		super();

		if (partial) {
			partial.configuration = new ReadConfigurationDto(
				partial.configuration as Configuration,
			);
			partial.device = new ReadDeviceDto(partial.device as Device);
		}
		Object.assign(this, partial);
	}
}
