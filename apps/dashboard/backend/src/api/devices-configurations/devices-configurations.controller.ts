import { DevicesConfigurationsService } from '@/api/devices-configurations/devices-configurations.service';
import { CreateDeviceConfigurationDto } from '@/api/devices-configurations/dtos/create-device-configuration.dto';
import { ReadDeviceConfigurationDto } from '@/api/devices-configurations/dtos/read-device-configuration.dto';
import { RemoveDeviceConfigurationDto } from '@/api/devices-configurations/dtos/remove-device-configuration.dto';
import { UpdateDeviceConfigurationDto } from '@/api/devices-configurations/dtos/update-device-configuration.dto';
import { CustomPost } from '@/common/decorators/custom-post.decorator';
import { GetMany } from '@/common/decorators/get-many.decorator';
import { MissingOrIncorrectFieldsResponse } from '@/common/openapi/responses';
import {
	Controller,
	Body,
	Query,
	BadRequestException,
	Delete,
	HttpCode,
	Patch,
} from '@nestjs/common';
import {
	ApiBadRequestResponse,
	ApiBody,
	ApiConflictResponse,
	ApiNoContentResponse,
	ApiNotFoundResponse,
	ApiOkResponse,
	ApiOperation,
	ApiQuery,
	ApiTags,
} from '@nestjs/swagger';
import { Device, DeviceConfigurationState } from '@prisma/client';
import { JwtOrApiKeyAuth } from '@/api/auth/jwt-or-api-key/jwt-or-api-key-auth.decorator';
import { AuthDevice } from '@/api/auth/decorators/auth-device.decorator';
import { JwtAuth } from '@/api/auth/jwt/jwt-auth.decorator';
import { DevicesService } from '@/api/devices/devices.service';

@ApiTags('Devices Configurations')
@Controller({
	path: 'devices-configurations',
	version: '1',
})
export class DevicesConfigurationsController {
	constructor(
		private readonly devicesConfigurationsService: DevicesConfigurationsService,
		private readonly deviceService: DevicesService,
	) {}

	@CustomPost({
		name: 'Device Configuration',
		summary: 'Create a new device configuration',
		bodyType: CreateDeviceConfigurationDto,
		responseType: ReadDeviceConfigurationDto,
		operationId: 'create',
	})
	@JwtAuth()
	async create(
		@Body()
		createDeviceConfigurationDto: CreateDeviceConfigurationDto,
	) {
		const newDeviceConfiguration =
			await this.devicesConfigurationsService.create(
				createDeviceConfigurationDto,
			);
		return new ReadDeviceConfigurationDto(newDeviceConfiguration);
	}

	@GetMany({
		name: 'Device Configuration',
		summary: 'Get the device configurations',
		operationId: 'findAll',
		responseType: [ReadDeviceConfigurationDto],
	})
	@JwtOrApiKeyAuth()
	@ApiQuery({
		name: 'deviceId',
		required: false,
		type: String,
	})
	@ApiQuery({
		name: 'configurationId',
		required: false,
		type: String,
	})
	@ApiQuery({
		name: 'state',
		required: false,
		enum: DeviceConfigurationState,
	})
	async findAll(
		@AuthDevice() device?: Device,
		@Query('deviceId') deviceId?: string,
		@Query('configurationId') configurationId?: string,
		@Query('state') state?: DeviceConfigurationState,
	) {
		if (state && !(state in DeviceConfigurationState)) {
			throw new BadRequestException('Invalid state');
		}

		// If the device is set, it means that the authentication is done with an API key
		// This allows us to know that the device is calling the API, and we can update the lastSeenAt field
		if (device) {
			deviceId = device.id;
			await this.deviceService.update(deviceId, {
				lastSeenAt: new Date(),
			});
		}

		const devicesConfigurations =
			await this.devicesConfigurationsService.findAll(
				deviceId,
				configurationId,
				state,
			);

		const deviceConfigurationsDto = devicesConfigurations.map(
			(deviceConfig) => new ReadDeviceConfigurationDto(deviceConfig),
		);

		return deviceConfigurationsDto;
	}

	@Patch()
	@JwtOrApiKeyAuth()
	@ApiOperation({
		summary: 'Update a device configuration by id',
		description: 'Update a device configuration by id.',
		operationId: 'update',
	})
	@ApiBody({
		description: `The device configuration details.`,
		type: UpdateDeviceConfigurationDto,
	})
	@ApiOkResponse({
		description: `The device configuration has been successfully created.`,
		type: ReadDeviceConfigurationDto,
	})
	@ApiNotFoundResponse({
		description: `The device configuration has not been found.`,
	})
	@ApiConflictResponse({
		description: `Another device configuration is in conflict with this one.`,
	})
	@ApiBadRequestResponse(MissingOrIncorrectFieldsResponse)
	async update(
		@Body() updateDeviceConfigurationDto: UpdateDeviceConfigurationDto,
		@AuthDevice() device?: Device,
	) {
		// If the device is set, it means that the authentication is done with an API key
		// This allows us to know that the device is calling the API, and we can update the lastSeenAt field
		if (device) {
			updateDeviceConfigurationDto.deviceId = device.id;
			await this.deviceService.update(updateDeviceConfigurationDto.deviceId, {
				lastSeenAt: new Date(),
			});
		}

		await this.devicesConfigurationsService.update(
			updateDeviceConfigurationDto,
			updateDeviceConfigurationDto.configurationId as string,
			updateDeviceConfigurationDto.deviceId as string,
		);

		const updatedDeviceConfiguration =
			await this.devicesConfigurationsService.findAll(
				updateDeviceConfigurationDto.deviceId as string,
				updateDeviceConfigurationDto.configurationId as string,
			);

		const updatedDeviceConfigurationDto = updatedDeviceConfiguration.map(
			(deviceConfig) => new ReadDeviceConfigurationDto(deviceConfig),
		);

		return updatedDeviceConfigurationDto;
	}

	@Delete()
	@HttpCode(204)
	@JwtAuth()
	@ApiOperation({
		summary: 'Remove a device configuration by id.',
		description: 'Remove a device configuration by id.',
		operationId: 'remove',
	})
	@ApiBody({
		description: `The Device Configuration details.`,
		type: RemoveDeviceConfigurationDto,
	})
	@ApiNoContentResponse({
		description: `The Device Configuration has been successfully deleted.`,
	})
	@ApiNotFoundResponse({
		description: `Device Configuration has not been found.`,
	})
	async remove(
		@Body() removeDeviceConfigurationDto: RemoveDeviceConfigurationDto,
	) {
		await this.devicesConfigurationsService.remove(
			removeDeviceConfigurationDto.configurationId as string,
			removeDeviceConfigurationDto.deviceId as string,
		);
	}
}
