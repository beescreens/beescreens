import { DevicesConfigurationsController } from '@/api/devices-configurations/devices-configurations.controller';
import { DevicesConfigurationsService } from '@/api/devices-configurations/devices-configurations.service';
import { DevicesService } from '@/api/devices/devices.service';
import { Test, TestingModule } from '@nestjs/testing';
import { PrismaModule } from 'nestjs-prisma';

describe('DevicesConfigurationsController', () => {
	let controller: DevicesConfigurationsController;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			imports: [PrismaModule],
			controllers: [DevicesConfigurationsController],
			providers: [DevicesConfigurationsService, DevicesService],
		}).compile();

		controller = module.get<DevicesConfigurationsController>(
			DevicesConfigurationsController,
		);
	});

	it('should be defined', () => {
		expect(controller).toBeDefined();
	});
});
