import { DevicesConfigurationsController } from '@/api/devices-configurations/devices-configurations.controller';
import { DevicesConfigurationsService } from '@/api/devices-configurations/devices-configurations.service';
import { DevicesService } from '@/api/devices/devices.service';
import { Module } from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';

@Module({
	controllers: [DevicesConfigurationsController],
	providers: [DevicesConfigurationsService, PrismaService, DevicesService],
	exports: [DevicesConfigurationsService],
})
export class DevicesConfigurationsModule {}
