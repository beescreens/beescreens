import { Injectable } from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';
import { DeviceConfigurationState, Prisma } from '@prisma/client';

@Injectable()
export class DevicesConfigurationsService {
	constructor(private readonly prisma: PrismaService) {}

	async findAll(
		deviceId?: string,
		configurationId?: string,
		state?: DeviceConfigurationState,
	) {
		return await this.prisma.deviceConfiguration.findMany({
			where: {
				deviceId,
				configurationId,
				state,
			},
			include: {
				configuration: true,
				device: true,
			},
		});
	}

	async create(
		deviceConfiguration: Prisma.DeviceConfigurationUncheckedCreateInput,
	) {
		return await this.prisma.deviceConfiguration.create({
			data: deviceConfiguration,
			include: {
				configuration: true,
				device: true,
			},
		});
	}

	async update(
		updatedDeviceConfiguration: Prisma.DeviceConfigurationUpdateInput,
		configurationId?: string,
		deviceId?: string,
	) {
		return await this.prisma.deviceConfiguration.updateMany({
			where: {
				configurationId,
				deviceId,
			},
			data: updatedDeviceConfiguration,
		});
	}

	async remove(configurationId?: string, deviceId?: string) {
		await this.prisma.deviceConfiguration.deleteMany({
			where: {
				configurationId,
				deviceId,
			},
		});
	}
}
