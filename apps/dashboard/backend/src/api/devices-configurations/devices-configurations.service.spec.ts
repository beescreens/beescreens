import { DevicesConfigurationsService } from '@/api/devices-configurations/devices-configurations.service';
import { Test, TestingModule } from '@nestjs/testing';
import { PrismaModule } from 'nestjs-prisma';

describe('DevicesConfigurationsService', () => {
	let service: DevicesConfigurationsService;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			imports: [PrismaModule],
			providers: [DevicesConfigurationsService],
		}).compile();

		service = module.get<DevicesConfigurationsService>(
			DevicesConfigurationsService,
		);
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});
});
