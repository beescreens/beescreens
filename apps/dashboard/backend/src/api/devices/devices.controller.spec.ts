import { DevicesController } from '@/api/devices/devices.controller';
import { DevicesService } from '@/api/devices/devices.service';
import { CreateDeviceDto } from '@/api/devices/dtos/create-device.dto';
import { ReadDeviceDto } from '@/api/devices/dtos/read-device.dto';
import { UpdateDeviceDto } from '@/api/devices/dtos/update-device.dto';
import { Test, TestingModule } from '@nestjs/testing';
import { randomUUID } from 'crypto';
import { PrismaModule } from 'nestjs-prisma';

describe('DevicesController', () => {
	let controller: DevicesController;

	beforeEach(async () => {
		const readDeviceDto = new ReadDeviceDto({
			name: 'a name',
			apiKey: '1',
			description: 'a description',
			createdAt: new Date(),
			updatedAt: new Date(),
			lastSeenAt: new Date(),
		});

		const module: TestingModule = await Test.createTestingModule({
			imports: [PrismaModule],
			controllers: [DevicesController],
			providers: [
				{
					provide: DevicesService,
					useValue: {
						create: jest.fn().mockImplementation(
							(dto: CreateDeviceDto) =>
								new ReadDeviceDto({
									id: randomUUID(),
									name: dto.name,
									apiKey: dto.apiKey,
									description: dto.description,
									createdAt: new Date(),
									updatedAt: new Date(),
									lastSeenAt: new Date(),
								}),
						),
						findOne: jest.fn().mockImplementation(
							(id: string) =>
								new ReadDeviceDto({
									...readDeviceDto,
									id,
								}),
						),
						findAll: jest.fn().mockImplementation(() => [
							new ReadDeviceDto({
								...readDeviceDto,
								id: randomUUID(),
							}),
							new ReadDeviceDto({
								...readDeviceDto,
								id: randomUUID(),
							}),
						]),
						update: jest.fn().mockImplementation(
							(id: string, dto: UpdateDeviceDto) =>
								new ReadDeviceDto({
									...readDeviceDto,
									id,
									name: dto.name,
									description: dto.description,
								}),
						),
						remove: jest.fn(),
					},
				},
			],
		}).compile();

		controller = module.get<DevicesController>(DevicesController);
	});

	it('should be defined', () => {
		expect(controller).toBeDefined();
	});

	describe('create', () => {
		it('should create a device', async () => {
			const input = new CreateDeviceDto({
				name: 'test',
				apiKey: '1',
				description: 'test',
			});

			const result = await controller.create(input);

			expect(result).toMatchSnapshot({
				id: expect.any(String),
				createdAt: expect.any(Date),
				updatedAt: expect.any(Date),
				lastSeenAt: expect.any(Date),
			});
		});
	});

	describe('update', () => {
		it('should update a device', async () => {
			const deviceId = '1';
			const updated = new UpdateDeviceDto({
				name: 'new test',
				description: 'new description',
			});

			const result = await controller.update(deviceId, updated);

			expect(result).toMatchSnapshot({
				id: deviceId,
				createdAt: expect.any(Date),
				updatedAt: expect.any(Date),
				lastSeenAt: expect.any(Date),
			});
		});
	});

	describe('findOne', () => {
		it('should return a device', async () => {
			const result = await controller.findOne('1');

			expect(result).toMatchSnapshot({
				id: expect.any(String),
				createdAt: expect.any(Date),
				updatedAt: expect.any(Date),
				lastSeenAt: expect.any(Date),
			});
		});
	});

	describe('findAll', () => {
		it('should return a list of devices', async () => {
			const devices: ReadDeviceDto[] = await controller.findAll();

			// toMatchSnapshot doesn't work with arrays of objects
			expect(devices[0]).toMatchSnapshot({
				id: expect.any(String),
				createdAt: expect.any(Date),
				updatedAt: expect.any(Date),
				lastSeenAt: expect.any(Date),
			});

			expect(devices[1]).toMatchSnapshot({
				id: expect.any(String),
				createdAt: expect.any(Date),
				updatedAt: expect.any(Date),
				lastSeenAt: expect.any(Date),
			});
		});
	});

	describe('remove', () => {
		it('should delete a device', async () => {
			const result = await controller.remove('1');

			expect(result).toBeUndefined();
		});
	});
});
