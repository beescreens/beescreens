import {
	IsDateString,
	IsNotEmpty,
	IsOptional,
	IsString,
	IsUUID,
} from 'class-validator';
import { Device } from '@prisma/client';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class DeviceDto implements Device {
	/**
	 * The id of the device
	 */
	@ApiProperty({ format: 'uuid' })
	@IsUUID()
	id: string;

	/**
	 * The API key of the device
	 */
	@IsString()
	@IsNotEmpty()
	apiKey: string;

	/**
	 * The name of the device
	 */
	@IsString()
	@IsNotEmpty()
	name: string;

	/**
	 * The description of the device
	 */
	@ApiPropertyOptional()
	@IsOptional()
	@IsString()
	description: string | null;

	/**
	 * The last time the device synchronized
	 */
	@ApiPropertyOptional()
	@IsOptional()
	@IsDateString()
	lastSeenAt: Date | null;

	/**
	 * The date when the device was created
	 */
	@IsDateString()
	createdAt: Date;

	/**
	 * The date when the device was last updated
	 */
	@IsDateString()
	updatedAt: Date;
}
