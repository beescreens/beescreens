import { DeviceDto } from '@/api/devices/dtos/device.dto';
import { OmitType, PartialType } from '@nestjs/swagger';

export class ReadDeviceDto extends PartialType(
	OmitType(DeviceDto, ['apiKey'] as const),
) {
	constructor(partial: Partial<DeviceDto>) {
		super();
		if (partial) {
			delete partial.apiKey;
			Object.assign(this, partial);
		}
	}
}
