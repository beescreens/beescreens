import { DeviceDto } from '@/api/devices/dtos/device.dto';
import { OmitType, PartialType } from '@nestjs/swagger';

export class UpdateDeviceDto extends PartialType(
	OmitType(DeviceDto, [
		'id',
		'apiKey',
		'lastSeenAt',
		'createdAt',
		'updatedAt',
	] as const),
) {
	constructor(partial: Partial<DeviceDto>) {
		super();
		if (partial) {
			delete partial.id;
			delete partial.apiKey;
			delete partial.lastSeenAt;
			delete partial.createdAt;
			delete partial.updatedAt;
			Object.assign(this, partial);
		}
	}
}
