import { DeviceDto } from '@/api/devices/dtos/device.dto';
import { OmitType } from '@nestjs/swagger';

export class CreateDeviceDto extends OmitType(DeviceDto, [
	'id',
	'lastSeenAt',
	'createdAt',
	'updatedAt',
] as const) {
	constructor(partial: Partial<DeviceDto>) {
		super();
		if (partial) {
			delete partial.id;
			delete partial.lastSeenAt;
			delete partial.createdAt;
			delete partial.updatedAt;
			Object.assign(this, partial);
		}
	}
}
