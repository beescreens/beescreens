import { Body, Controller, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { GetMany } from '@/common/decorators/get-many.decorator';
import { GetOne } from '@/common/decorators/get-one.decorator';
import { DevicesService } from '@/api/devices/devices.service';
import { CreateDeviceDto } from '@/api/devices/dtos/create-device.dto';
import { ReadDeviceDto } from '@/api/devices/dtos/read-device.dto';
import { UpdateDeviceDto } from '@/api/devices/dtos/update-device.dto';
import { CustomPost } from '@/common/decorators/custom-post.decorator';
import { CustomPatch } from '@/common/decorators/custom-patch.decorator';
import { CustomDelete } from '@/common/decorators/custom-delete.decorator';
import { JwtAuth } from '@/api/auth/jwt/jwt-auth.decorator';

@JwtAuth()
@ApiTags('Devices')
@Controller({
	path: 'devices',
	version: '1',
})
export class DevicesController {
	constructor(private readonly devicesService: DevicesService) {}

	@CustomPost({
		name: 'Device',
		summary: 'Create a new device',
		bodyType: CreateDeviceDto,
		responseType: ReadDeviceDto,
		operationId: 'create',
	})
	async create(@Body() device: CreateDeviceDto) {
		const newDevice = await this.devicesService.create(device);
		return new ReadDeviceDto(newDevice);
	}

	@GetMany({
		name: 'Devices',
		summary: 'Get the devices',
		operationId: 'findAll',
		responseType: [ReadDeviceDto],
	})
	async findAll() {
		const devices = await this.devicesService.findAll();
		const devicesDto = devices.map((device) => new ReadDeviceDto(device));
		return devicesDto;
	}

	@GetOne({
		name: 'Device',
		summary: 'Get a device by id',
		operationId: 'findOne',
		responseType: ReadDeviceDto,
	})
	async findOne(@Param('id') id: string) {
		const device = await this.devicesService.findOne(id);
		return new ReadDeviceDto(device);
	}

	@CustomPatch({
		name: 'Device',
		summary: 'Update a device by id',
		bodyType: UpdateDeviceDto,
		responseType: ReadDeviceDto,
		operationId: 'update',
	})
	async update(@Param('id') id: string, @Body() device: UpdateDeviceDto) {
		const updatedDevice = await this.devicesService.update(id, device);
		return new ReadDeviceDto(updatedDevice);
	}

	@CustomDelete({
		name: 'Device',
		summary: 'Delete a device by id',
		operationId: 'remove',
	})
	async remove(@Param('id') id: string) {
		await this.devicesService.remove(id);
	}
}
