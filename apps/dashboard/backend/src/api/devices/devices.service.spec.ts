import { DevicesService } from '@/api/devices/devices.service';
import { CreateDeviceDto } from '@/api/devices/dtos/create-device.dto';
import { Test, TestingModule } from '@nestjs/testing';
import { Prisma, Device } from '@prisma/client';
import { PrismaService } from 'nestjs-prisma';

describe('DevicesService', () => {
	let service: DevicesService;

	const defaultDevice: Device = {
		id: '1',
		name: 'a name',
		apiKey: '1',
		description: 'a description',
		createdAt: new Date(),
		updatedAt: new Date(),
		lastSeenAt: new Date(),
	};

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [
				DevicesService,
				{
					provide: PrismaService,
					useValue: {
						device: {
							create: jest
								.fn()
								.mockImplementation(
									(args: { data: Prisma.DeviceCreateInput }) => {
										const device: Device = {
											...defaultDevice,
											name: args.data.name,
											apiKey: args.data.apiKey,
											description: args.data.description as string | null,
										};
										return device;
									},
								),
							update: jest
								.fn()
								.mockImplementation(
									(args: {
										where: { id: string };
										data: Prisma.DeviceUpdateInput;
									}) => {
										const device: Device = {
											...defaultDevice,
											name: args.data.name as string,
											apiKey: args.data.apiKey as string,
											description: args.data.description as string | null,
										};
										return device;
									},
								),
							delete: jest.fn().mockImplementation(() => {
								return;
							}),
							findMany: jest.fn().mockImplementation(() => {
								const devices: Device[] = [
									defaultDevice,
									{
										...defaultDevice,
										id: '2',
										name: 'test 2',
										apiKey: '2',
										description: 'test 2',
									},
								];
								return devices;
							}),
							findUniqueOrThrow: jest.fn().mockImplementation(() => {
								return defaultDevice;
							}),
						},
					},
				},
			],
		}).compile();

		service = module.get<DevicesService>(DevicesService);
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});

	describe('findAll', () => {
		it('should return an array of devices', async () => {
			const result = await service.findAll();

			expect(result[0]).toMatchSnapshot({
				id: expect.any(String),
				createdAt: expect.any(Date),
				updatedAt: expect.any(Date),
				lastSeenAt: expect.any(Date),
			});
			expect(result[1]).toMatchSnapshot({
				id: expect.any(String),
				createdAt: expect.any(Date),
				updatedAt: expect.any(Date),
				lastSeenAt: expect.any(Date),
			});
		});
	});

	describe('findOne', () => {
		it('should return a device', async () => {
			const result = await service.findOne('1');

			expect(result).toMatchSnapshot({
				id: expect.any(String),
				createdAt: expect.any(Date),
				updatedAt: expect.any(Date),
				lastSeenAt: expect.any(Date),
			});
		});
	});

	describe('create', () => {
		it('should create a device', async () => {
			const input = new CreateDeviceDto({
				name: 'test',
				apiKey: '1',
				description: 'test',
			});
			const result = await service.create(input);

			expect(result).toMatchSnapshot({
				id: expect.any(String),
				apiKey: expect.any(String),
				createdAt: expect.any(Date),
				updatedAt: expect.any(Date),
				lastSeenAt: expect.any(Date),
			});
		});
	});

	describe('update', () => {
		it('should update a device', async () => {
			const input = new CreateDeviceDto({
				name: 'test',
				apiKey: '1',
				description: 'test',
			});

			const result = await service.update('1', input);

			expect(result).toMatchSnapshot({
				id: expect.any(String),
				createdAt: expect.any(Date),
				updatedAt: expect.any(Date),
				lastSeenAt: expect.any(Date),
			});
		});
	});

	describe('remove', () => {
		it('should remove a device', async () => {
			const result = await service.remove('1');

			expect(result).toBeUndefined();
		});
	});
});
