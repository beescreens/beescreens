import { DevicesController } from '@/api/devices/devices.controller';
import { DevicesService } from '@/api/devices/devices.service';
import { Module } from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';

@Module({
	controllers: [DevicesController],
	providers: [DevicesService, PrismaService],
	exports: [DevicesService],
})
export class DevicesModule {}
