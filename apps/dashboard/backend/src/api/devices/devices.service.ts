import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'nestjs-prisma';
import { createHash } from 'crypto';

@Injectable()
export class DevicesService {
	constructor(private readonly prisma: PrismaService) {}

	async findAll() {
		return await this.prisma.device.findMany();
	}

	async findOne(id: string) {
		return await this.prisma.device.findUniqueOrThrow({
			where: {
				id,
			},
		});
	}

	async findOneByApiKey(apiKey: string) {
		return await this.prisma.device.findUniqueOrThrow({
			where: {
				apiKey: createHash('sha256').update(apiKey).digest('hex'),
			},
		});
	}

	async create(device: Prisma.DeviceCreateInput) {
		return await this.prisma.device.create({
			data: {
				...device,
				apiKey: createHash('sha256').update(device.apiKey).digest('hex'),
			},
		});
	}

	async update(id: string, device: Prisma.DeviceUpdateInput) {
		return await this.prisma.device.update({
			where: {
				id,
			},
			data: device,
		});
	}

	async remove(id: string) {
		await this.prisma.device.delete({
			where: {
				id,
			},
		});
	}
}
