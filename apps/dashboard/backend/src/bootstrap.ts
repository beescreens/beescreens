import { HttpAdapterHost } from '@nestjs/core';
import { HttpStatus, ValidationPipe, VersioningType } from '@nestjs/common';
import { NestExpressApplication } from '@nestjs/platform-express';
import { PrismaClientExceptionFilter, PrismaService } from 'nestjs-prisma';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as cookieParser from 'cookie-parser';
import {
	API_KEY_HEADER_NAME,
	PASSPORT_STRATEGY_NAME,
} from '@/api/auth/auth.constants';

export async function bootstrap(
	app: NestExpressApplication,
): Promise<NestExpressApplication> {
	app.useGlobalPipes(
		new ValidationPipe({
			whitelist: true,
		}),
	);
	app.enableVersioning({
		type: VersioningType.URI,
		prefix: 'api/v',
	});

	const { httpAdapter } = app.get(HttpAdapterHost);
	app.useGlobalFilters(
		new PrismaClientExceptionFilter(httpAdapter, {
			P2003: HttpStatus.BAD_REQUEST,
		}),
	);

	const prismaService = app.get(PrismaService);
	await prismaService.enableShutdownHooks(app);

	const config = new DocumentBuilder()
		.setTitle('Dashboard API')
		.setDescription('The dashboard API description')
		.setVersion(process.env.npm_package_version as string)
		.addBearerAuth(
			{
				type: 'http',
				name: 'jwt',
				description: 'The JWT to access protected endpoints',
			},
			PASSPORT_STRATEGY_NAME.JWT,
		)
		.addApiKey(
			{
				type: 'apiKey',
				description: 'The API key to access protected endpoints',
				name: API_KEY_HEADER_NAME,
			},
			PASSPORT_STRATEGY_NAME.API_KEY,
		)
		.build();

	const document = SwaggerModule.createDocument(app, config);

	SwaggerModule.setup('api', app, document, {
		swaggerOptions: {
			// docExpansion: 'none',
			showExtensions: true,
			tagsSorter: 'alpha',
			operationsSorter: 'alpha',
			persistAuthorization: true,
		},
	});

	app.use(cookieParser());

	return app;
}
