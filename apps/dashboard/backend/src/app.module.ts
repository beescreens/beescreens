import { ConfigurationsModule } from '@/api/configurations/configurations.module';
import { DevicesConfigurationsModule } from '@/api/devices-configurations/devices-configurations.module';
import { DevicesModule } from '@/api/devices/devices.module';
import { AppController } from '@/app.controller';
import { ConfigModule as NestConfigModule } from '@/config/config.module';
import { Module } from '@nestjs/common';
import { PrismaModule } from 'nestjs-prisma';
import { AuthModule } from './api/auth/auth.module';
import { UsersModule } from './users/users.module';

@Module({
	imports: [
		NestConfigModule,
		PrismaModule,
		DevicesModule,
		ConfigurationsModule,
		DevicesConfigurationsModule,
		AuthModule,
		UsersModule,
	],
	controllers: [AppController],
})
export class AppModule {}
