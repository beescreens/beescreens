export const PORT = 'PORT';
export const JWT_SECRET = 'JWT_SECRET';
export const JWT_EXPIRATION_TIME = 'JWT_EXPIRATION_TIME';

export interface EnvironmentVariables {
	[PORT]: string;
	[JWT_SECRET]: string;
	[JWT_EXPIRATION_TIME]: string;
}
