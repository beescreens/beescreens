import {
	PORT,
	JWT_SECRET,
	JWT_EXPIRATION_TIME,
} from '@/config/config.constants';

export const ConfigConfiguration = () => ({
	[PORT]: process.env.PORT as string,
	[JWT_SECRET]: process.env.JWT_SECRET as string,
	[JWT_EXPIRATION_TIME]: process.env.JWT_EXPIRATION_TIME as string,
});
