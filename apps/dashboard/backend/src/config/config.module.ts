import { Module } from '@nestjs/common';
import { ConfigModule as NestConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import {
	PORT,
	JWT_SECRET,
	JWT_EXPIRATION_TIME,
} from '@/config/config.constants';

@Module({
	imports: [
		NestConfigModule.forRoot({
			validationSchema: Joi.object({
				[PORT]: Joi.number().default(4000),
				[JWT_SECRET]: Joi.string().required(),
				[JWT_EXPIRATION_TIME]: Joi.string().required(),
			}),
			validationOptions: {
				allowUnknown: true,
				abortEarly: false,
			},
		}),
	],
	exports: [NestConfigModule],
})
export class ConfigModule {}
