import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { AppModule } from '@/app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import { bootstrap } from '@/bootstrap';

const main = async () => {
	const instance = await NestFactory.create<NestExpressApplication>(AppModule);
	const app = await bootstrap(instance);

	const configService = app.get(ConfigService);
	const port = configService.get<number>('PORT');

	await app.listen(port as number);
};

main();
