import { PrismaClient } from '@prisma/client';
import * as argon2id from 'argon2';

const prisma = new PrismaClient();

async function main() {
	await prisma.user.upsert({
		where: { username: process.env.ADMIN_USERNAME as string },
		update: {},
		create: {
			username: process.env.ADMIN_USERNAME as string,
			password: await argon2id.hash(process.env.ADMIN_PASSWORD as string),
		},
	});

	console.log('Database seeded with default account');
}

main()
	.then(async () => {
		await prisma.$disconnect();
	})
	.catch(async (e) => {
		console.error(e);
		await prisma.$disconnect();
		process.exit(1);
	});
