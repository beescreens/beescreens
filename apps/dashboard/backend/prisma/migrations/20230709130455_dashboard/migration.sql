-- DropForeignKey
ALTER TABLE "devices_configurations" DROP CONSTRAINT "devices_configurations_configuration_id_fkey";

-- DropForeignKey
ALTER TABLE "devices_configurations" DROP CONSTRAINT "devices_configurations_device_id_fkey";

-- AddForeignKey
ALTER TABLE "devices_configurations" ADD CONSTRAINT "devices_configurations_configuration_id_fkey" FOREIGN KEY ("configuration_id") REFERENCES "configurations"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "devices_configurations" ADD CONSTRAINT "devices_configurations_device_id_fkey" FOREIGN KEY ("device_id") REFERENCES "devices"("id") ON DELETE CASCADE ON UPDATE CASCADE;
