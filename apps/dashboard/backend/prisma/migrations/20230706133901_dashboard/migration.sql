/*
  Warnings:

  - The primary key for the `devices_configurations` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `configId` on the `devices_configurations` table. All the data in the column will be lost.
  - You are about to drop the column `deviceId` on the `devices_configurations` table. All the data in the column will be lost.
  - The `state` column on the `devices_configurations` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - Added the required column `config_id` to the `devices_configurations` table without a default value. This is not possible if the table is not empty.
  - Added the required column `device_id` to the `devices_configurations` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "DeviceConfigurationState" AS ENUM ('PENDING', 'SUCCESS', 'ERROR', 'NEW');

-- DropForeignKey
ALTER TABLE "devices_configurations" DROP CONSTRAINT "devices_configurations_configId_fkey";

-- DropForeignKey
ALTER TABLE "devices_configurations" DROP CONSTRAINT "devices_configurations_deviceId_fkey";

-- AlterTable
ALTER TABLE "devices_configurations" DROP CONSTRAINT "devices_configurations_pkey",
DROP COLUMN "configId",
DROP COLUMN "deviceId",
ADD COLUMN     "config_id" TEXT NOT NULL,
ADD COLUMN     "device_id" TEXT NOT NULL,
DROP COLUMN "state",
ADD COLUMN     "state" "DeviceConfigurationState" NOT NULL DEFAULT 'NEW',
ADD CONSTRAINT "devices_configurations_pkey" PRIMARY KEY ("config_id", "device_id");

-- DropEnum
DROP TYPE "DeviceState";

-- AddForeignKey
ALTER TABLE "devices_configurations" ADD CONSTRAINT "devices_configurations_config_id_fkey" FOREIGN KEY ("config_id") REFERENCES "configurations"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "devices_configurations" ADD CONSTRAINT "devices_configurations_device_id_fkey" FOREIGN KEY ("device_id") REFERENCES "devices"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
