-- CreateEnum
CREATE TYPE "DeviceState" AS ENUM ('PENDING', 'SUCCESS', 'ERROR', 'NEW');

-- CreateTable
CREATE TABLE "devices" (
    "id" TEXT NOT NULL,
    "api_key" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT,
    "last_seen_at" TIMESTAMP(3),
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "devices_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "configurations" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT,
    "repository_url" TEXT NOT NULL,
    "repository_path" TEXT NOT NULL,
    "repository_branch" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "configurations_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "configurations_on_devices" (
    "configId" TEXT NOT NULL,
    "deviceId" TEXT NOT NULL,
    "state" "DeviceState" NOT NULL DEFAULT 'NEW',
    "log" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "configurations_on_devices_pkey" PRIMARY KEY ("configId","deviceId")
);

-- CreateIndex
CREATE UNIQUE INDEX "devices_api_key_key" ON "devices"("api_key");

-- AddForeignKey
ALTER TABLE "configurations_on_devices" ADD CONSTRAINT "configurations_on_devices_configId_fkey" FOREIGN KEY ("configId") REFERENCES "configurations"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "configurations_on_devices" ADD CONSTRAINT "configurations_on_devices_deviceId_fkey" FOREIGN KEY ("deviceId") REFERENCES "devices"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
