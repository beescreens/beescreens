/*
  Warnings:

  - The primary key for the `devices_configurations` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `config_id` on the `devices_configurations` table. All the data in the column will be lost.
  - Added the required column `configuration_id` to the `devices_configurations` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "devices_configurations" DROP CONSTRAINT "devices_configurations_config_id_fkey";

-- AlterTable
ALTER TABLE "devices_configurations" DROP CONSTRAINT "devices_configurations_pkey",
DROP COLUMN "config_id",
ADD COLUMN     "configuration_id" TEXT NOT NULL,
ADD CONSTRAINT "devices_configurations_pkey" PRIMARY KEY ("configuration_id", "device_id");

-- AddForeignKey
ALTER TABLE "devices_configurations" ADD CONSTRAINT "devices_configurations_configuration_id_fkey" FOREIGN KEY ("configuration_id") REFERENCES "configurations"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
