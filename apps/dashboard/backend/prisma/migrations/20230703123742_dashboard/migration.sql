/*
  Warnings:

  - You are about to drop the `configurations_on_devices` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "configurations_on_devices" DROP CONSTRAINT "configurations_on_devices_configId_fkey";

-- DropForeignKey
ALTER TABLE "configurations_on_devices" DROP CONSTRAINT "configurations_on_devices_deviceId_fkey";

-- DropTable
DROP TABLE "configurations_on_devices";

-- CreateTable
CREATE TABLE "devices_configurations" (
    "configId" TEXT NOT NULL,
    "deviceId" TEXT NOT NULL,
    "state" "DeviceState" NOT NULL DEFAULT 'NEW',
    "log" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "devices_configurations_pkey" PRIMARY KEY ("configId","deviceId")
);

-- AddForeignKey
ALTER TABLE "devices_configurations" ADD CONSTRAINT "devices_configurations_configId_fkey" FOREIGN KEY ("configId") REFERENCES "configurations"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "devices_configurations" ADD CONSTRAINT "devices_configurations_deviceId_fkey" FOREIGN KEY ("deviceId") REFERENCES "devices"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
