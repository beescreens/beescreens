# Dashboard Backend

## Prerequisites

The following prerequisites must be filled to run this service:

- [Node.js](../../../docs/explanations/about-nodejs/index.md) must be installed.
- [pnpm](../../../docs/explanations/about-pnpm/index.md) must be installed.

## Set up the service

```sh
# Install the dependencies
pnpm install
```

## Start the service in development mode

```sh
# Start the service in development mode
pnpm dev
```

The service should be now accessible on <http://localhost:4000/api>

## Build the service

```sh
# Build the service
pnpm build
```

## Test the service

```sh
# Test the service
pnpm test
```

## Start the service in production mode

```sh
# Start the service in production mode
pnpm start
```
