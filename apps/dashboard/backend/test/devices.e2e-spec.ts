import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { NestExpressApplication } from '@nestjs/platform-express';
import { bootstrap } from '@/bootstrap';
import { AppModule } from '@/app.module';
import { randomUUID } from 'crypto';
import { PrismaService } from 'nestjs-prisma';

describe('Device (e2e)', () => {
	let app: INestApplication;
	let prisma: PrismaService;

	const apiPrefix = '/api/v1';
	let jwt: string;

	beforeEach(async () => {
		const moduleFixture: TestingModule = await Test.createTestingModule({
			imports: [AppModule],
		}).compile();

		const instance =
			moduleFixture.createNestApplication<NestExpressApplication>();

		app = await bootstrap(instance);

		prisma = moduleFixture.get<PrismaService>(PrismaService);

		await app.init();

		const res = await request(app.getHttpServer())
			.post(apiPrefix + '/auth/login')
			.send({
				username: process.env.ADMIN_USERNAME,
				password: process.env.ADMIN_PASSWORD,
			});

		jwt = res.body.jwt;
	});

	afterEach(async () => {
		await prisma.$transaction([
			prisma.deviceConfiguration.deleteMany(),
			prisma.device.deleteMany(),
			prisma.configuration.deleteMany(),
		]);
	});

	it('/devices (GET)', async () => {
		const payload1 = {
			name: 'test',
			apiKey: randomUUID(),
			description: 'test',
		};

		const payload2 = {
			name: 'test 2',
			apiKey: randomUUID(),
			description: 'test',
		};

		await request(app.getHttpServer())
			.post(apiPrefix + '/devices')
			.set('Authorization', `Bearer ${jwt}`)
			.send(payload1);

		await request(app.getHttpServer())
			.post(apiPrefix + '/devices')
			.set('Authorization', `Bearer ${jwt}`)
			.send(payload2);

		const devices = await request(app.getHttpServer())
			.get(apiPrefix + '/devices')
			.set('Authorization', `Bearer ${jwt}`);

		expect(devices.status).toEqual(HttpStatus.OK);
		expect(devices.body[0]).toMatchSnapshot({
			id: expect.any(String),
			createdAt: expect.any(String),
			updatedAt: expect.any(String),
		});
		expect(devices.body[1]).toMatchSnapshot({
			id: expect.any(String),
			createdAt: expect.any(String),
			updatedAt: expect.any(String),
		});
	});

	it('/devices/{id} (GET) - get a specific device', async () => {
		const payload = {
			name: 'test',
			apiKey: randomUUID(),
			description: 'test',
		};

		const createdDevice = await request(app.getHttpServer())
			.post(apiPrefix + '/devices')
			.set('Authorization', `Bearer ${jwt}`)
			.send(payload);

		const res = await request(app.getHttpServer())
			.get(apiPrefix + '/devices/' + createdDevice.body.id)
			.set('Authorization', `Bearer ${jwt}`);

		expect(res.status).toEqual(HttpStatus.OK);
		expect(res.body).toMatchSnapshot({
			id: expect.any(String),
			createdAt: expect.any(String),
			updatedAt: expect.any(String),
		});
	});

	it('/devices (POST)', async () => {
		const payload = {
			name: 'test',
			apiKey: randomUUID(),
			description: 'test',
		};

		const createdDevice = await request(app.getHttpServer())
			.post(apiPrefix + '/devices')
			.set('Authorization', `Bearer ${jwt}`)
			.send(payload);

		expect(createdDevice.status).toEqual(HttpStatus.CREATED);
		expect(createdDevice.body).toMatchSnapshot({
			id: expect.any(String),
			createdAt: expect.any(String),
			updatedAt: expect.any(String),
		});
	});

	it('/devices (POST) - missing apiKey', async () => {
		const payload = {
			name: 'test',
			description: 'test',
		};

		const response = await request(app.getHttpServer())
			.post(apiPrefix + '/devices')
			.set('Authorization', `Bearer ${jwt}`)
			.send(payload);

		expect(response.status).toEqual(HttpStatus.BAD_REQUEST);
	});

	it('/devices (POST) - missing name', async () => {
		const newPayload = {
			apiKey: randomUUID(),
			description: 'test',
		};

		const response = await request(app.getHttpServer())
			.post(apiPrefix + '/devices')
			.set('Authorization', `Bearer ${jwt}`)
			.send(newPayload);

		expect(response.status).toEqual(HttpStatus.BAD_REQUEST);
	});

	it('/devices/{id} (PATCH)', async () => {
		const payload = {
			name: 'test',
			apiKey: randomUUID(),
			description: 'test',
		};

		const createdDevice = await request(app.getHttpServer())
			.post(apiPrefix + '/devices')
			.set('Authorization', `Bearer ${jwt}`)
			.send(payload);

		const newPayload = {
			apiKey: '1',
			name: 'new test',
			description: 'new description',
		};

		const res = await request(app.getHttpServer())
			.patch(apiPrefix + '/devices/' + createdDevice.body.id)
			.set('Authorization', `Bearer ${jwt}`)
			.send(newPayload);

		expect(res.status).toEqual(HttpStatus.OK);
		expect(res.body).toMatchSnapshot({
			id: expect.any(String),
			createdAt: expect.any(String),
			updatedAt: expect.any(String),
		});
	});

	it('/devices/{id} (PATCH) - device not found', async () => {
		const newPayload = {
			apiKey: randomUUID(),
			name: 'new test',
			description: 'new description',
		};

		const res = await request(app.getHttpServer())
			.patch(apiPrefix + '/devices/1')
			.set('Authorization', `Bearer ${jwt}`)
			.send(newPayload);

		expect(res.status).toEqual(HttpStatus.NOT_FOUND);
	});

	it('/devices/{id} (DELETE)', async () => {
		const payload = {
			name: 'test',
			apiKey: '1',
			description: 'test',
		};

		const createdDevice = await request(app.getHttpServer())
			.post(apiPrefix + '/devices')
			.set('Authorization', `Bearer ${jwt}`)
			.send(payload);

		expect(createdDevice.status).toEqual(HttpStatus.CREATED);

		const res = await request(app.getHttpServer())
			.delete(apiPrefix + '/devices/' + createdDevice.body.id)
			.set('Authorization', `Bearer ${jwt}`);

		expect(res.status).toEqual(HttpStatus.NO_CONTENT);
	});

	it('/devices/{id} (DELETE) - device not found', async () => {
		const res = await request(app.getHttpServer())
			.delete(apiPrefix + '/devices/1')
			.set('Authorization', `Bearer ${jwt}`);

		expect(res.status).toEqual(HttpStatus.NOT_FOUND);
	});

	it('/devices/{id} (POST) - unauthorized (missing JWT)', async () => {
		const payload = {
			name: 'test',
			apiKey: randomUUID(),
			description: 'test',
		};

		const createdDevice = await request(app.getHttpServer())
			.post(apiPrefix + '/devices')
			.send(payload);

		expect(createdDevice.status).toEqual(HttpStatus.UNAUTHORIZED);
	});
});
