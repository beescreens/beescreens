# Dashboard

Check the online reference **Dashboard** at <https://docs.beescreens.ch/reference/dashboard/>.

You can also check the offline reference in this repository at [`../../docs/reference/dashboard.md`](../../docs/reference/dashboard.md).
