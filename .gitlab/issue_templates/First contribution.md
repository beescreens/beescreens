Thank you for considering contributing to BeeScreens! You are awesome! ✨

Before you submit your first issue, please review the following elements. Check the boxes below to confirm that you have read and understood each item.

- [ ] I have read the [Become a BeeScreens contributor](https://docs.beescreens.ch/tutorials/how-to-contribute-to-beescreens/) tutorial.
	- [ ] I have configured my GitLab account accordingly.
	- [ ] I have configured my development environment accordingly.
- [ ] I have read the [How to contribute to BeeScreens](https://docs.beescreens.ch/how-to-guides/how-to-contribute-to-beescreens/) how-to guide.
- [ ] I have read and approved the BeeScreens' [Contributor Covenant Code of Conduct](https://gitlab.com/beescreens/beescreens/-/blob/main/CODE_OF_CONDUCT.md).
- [ ] I am aware that my contributions will be publicly visible and that I will be credited for them.
- [ ] I am aware that all my contributions are licensed under the [GNU Affero General Public License v3.0](https://gitlab.com/beescreens/beescreens/-/blob/main/COPYING). More about this here: [GNU Affero General Public License v3.0 (AGPL-3.0) Explained in Plain English - TLDRLegal](https://www.tldrlegal.com/license/gnu-affero-general-public-license-v3-agpl-3-0).

/label ~"First contribution"
/assign me
