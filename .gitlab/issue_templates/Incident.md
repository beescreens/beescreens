# New incident

By opening an incident, you agree to abide by the BeeScreens' [Contributor Covenant Code of Conduct](https://gitlab.com/beescreens/beescreens/-/blob/main/CODE_OF_CONDUCT.md).

For details, you can check the documentation at [How to contribute to BeeScreens](https://docs.beescreens.ch/how-to-guides/how-to-contribute-to-beescreens).

Before you submit your incident, please review the following elements. Check the boxes below to confirm that you have read and understood each item.

- [ ] I have checked that the incident does not already exist or has not already been resolved in another [issue](https://gitlab.com/beescreens/beescreens/-/issues) or [merge request](https://gitlab.com/beescreens/beescreens/-/merge_requests).
- [ ] I have tested my issue on the [latest version of BeeScreens](https://gitlab.com/beescreens/beescreens/-/tree/main).
- [ ] I have tested my issue inside the [Dev container environment](https://docs.beescreens.ch/tutorials/install-and-configure-dev-containers/)

## Summary

Describe the incident here. The more details you provide, the easier it will be for the team to understand the issue and the faster it will be resolved.

## Current behavior

Describe the current behavior here. The more details you provide, the easier it will be for the team to understand the issue and the faster it will be resolved.

## Expected behavior

Describe the expected behavior here. The more details you provide, the easier it will be for the team to understand the issue and the faster it will be resolved.

## Steps to reproduce

Describe the steps to reproduce the issue here. The more details you provide, the easier it will be for the team to understand the issue and the faster it will be resolved.

## Attempts to solve the incident

Describe the attempts you tried to solve the incident if relevant. The more details you provide, the easier it will be for the team to understand the issue and the faster it will be resolved.

## More information

Provide more information here if needed.
