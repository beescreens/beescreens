# Prometheus

## Introduction

Prometheus is an open-source monitoring system with a dimensional data model, flexible query language, efficient time series database and modern alerting approach.

## Base configuration

You might want to edit the `*.env` files to reflect the user to run the container(s), FQDNs and the configuration for each service described in the `docker-compose.yaml` file.

## Additional configuration

[_View Grafana configuration_](../grafana)

You might as well need to create the directories for the volumes described in the `docker-compose.yaml` file. Most of them are ignored in the `.gitignore` file.

Create the `data` directory for the Prometheus volume.

```bash
# Create the data directory
mkdir data
```

Then you can start the Prometheus service.

## Additional resources

- [Prometheus](https://prometheus.io/) - An open-source monitoring system with a dimensional data model, flexible query language, efficient time series database and modern alerting approach.
