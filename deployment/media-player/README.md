# Media Player

## Introduction

This is the Media Player app.

## Base configuration

You might want to edit the `*.env` files to reflect the user to run the container(s), FQDNs and the configuration for each service described in the `docker-compose.yaml` file.

## Additional configuration

You might as well need to create the directories for the volumes described in the `docker-compose.yaml` file. Most of them are ignored in the `.gitignore` file.

Create the `postgres-data` directory for the Media Player volume.

```bash
# Create the postgres-data directory
mkdir postgres-data
```

Then you can start the Media Player service.
