# BeePlace

## Introduction

This is the BeePlace app.

## Base configuration

You might want to edit the `*.env` files to reflect the user to run the container(s), FQDNs and the configuration for each service described in the `docker-compose.yaml` file.

## Additional configuration

You might as well need to create the directories for the volumes described in the `docker-compose.yaml` file. Most of them are ignored in the `.gitignore` file.

Create the `postgres-data` and `redis-data` directories for the BeePlace volumes.

```bash
# Create the postgres-data directory
mkdir postgres-data

# Create the redis-data directory
mkdir redis-data
```

Then you can start the BeePlace service.
