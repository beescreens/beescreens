# Traefik

## Introduction

Traefik is the reverse proxy used to access all the BeeScreens' apps.

## Base configuration

You might want to edit the `*.env` files to reflect the user to run the container(s), FQDNs and the configuration for each service described in the `docker-compose.yaml` file.

## Additional configuration

_None_

## Additional resources

- [Traefik](https://containo.us/traefik/) - Open-source Edge Router that makes publishing your services a fun and easy experience.
