# Dashboard

## Introduction

This is the Dashboard app.

## Base configuration

You might want to edit the `*.env` files to reflect the user to run the container(s), FQDNs and the configuration for each service described in the `docker-compose.yaml` file.

## Additional configuration

You might as well need to create the directories for the volumes described in the `docker-compose.yaml` file. Most of them are ignored in the `.gitignore` file.

Create the `postgres-data` directory for the Dashboard volume.

```bash
# Create the postgres-data directory
mkdir postgres-data
```

Then you can start the Dashboard service.

Optional: seed the database with default data.

```bash
# Seed the database with default data
docker compose exec backend npm run prisma:seed
```
