# Pimp My Wall

## Introduction

This is the Pimp My Wall app.

## Base configuration

You might want to edit the `*.env` files to reflect the user to run the container(s), FQDNs and the configuration for each service described in the `docker-compose.yaml` file.

## Additional configuration

You might as well need to create the directories for the volumes described in the `docker-compose.yaml` file. Most of them are ignored in the `.gitignore` file.
