# Dufs

## Introduction

Dufs is a distinctive utility file server that supports static serving, uploading, searching, accessing control, and many other features.

## Base configuration

You might want to edit the `*.env` files to reflect the user to run the container(s), FQDNs and the configuration for each service described in the `docker-compose.yaml` file.

## Additional configuration

_None_
