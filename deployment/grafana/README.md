# Grafana

## Introduction

Grafana is the open source analytics & monitoring solution for every database.

## Base configuration

You might want to edit the `*.env` files to reflect the user to run the container(s), FQDNs and the configuration for each service described in the `docker-compose.yaml` file.

## Additional configuration

[_View Prometheus configuration_](../prometheus)

You might as well need to create the directories for the volumes described in the `docker-compose.yaml` file. Most of them are ignored in the `.gitignore` file.

Create the `data` directory for the Grafana volume.

```bash
# Create the data directory
mkdir data
```

Then you can start the Grafana service.

When Grafana is started for the first time, log in with `admin`/`admin` credentials. Then, add the [Prometheus](https://grafana.com/docs/grafana/latest/datasources/prometheus/) datasource (endpoint: `http://prometheus:9090`) and the [Node Exporter Quickstart and Dashboard](https://grafana.com/grafana/dashboards/13978) dashboard.

## Additional resources

- [Grafana](https://grafana.com/) - Grafana is the open source analytics & monitoring solution for every database.
